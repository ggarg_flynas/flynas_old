//
//  flynas-iOS-mb-Bridging-Header.h
//  flynas-iOS-mb
//
//  Created by River Huang on 9/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

#ifndef flynas_iOS_mb_Bridging_Header_h
#define flynas_iOS_mb_Bridging_Header_h

#import "../Plugins/SWRevealViewController/SWRevealViewController.h"

#import <RestKit/RestKit.h>
#import <AFNetworking.h>
#import <AFHTTPRequestOperationLogger.h>

#import "CustomSearchBar.h"
#import "DLRadioButton.h"

#import "CXDurationPickerView.h"
#import "CXDurationPickerUtils.h"

#import <ActionSheetPicker.h>

#import <SVProgressHUD.h>
#import <SHEmailValidationTextField.h>
#import <SHEmailValidator.h>
#import <UIViewController+MJPopupViewController.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <BKCardNumberField.h>
#import <BKCardExpiryField.h>
#import "SVModalWebViewController.h"
#import "SwipeView.h"
#import "ActionSheetDatePicker2.h"
#import "SMPageControl.h"
#import "IPAddressHelper.h"
#import <Adjust/Adjust.h>

//#import <GoogleTagManager/TAGManager.h>
//#import <GoogleTagManager/TAGContainer.h>
//#import <GoogleTagManager/TAGContainerOpener.h>
//#import <GoogleTagManager/TAGDataLayer.h>
//#import <GoogleTagManager/TAGLogger.h>

NSDate *RKDateFromStringWithFormatters(NSString *dateString, NSArray *formatters);

#import "CardIO/CardIO.h"

#endif
