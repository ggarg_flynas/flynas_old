//
//  FDWCiPassengerDetailVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWCiPassengerDetailVC: FDBookingCommonVC, UITableViewDelegate, UITableViewDataSource {
  
  var nasmilesNumber: [String] = []
  
  var checkinJourney = FDWciJourney() {
    didSet {
      if checkinJourney.segments.count>0 {
        checkinPassengers = checkinJourney.segments[0].passengers.filter({$0.prepareToCheckin})
      }
    }
  }
  var checkinPassengers = [FDWciPassenger]()
  
  var documentTypes = FDDocumentTypes()
  var wciPassengerDetails = FDWciPassengerDetails()
  var isDomestic = false
  
  @IBOutlet var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("web_checkin_passenger_details_page")
    tableView.delegate = self
    tableView.dataSource = self
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 164
    userBooking.pnr = SharedModel.shared.pnr
    userBooking.userEmail = "aa@rr.com"
    viewTripSummary?.update(userBooking)
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.bookingPassengers(nil, success: { (passengersInput) -> Void in
      self.getDocTypes()
      if let passengersInput = passengersInput {
        if passengersInput.regionFlightType == "Domestic" {
          self.isDomestic = true
        }
      }
    }, fail: { (reason) -> Void in
      
    })
  }
  
  func getDocTypes() {
    FDBookingManager.sharedInstance.getDocumentTypes({ (documentTypes) -> Void in
      self.documentTypes = documentTypes
      FDWebCheckinManager.sharedInstance.passengerDetails(nil, success:{ (wciPassengerDetails) -> Void in
        SVProgressHUD.dismiss()
        self.wciPassengerDetails = wciPassengerDetails
        for p1 in wciPassengerDetails.passengerDetails {
          for p2 in self.checkinPassengers {
            if p1.passengerNumber == p2.passengerNumber && p1.fullName == p2.fullName {
              p2.travelDocuments = p1.travelDocuments
              p2.paxType = p1.paxType
              p2.dob = p1.dob
            }
          }
        }
        self.tableView.reloadData()
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToWebCheckinIfExpired(reason)
      })
    }, fail: { (reason) -> Void in
      SVProgressHUD.dismiss()
      _ = self.errorHandleAndSwitchToWebCheckinIfExpired(reason)
    })
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return checkinPassengers.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell:FDWsiPassengerTableCell = self.tableView.dequeueReusableCell(withIdentifier: "FDWsiPassengerTableCell") as! FDWsiPassengerTableCell
    cell.isDomestic = isDomestic
    cell.passenger = checkinPassengers[indexPath.row]
    cell.documentTypes = documentTypes
    cell.delegate = self
    return cell
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    // todo validation
    var err = ValidateErrorCode.none
    var index = -1
    for i in 0..<checkinPassengers.count {
      let p = checkinPassengers[i]
      err = p.validateAll
      if err != .none {
        index = i
        break
      }
    }
    if err == .none {
      var passengerDetails = [[String:Any]]()
      for p in checkinPassengers {
        var docs = [[String:Any]]()
        for d in p.travelDocuments {
          docs.append([
            "documentNumber": d.docNumber as AnyObject,
            "documentTypeCode": d.docTypeCode as AnyObject,
            "nationality": d.nationality as AnyObject,
            "expirationDate": d.expirationDate != nil ? d.expirationDate!.toString(format: .custom("yyyy-MM-dd"), applyLocele:false) : "",
            "issuingCountry": d.docIssuingCountry
            ])
        }
        let pi: [String : Any] = [
          "firstName":p.firstName,
          "lastName":p.lastName,
          "passengerNumber":p.passengerNumber,
          "paxType":p.paxType,
          "travelDocuments":docs,
          "dob": p.dob != nil ? p.dob!.toString(format: .custom("yyyy-MM-dd"), applyLocele:false) : "",
          ]
        passengerDetails.append(pi)
      }
      let parameters =
        ["wciPassengerDetails":
          ["PassengerDetails":
            passengerDetails
          ]
      ]
      SVProgressHUD.show()
      FDWebCheckinManager.sharedInstance.passengerDetails(parameters, success: { (wciPassengerDetails) -> Void in
        self.checkUserSeats()
//        self.updateBookingDetail {
//          SharedModel.shared.checkinPassengers = self.checkinPassengers
//          SharedModel.shared.wciPassengerDetails = self.wciPassengerDetails
//          SharedModel.shared.checkinJourney = self.checkinJourney
//          self.gotoExtraPage()
//        }
      }, fail:{ (reason) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
      })
    } else {
      if index != -1 {
        tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .none, animated: true)
      }
      FDErrorHandle.validateErrorHandle(err)
    }
  }
  
  func checkUserSeats() {
    SVProgressHUD.show()
    if checkinJourney.allPassengerHasSeat {
      // go to complete
      self.checkin()
    } else {
      FDWebCheckinManager.sharedInstance.passengerSeats(nil, success: { (wsiFlightSeatMap) -> Void in
        SVProgressHUD.dismiss()
        // update seatMap user name
        for sm in wsiFlightSeatMap.seatMaps {
          for ps in sm.passengerSeatInfos {
            if let p = self.wciPassengerDetails.getPassengerInfo(ps.passengerNumber) {
              ps.fullName = p.fullName
              ps.nameInitial = p.nameInitial
              ps.paxType = p.paxType
              ps.hasInfantAttached = self.wciPassengerDetails.getInfantPassengerInfo(ps.passengerNumber) != nil
            }
          }
          sm.syncSeatMap()
        }
        // check if need seats
        // if all passenger has seat
        if wsiFlightSeatMap.allPassengerHasSeat {
          // go to complete
          self.checkin()
        } else {
          // go to seat map
          self.pushViewController("BookingFlow2", storyboardID: "FDSelSeatVC") { (vc) -> Void in
            let c = vc as! FDSelSeatVC
            c.seatMap = wsiFlightSeatMap
            c.checkinJourney = self.checkinJourney
            c.seatMapApi = .webCheckin
          }
        }
      }, fail:{ (reason) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
      })
    }
  }
  
  func checkin() {
    SVProgressHUD.show()
    FDWebCheckinManager.sharedInstance.checkIn(nil, success: { () -> Void in
      SVProgressHUD.dismiss()
      // go to complete
      self.pushViewController("WebCheckin", storyboardID: "FDWCiCompleteVC") { (vc) -> Void in
        let c = vc as! FDWCiCompleteVC
        c.checkinJourney = self.checkinJourney
        AdjustSDKMan.onlineCheckin(naSmilesNumber: unwrap(str: SharedModel.shared.memberlogin?.naSmilesNumber))
      }
    }, fail: { (reason) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    })
  }
  
  func updateBookingDetail(completion: @escaping (() -> Void)) {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getBookingDetail(false, success: { (bookingDetails, bookingPayments, bookingFlights : FDBookingFlights?, contactInput : FDContactInput?, passengersInput : FDPassengersInput?, baggageSsr : FDBaggageSsr?,mealSsr : FDMealSsr?, sportSsr: FDSportsSsr?, flightSeatMap : FDFlightSeatMap?) -> Void in
//      self.bookingFlights = bookingFlights
      if self.userBooking.bookingFlow == .manageMyBooking && !FDResourceManager.sharedInstance.isSessionLogined {
        var searchItems = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
        let searchItem = SearchItem(pnr: bookingDetails.recordLocator, lastname: unwrap(str: passengersInput?.passengers.first?.lastName), origin: unwrap(str: bookingFlights?.journeys.first?.origin), destination: unwrap(str: bookingFlights?.journeys.first?.destination), departDate: bookingFlights?.journeys.first?.departureDate ?? Date().dateByAddingDays(-1))
        if !searchItems.map({$0.pnr}).contains(searchItem.pnr) {
          searchItems.append(searchItem)
          ArchiveManager.saveArray(searchItems, key: SearchItem.kMMBSearchDefaults)
        }
      }
      SVProgressHUD.dismiss()
      self.userBooking.bookingDetails = bookingDetails
      self.userBooking.bookingPayments = bookingPayments
      self.userBooking.mealSsr = mealSsr
      self.userBooking.baggageSsr = baggageSsr
      self.userBooking.sportsSsr = sportSsr
      if let contactInput = contactInput {
        self.userBooking.contactInfo = contactInput.contactItemInfo
      }
      if let bookingFlights = bookingFlights {
        if bookingFlights.journeys.count > 0 {
          self.userBooking.flightOption = .oneWay
          let j = bookingFlights.journeys[0]
          self.userBooking.departureFlight = j.getFlight()
          self.userBooking.stationDepart = FDResourceManager.sharedInstance.getStation(j.origin)
          self.userBooking.stationArrive = FDResourceManager.sharedInstance.getStation(j.destination)
          self.userBooking.departureDate = j.departureDate
        } else {
          self.userBooking.departureFlight = nil
        }
        if bookingFlights.journeys.count > 1 {
          let j = bookingFlights.journeys[1]
          self.userBooking.returnFlight = j.getFlight()
          self.userBooking.returnDate = j.departureDate
          self.userBooking.stationDepart2 = FDResourceManager.sharedInstance.getStation(j.origin)
          self.userBooking.stationArrive2 = FDResourceManager.sharedInstance.getStation(j.destination)
          if self.userBooking.stationArrive?.code == self.userBooking.stationDepart2?.code {
            self.userBooking.flightOption = .return
          } else {
          }
        } else {
          self.userBooking.returnFlight = nil
        }
      }
      if let passengersInput = passengersInput {
        self.userBooking.passengers = passengersInput.passengers
      } else {
        self.userBooking.passengers.removeAll()
      }
      if let flightSeatMap = flightSeatMap {
        self.userBooking.flightSeatMap = flightSeatMap
      }
      self.tableView.reloadData()
//      self.heightTableView.constant = self.getTableViewHeight()
//      self.vwBookingDetail.isHidden = self.numberOfSections(in: self.tableView) == 0
//      self.updateUI()
      self.updatePriceSummary() {
        let currencySymbol = self.userBooking.priceSummary.currencyCode
        if let priceString = FDUtility.formatCurrency(self.userBooking.priceSummary.balanceDue) {
//          self.labelMMBPrice.text = currencySymbol + " " + priceString
          if self.userBooking.priceSummary.balanceDue.floatValue > 0 {
//            self.btnPurchase.setTitle(FDLocalized("Purchase"), for: UIControlState())
          } else {
//            self.btnPurchase.setTitle(FDLocalized("Confirm Changes"), for: UIControlState())
          }
        } else {
//          self.btnPurchase.setTitle(FDLocalized("Confirm Changes"), for: UIControlState())
//          self.labelMMBPrice.text = currencySymbol + " " + FDUtility.formatCurrency(0)!
        }
//        self.btnReset.isHidden = !self.userBooking.priceSummary.hasUncommittedChanges
//        if self.btnHome.isHidden {
//          self.btnShareRight.isHidden = self.userBooking.priceSummary.hasUncommittedChanges
//        }
        if self.userBooking.bookingFlow == .manageMyBooking && self.userBooking.priceSummary.hasUncommittedChanges {
//          self.vwMmbPurchaseButton.isHidden = false
        } else {
//          self.vwMmbPurchaseButton.isHidden = true
        }
        self.tableView.reloadData()
      }
      self.view.layoutIfNeeded()
      completion()
    },fail: { (reason) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    })
  }
  
}

extension FDWCiPassengerDetailVC: FDWsiPassengerTableCellDelegate {
  func updateTableCellHeight(_ cell: FDWsiPassengerTableCell) {
    if let i = tableView.indexPath(for: cell) {
      tableView.reloadRows(at: [i], with: UITableViewRowAnimation.automatic)
    }
  }
  
}

