//
//  BaseVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 4/7/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit
import Firebase

class BaseVC: UIViewController {
  
  override func viewWillAppear(_ animated: Bool) {
    print("current VC: \(self)")
    ContainerVC.shouldHideTabbar(self)
  }
  
  func log(_ event: String) {
    Analytics.logEvent(event, parameters: nil)
  }
  
}
