//
//  SadadOlpWebViewVC.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 18/6/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import UIKit

protocol SadadOlpVCDelegate {
  func shouldDoPolling()
}

class SadadOlpWebViewVC: UIViewController {
  
  @IBOutlet weak var webview: UIWebView!
  
  var sourceStr = ""
  var delegate: SadadOlpVCDelegate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    webview.loadHTMLString(sourceStr, baseURL: URL(string: "https://sbsimulator.payfort.com"))
  }
  
  @IBAction func onBtnBack(_ sender: FDButton) {
    dismiss(animated: true, completion: nil)
  }
  
}

extension SadadOlpWebViewVC: UIWebViewDelegate {
  
  func webViewDidStartLoad(_ webView: UIWebView) {
    SVProgressHUD.show()
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    SVProgressHUD.dismiss()
  }

  func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    if unwrap(str: request.url?.absoluteString).contains(FDApiBaseUrl) {
      dismiss(animated: true) {
        self.delegate.shouldDoPolling()
      }
    }
    return true
  }

}
