//
//  Aircraft767Cell.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 6/4/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import UIKit

protocol Aircraft767CellDelegate: class {
  func seatDidSelect(seatNumber: String)
}

class Aircraft767Cell: UITableViewCell {
  
  @IBOutlet weak var imvSeatA: UIImageView!
  @IBOutlet weak var imvSeatB: UIImageView!
  @IBOutlet weak var imvSeatC: UIImageView!
  @IBOutlet weak var imvSeatD: UIImageView!
  @IBOutlet weak var imvSeatE: UIImageView!
  @IBOutlet weak var imvSeatF: UIImageView!
  @IBOutlet weak var imvSeatG: UIImageView!
  @IBOutlet weak var imvSeatH: UIImageView!
  @IBOutlet weak var btnSeatA: UIButton!
  @IBOutlet weak var btnSeatB: UIButton!
  @IBOutlet weak var btnSeatC: UIButton!
  @IBOutlet weak var btnSeatD: UIButton!
  @IBOutlet weak var btnSeatE: UIButton!
  @IBOutlet weak var btnSeatF: UIButton!
  @IBOutlet weak var btnSeatG: UIButton!
  @IBOutlet weak var btnSeatH: UIButton!
  
  internal var chunks: [IndexPath : [[FDFlightSeatMap.SeatMap.SeatGroup.Seat]]] = [:]
  weak var delegate: Aircraft767CellDelegate?
  var indexPath: IndexPath?
  var seatMap: FDSeatMap?
  var userBooking: FDUserBookingInfo?
  var passengerInfo: FDFlightSeatMap.SeatMap.PassengerSeatInfo?
  var seatPriceType  = SeatPriceType.Standard
  
  func prepareSeats() -> Int {
    hideSeats()
    let seatGroup = seatMap!.seatGroups[indexPath!.section]
    let chunks = filterSeats(seatGroup: seatGroup)
    let seats = chunks[self.indexPath!.row]
    for seat in seats {
      let column = unwrap(str: seat.number.substringFromIndex(seat.number.count - 1, toIndex: seat.number.count))
      switch column {
      case "A":
        self.btnSeatA.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatA.setTitle(seat.number, for: .normal)
        self.imvSeatA.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatA, btnImage: self.imvSeatA)
      case "B":
        self.btnSeatB.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatB.setTitle(seat.number, for: .normal)
        self.imvSeatB.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatB, btnImage: self.imvSeatB)
      case "C":
        self.btnSeatC.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatC.setTitle(seat.number, for: .normal)
        self.imvSeatC.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatC, btnImage: self.imvSeatC)
      case "D":
        self.btnSeatD.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatD.setTitle(seat.number, for: .normal)
        self.imvSeatD.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatD, btnImage: self.imvSeatD)
      case "E":
        self.btnSeatE.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatE.setTitle(seat.number, for: .normal)
        self.imvSeatE.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatE, btnImage: self.imvSeatE)
      case "F":
        self.btnSeatF.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatF.setTitle(seat.number, for: .normal)
        self.imvSeatF.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatF, btnImage: self.imvSeatF)
      case "G":
        self.btnSeatG.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatG.setTitle(seat.number, for: .normal)
        self.imvSeatG.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatG, btnImage: self.imvSeatG)
      case "H":
        self.btnSeatH.setTitleColor(UIColor.white, for: .normal)
        self.btnSeatH.setTitle(seat.number, for: .normal)
        self.imvSeatH.image = self.getSeatImage(seatGroup: seatGroup)
        self.checkSeat(seat: seat, btnSeat: self.btnSeatH, btnImage: self.imvSeatH)
      default:
        continue
      }
    }
    return seats.count
  }
  
  func filterSeats(seatGroup: FDFlightSeatMap.SeatMap.SeatGroup) -> [[FDFlightSeatMap.SeatMap.SeatGroup.Seat]] {
    if let chunkToReturn = chunks[indexPath!] { return chunkToReturn }
    var seatChunks: [[FDFlightSeatMap.SeatMap.SeatGroup.Seat]] = []
    for i in 0..<seatGroup.seats.count {
      let index = unwrap(int: Int(unwrap(str: seatGroup.seats.first?.number.dropLast()))) + i
      let seatsGroup = seatGroup.seats.filter({$0.number.dropLast() == "\(index)"})
      if seatsGroup.isEmpty { continue }
      seatChunks.append(seatsGroup)
    }
    self.chunks[self.indexPath!] = seatChunks
    return seatChunks
  }
  
  func checkSeat(seat: FDSeat, btnSeat: UIButton, btnImage: UIImageView) {
    if unwrap(str: passengerInfo?.selectedSeat) == seat.number {
      btnSeat.setTitle(unwrap(str: passengerInfo?.nameInitial), for: .normal)
      btnSeat.setTitleColor(UIColor.black, for: .normal)
      let seatType = seatMap!.getGroupType(indexPath!.section)!
      switch seatType {
      case .Business:
        btnImage.image = UIImage(named: "icn-seat-business-selected")
        break
      case .UpFront:
        btnImage.image = UIImage(named: "icn-seat-practical-selected")
        break
      case .ExtraLeg:
        btnImage.image = UIImage(named: "icn-seat-spacious-selected")
        break
      case .Premium:
        btnImage.image = UIImage(named: "icn-seat-superpractical-selected")
        break
      case .Simple:
        btnImage.image = UIImage(named: "icn-seat-simple-selected")
        break
      case .Standard:
        btnImage.image = UIImage(named: "icn-seat-simple-selected")
        break
      case .FemaleOnly:
        btnImage.image = UIImage(named: "icn-seat-lady-selected")
        break
      }
    }
    // user selected
    let pn = seatMap?.getSeatUserIndex(seat.number)
    if pn! < 0 {
      // infantAllowed
      if let p = passengerInfo {
        if userBooking?.passengers.count == 0 {
          userBooking?.passengers = SharedModel.shared.wciPassengers
        }
        let pax = userBooking?.getPassengerInfo(p.passengerNumber)
        if p.hasInfantAttached && !seat.infantAllowed {
          setSeatNotAvailable(imv: btnImage, btnSeat: btnSeat)
        } else if p.paxType == PaxType.Child.rawValue && !seat.childAllowed {
          setSeatNotAvailable(imv: btnImage, btnSeat: btnSeat)
        } else if !seat.available {
          setSeatNotAvailable(imv: btnImage, btnSeat: btnSeat)
        } else if seat.isFemaleOnly && (unwrap(str: pax?.title) == "MR" || unwrap(str: pax?.title) == "MSTR") {
          setSeatNotAvailable(imv: btnImage, btnSeat: btnSeat)
        } else {
          // seat is enable by seatView.setSeatPriceType
        }
      } else {
        setSeatNotAvailable(imv: btnImage, btnSeat: btnSeat)
      }
    }
  }
  
  func setSeatNotAvailable(imv: UIImageView, btnSeat: UIButton) {
    imv.image = UIImage(named: "icn-seat-unavailable")
    btnSeat.isEnabled = false
    btnSeat.setTitle("", for: .normal)
  }
  
  func hideSeats() {
    imvSeatA.image = nil
    imvSeatB.image = nil
    imvSeatC.image = nil
    imvSeatD.image = nil
    imvSeatE.image = nil
    imvSeatF.image = nil
    imvSeatG?.image = nil
    imvSeatH?.image = nil
  }
  
  func getSeatImage(seatGroup: FDFlightSeatMap.SeatMap.SeatGroup) -> UIImage? {
    switch seatGroup.type {
    case SeatPriceType.Business.rawValue:
      return UIImage(named: "icn-seat-business")
    case SeatPriceType.UpFront.rawValue:
      return UIImage(named: "icn-seat-practical")
    case SeatPriceType.ExtraLeg.rawValue:
      return UIImage(named: "icn-seat-spacious")
    case SeatPriceType.Premium.rawValue:
      return UIImage(named: "icn-seat-superpractical")
    case SeatPriceType.Simple.rawValue:
      return UIImage(named: "icn-seat-simple")
    case SeatPriceType.Standard.rawValue:
      return UIImage(named: "icn-seat-simple")
    case SeatPriceType.FemaleOnly.rawValue:
      return UIImage(named: "icn-seat-lady")
    default:
      return nil
    }
  }
  
  @IBAction func onBtnDidSelect(_ sender: UIButton) {
    delegate!.seatDidSelect(seatNumber: unwrap(str: sender.titleLabel?.text))
  }
  
}
