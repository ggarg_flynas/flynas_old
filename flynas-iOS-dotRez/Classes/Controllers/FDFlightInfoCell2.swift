//
//  FDFlightInfoCell2.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightInfoCell2: UITableViewCell {
  
  @IBOutlet weak var imvAirline: UIImageView!
  @IBOutlet var labelFlightDirection: UILabel!
  @IBOutlet weak var imvFlightDirection: UIImageView!
  @IBOutlet weak var imvAirplane: UIImageView!
  @IBOutlet var labelDepartTime: UILabel!
  @IBOutlet var labelArriveTime: UILabel!
  @IBOutlet var labelOriginCityName: UILabel!
  @IBOutlet var labelDestinationCityName: UILabel!
  @IBOutlet var labelFlightCode: UILabel!
  @IBOutlet var labelDuration: UILabel!
  @IBOutlet var labelStops: UILabel!
  @IBOutlet weak var lbFlightDate: UILabel!
  
  var leg : FDLeg? {
    didSet {
      if let myleg = leg {
        labelDepartTime.text = myleg.departureDate?.toStringUTC(format:.custom("HH:mm"))
        labelArriveTime.text = FDUtility.arrivalDateHHMMString(myleg.departureDate, arrivalDate: myleg.arrivalDate)
        let originStation = FDResourceManager.sharedInstance.getStation(myleg.origin)
        let destinationStation = FDResourceManager.sharedInstance.getStation(myleg.destination)
        labelOriginCityName.text = unwrap(str: originStation?.code)
        labelDestinationCityName.text = unwrap(str: destinationStation?.code)
        labelFlightCode.text = myleg.carrierCode + myleg.flightNumber
        labelDuration.text = FDUtility.formatDuration(myleg.utcDepartureDate!, end: myleg.utcArrivalDate!)
        lbFlightDate.text = myleg.departureDate?.toStringUTC(format:.custom("EEE, dd MMM yyyy"))
        if isAr() { imvAirplane.rotate(180.0) }
      }
    }
  }
  
  var flight: FDFlight? {
    didSet {
      if let myflight = flight {
        let operatorCode = getOperatorCode(unwrap(str: myflight.operatedByFlightNumber))
        imvAirline.image = getOperatedBy(operatorCode)
        labelStops.text = FDUtility.formatStops(SharedModel.shared.numberOfStops)
      }
    }
  }
  
  func getOperatorCode(_ operatedByFlightNumber: String) -> String {
    let operatedByFlightNumber = operatedByFlightNumber.trimmingCharacters(in: CharacterSet.whitespaces)
    if operatedByFlightNumber.isEmpty || operatedByFlightNumber == " " || operatedByFlightNumber == "/" || operatedByFlightNumber.lengthOfBytes(using: String.Encoding.utf8) < 2  {
      return ""
    } else {
      return operatedByFlightNumber.substring(to: operatedByFlightNumber.index(operatedByFlightNumber.startIndex, offsetBy: 2))
    }
  }
  
  func getOperatedBy(_ code: String) -> UIImage {
    switch code {
    case "EY":
      return UIImage(named: "icn-air-etihad")!
    case "PC":
      return UIImage(named: "icn-air-pegasus")!
    default:
      return UIImage(named: "icn-air-flynas")!
    }
  }
  
}
