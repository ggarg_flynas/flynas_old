//
//  FDPassengerExtraDetail.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDPassengerExtraDetailDelegate : class {
  func onBtnExpandClicked(_ cell: FDConfirmationPassengerCell)
}

class FDConfirmationPassengerCell: UITableViewCell {
  
  @IBOutlet var labelPassengerFullName: UILabel!
  @IBOutlet var labelBaggage: UILabel!
  @IBOutlet var labelMeal: UILabel!
  @IBOutlet var labelSeat: UILabel!
  @IBOutlet weak var lblSports: UILabel!
  @IBOutlet weak var lblLounge: UILabel!
  @IBOutlet weak var imvExpanded: UIImageView!
  @IBOutlet weak var lbBundleCode: UILabel!
  @IBOutlet weak var lbIncludedBaggage: UILabel!
  
  var expanded = false {
    didSet {
      updateUI()
    }
  }
  
  weak var delegate: FDPassengerExtraDetailDelegate?
  
  @IBAction func onBtnExpandClicked(_ sender: UIButton) {
    delegate?.onBtnExpandClicked(self)
  }
  
  func updateUI () {
    imvExpanded.image = UIImage(named: expanded ? "icn-arrow-up-red" : "icn-arrow-down-red")
    layoutIfNeeded()
  }
}
