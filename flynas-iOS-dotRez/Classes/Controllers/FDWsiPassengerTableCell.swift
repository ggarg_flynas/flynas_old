//
//  FDWsiPassengerTableCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDWsiPassengerTableCellDelegate : class  {
    func updateTableCellHeight(_ cell:FDWsiPassengerTableCell)
}

class FDWsiPassengerTableCell: UITableViewCell {

    var passenger = FDWciPassenger() {
        didSet {
            if passenger.travelDocuments.count == 0 {
                travelDocument = FDTravelDocument()
                passenger.travelDocuments.append(travelDocument)
            } else {
                travelDocument = passenger.travelDocuments[0]
            }

            updatePassengersDetail()
        }
    }

    var travelDocument = FDTravelDocument()
    var documentTypes = FDDocumentTypes() {
        didSet {
            updatePassengersDetail()
        }
    }


    @IBOutlet var labelPassengeFullName: UILabel!
    @IBOutlet var btnDocTypeCode: UIButton!
    @IBOutlet var btnIssuingCountry: UIButton!
    @IBOutlet var tfDocNumber: UITextField!
    @IBOutlet var btnExpirationDate: UIButton!
//    @IBOutlet var btnExpirationDateHijri: UIButton!
    @IBOutlet var hideExpiryDate: NSLayoutConstraint!
    @IBOutlet var btnNationality: UIButton!

    @IBOutlet var btnDob: UIButton!
    @IBOutlet var btnDobHijri: UIButton!
    @IBOutlet var btnIDNumber: UIButton!
  
  var isDomestic = false

    weak var delegate: FDWsiPassengerTableCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

        tfDocNumber.addTarget(self, action: #selector(FDWsiPassengerTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)

        let imageHighlight = UIImage(named: "bg-btn-highlight")

        btnDob.setBackgroundImage(imageHighlight, for: .highlighted)
        btnNationality.setBackgroundImage(imageHighlight, for: .highlighted)

        btnIssuingCountry.setBackgroundImage(imageHighlight, for: .highlighted)
        btnDocTypeCode.setBackgroundImage(imageHighlight, for: .highlighted)

        btnExpirationDate.setBackgroundImage(imageHighlight, for: .highlighted)
        btnIDNumber.setBackgroundImage(imageHighlight, for: .highlighted)
    }

    func textFieldEditingDidEnd(_ sender:UITextField) {

        switch (sender) {

        case tfDocNumber:
            travelDocument.docNumber = sender.text != nil ? sender.text! : ""
            break

        default:break
        }

        updatePassengersDetail()
    }

    func updatePassengersDetail() {
        labelPassengeFullName.text = passenger.fullName
        btnDocTypeCode.setTitle(documentTypes.getDocumentTypeName(travelDocument.docTypeCode), for: UIControlState())
        btnNationality.setTitle(FDResourceManager.sharedInstance.countryNameByCode(travelDocument.nationality), for: UIControlState())
        btnIssuingCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(travelDocument.docIssuingCountry), for: UIControlState())
        tfDocNumber.text = travelDocument.docNumber
        btnExpirationDate.setTitle(travelDocument.expirationDate?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar:travelDocument.expirationDateHijri), for: UIControlState())
//        btnExpirationDateHijri.selected = travelDocument.expirationDateHijri
        btnDob.setTitle(passenger.dob?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar: passenger.dateOfBirthHijri), for: UIControlState())
        btnDobHijri.isSelected = passenger.dateOfBirthHijri
        hideExpiryDate.priority = shouldHideExpiryDate() ? 900 : 100
    }
  
  func shouldHideExpiryDate() -> Bool {
    if isDomestic {
      if travelDocument.docTypeCode == "P" {
        return false
      } else {
        return true
      }
    } else {
      return false
    }
  }

    @IBAction func onIssuingCountryClicked(_ sender: UIView) {
        self.endEditing(true)
        FDUtility.selectCountry(FDLocalized("Issuing Country"), initValue: travelDocument.docIssuingCountry, empty: false, origin:sender) { (selectedValue) -> Void in
            self.travelDocument.docIssuingCountry = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onNationalityClicked(_ sender: UIView) {
        endEditing(true)
        FDUtility.selectCountry(FDLocalized("Nationality"), initValue: travelDocument.nationality, empty: true, origin:sender) { (selectedValue) -> Void in
            self.travelDocument.nationality = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onTravelDocTypeClicked(_ sender: UIView) {
        endEditing(true)
        let items = documentTypes.documentTypes.map({$0.name})
        let index = documentTypes.documentTypes.index(where: {$0.code == travelDocument.docTypeCode})
        ActionSheetStringPicker.show(withTitle: FDLocalized("Document Type"), rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
            picker, selectedIndex, selectedValue in
            self.travelDocument.docTypeCode = self.documentTypes.documentTypes[selectedIndex].code
            self.updatePassengersDetail()
            self.delegate?.updateTableCellHeight(self)
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }

    @IBAction func onIDExpirationDateClicked(_ sender: UIView) {
        endEditing(true)
        let selectedDate = travelDocument.expirationDate != nil ? travelDocument.expirationDate : Date()
        FDUtility.showPickerWithTitle(FDLocalized("Expiry Date"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: travelDocument.expirationDateHijri, minimumDate:Date(),maximumDate:nil,  doneBlock: { (picker, value, index) -> Void in
            self.travelDocument.expirationDate = value as? Date
            self.updatePassengersDetail()
            }, cancelBlock: { (_) -> Void in
            }, origin: sender)
    }

    @IBAction func onIDExpirationDateHijriClicked(_ sender: AnyObject) {
        endEditing(true)
        travelDocument.expirationDateHijri = !travelDocument.expirationDateHijri
        updatePassengersDetail()
    }

    @IBAction func onDobClicked(_ sender: UIView) {
        endEditing(true)
        let maximumDate = Date().dateByAddingDays(-1)
        let selectedDate = passenger.dob != nil ? passenger.dob : Date()
        FDUtility.showPickerWithTitle(FDLocalized("Date of birth"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: passenger.dateOfBirthHijri,
                                      minimumDate: nil,
                                      maximumDate: maximumDate,
                                      doneBlock: { (picker, value, index) -> Void in
            self.passenger.dob = value as? Date
            self.updatePassengersDetail()
            }, cancelBlock: { (_) -> Void in
            }, origin: sender)
    }

    @IBAction func onDobHijriClicked(_ sender: AnyObject) {
        self.endEditing(true)
        passenger.dateOfBirthHijri = !passenger.dateOfBirthHijri
        updatePassengersDetail()
    }


    func highlightErrorField(_ markErr: Bool) {
        if markErr {
            btnDob.isHighlighted = passenger.dob == nil
            btnNationality.isHighlighted = travelDocument.nationality.isEmpty
            btnIssuingCountry.isHighlighted = travelDocument.docIssuingCountry.isEmpty
            btnDocTypeCode.isHighlighted = travelDocument.docTypeCode.isEmpty
            btnIDNumber.isHighlighted = travelDocument.validateDocumentNumber != .none
            btnExpirationDate.isHighlighted = travelDocument.expirationDate == nil
        } else {
            btnDob.isHighlighted = false
            btnNationality.isHighlighted = false
            btnIssuingCountry.isHighlighted = false
            btnDocTypeCode.isHighlighted = false
            btnExpirationDate.isHighlighted = false
            btnIDNumber.isHighlighted = false
        }
    }
}
