//
//  FDMyBookingTableViewCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 4/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

protocol FDMyBookingTableViewCellDelegate : class  {
    func btnViewClicked(_ cell:FDMyBookingTableViewCell)
    func btnCheckinClicked(_ cell:FDMyBookingTableViewCell)
}

class FDMyBookingTableViewCell: UITableViewCell {


    @IBOutlet var labelOrigin: UILabel!
    @IBOutlet var labelDestination: UILabel!
    @IBOutlet var labelFlightTime: UILabel!
    @IBOutlet var labelPNR: UILabel!
    @IBOutlet var widthManagerBtn: NSLayoutConstraint!
    @IBOutlet var manageBtn: FDButton!
    @IBOutlet var btnCheckin: FDButton!

    weak var delegate: FDMyBookingTableViewCellDelegate?

    override func setSelected(_ selected: Bool, animated: Bool) {
    }

    @IBAction func onBtnViewClicked(_ sender: UIButton) {
        delegate?.btnViewClicked(self)
    }

    @IBAction func onBtnCheckinClicked(_ sender: FDButton) {
        delegate?.btnCheckinClicked(self)
    }

    func showManageBtn(_ showBtn:Bool) {
        if showBtn {
            // manager
            manageBtn.setTitle(FDLocalized("Manage"), for: UIControlState())
        } else {
            // view
            manageBtn.setTitle(FDLocalized("View"), for: UIControlState())
        }

        manageBtn.isHidden = !showBtn

//        widthManagerBtn.constant = showBtn ? 60 : 0
    }
}
