//
//  PassengerLoungeServices.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 20/7/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import Foundation

struct LoungeSsrStruct {
  var passengerServices: [PassengerServiceStruct] = []
}

struct PassengerServiceStruct {
  var origin = ""
  var destination = ""
  var passengerLoungeServices: [PassengerLoungeServicesStruct] = []
}

struct PassengerLoungeServicesStruct {
  var passengerNumber = ""
  var services: [ServiceStruct] = []
}

struct ServiceStruct {
  var code = ""
  var price = ""
  var count = ""
  var remainingAmount = ""
  var smilePointsRedeemable = ""
}

class PassengerLoungeServices {
  
  class func getLounge(_ completion: @escaping (LoungeSsrStruct) -> Void) {
    var loungeToReturn = LoungeSsrStruct()
    let url = URL(string: "\(FDApiBaseUrl)\(FDApiVersion)Lounge")
    let request = NSMutableURLRequest(url: url!)
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let token = UserDefaults.standard.string(forKey: "sessionToken")
    request.addValue(unwrap(str: token), forHTTPHeaderField: "X-Session-Token")
    request.httpMethod = "GET"
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    print("GET \(url!)")
    print(request.allHTTPHeaderFields ?? "")
    let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
      DispatchQueue.main.async {
        guard let myResponse = response else { completion(loungeToReturn); return }
        print("\((myResponse as! HTTPURLResponse).statusCode) \(url!)")
        print((myResponse as! HTTPURLResponse).allHeaderFields)
        print(String(data: data!, encoding: String.Encoding.ascii) ?? "")
        if error != nil { completion(loungeToReturn); return }
        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
        let loungeSsrJson = (json as AnyObject)["loungeSsr"] as? [String: AnyObject]
        var passengerServiceItems: [PassengerServiceStruct] = []
        guard let passengerServicesJson = loungeSsrJson?["passengerServices"] as? [[String: AnyObject]] else { completion(loungeToReturn); return }
        for passengerService in passengerServicesJson {
          var passengerLoungeServiceItems: [PassengerLoungeServicesStruct] = []
          guard let passengerLoungeServicesJson = passengerService["passengerLoungeServices"] as? [[String: AnyObject]] else { completion(loungeToReturn); return }
          for passengerLoungeService in passengerLoungeServicesJson {
            var serviceItems: [ServiceStruct] = []
            guard let servicesJson = passengerLoungeService["services"] as? [[String: AnyObject]] else { completion(loungeToReturn); return }
            for service in servicesJson {
              let serviceItem = ServiceStruct(code: unwrap(str: service["code"]),
                                              price: unwrap(str: service["price"]),
                                              count: unwrap(str: service["count"]),
                                              remainingAmount: unwrap(str: service["remainingAmount"]),
                                              smilePointsRedeemable: unwrap(str: service["smilePointsRedeemable"]))
              serviceItems.append(serviceItem)
            }
            let passengerLoungeServiceItem = PassengerLoungeServicesStruct(passengerNumber: unwrap(str: passengerLoungeService["passengerNumber"]),
                                                                           services: serviceItems)
            passengerLoungeServiceItems.append(passengerLoungeServiceItem)
          }
          let passengerServiceItem = PassengerServiceStruct(origin: unwrap(str: passengerService["origin"]),
                                                            destination: unwrap(str: passengerService["destination"]),
                                                            passengerLoungeServices: passengerLoungeServiceItems)
          passengerServiceItems.append(passengerServiceItem)
        }
        loungeToReturn = LoungeSsrStruct(passengerServices: passengerServiceItems)
        completion(loungeToReturn)
      }
    }
    task.resume()
  }
  
  class func getLoungeServiceNumberAndPrice(_ loungeSsr: LoungeSsrStruct?, origin:String?, destination:String?) -> (Int, Float) {
    guard let lounge = loungeSsr else { return (0, 0) }
    var serviceCount = 0
    var servicePrice : Float = 0
    for passengerService in lounge.passengerServices {
      if origin == nil && destination == nil {
        for passengerLoungeService in passengerService.passengerLoungeServices {
          for service in passengerLoungeService.services {
            serviceCount += Int(service.count)!
            servicePrice += Float(service.price)! * Float(serviceCount)
          }
        }
      } else {
        if passengerService.origin == unwrap(str: origin) && passengerService.destination == unwrap(str: destination) {
          for passengerLoungeService in passengerService.passengerLoungeServices {
            for service in passengerLoungeService.services {
              serviceCount += Int(service.count)!
              servicePrice += Float(service.price)! * Float(service.count)!
            }
          }
        }
      }
      
    }
    return (serviceCount, servicePrice)
  }
}
