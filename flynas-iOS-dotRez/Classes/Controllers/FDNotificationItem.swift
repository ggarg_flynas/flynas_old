//
//  FDNotificationItem.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 5/07/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation
import RealmSwift

enum FDNotificationType : Int {
    case none = 0
    case textMsg = 1
}
class FDNotificationItem: Object {


    dynamic var title = ""
    dynamic var message = ""
    dynamic var receiveDate : Date?
    dynamic var type : Int = FDNotificationType.none.rawValue
    
}
