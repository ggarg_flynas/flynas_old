//
//  FDExtraVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 12/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum BundleCode: String {
  case LIGH
  case LIT1
  case LIT2
  case PLUS
  case PLS1
  case PLS2
  case PRUM
  case PRM1
  case PRM2
}

class FDExtraVC: FDBookingCommonVC {
  
  @IBOutlet weak var vwBaggage: UIView!
  @IBOutlet weak var vwLounge: UIView!
  @IBOutlet weak var vwMeal: UIView!
  @IBOutlet weak var heightBaggageTable: NSLayoutConstraint!
  @IBOutlet weak var heightMealTable: NSLayoutConstraint!
  @IBOutlet weak var heightLoungeTable: NSLayoutConstraint!
  @IBOutlet var tvBaggage: UITableView!
  @IBOutlet var tvMeals: UITableView!
  @IBOutlet weak var tvLounge: UITableView!
  @IBOutlet weak var imvExpandBaggage: UIImageView!
  @IBOutlet weak var imvExpandMeal: UIImageView!
  @IBOutlet weak var imvExpandLounge: UIImageView!
  
  static let BaggageTableCellHeightExpanded : CGFloat = 350.0 + 1.0
  static let BaggageTableCellHeightNotExpanded: CGFloat = 60.0 + 1.0
  static let HeightMealTableCell : CGFloat = 60.0 + 1.0
  static let LoungeTableViewCellHeight: CGFloat = 60.0 + 1.0
  var extendedRows: [String: Bool] = [:]
  var expandedSections = [true, false, false]
  var loungeServices: [(FDFlight, [PassengerLoungeServicesStruct])] {
    var items: [(FDFlight, [PassengerLoungeServicesStruct])] = []
    for section in 0..<userBooking.flightNumber {
      let (flight, _) = userBooking.getFlightInfo(section)
      let services = userBooking.loungeSsrStruct.passengerServices
      let passengerService = services.filter({$0.origin == unwrap(str: flight?.origin)})
      if unwrap(int: passengerService.first?.passengerLoungeServices.count) > 0 {
        items.append((flight!, passengerService.first!.passengerLoungeServices))
      }
    }
    return items
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("extras_page")
    viewTripSummary?.isHidden = userBooking.bookingDetails.isGdsBooking && (SharedModel.shared.isMMB || SharedModel.shared.isWCI)
    if userBooking.bookingFlow == .manageMyBooking {
      setShowPriceChanged(true)
    }
    for s in 0..<numberOfSections(in: tvBaggage) {
      for r in 0...userBooking.passengers.filter({$0.paxType != PaxType.Infant.rawValue}).count - 1 {
        extendedRows["\(s)\(r)"] = s == 0 && r == 0
      }
    }
    updateTableHeight(false)
    updatePriceSummary()
    if userBooking.bookingFlow == .manageMyBooking {
      btnNext.setTitle(FDLocalized("Continue"), for: UIControlState())
    }
    updateUI()
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    if let actionDelegate = actionDelegate {
      actionDelegate.updatePage()
      actionDelegate.onBtnBackClicked()
    }
  }
  
  func updateUI() {
    vwBaggage.isHidden = !(userBooking.baggageSsr != nil && userBooking.baggageSsr!.needUserAction())
    vwMeal.isHidden = !(userBooking.mealSsr != nil && userBooking.mealSsr!.needUserAction())
    vwLounge.isHidden = userBooking.loungeSsrStruct.passengerServices.count == 0
    imvExpandBaggage.image = expandedSections[0] ? UIImage(named: "icn-arrow-up-red") : UIImage(named: "icn-arrow-down-red")
    imvExpandMeal.image = expandedSections[1] ? UIImage(named: "icn-arrow-up-red") : UIImage(named: "icn-arrow-down-red")
    imvExpandLounge.image = expandedSections[2] ? UIImage(named: "icn-arrow-up-red") : UIImage(named: "icn-arrow-down-red")
    view.layoutIfNeeded()
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    if SharedModel.shared.isWCI {
      checkUserSeats()
      return
    }
    if userBooking.bookingFlow == .normalBooking {
      gotoSeatMapPage()
    } else {
      self.navigationController?.popViewController(animated: true)
      if let actionDelegate = actionDelegate {
        actionDelegate.updatePage()
        actionDelegate.onBtnContinueClicked()
      }
    }
  }
  
  func checkUserSeats() {
    SVProgressHUD.show()
    if SharedModel.shared.checkinJourney.allPassengerHasSeat {
      // go to complete
      self.checkin()
    } else {
      FDWebCheckinManager.sharedInstance.passengerSeats(nil, success: { (wsiFlightSeatMap) -> Void in
        SVProgressHUD.dismiss()
        // update seatMap user name
        for sm in wsiFlightSeatMap.seatMaps {
          for ps in sm.passengerSeatInfos {
            if let p = SharedModel.shared.wciPassengerDetails.getPassengerInfo(ps.passengerNumber) {
              ps.fullName = p.fullName
              ps.nameInitial = p.nameInitial
              ps.paxType = p.paxType
              ps.hasInfantAttached = SharedModel.shared.wciPassengerDetails.getInfantPassengerInfo(ps.passengerNumber) != nil
            }
          }
          sm.syncSeatMap()
        }
        // check if need seats
        // if all passenger has seat
        if wsiFlightSeatMap.allPassengerHasSeat {
          // go to complete
          self.checkin()
        } else {
          // go to seat map
          self.pushViewController("BookingFlow2", storyboardID: "FDSelSeatVC") { (vc) -> Void in
            let c = vc as! FDSelSeatVC
            c.seatMap = wsiFlightSeatMap
            c.checkinJourney = SharedModel.shared.checkinJourney
            c.seatMapApi = .webCheckin
          }
        }
      }, fail:{ (reason) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
      })
    }
  }
  
  func checkin() {
    SVProgressHUD.show()
    FDWebCheckinManager.sharedInstance.checkIn(nil, success: { () -> Void in
      SVProgressHUD.dismiss()
      // go to complete
      self.pushViewController("WebCheckin", storyboardID: "FDWCiCompleteVC") { (vc) -> Void in
        let c = vc as! FDWCiCompleteVC
        c.checkinJourney = SharedModel.shared.checkinJourney
        AdjustSDKMan.onlineCheckin(naSmilesNumber: unwrap(str: SharedModel.shared.memberlogin?.naSmilesNumber))
      }
    }, fail:{ (reason) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    })
  }
  
  func updateTableHeight(_ animated: Bool) {
    let baggageSectionCount = CGFloat(numberOfSections(in: tvBaggage))
    let extendedRowCount = CGFloat(extendedRows.filter({$0.1 == true}).count)
    let notExtendedRowCount = CGFloat(extendedRows.filter({$0.1 == false}).count)
    heightBaggageTable.constant = (baggageSectionCount * TableHeaderHeight) + (extendedRowCount * FDExtraVC.BaggageTableCellHeightExpanded) + (notExtendedRowCount * FDExtraVC.BaggageTableCellHeightNotExpanded)
    heightBaggageTable.constant = expandedSections[0] ? heightBaggageTable.constant : 0.0
    let mealSectionCount = CGFloat(numberOfSections(in: tvMeals))
    let passengerCount = CGFloat(userBooking.passengers.filter({$0.paxType != PaxType.Infant.rawValue}).count)
    heightMealTable.constant = (mealSectionCount * TableHeaderHeight) + ((mealSectionCount * passengerCount) * FDExtraVC.HeightMealTableCell)
    heightMealTable.constant = expandedSections[1] ? heightMealTable.constant : 0.0
    let loungeSections = CGFloat(numberOfSections(in: tvLounge))
    heightLoungeTable.constant = (loungeSections * TableHeaderHeight) + (loungeSections * passengerCount * FDExtraVC.LoungeTableViewCellHeight)
    heightLoungeTable.constant = expandedSections[2] ? heightLoungeTable.constant : 0.0
    if animated {
      UIView.animate(withDuration: 0.3, animations: {
        self.view.layoutIfNeeded()
      })
    } else {
      view.layoutIfNeeded()
    }
  }
  
  @IBAction func onBtnExpandBaggage(_ sender: UIButton) {
    expandedSections = [!expandedSections[0], false, false]
    updateTableHeight(true)
    updateUI()
  }
  
  @IBAction func onBtnExpandMeal(_ sender: UIButton) {
    expandedSections = [false, !expandedSections[1], false]
    updateTableHeight(true)
    updateUI()
  }
  
  @IBAction func onBtnExpandLounge(_ sender: UIButton) {
    expandedSections = [false, false, !expandedSections[2]]
    updateTableHeight(true)
    updateUI()
  }
  
}

// MARK: - UITableViewDataSource / Delegate
extension FDExtraVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    switch tableView {
    case tvBaggage:
      return userBooking.flightNumber
    case tvMeals:
      return userBooking.legNumber
    case tvLounge:
      return loungeServices.count
    default:
      return 0
    }
    
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell:FDFlightInfoCell = tableView.dequeueReusableCell(withIdentifier: "FDFlightInfoCell") as! FDFlightInfoCell
    let flight = userBooking.getFlightInfo(section).0
    let leg = userBooking.getLegInfo(section).0
    let direction = userBooking.getFlightInfo(section).1
    cell.lbDirection.text = direction == .arrive ? FDLocalized("Return Flight:") : FDLocalized("Departing Flight:")
    if tvBaggage == tableView {
      cell.lbDirection.text = direction == .arrive ? FDLocalized("Return Flight:") : FDLocalized("Departing Flight:")
      cell.lbFlight.text = unwrap(str: flight?.origin) + " - " + unwrap(str: flight?.destination)
    } else if tvMeals == tableView {
      cell.lbDirection.text = direction == .arrive ? FDLocalized("Return Flight:") : FDLocalized("Departing Flight:")
      cell.lbFlight.text = unwrap(str: leg?.origin) + " - " + unwrap(str: leg?.destination)
    } else if tableView == tvLounge {
      cell.lbDirection.text = FDLocalized("Airport")
      cell.lbFlight.text = unwrap(str: loungeServices[section].0.origin)
    }
    return cell.contentView
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return userBooking.passengerNumberExcludeInfant
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch tableView {
    case tvBaggage:
      return extendedRows["\(indexPath.section)\(indexPath.row)"]! ? FDExtraVC.BaggageTableCellHeightExpanded : FDExtraVC.BaggageTableCellHeightNotExpanded
    case tvMeals:
      return FDExtraVC.HeightMealTableCell
    case tvLounge:
      return FDExtraVC.LoungeTableViewCellHeight
    default:
      return UITableViewAutomaticDimension
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let passenger = userBooking.passengers[indexPath.row]
    switch tableView {
    case tvBaggage:
      let cell = tableView.dequeueReusableCell(withIdentifier: "FDExtraBaggageCell") as! FDExtraBaggageCell
      cell.lbPaxName.text = passenger.fullName
//      cell.shouldAllowChanges(userBooking.getFlights()[indexPath.section])
      cell.baggageService = userBooking.getBaggageService(indexPath.section, passengerNumber: indexPath.row)
      cell.sportsService = userBooking.getSportsService(indexPath.section, passengerNumber: indexPath.row)
      cell.pn = indexPath.row
      cell.delegate = self
      cell.key = "\(indexPath.section)\(indexPath.row)"
      cell.imgArrow.image = extendedRows[cell.key]! ? UIImage(named: "icn-arrow-up-red") : UIImage(named: "icn-arrow-down-red")
      return cell
    case tvMeals:
      let cell = tableView.dequeueReusableCell(withIdentifier: "FDExtraMealCell") as! FDExtraMealCell
      cell.labelPassengerFullName.text = passenger.fullName
      cell.mealService = userBooking.getMealService(indexPath.section, passengerNumber: indexPath.row)
      cell.pn = indexPath.row
      cell.menuExpand = false
      cell.delegate = self
      cell.userBooking = userBooking
      cell.section = indexPath.section
      return cell
    case tvLounge:
      let cell = tableView.dequeueReusableCell(withIdentifier: "FDExtraLoungeCell") as! FDExtraLoungeCell
      cell.lblName.text = passenger.fullName
      cell.pn = indexPath.row
      cell.loungeService = loungeServices[indexPath.section]
      cell.updateLounge()
      cell.delegate = self
      return cell
    default:
      return UITableViewCell()
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch tableView {
    case tvMeals:
      tableView.deselectRow(at: indexPath, animated: true)
      selectMeal(tableView.cellForRow(at: indexPath) as! FDExtraMealCell)
    default:
      return
    }
  }
  
}

// MARK: - FDExtraBaggageCellDelegate
extension FDExtraVC: FDExtraBaggageCellDelegate {
  
  func baggageErrorHandle(_ reason: String?) {
    if !errorHandleAndSwitchToSearchIfExpired(reason) {
      self.updatePriceSummary()
    }
  }
  
  func itemChanged() {
    updatePriceSummary()
  }
  
  func didExpandButtonPressed(_ key: String) {
    for myki in extendedRows.keys {
      if myki == key {
        extendedRows[myki] = !extendedRows[myki]!
      } else {
        extendedRows[myki] = false
      }
    }
    tvBaggage.reloadWithAnimation()
    updateTableHeight(false)
  }
  
}

// MARK: - FDExtraMealCellDelegate
extension FDExtraVC: FDExtraMealCellDelegate {
  
  func selectMeal(_ cell: FDExtraMealCell) {
    guard let indexPath = tvMeals.indexPath(for: cell) else { return }
    guard let mealService = userBooking.getMealService(indexPath.section, passengerNumber: indexPath.row) else { return }
    if mealService.services.count == 0 { return }
    let mealVC = storyboard!.instantiateViewController(withIdentifier: "MealVC") as! MealVC
    mealVC.modalTransitionStyle = .crossDissolve
    mealVC.modalPresentationStyle = .overFullScreen
    mealVC.mealService = mealService
    mealVC.delegate = self
    present(mealVC, animated: true, completion: nil)
  }
  
  func mealChanged() {
    updatePriceSummary()
    tvMeals.reloadData()
  }
  
  func mealErrorHandle(_ reason: String?) {
    _ = errorHandleAndSwitchToSearchIfExpired(reason)
  }
  
}
