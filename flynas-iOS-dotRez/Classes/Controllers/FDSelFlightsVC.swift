//
//  FDSelFlightsVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 29/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol FDPopOutVCDelegate : class {
  func dismissInfo()
}

class FDSelFlightsVC: FDBookingCommonVC, UITableViewDelegate, UITableViewDataSource, FDFlightTableViewCellDelegate,FDPopOutVCDelegate {
  
  @IBOutlet weak var imvTripType: UIImageView!
  @IBOutlet var lbTripType: UILabel!
  @IBOutlet weak var lbCities: UILabel!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var viewNoFlight: UIView!
  @IBOutlet var viewFlightDateFare: UIView!
  @IBOutlet var flightDateLowFareCollectionView: UICollectionView!
  @IBOutlet var btnRightArrow: UIButton!
  @IBOutlet var btnLeftArrow: UIButton!
  
  static let TAG_LOW_FARE_DATE_VIEW = 30
  static let TOTAL_FLIGHT_SCHEDULE_DAYS = 510
  
  var flightsAvailability: FDFlightsAvailability?
  var lowFareAvailability = FDLowFareAvailability()
  var currentTrip : FDTrip?
  var tripIndex = 0
  var expandedIndexPath = [-1,-1]
  var currentSelectedFare: String?
  var viewDatePrevious: FDFlightDateView?
  var viewDateCurrent: FDFlightDateView?
  var viewDateNext: FDFlightDateView?
  var viewDateSpare: FDFlightDateView?
  var dateFareMargin: CGFloat = 0
  var dateFareWidth: CGFloat = 0.0
  var dateFareHeight: CGFloat = 0.0
  var beginCacheLowFare =  false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("flight_select_page")
    viewTripSummary?.isHidden = userBooking.bookingDetails.isGdsBooking && (SharedModel.shared.isMMB || SharedModel.shared.isWCI)
    if  userBooking.managingFlights.count > 0 {
      tripIndex = userBooking.managingFlights[0]
    }
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 35
    initFlightDateFareView()
    doSearch()
    updateTableView()
    updateTripType()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if let flightDate = getCurrentTripDate() {
      let currentItemIndex = flightDate.daysAfterDate(Date().dateAtStartOfDay(true))
      updateLowFareDateScrollPosition(currentItemIndex, animated: false,direction: .centeredHorizontally)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    switch segue.identifier! {
    case "Passengers" :
      let controller = segue.destination as! FDPassengerVC
      let passengerNumber = min(userBooking.passengers.count, FDResourceManager.sharedInstance.savedPassenger.count)
      // todo - more smart copy, ADT -> ADT CHD -> CHD
      for i in 0..<passengerNumber {
        userBooking.passengers[i].copyFrom(FDResourceManager.sharedInstance.savedPassenger[i])
      }
      userBooking.contactInfo.copyContact(FDResourceManager.sharedInstance.savedContactInfo)
      userBooking.contactInfo.emailAddressConfirm = userBooking.contactInfo.emailAddress
      controller.userBooking = self.userBooking
      break
    default: break
    }
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    if tripIndex == 1 && userBooking.managingFlights.count != 1 {
      changeTripTo(tripIndex-1)
    } else {
      self.navigationController?.popViewController(animated: true)
      if self.userBooking.bookingFlow == .normalBooking {
      } else {
        self.actionDelegate?.updatePage()
        self.actionDelegate?.onBtnBackClicked()
      }
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if currentTrip != nil {
      return (currentTrip?.flights.count)!
    } else {
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FlightCell") as! FDFlightTableViewCell
    cell.flight = currentTrip!.flights[indexPath.row]
    cell.delegate = self
    if userBooking.departureFlight != nil && userBooking.departureFlight!.journeyKey == cell.flight!.journeyKey {
      cell.setFare(userBooking.departureFare!)
    } else if userBooking.returnFlight != nil && userBooking.returnFlight!.journeyKey == cell.flight!.journeyKey {
      cell.setFare(userBooking.returnFare!)
    } else {
      cell.setFare(nil)
    }
    cell.tripIndex = tripIndex
    cell.setDetails(expandedIndexPath[tripIndex] == indexPath.row)
    return cell
  }
  
  func updateTableView() {
    tableView.reloadData()
    delay(0.5) {
      let indexPath = IndexPath(row: 0, section: 0)
      if self.tableView.numberOfRows(inSection: 0) < 2 || self.expandedIndexPath[self.tripIndex] == indexPath.row { return }
      self.toggleFlightDetail(indexPath)
    }
  }
  
  // MARK: - FDFlightTableViewCellDelegate
  func btnFareInfoClicked(_ cell:FDFlightTableViewCell, priceOption:PriceOption) {
    let flightConnectionInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FDFlightFareInfoVC") as! FDFlightFareInfoVC
    flightConnectionInfoVC.delegate = self
    flightConnectionInfoVC.priceOption = priceOption
    self.presentPopupViewController(flightConnectionInfoVC, animationType: MJPopupViewAnimationFade)
  }
  
  func btnStopInfoClicked(_ cell: FDFlightTableViewCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      if let f = currentTrip?.flights[indexPath.row] {
        if f.legs.count > 1 {
          let flightConnectionInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FDFlightConnectionInfoVC") as! FDFlightConnectionInfoVC
          flightConnectionInfoVC.delegate = self
          flightConnectionInfoVC.flight = f
          self.presentPopupViewController(flightConnectionInfoVC, animationType: MJPopupViewAnimationFade)
        }
      }
    }
  }
  
  func btnExpandClicked(_ cell: FDFlightTableViewCell) {
    if let i = tableView.indexPath(for: cell) {
      toggleFlightDetail(i)
    }
  }
  
  func toggleFlightDetail(_ indexPath: IndexPath) {
    if expandedIndexPath[tripIndex] == indexPath.row { // close
      if let cell = tableView.cellForRow(at: indexPath) as? FDFlightTableViewCell {
        cell.setDetails(false)
      }
      expandedIndexPath[tripIndex] = -1
      tableView.reloadWithAnimation()
    } else { // open
      if expandedIndexPath[tripIndex] != -1 {
        let closedIndex = IndexPath(row: expandedIndexPath[tripIndex], section: 0)
        if let cell = tableView.cellForRow(at: closedIndex) as? FDFlightTableViewCell {
          cell.setDetails(false)
        }
      }
      expandedIndexPath[tripIndex] = indexPath.row
      if let cell = tableView.cellForRow(at: indexPath) as? FDFlightTableViewCell {
        if cell.imvExpand.isHidden == true {
          let newIndexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)
          toggleFlightDetail(newIndexPath)
          return
        }
        cell.setDetails(true)
      }
      tableView.reloadWithAnimation()
      let numberOfRows = tableView.numberOfRows(inSection: indexPath.section)
      let myIndexPath = IndexPath(row: min(indexPath.row, numberOfRows-1), section: indexPath.section)
      tableView.scrollToRow(at: myIndexPath, at: .top, animated: true)
    }
  }
  
  func selectFlightFare(_ selectedFlight: FDFlight, selectedFare: FDFare, expand: Bool) {
    if tripIndex == 0 {
      userBooking.departureFlight = selectedFlight
      userBooking.departureFare = selectedFare
    } else {
      userBooking.returnFlight = selectedFlight
      userBooking.returnFare = selectedFare
    }
    updateUI()
    if expand {
      tableView.reloadData()
      return
    } else if selectedFare.fareType == "Business" {
      tableView.reloadData()
    }
    updatePriceSummary(true)
    SVProgressHUD.show()
    delay(2.0) {
      self.onBtnNextClicked(self.btnNext)
    }
  }
  
  func updateTripType(_ animated: Bool = false) {
    var tripTypeStr = ""
    var citiesStr = ""
    if tripIndex == 0 {
      tripTypeStr = FDLocalized("Departing Flight")
      citiesStr = "\(userBooking.stationDepart!.name) \(FDLocalized("to")) \(userBooking.stationArrive!.name)"
      imvTripType.rotate(isEn() ? 0.0 : 180.0)
    } else {
      tripTypeStr = FDLocalized("Returning Flight")
      citiesStr = "\(unwrap(str: userBooking.stationArrive?.name)) \(FDLocalized("to")) \(unwrap(str: userBooking.stationDepart?.name))"
      imvTripType.rotate(isEn() ? 180.0 : 0.0)
    }
    if animated {
      if tripIndex == 0 {
        FDCubeTransition(label: lbTripType, text: tripTypeStr, direction: .positive)
        FDCubeTransition(label: lbCities, text: citiesStr, direction: .positive)
      } else {
        FDCubeTransition(label: lbTripType, text: tripTypeStr, direction: .negative)
        FDCubeTransition(label: lbCities, text: citiesStr, direction: .negative)
      }
    } else {
      lbTripType.text = tripTypeStr
      lbCities.text = citiesStr
    }
  }
  
  func initFlightDateFareView() {
    view.layoutIfNeeded()
    if let flow = flightDateLowFareCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      dateFareMargin = flow.minimumLineSpacing
    }
    dateFareWidth = (viewFlightDateFare.frame.size.width - 2 * dateFareMargin) / 3
    dateFareHeight = viewFlightDateFare.frame.size.height
    flightDateLowFareCollectionView.dataSource = self
    flightDateLowFareCollectionView.delegate = self
    flightDateLowFareCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
  }
  
  func setFareCalendarDate(_ animationDirection: AnimationDirection = .none) {
    var date1 : Date?
    var date2 : Date?
    var date3 : Date?
    if let flightDate = getCurrentTripDate() {
      if flightDate.isToday() {
        date1 = flightDate
        date2 = flightDate.dateByAddingDays(1)
        date3 = flightDate.dateByAddingDays(2)
      } else {
        date1 = flightDate.dateByAddingDays(-1)
        date2 = flightDate
        date3 = flightDate.dateByAddingDays(1)
      }
    }
    viewDatePrevious?.setDateWithAnimation(date1, animationDirection: animationDirection)
    viewDateCurrent?.setDateWithAnimation(date2, animationDirection: animationDirection)
    viewDateNext?.setDateWithAnimation(date3, animationDirection: animationDirection)
  }
  
  func setFareCalendarStyle() {
    let tripDate = getCurrentTripDate()
    func setFareCalendarViewStyle(_ viewDate: FDFlightDateView?) {
      if let date = viewDate?.date, let tripDate = tripDate, date.isEqualToDateIgnoringTime(tripDate) {
        viewDate?.setStyle(.current)
      } else {
        viewDate?.setStyle(.other)
      }
    }
    setFareCalendarViewStyle(viewDatePrevious)
    setFareCalendarViewStyle(viewDateCurrent)
    setFareCalendarViewStyle(viewDateNext)
    setFareCalendarViewStyle(viewDateSpare)
  }
  
  func setViewDateFareClickHandle () {
    let tripDate = getCurrentTripDate()
    func setFareCalendarViewClickHandle(_ viewDate: FDFlightDateView?) {
      if let date = viewDate?.date, let tripDate = tripDate, !date.isEqualToDateIgnoringTime(tripDate) {
        viewDate?.clicked = {[weak self] (_) -> Void in
          if self != nil {
            self?.onFareCalendarDateClicked(date as AnyObject)
          }
        }
      } else {
        viewDate?.clicked = nil
      }
    }
    if let flightCalendarDate = viewDatePrevious?.date, !flightCalendarDate.isToday() {
      btnLeftArrow.isEnabled = true
    } else {
      btnLeftArrow.isEnabled = false
    }
    setFareCalendarViewClickHandle(viewDatePrevious)
    setFareCalendarViewClickHandle(viewDateCurrent)
    setFareCalendarViewClickHandle(viewDateNext)
    setFareCalendarViewClickHandle(viewDateSpare)
  }
  
  func getCurrentTripDate() -> Date? {
    if tripIndex == 0 {
      return userBooking.departureDate as Date?
    } else {
      return userBooking.returnDate as Date?
    }
  }
  
  func setFareCalendarLowFare() {
    flightDateLowFareCollectionView.reloadData() // set lowest fare
  }
  
  func updateUI() {
    if flightsAvailability != nil {
      if currentTrip != nil {
        viewNoFlight.isHidden = currentTrip!.flights.count > 0
      } else {
        viewNoFlight.isHidden = false
      }
      tableView.isHidden = !viewNoFlight.isHidden
    } else {
      tableView.isHidden = true
      viewNoFlight.isHidden = true
    }
    btnNext.isEnabled = (tripIndex == 0 && userBooking.departureFare != nil) ||  (tripIndex == 1 && userBooking.returnFare != nil)
  }
  
  func doSearch() {
    SVProgressHUD.show()
    guard let depart = userBooking.stationDepart, let arrive = userBooking.stationArrive, let depDate = userBooking.departureDate, let currency = userBooking.currencyCode else { return }
    FDBookingManager.sharedInstance.searchFlight(depart.code, destinationCode: arrive.code, departureDate: depDate, returnDate: userBooking.returnDate, adultCount: userBooking.adultNum, childCount: userBooking.childNum, infantCount: userBooking.infantNum, currencyCode: currency, isChangeFlow:userBooking.bookingFlow == .manageMyBooking, promoCode: userBooking.promoCode, success: { (flightsAvailability, lowFareAvailability) -> Void in
      SVProgressHUD.dismiss()
      self.flightsAvailability = flightsAvailability
      self.lowFareAvailability.addLowFares(lowFareAvailability)
      if self.tripIndex < self.flightsAvailability?.trips.count {
        self.currentTrip = self.flightsAvailability?.trips[self.tripIndex]
      } else {
        self.currentTrip = nil
      }
      self.expandedIndexPath[self.tripIndex] = -1
      self.updateExpandedIndexPath()
      self.updateTableView()
      self.updateUI()
      self.setFareCalendarLowFare()
      self.beginCacheLowFare = true
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
      _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
    }
  }
  
  func updateExpandedIndexPath() {
    if let flightsAvailability = flightsAvailability {
      for i in 0 ..< flightsAvailability.trips.count {
        if flightsAvailability.trips[i].flights.count == 1 {
          self.expandedIndexPath[i] = 0
        } else {
          var journeyKey : String?
          if i == 0 && userBooking.departureFlight != nil {
            journeyKey = userBooking.departureFlight?.journeyKey
          } else if i == 1 && userBooking.returnFlight != nil {
            journeyKey = userBooking.returnFlight?.journeyKey
          }
          if let journeyKey = journeyKey {
            for j in 0 ..< flightsAvailability.trips[i].flights.count {
              let flight = flightsAvailability.trips[i].flights[j]
              if journeyKey == flight.journeyKey {
                self.expandedIndexPath[i] = j
                break
              }
            }
          }
        }
      }
    }
  }
  
  func moveDateFare(_ forward:Bool) {
    let (indexMin, indexMax) = getMinMaxVisibleCellIndex()
    if !forward {
      updateLowFareDateScrollPosition(indexMin-1, animated: true, direction: isEn() ? .left : .right)
    } else {
      updateLowFareDateScrollPosition(indexMax+1, animated: true, direction: isEn() ? .right : .left)
    }
  }
  
  @IBAction func onFareCalendarDateClicked(_ sender: AnyObject?) {
    guard let date = sender as? Date, let flightDate = getCurrentTripDate() else {return}
    if !flightDate.isEqualToDateIgnoringTime(date) {
      if tripIndex == 0 {
        userBooking.departureDate = date
        userBooking.clearUserSelect()
        if let returnDate = userBooking.returnDate {
          if returnDate.isEarlierThanDate(date) || returnDate.isEqualToDateIgnoringTime(date) {
            userBooking.returnDate = date.dateByAddingDays(1)
            userBooking.clearReturnFlight()
          }
        }
      } else {
        userBooking.returnDate = date
        userBooking.clearReturnFlight()
      }
      setFareCalendarLowFare()
      doSearch()
    }
  }
  
  @IBAction func onBtnLeftClicked(_ sender: UIButton?) {
    moveDateFare(false)
  }
  
  @IBAction func onBtnRightClicked(_ sender: UIButton?) {
    moveDateFare(true)
  }
  
  func showSummary(_ show:Bool) {
    //        heightSummary.constant = show ? 0 : 30;
    //        view.layoutIfNeeded()
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    if (tripIndex == 0 && userBooking.returnDate == nil) || tripIndex == 1 || userBooking.managingFlights.count == 1 {
      
      SVProgressHUD.show()
      //            btnNext.enabled = false
      // call FlightSell
      FDBookingManager.sharedInstance.flightAdd(userBooking.getSellKeys(), success: { () -> Void in
        
        //                self.btnNext.enabled = true
        
        
        let gotoPaxPage = {
          SVProgressHUD.dismiss()
          // go to pax page
          self.performSegue(withIdentifier: "Passengers", sender: sender)
        }
        
        if self.userBooking.bookingFlow == .normalBooking {
          
          FDLoginManager.sharedInstance.retrieveMemberProfile(nil, success: { (memberProfile) -> Void in
            gotoPaxPage()
            }, fail: { (r) -> Void in
              gotoPaxPage()
          })
          
        } else {
          self.navigationController?.popViewController(animated: true)
          
          self.actionDelegate?.updatePage()
          self.actionDelegate?.onBtnContinueClicked()
          self.updateTableView()
        }
        
        }, fail: { (reason) -> Void in
          FDErrorHandle.apiCallErrorWithAler(reason)
          //                    self.btnNext.enabled = true
          SVProgressHUD.dismiss()
      })
    } else {
      updateTableView()
      SVProgressHUD.dismiss()
      changeTripTo(tripIndex+1)
    }
  }
  
  func updateLowFareDateScrollPosition(_ index:Int, animated:Bool, direction: UICollectionViewScrollPosition) {
    var index = max(index, 0)
    index = min(index, collectionView(flightDateLowFareCollectionView, numberOfItemsInSection: 0) - 1)
    
    let indexPath = IndexPath(item: index, section: 0)
    flightDateLowFareCollectionView.scrollToItem(at: indexPath, at: direction, animated: animated)
    
    updateLowFareDateViewRightLeftArrorwStatus()
  }
  
  func changeTripTo(_ index: Int, animated: Bool = true) {
    if tripIndex != index {
      tripIndex = index
      if self.tripIndex < self.flightsAvailability?.trips.count {
        self.currentTrip = self.flightsAvailability?.trips[self.tripIndex]
      } else {
        self.currentTrip = nil
      }
      tableView.reloadWithAnimation()
      updateTripType(animated)
      updateUI()
      updateExpandedIndexPath()
      setFareCalendarLowFare()
      if let flightDate = getCurrentTripDate() {
        updateLowFareDateScrollPosition(flightDate.daysAfterDate(Date().dateAtStartOfDay(true)), animated: true, direction: .centeredHorizontally)
      }
    }
  }
  
  //MARK: - FDPopOutVCDelegate
  func dismissInfo() {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
  }
  
}

extension FDSelFlightsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return FDSelFlightsVC.TOTAL_FLIGHT_SCHEDULE_DAYS
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LowFareDateCell", for: indexPath)
    let cellDate = Date().dateAtStartOfDay(true).dateByAddingDays(indexPath.row)
    let tripIndex = self.tripIndex
    if let lowFareDateView = cell.viewWithTag(FDSelFlightsVC.TAG_LOW_FARE_DATE_VIEW) as? FDFlightDateView {
      lowFareDateView.setDateWithAnimation(cellDate, animationDirection: .none)
      if let lowFareDate = self.lowFareAvailability.getLowFee(lowFareDateView.date, tripIndex: tripIndex) {
        lowFareDateView.setFare(lowFareDate)
      } else {
        if let date = lowFareDateView.date, self.beginCacheLowFare {
          // cache next
          let dates = lowFareAvailability.getCacheLowFeeDates(date, tripIndex: tripIndex)
          lowFareDateView.setFare(FDLowFare.LoadingPrice)
          FDBookingManager.sharedInstance.cacheLowFare(dates, success: { [weak self] (lowFareAvailability) in
            self?.lowFareAvailability.addLowFares(lowFareAvailability)
            if let cell = self?.flightDateLowFareCollectionView.cellForItem(at: indexPath), let lowFareDateView = cell.viewWithTag(FDSelFlightsVC.TAG_LOW_FARE_DATE_VIEW) as? FDFlightDateView, tripIndex == self?.tripIndex {
              lowFareDateView.setFare(self?.lowFareAvailability.getLowFee(lowFareDateView.date, tripIndex: tripIndex))
            }
            }, fail: { (reason) -> Void in
          })
        } else {
          lowFareDateView.setFare(nil)
        }
      }
      if let tripDate = getCurrentTripDate(), let date = lowFareDateView.date, date.isEqualToDateIgnoringTime(tripDate) {
        lowFareDateView.setStyle(.current)
        lowFareDateView.clicked = nil
      } else {
        lowFareDateView.setStyle(.other)
        lowFareDateView.clicked = {[weak self] (_) -> Void in
          if self != nil {
            self?.onFareCalendarDateClicked(cellDate as AnyObject)
          }
        }
      }
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                             sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: dateFareWidth, height: dateFareHeight)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                             insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  }
  
  func getMinMaxVisibleCellIndex() -> (Int, Int) {
    var indexMin = Int.max
    var indexMax = Int.min
    for cell in flightDateLowFareCollectionView.visibleCells  as [UICollectionViewCell] {
      if let indexPath = flightDateLowFareCollectionView.indexPath(for: cell as UICollectionViewCell) {
        indexMax = max(indexPath.row, indexMax)
        indexMin = min(indexPath.row, indexMin)
      }
    }
    return (indexMin, indexMax)
  }
  
  // direction : Right, finger drage from left to right, Left: frome right to left
  func snapToNearestCell(_ scrollView: UIScrollView, direction: UICollectionViewScrollPosition) {
    let (indexMin, indexMax) = getMinMaxVisibleCellIndex()
    if (direction == .right && isEn()) || (direction == .left && isAr()) {
      updateLowFareDateScrollPosition(indexMin, animated: true, direction: isEn() ? .left : .right)
    } else {
      updateLowFareDateScrollPosition(indexMax, animated: true, direction: isEn() ? .right : .left)
    }
  }
  
  func updateLowFareDateViewRightLeftArrorwStatus() {
    let (indexMin, indexMax) = getMinMaxVisibleCellIndex()
    let cellCount = collectionView(flightDateLowFareCollectionView, numberOfItemsInSection: 0)
    btnLeftArrow.isEnabled = indexMin > 0 && indexMin < cellCount
    btnRightArrow.isEnabled = indexMax < cellCount - 1 && indexMax >= 0
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
    if (actualPosition.x > 0){
      // Dragging left
      snapToNearestCell(scrollView, direction: .right)
    }else{
      // Dragging right
      snapToNearestCell(scrollView, direction: .left)
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
    if (actualPosition.x > 0){
      // Dragging left
      snapToNearestCell(scrollView, direction: .right)
    }else{
      // Dragging right
      snapToNearestCell(scrollView, direction: .left)
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    updateLowFareDateViewRightLeftArrorwStatus()
  }
  
}
