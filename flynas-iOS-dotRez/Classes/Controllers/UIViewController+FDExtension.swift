//
//  UIViewController+FDExtension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum AnimationDirection: Int {
    case positive = 1
    case none = 0
    case negative = -1
}

extension UIViewController {

  func getFDHomeVC() -> FDHomePageVC {
    let story = UIStoryboard(name: "Main", bundle: nil)
    let navigationVC = story.instantiateViewController(withIdentifier: "FDNavigationController") as! UINavigationController
    //        let navigationVc = self.revealViewController().frontViewController as! UINavigationController
    return navigationVC.viewControllers.first as! FDHomePageVC
  }

    func pushViewController(_ storyboardName: String, storyboardID:String, prepare:((UIViewController) -> Void)? ) {
        let sb = UIStoryboard(name: storyboardName, bundle: nil)

        let targetVC = sb.instantiateViewController(withIdentifier: storyboardID)

        prepare?(targetVC)

        self.navigationController?.pushViewController(targetVC, animated: true)
    }

    func errorHandle(_ reason:String?) {
        FDErrorHandle.apiCallErrorWithAler(reason)
    }

    func backToHome () {
        for vc in self.navigationController!.viewControllers {
            if vc.isKind(of: FDSearchVC.self) {
                self.navigationController!.popToViewController(vc, animated: true)
                return
            }
        }

        self.navigationController!.popToRootViewController(animated: true)
    }

    func backToWebCheckin () {
        for vc in self.navigationController!.viewControllers {
            if vc.isKind(of: FDWebCheckinVC.self) {
                self.navigationController!.popToViewController(vc, animated: true)
                return
            }
        }
        self.navigationController!.popToRootViewController(animated: true)
    }

    func errorHandleAndSwitchToHomePageIfExpired(_ reason:String?) -> Bool {
        if reason == FDErrorHandle.API_SESSION_ERROR_EXPIRE {
            FDRM.sharedInstance.checkSessionToken({ (_) -> Void in
                // activate success
                }, fail: { (reason) -> Void in
                    FDErrorHandle.apiCallError(reason)
            })
            self.backToHome()
            return true
        } else {
            FDErrorHandle.apiCallErrorWithAler(reason)
            return false
        }
    }

    func errorHandleAndSwitchToWebCheckinIfExpired(_ reason:String?) -> Bool {
        if reason == FDErrorHandle.API_SESSION_ERROR_EXPIRE {
            FDRM.sharedInstance.checkSessionToken({ (_) -> Void in
                // activate success
                }, fail: { (reason) -> Void in
                    FDErrorHandle.apiCallError(reason)
            })
            self.backToWebCheckin()
            return true
        } else {
            FDErrorHandle.apiCallErrorWithAler(reason)
            return false
        }
    }

    func backToSearhingFlight () {
        if let navigationController = self.navigationController {
            for vc in navigationController.viewControllers {
                if vc.isKind(of: FDSearchVC.self) {
                    navigationController.popToViewController(vc, animated: true)
                    return
                }
            }
        }
    }

    // return ture if switching
    func errorHandleAndSwitchToSearchIfExpired(_ reason:String?) -> Bool {
        if reason == FDErrorHandle.API_SESSION_ERROR_EXPIRE {
            FDRM.sharedInstance.checkSessionToken({ (_) -> Void in
                // activate success
                }, fail: { (reason) -> Void in
                    FDErrorHandle.apiCallError(reason)
            })
            self.backToSearhingFlight()
            return true
        } else {
            FDErrorHandle.apiCallErrorWithAler(reason)
            return false
        }
    }

    func revealToggle(_ sender: UIButton) {
        if isRTL() {
            self.revealViewController().rightRevealToggle(sender)
        } else {
            self.revealViewController().revealToggle(sender)
        }
    }

}
