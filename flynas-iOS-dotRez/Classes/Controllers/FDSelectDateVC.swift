//
//  FDSelectDateVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 25/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

public protocol FDSelectDateVCDelegate : NSObjectProtocol {
  func didSelect(_ dateDepart: Date, dateReturn:Date?)
}

class FDSelectDateVC: BaseVC, CXDurationPickerViewDelegate {
  
  enum DestinationOption : Int {
    case depart = 30
    case arrive
  }
  
  @IBOutlet var btnDeparture: DLRadioButton!
  @IBOutlet var btnArrive: DLRadioButton!
  @IBOutlet var btnSelect: UIButton!
  @IBOutlet var datePicker: CXDurationPickerView!
  
  var backAnimation = true
  var departOnly = false
  var destinationOption = DestinationOption.depart
  var departureDate : Date?
  var returnDate : Date?
  weak var delegate: FDSelectDateVCDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("select_date_page")
    btnDeparture.flipButton()
    btnArrive.flipButton()
    
    datePicker.allowSelectionsInPast = false
    
    if departOnly {
      datePicker.type = .single
      datePicker.mode = .singleDate
      returnDate = nil
      
      datePicker.singleDate = departureDate != nil ? CXDurationPickerUtils.pickerDate(fromUTCDate: departureDate) : CXDurationPickerDate(year: 0, month: 0, day: 0)
    } else {
      datePicker.type = .duration
      //            datePicker.mode = .StartDate
      
      datePicker.startDate = departureDate != nil ? CXDurationPickerUtils.pickerDate(fromUTCDate: departureDate) : CXDurationPickerDate(year: 0, month: 0, day: 0)
      datePicker.endDate = returnDate != nil ? CXDurationPickerUtils.pickerDate(fromUTCDate: returnDate) : CXDurationPickerDate(year: 0, month: 0, day: 0)
    }
    
    datePicker.delegate = self
    
    
    
    datePicker.scroll(toStartMonth: false)
    
    updateOption(false)
    
  }
  
  @IBAction func onBtnDestinationOptionClicked(_ sender: AnyObject) {
    //        let newOption = DestinationOption(rawValue:(sender as! UIButton).tag)!
    //
    //        if destinationOption != newOption {
    //            destinationOption = newOption
    //
    //            if destinationOption == .Depart {
    //                datePicker.mode = .StartDate
    //            } else {
    //                datePicker.mode = .EndDate
    //            }
    //
    //            updateOption(true)
    //        }
  }
  
  func updateOption(_ animated: Bool) {
    
    if departOnly {
      //            destinationOption = .Depart
      returnDate = nil
      
      btnArrive.isEnabled = false
    } else {
      btnArrive.isEnabled = true
    }
    
    (view.viewWithTag(destinationOption.rawValue) as? UIButton)?.isSelected = true;
    
    btnDeparture.setTitle(departureDate?.toStringUTC(format: .custom("EEE, dd MMM")), for: UIControlState())
    btnArrive.setTitle(returnDate?.toStringUTC(format: .custom("EEE, dd MMM")), for: UIControlState())
    
    if (departureDate == nil) || (!departOnly && returnDate == nil) {
      btnSelect.isEnabled = false
    } else {
      btnSelect.isEnabled = true
    }
    
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    
  }
  
  @IBAction func onBtnSelectClicked(_ sender: AnyObject) {
    navigationController?.popViewController(animated: backAnimation)
    
    delegate?.didSelect(departureDate!, dateReturn: returnDate )
  }
  
  // MARK: - CXDurationPickerViewDelegate
  func durationPicker(_ durationPicker:CXDurationPickerView, endDateChanged date:CXDurationPickerDate) {
    NSLog("endDateChanged")
    
    returnDate = CXDurationPickerUtils.date(from: date)
    datePicker.mode = .startDate
    updateOption(true)
  }
  
  func durationPicker(_ durationPicker:CXDurationPickerView, singleDateChanged date:CXDurationPickerDate) {
    NSLog("singleDateChanged")
    departureDate = CXDurationPickerUtils.date(from: date)
    updateOption(true)
  }
  
  func durationPicker(_ durationPicker:CXDurationPickerView, startDateChanged date:CXDurationPickerDate) {
    NSLog("startDateChanged")
    departureDate = CXDurationPickerUtils.date(from: date)
    datePicker.endDate = date
    returnDate = nil
    
    if departOnly {
      
    } else {
      datePicker.mode = .endDate
      destinationOption = .arrive
    }
    updateOption(true)
  }
  
  func durationPicker(_ durationPicker:CXDurationPickerView, invalidEndDateSelected date:CXDurationPickerDate) {
    NSLog("invalidEndDateSelected")
    departureDate = CXDurationPickerUtils.date(from: date)
    
    if departOnly {
    } else {
      datePicker.startDate = date
      datePicker.endDate = date
      
      datePicker.mode = .endDate
      returnDate = nil
      destinationOption = .arrive
    }
    
    updateOption(true)
  }
  
  func durationPicker(_ durationPicker:CXDurationPickerView, invalidStartDateSelected date:CXDurationPickerDate) {
    NSLog("invalidStartDateSelected")
    
    if departOnly {
      datePicker.singleDate = date
    } else {
      datePicker.startDate = date
      datePicker.endDate = date
      
      datePicker.mode = .endDate
      returnDate = nil
      destinationOption = .arrive
      
    }
    departureDate = CXDurationPickerUtils.date(from: date)
    updateOption(true)
  }
  
  func durationPicker(_ durationPicker:CXDurationPickerView, didSelectDateInPast date:CXDurationPickerDate) {
    NSLog("didSelectDateInPast")
  }
  
  func durationPicker(_ durationPicker: CXDurationPickerView!, didSelectDateInPast date: CXDurationPickerDate, for mode: CXDurationPickerMode) {
    NSLog("didSelectDateInPast2")
    
  }
}
