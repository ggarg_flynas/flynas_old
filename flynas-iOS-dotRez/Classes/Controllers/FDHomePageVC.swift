//
//  FDHomePageVC.swift
//  flynas-dotRez
//
//  Created by River Huang on 9/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit
import MessageUI

class FDHomePageVC: BaseVC, SWRevealViewControllerDelegate, FDLoginVCDelegate {
  
  @IBOutlet var bannerLoadingIndicator: UIActivityIndicatorView!
  @IBOutlet weak var cvBanner: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var lbNotificationBadge: UILabel!
  @IBOutlet weak var widthNotificationBadgeLabel: NSLayoutConstraint!
  @IBOutlet weak var btnLogin: UIButton!
  @IBOutlet var scrollView: UIScrollView!
  
  static let ivLoginWidth: CGFloat = 24.0
  let refreshControl = UIRefreshControl()
  let updateResourceTimerInterval = 30
  var updateResourceTimer : Timer?
  var curLanguage = 0
  var popOutWarning = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("home_page")
    downloadBanners()
    checkOSVersion ()
    addNotifications()
    if isEn() {
      if self.revealViewController() != nil {
        self.revealViewController().rightViewController = nil
      }
    } else {
      if self.revealViewController() != nil {
        self.revealViewController().rearViewController = nil
      }
    }
    if self.revealViewController() != nil {
      self.revealViewController().delegate = self
      self.navigationController?.navigationBar.barTintColor = UIColor(red: 35.0/255.0, green: 188.0/255.0, blue: 185.0/255.0, alpha: 1)
      self.revealViewController().frontViewShadowOpacity = 0.0
      self.revealViewController().rearViewRevealWidth = 227
      self.revealViewController().rightViewRevealWidth = 226
    }
    self.refreshControl .addTarget(self, action: #selector(FDHomePageVC.handleRefresh(_:)), for: .valueChanged)
    scrollView.addSubview(refreshControl)
    //        [self.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    //        [self.tblPop addSubview:self.refreshControl];
    let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(updateLoginView), userInfo: nil, repeats: true)
    timer.fire()
    let bannerTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollBanner), userInfo: nil, repeats: true)
    timer.fire()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    loadAds()
    showRateMe()
    //    setNotificationBadge(String(FDAPNManager.sharedInstance.notifications.count))
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    cvBanner.collectionViewLayout.invalidateLayout()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    ApplePayHandler.showSplashIfNeeded()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func addNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_:)), name: NSNotification.Name(rawValue: FD_RECEIVE_NOTIFICATION), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FDHomePageVC.newVersionAvailable(_:)), name: NSNotification.Name(rawValue: SERVER_VERSION_NEWVERSIONAVAILABLE), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FDHomePageVC.resourceUpdateFail(_:)), name: NSNotification.Name(rawValue: RESOURCE_UPDATE_FAIL), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FDHomePageVC.handleEnterForeground(_:)), name: NSNotification.Name(rawValue: "UIApplicationWillEnterForegroundNotification"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FDHomePageVC.handleShortCutItem(_:)), name: NSNotification.Name(rawValue: ACTION_FOR_SHORTCUTITEM), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnLogin), name: NSNotification.Name(rawValue: "#selector(onBtnLogin)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnRegister), name: NSNotification.Name(rawValue: "#selector(onBtnRegister)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnMyProfile), name: NSNotification.Name(rawValue: "#selector(onBtnMyProfile)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnMyBoardingPassClicked), name: NSNotification.Name(rawValue: "#selector(onBtnMyBoardingPassClicked)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnMyNotifications), name: NSNotification.Name(rawValue: "#selector(onBtnMyNotifications)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(bookAHotel), name: NSNotification.Name(rawValue: "#selector(bookAHotel)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(rentACar), name: NSNotification.Name(rawValue: "#selector(rentACar)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(airportTransfer), name: NSNotification.Name(rawValue: "#selector(airportTransfer)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnFlightStatusClick), name: NSNotification.Name(rawValue: "#selector(onBtnFlightStatusClick)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnFlightSchedule), name: NSNotification.Name(rawValue: "#selector(onBtnFlightSchedule)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnNaSmileLogin), name: NSNotification.Name(rawValue: "#selector(onBtnNaSmileLogin)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(naSmileAbout), name: NSNotification.Name(rawValue: "#selector(naSmileAbout)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(faqs), name: NSNotification.Name(rawValue: "#selector(faqs)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(naSmileEarningSMILEPoints), name: NSNotification.Name(rawValue: "#selector(naSmileEarningSMILEPoints)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(naSmileTierBenefits), name: NSNotification.Name(rawValue: "#selector(naSmileTierBenefits)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(smilePointCalculator), name: NSNotification.Name(rawValue: "#selector(smilePointCalculator)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(partnersCalculator), name: NSNotification.Name(rawValue: "#selector(partnersCalculator)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(naSmileJoinNow), name: NSNotification.Name(rawValue: "#selector(naSmileJoinNow)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnContactUsClicked), name: NSNotification.Name(rawValue: "#selector(onBtnContactUsClicked)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnAppFeedback), name: NSNotification.Name(rawValue: "#selector(onBtnAppFeedback)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnMyNaSmileCard), name: NSNotification.Name(rawValue: "#selector(onBtnMyNaSmileCard)"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnLogout), name: NSNotification.Name(rawValue: "#selector(onBtnLogout)"), object: nil)
  }
  
  func scrollBanner() {
    var index = cvBanner.indexPathsForVisibleItems.first!.row
    index += 1
    index = index % 3
    let indexPath = IndexPath(row: index, section: 0)
    cvBanner.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
  }
  
  func setNotificationBadge(_ badge: String) {
    lbNotificationBadge.isHidden = Int(badge) == 0
    lbNotificationBadge.text = badge
    let constraintSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: lbNotificationBadge.bounds.size.height)
    var sizeThatFits = lbNotificationBadge.sizeThatFits(constraintSize)
    sizeThatFits.width += 10
    widthNotificationBadgeLabel.constant = min(50, max(20, sizeThatFits.width))
    view.layoutIfNeeded()
  }
  
  func updateLoginView() {
    if FDResourceManager.sharedInstance.isSessionLogined {
      let name = FDLoginManager.sharedInstance.userName?.components(separatedBy: " ").first
      btnLogin.setImage(nil, for: UIControlState())
      btnLogin.setTitle("\(FDLocalized("Hi")) \(unwrap(str: name))", for: UIControlState())
    } else {
      btnLogin.setTitle(FDLocalized("Login"), for: UIControlState())
      btnLogin.setImage(UIImage(named: "icn-login"), for: UIControlState())
    }
  }
  
  @IBAction func onBtnNavLogin(_ sender: UIButton) {
    if FDResourceManager.sharedInstance.isSessionLogined {
      onBtnMyProfile(sender)
    } else {
      onBtnLogin(sender)
    }
  }
  
  func handleRefresh(_ sender:AnyObject) {
    //        FDRM.sharedInstance.refreshImage()
//    loadAds()
  }
  
  func resourceUpdateFail(_ notification: Notification) {
    if self.updateResourceTimer == nil {
      self.updateResourceTimer = Timer.scheduledTimer(timeInterval: TimeInterval( self.updateResourceTimerInterval), target: self, selector: #selector(FDHomePageVC.updateResourceTimeout), userInfo: nil, repeats: false)
    }
  }
  
  func updateResourceTimeout() {
    if self.updateResourceTimer != nil {
      self.updateResourceTimer!.invalidate()
      self.updateResourceTimer = nil
      FDRM.sharedInstance.updateResourcesWhenExpire()
    }
  }
  
  @IBAction func onBtnMenuClicked(_ sender: AnyObject) {
    let revealViewController = self.revealViewController() as SWRevealViewController
    if isRTL() {
      revealViewController.rightRevealToggle(sender)
    } else {
      revealViewController.revealToggle(sender)
    }
  }
  
  // MARK: - SWRevealViewControllerDelegate
  func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
    
    if revealController.frontViewPosition == FrontViewPosition.right || revealController.frontViewPosition == FrontViewPosition.leftSide {
      
      let lockingView = UIView()
      lockingView.translatesAutoresizingMaskIntoConstraints = false
      
      let tap = UITapGestureRecognizer(target: self, action:#selector(UIViewController.revealToggle(_:)))
      lockingView.addGestureRecognizer(tap)
      lockingView.addGestureRecognizer(revealController.panGestureRecognizer())
      lockingView.tag = 1000
      revealController.frontViewController.view.addSubview(lockingView)
      
      let viewsDictionary = ["lockingView":lockingView] //NSDictionaryOfVariableBindings(lockingView)
      
      revealController.frontViewController.view.addConstraints(
        NSLayoutConstraint.constraints(withVisualFormat: "|[lockingView]|",
          options:NSLayoutFormatOptions(),
          metrics:nil,
          views:viewsDictionary))
      revealController.frontViewController.view.addConstraints(
        NSLayoutConstraint.constraints(withVisualFormat: "V:|[lockingView]|",
          options:NSLayoutFormatOptions(),
          metrics:nil,
          views:viewsDictionary))
      lockingView.sizeToFit()
      
    } else {
      //        self.view.userInteractionEnabled = YES;
      revealController.frontViewController.view.viewWithTag(1000)?.removeFromSuperview()
      self.view.addGestureRecognizer(revealController.panGestureRecognizer())
      //        [self.naviBarView addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    
  }
  
  // MARK: - SideMenu
  func onBtnFlightStatusClick(_ sender: UIButton?) {
    navigationController?.popToRootViewController(animated: true)
    pushViewController("FlightStatus", storyboardID: "FDFlightStatusVC", prepare: nil)
  }
  
  func onBtnFlightSchedule(_ sender: UIButton) {
    navigationController?.popToRootViewController(animated: true)
    pushViewController("FlightStatus", storyboardID: "FDFlightScheduleVC", prepare: nil)
  }
  
  // Online check-in button:
  @IBAction func onBtnOnlineCheckin(_ sender: UIButton?) {
    navigationController?.popToRootViewController(animated: false)
    postNotification(ContainerVC.kBtnCheckin, userInfo: [:])
    //        let isMemberLogin = FDLoginManager.sharedInstance.hasSavedUserCredential
    //        if isMemberLogin {
    //            self.pushViewController("ManageMyBooking", storyboardID: "FDMMBCheckinListVC", prepare: nil)
    //        } else {
    //            self.pushViewController("WebCheckin", storyboardID: "FDWebCheckinVC", prepare: nil)
    //        }
  }
  
  // My boarding passes button:
  @IBAction func onBtnMyBoardingPassClicked(_ sender: UIButton?) {
    //    self.navigationController?.popToRootViewControllerAnimated(true)
    //    self.pushViewController("WebCheckin", storyboardID: "FDFlightBoardingPassesVC", prepare: nil)
    postNotification(ContainerVC.kBtnPasses, userInfo: [:])
  }
  
  @IBAction func onBtnLogin(_ sender: UIButton) {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FDLoginVC") as! FDLoginVC
    vc.delegate = self
    present(vc, animated: true, completion: nil)
  }
  
  @IBAction func onBtnLogout(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.logoutUser({ () -> Void in
      SVProgressHUD.dismiss()
      FDAPNManager.sharedInstance.updateDeviceToken({
      }) { (_) in
      }
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
    }
  }
  
  @IBAction func onBtnMyProfile(_ sender: UIButton) {
    //    navigationController?.popToRootViewControllerAnimated(true)
    //    pushViewController("Member", storyboardID: "FDMyProfileVC", prepare: nil)
    let story = UIStoryboard(name: "Member", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDMyProfileVC")
    present(vc, animated: true, completion: nil)
    //    pushViewController("Member", storyboardID: "FDMyProfileVC", prepare: nil)
  }
  
  @IBAction func onBtnContactUsClicked(_ sender: UIButton?) {
    //        self.navigationController?.popToRootViewControllerAnimated(true)
    //        pushViewController("Member", storyboardID: "FDContactUsVC", prepare: nil)
    let story = UIStoryboard(name: "Member", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDContactUsVC")
    present(vc, animated: true, completion: nil)
  }
  
  // MARK: - FDLoginVCDelegate
  func loginDidCancel() {
    //        dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
  }
  
  func loginResetPass(_ userEmail:String?) {
    pushViewController("Member", storyboardID: "FDResetPasswordVC", prepare: { vc in
      (vc as! FDResetPasswordVC).userEmail = userEmail != nil ? userEmail! : ""
    })
  }
  
  func loginDidSuccess(_ memberLogin: FDMemberLogin) {
    AdjustSDKMan.login(naSmilesNum: memberLogin.naSmilesNumber)
    FDAPNManager.sharedInstance.updateDeviceToken({
    }) { (_) in
    }
    if FDLoginManager.sharedInstance.forcePasswordReset {
      pushViewController("Member", storyboardID: "FDNewPasswordVC", prepare: { vc in
        //                (vc as! FDNewPasswordVC).userEmail = userEmail != nil ? userEmail! : ""
      })
    }
  }
  
  func onBtnRegister() {
    let story = UIStoryboard(name: "Member", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDRegisterVC")
    present(vc, animated: true, completion: nil)
  }
  
  func newVersionAvailable(_ notification: Notification){
    navigationController?.popToRootViewController(animated: true)
    let msg = FDLocalized("A new version has been released, please go to the app store and download it")
    UIAlertView.showWithTitle("", message: msg, cancelButtonTitle: FDLocalized("Download")) { (alertView, buttonIndex) -> Void in
      // open app store
      UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/us/app/flynas-tyran-nas/id871947665?ls=1&mt=8")!)
      //            exit(0)
    }
  }
  
  // Manage my booking button:
  @IBAction func onBtnManagerMyBookingClicked(_ sender: UIButton) {
    navigationController?.popToRootViewController(animated: false)
    if FDResourceManager.sharedInstance.isSessionLogined {
      self.pushViewController("ManageMyBooking", storyboardID: "FDMMBBookingListVC", prepare: { vc in
        (vc as! FDMMBBookingListVC).showPastBooking = false
      })
    } else {
      self.pushViewController("ManageMyBooking", storyboardID: "FDMMBSearchVC", prepare: nil)
    }
  }
  
  func comingSoon() {
    let msg = FDLocalized("Coming soon")
    UIAlertView.showWithTitle("", message: msg, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) -> Void in
    }
  }
  
  @IBAction func onBtnTermClicked(_ sender: AnyObject) {
    FDUtility.onBtnTermClicked(self)
  }
  
  func checkOSVersion () {
    if self.isAr() && self.SYSTEM_VERSION_LESS_THAN("9.0") {
      let changeLangConfirm = FDLocalized("iOS 8.4 or lower version can only use English language. Please restart the app.")
      // force to En
      UIAlertView.showWithTitle("", message: changeLangConfirm, cancelButtonTitle: FDLocalized("YES")) { (alertView, buttonIndex) -> Void in
        UserDefaults.standard.set([IOS_LANGUAGE_CODE_EN], forKey:"AppleLanguages")
        FDResourceManager.sharedInstance.currentCulture = CULTURECODE_EN
        UserDefaults.standard.synchronize()
        exit(0)
      }
    }
  }
  
//  func loadAds () {
//    let imageUrl1 = isAr() ? HOMEPAGE_MAIN_IMAGE_AR : HOMEPAGE_MAIN_IMAGE_EN
//    let imageUrl2 = isAr() ? HOMEPAGE_MAIN_IMAGE_AR : HOMEPAGE_MAIN_IMAGE_EN
//    let imageUrl3 = isAr() ? HOMEPAGE_MAIN_IMAGE_AR : HOMEPAGE_MAIN_IMAGE_EN
//    FDRM.sharedInstance.refreshImage()
//    FDRM.sharedInstance.getHomePageMainImage({ (image) -> Void in
//      self.refreshControl.endRefreshing()
//      self.banners = [image]
//      self.cvBanner.reloadData()
//      self.pageControl.numberOfPages = self.banners.count
//      self.pageControl.hidden = self.banners.count < 2
//    }) { (r) -> Void in
//      self.refreshControl.endRefreshing()
//    }
//  }
  
  func handleEnterForeground(_ sender:Notification) {
  }
}

extension FDHomePageVC {
  
  func showSendMailErrorAlert() {
    let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
    sendMailErrorAlert.show()
  }
  
}

extension FDHomePageVC: MFMailComposeViewControllerDelegate {
  
  func getDeviceInfo() -> String {
    let AppVersion = "\(FDLocalized("App Version:")) \(FDUtility.getVersionNumber())"
    let OSversion = "\(FDLocalized("OS Version:")) \(UIDevice.current.systemVersion)"
    let DeviceModel = "\(FDLocalized("Device model:")) \(UIDevice.current.modelName)"
    
    var PushToken = ""
    if let token = FDAPNManager.sharedInstance.deviceToken {
      PushToken = "\(FDLocalized("Device push token:")) \(token)"
    }
    
    return "\n\(AppVersion)\n\(OSversion)\n\(DeviceModel)\n\(PushToken)\n"
  }
  
  @IBAction func onBtnAppFeedback(_ sender: UIButton) {
    print(getDeviceInfo())
    if MFMailComposeViewController.canSendMail() {
      let mailComposerVC = MFMailComposeViewController()
      mailComposerVC.mailComposeDelegate = self
      mailComposerVC.setToRecipients(["appsupport@flynas.com"])
      mailComposerVC.setSubject(FDLocalized("flynas Feedback"))
      mailComposerVC.setMessageBody(getDeviceInfo(), isHTML: false)
      present(mailComposerVC, animated: true, completion: nil)
    } else {
      showSendMailErrorAlert()
    }
  }
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
}

extension FDHomePageVC: FDNaSmileLoginVCDelegate {
  
  @IBAction func onBtnMyNaSmileCard(_ refresh:Bool) {
    //        navigationController?.popToRootViewControllerAnimated(true)
    let story = UIStoryboard(name: "naSmile", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDNaSmileCardVC")
    present(vc, animated: true, completion: nil)
    //        if FDNaSmileManager.sharedInstance.memberLogin != nil {
    //            pushViewController("naSmile", storyboardID: "FDNaSmileCardVC", prepare: { vc in
    //                (vc as! FDNaSmileCardVC).refresh = refresh
    //            })
    //        }
  }
  
  func naSmileLoginDidCancel() {
    
  }
  
  @IBAction func onBtnNaSmileLogin(_ sender: UIButton?) {
    self.navigationController?.popToRootViewController(animated: true)
    let vc = UIStoryboard(name: "naSmile", bundle: nil).instantiateViewController(withIdentifier: "FDNaSmileLoginVC") as! FDNaSmileLoginVC
    vc.delegate = self
    self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
  }
  
  @IBAction func onBtnNaSmileLogout(_ sender: UIButton?) {
    FDNaSmileManager.sharedInstance.logoutUser({
    }) { (_) in
    }
  }
  
  func naSmileJoinNow() {
    //        let url = isEn() ? "https://nasmiles.flynas.com/Portal/Modules/Members/Enrollment.aspx" : "https://nasmiles.flynas.com/Portal/Modules/Members/Enrollment.aspx?LanguageId=2"
    //        FDUtility.showUrl(self, url:url)
    //
    
    //        if FDNaSmileManager.sharedInstance.memberLogin != nil {
    //            self.pushViewController("naSmile", storyboardID: "FDNaSmileJoinVC", prepare: { vc in
    //
    //                if let userEmail = FDLoginManager.sharedInstance.userEmail {
    //                    (vc as! FDNaSmileJoinVC).naSmilesID = userEmail
    //                    (vc as! FDNaSmileJoinVC).delegate = self
    //                }
    //            })
    //        }
    
    self.navigationController?.popToRootViewController(animated: true)
    
    if FDNaSmileManager.sharedInstance.memberLogin != nil, let userEmail = FDLoginManager.sharedInstance.userEmail {
      let vc = UIStoryboard(name: "naSmile", bundle: nil).instantiateViewController(withIdentifier: "FDNaSmileJoinVC") as! FDNaSmileJoinVC
      vc.delegate = self
      vc.naSmilesID = userEmail
      self.presentPopupViewController(vc, animationType: MJPopupViewAnimationFade)
    } else {
      //          postNotification(ContainerVC.kBtnMe, userInfo: [:])
      self.pushViewController("Member", storyboardID: "FDRegisterVC", prepare: nil)
    }
  }
  
  func bookAHotel() {
    let url = "https://sp.booking.com/index.html?aid=922212&label=fn-appsidebar-link"
    FDUtility.showUrl(self, url: url)
  }
  
  func rentACar() {
    let url = isEn() ? "https://www.rentalcars.com/?affiliateCode=nasair-web" : "https://www.rentalcars.com/?affiliateCode=nasair-web&preflang=ar"
    FDUtility.showUrl(self, url: url)
  }
  
  func airportTransfer() {
    let url = isEn() ? "http://transfers.flynas.com/en/?utm_source=flynas&utm_medium=referral&utm_campaign=Mobile-app" : "http://transfers.flynas.com/ar/?utm_source=flynas&utm_medium=referral&utm_campaign=Mobile-app"
    FDUtility.showUrl(self, url: url)
  }
  
  func naSmileAbout() {
    let url = isEn() ? "http://www.flynas.com/en/nasmiles/about-nasmiles" : "http://www.flynas.com/ar/nasmiles/about-nasmiles"
    FDUtility.showUrl(self, url:url)
  }
  
  func faqs() {
    let url = isEn() ? "http://www.flynas.com/en/contact-us/faqs" : "http://www.flynas.com/ar/contact-us/faqs"
    FDUtility.showUrl(self, url: url)
  }
  
  func naSmileEarningSMILEPoints() {
    let url = isEn() ? "http://www.flynas.com/en/nasmiles/earn-smiles" : "http://www.flynas.com/ar/nasmiles/earn-smiles"
    FDUtility.showUrl(self, url:url)
  }
  
  func naSmileTierBenefits() {
    FDUtility.naSmileTierBenefits(self)
  }
  
  func smilePointCalculator() {
    let url = isEn() ? "https://www.flynas.com/en/nasmiles-calculator" : "https://www.flynas.com/ar/nasmiles-calculator"
    FDUtility.showUrl(self, url: url)
  }
  
  func partnersCalculator() {
    let url = isEn() ? "https://www.flynas.com/en/partners-calculator" : "https://www.flynas.com/ar/partners-calculator"
    FDUtility.showUrl(self, url: url)
  }
  
  func naSmileLoginDidSuccess(_ memberLogin: FDNaSmileLoyalty) {
    if isEn() {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideRightLeft)
    } else {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideLeftRight)
    }
    onBtnMyNaSmileCard(false)
  }
  
  func naSmileLoginJoinNow() {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    naSmileJoinNow()
  }
}

extension FDHomePageVC {
  
  func handleShortCutItem(_ notification: Notification){
    if let shortCutType = notification.object as? String {
      print (shortCutType)
      switch (shortCutType) {
      case ShortcutIdentifier.First.type:
        // Handle shortcut 1 (booking).
        postNotification(ContainerVC.kBtnBook, userInfo: [:])
        break
      case ShortcutIdentifier.Second.type:
        // Handle shortcut 2 (Check-in).
        onBtnOnlineCheckin(nil)
        break
      case ShortcutIdentifier.Third.type:
        // Handle shortcut 3 (Boarding pass).
        onBtnMyBoardingPassClicked(nil)
        break
      case ShortcutIdentifier.Fourth.type:
        // Handle shortcut 4 (Flight status).
        onBtnFlightStatusClick(nil)
        break
      default:
        break
      }
    }
  }
}

extension FDHomePageVC {
  
  @IBAction func onBtnMyNotifications(_ sender: UIButton) {
    pushViewController("MyNotifications", storyboardID: "FDMyNotificationsVC", prepare: nil)
  }
  
  func handleNotification(_ notification: Notification) {
    if let msg = notification.object as? FDNotificationItem {
      showNotification(msg)
    }
  }
  
  func showNotification(_ notification:FDNotificationItem) {
    //        UIAlertView.showWithTitle(notification.title, message: notification.message, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) -> Void in
    //        }
    
    FDUtility.popInfo(FDLocalized(notification.title),message: notification.message)
  }
}

extension FDHomePageVC {
  func showRateMe() {
    //        RateMyApp.sharedInstance.trackEventUsage()
  }
}

extension FDHomePageVC: FDNaSmileJoinVCDelegate {
  func naSmileJoinSuccess() {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
  }
  
  func naSmileJoinBack() {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
  }
  
}

// MARK: - UICollectionViewDelegate
extension FDHomePageVC: UICollectionViewDataSource, UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 3
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! BannerCollectionCell
    cell.imgBanner.image = SharedModel.shared.banners[indexPath.row]
    //    cell.lbTitle.text = ""
    return cell
  }
  
  func downloadBanners() {
    if let url0 = URL(string: isAr() ? HOMEPAGE_MAIN_IMAGE_AR : HOMEPAGE_MAIN_IMAGE_EN) {
      let request = URLRequest(url: url0, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 0)
      UIImageView().setImageWith(request, placeholderImage: UIImage(named: "Splash5")!, success: { (myRequest, response, image) in
        if let img = image { SharedModel.shared.banners[0] = img }
      }) { (req, res, err) in
      }
    }
    if let url1 = URL(string: isAr() ? "https://www.flynas.com/images/app/ar/2_375x593_ar_1.png" : "https://www.flynas.com/images/app/en/2_375x593_en_1.png") {
      let request = URLRequest(url: url1, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 0)
      UIImageView().setImageWith(request, placeholderImage: UIImage(named: "Splash5")!, success: { (myRequest, response, image) in
        if let img = image { SharedModel.shared.banners[1] = img }
      }) { (req, res, err) in
      }
    }
    if let url2 = URL(string: isAr() ? "https://www.flynas.com/images/app/ar/2_375x593_ar_2.png" : "https://www.flynas.com/images/app/en/2_375x593_en_2.png") {
      let request = URLRequest(url: url2, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 0)
      UIImageView().setImageWith(request, placeholderImage: UIImage(named: "Splash5")!, success: { (myRequest, response, image) in
        if let img = image { SharedModel.shared.banners[2] = img }
      }) { (req, res, err) in
      }
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
    return collectionView.bounds.size
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
}

extension FDHomePageVC: UIScrollViewDelegate {
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    updatePageControl()
  }
  
  func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    updatePageControl()
  }
  
  func updatePageControl() {
    let pageWidth = cvBanner.bounds.width
    let index = Int(cvBanner.contentOffset.x) / Int(pageWidth)
    pageControl.currentPage = index
  }
  
}
