//
//  FDSearchPaxVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 20/9/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

struct PaxStruct {
  var adult = 0
	var child = 0
  var infant = 0
}

protocol FDSearchPaxVCDelegate {
  func didPressDone(_ paxInfo: PaxStruct)
}

class FDSearchPaxVC: BaseVC {
  
  @IBOutlet weak var lbAdultNum: UILabel!
  @IBOutlet weak var lbChildNum: UILabel!
  @IBOutlet weak var lbInfantNum: UILabel!
  @IBOutlet weak var btnAddAdult: UIButton!
  @IBOutlet weak var btnAddChild: UIButton!
  @IBOutlet weak var btnAddInfant: UIButton!
  @IBOutlet weak var btnRemoveAdult: UIButton!
  @IBOutlet weak var btnRemoveChild: UIButton!
  @IBOutlet weak var btnRemoveInfant: UIButton!
  
  var delegate: FDSearchPaxVCDelegate?
  var paxInfo = PaxStruct()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    updateUI()
  }
  
  func updateUI() {
    paxInfo.infant = min(paxInfo.infant, paxInfo.adult)
    lbAdultNum.text = FDLocalizeInt(paxInfo.adult)
    lbChildNum.text = FDLocalizeInt(paxInfo.child)
    lbInfantNum.text = FDLocalizeInt(paxInfo.infant)
    btnAddAdult.isEnabled = (paxInfo.adult + paxInfo.child) < 9
    btnAddChild.isEnabled = (paxInfo.adult + paxInfo.child) < 9
    btnAddInfant.isEnabled = paxInfo.infant < paxInfo.adult
    btnRemoveAdult.isEnabled = paxInfo.adult > 1
    btnRemoveChild.isEnabled = paxInfo.child > 0
    btnRemoveInfant.isEnabled = paxInfo.infant > 0
  }
  
  @IBAction func onBtnAddAdult(_ sender: UIButton) {
    paxInfo.adult += 1
    updateUI()
  }
  
  @IBAction func onBtnAddChild(_ sender: UIButton) {
    paxInfo.child += 1
    updateUI()
  }
  
  @IBAction func onBtnAddInfant(_ sender: UIButton) {
    paxInfo.infant += 1
    updateUI()
  }
  
  @IBAction func onBtnRemoveAdult(_ sender: UIButton) {
    paxInfo.adult -= 1
    updateUI()
  }
  
  @IBAction func onBtnRemoveChild(_ sender: UIButton) {
    paxInfo.child -= 1
    updateUI()
  }
  
  @IBAction func onBtnRemoveInfant(_ sender: UIButton) {
    paxInfo.infant -= 1
    updateUI()
  }
  
  @IBAction func onBtnDone(_ sender: UIButton) {
    delegate!.didPressDone(paxInfo)
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onBtnCancel(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
  }
  
}
