//
//  FDExtraLoungeCell.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 18/7/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol FDExtraLoungeDelegate {
  func baggageErrorHandle(_ reason:String?)
  func itemChanged()
}

class FDExtraLoungeCell: UITableViewCell {

  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblCost: UILabel!
  @IBOutlet weak var btnSelect: UIButton!
  
  var delegate: FDExtraBaggageCellDelegate?
  var pn = 0
  var loungeService: (FDFlight, [PassengerLoungeServicesStruct])?
  
  @IBAction func onBtnSelect(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    loungeItemChange(!sender.isSelected) { (success) in
      sender.isSelected = success ? sender.isSelected : !sender.isSelected
    }
  }
  
  fileprivate func loungeItemChange(_ remove: Bool, success: @escaping (Bool) -> Void) {
    let parameters: [String: Any] = [
      "origin":      unwrap(str: loungeService?.0.origin),
      "destination": unwrap(str: loungeService?.0.destination),
      "pn":          pn,
      "code":        "VIP"
    ]
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getLounge(parameters, remove: remove, success: { (sports) in
      SVProgressHUD.dismiss()
      self.delegate?.itemChanged()
      success(true)
    }) { (reason) in
      SVProgressHUD.dismiss()
      self.delegate?.baggageErrorHandle(reason)
      success(false)
    }
  }
  
  func updateLounge() {
    guard let loungeServices = loungeService?.1 else { return }
    let service = loungeServices.indices.contains(pn) ? loungeServices[pn] : PassengerLoungeServicesStruct()
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
      let strPoints = "\(unwrap(str: service.services.first?.smilePointsRedeemable)) \(FDLocalized("points"))"
      let strSar = FDUtility.formatCurrencyWithCurrencySymbol(NSNumber(value: Double(unwrap(str: service.services.first?.remainingAmount))!))
      lblCost.text = "\(strPoints) + \(unwrap(str: strSar))"
    } else { // show in cash
      let price = unwrap(str: service.services.first?.price)
      lblCost.text = FDUtility.formatCurrencyWithCurrencySymbol(NSNumber(value: Double(price)!))
    }
    let count = unwrap(str: service.services.first?.count)
    btnSelect.isSelected = Int(count) > 0
  }
  
}
