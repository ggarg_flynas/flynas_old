//
//  FDMMBCheckinListVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 26/10/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMMBCheckinListVC: FDBookingCommonVC {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var viewNoItem: UIView!
  
  var memberProfile : FDMemberProfile?
  var userEmail = ""
  var openBookings = [FDBookings]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("checkin_list_page")
    tableView.delegate = self
    tableView.dataSource = self
    getMemberProfile()
    updateUI ()
    SharedModel.shared.isMMB = false
    SharedModel.shared.isWCI = true
  }
  
  func updateUI () {
    if openBookings.count > 0 {
      viewNoItem.isHidden = true
      tableView.isHidden = false
    } else {
      viewNoItem.isHidden = false
      tableView.isHidden = true
    }
  }
  
  func getMemberProfile() {
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.forceLoginAgain({ (memberLogin) -> Void in
      FDLoginManager.sharedInstance.retrieveMemberProfile(nil, success: { (memberProfile) -> Void in
        if let e = FDLoginManager.sharedInstance.userEmail {
          self.userEmail = e
        }
        self.memberProfile = memberProfile
        self.openBookings = memberProfile.customerBookings.currentBookings.filter({$0.isCheckInAllowed})
        self.updateUI()
        self.tableView.reloadData()
        SharedModel.shared.wciPassengers = []
        SharedModel.shared.wciPassengers.append(memberProfile.profileDetails.details)
        for member in memberProfile.memberFamily.members {
          SharedModel.shared.wciPassengers.append(member)
        }
        SVProgressHUD.dismiss()
      }) { (r) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(r)
      }
    }) { (r) -> Void in
      self.errorHandle(r)
      SVProgressHUD.dismiss()
    }
  }
  
  @IBAction override func onBtnHomeClicked(_ sender: UIButton) {
    let story = UIStoryboard(name: "WebCheckin", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDWebCheckinNC")
    present(vc, animated: true, completion: nil)
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
}

// MARK: - UITableViewDelegate/DataSource
extension FDMMBCheckinListVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDMyBookingTableViewHeader") {
      if let seatPassengerName = cell.viewWithTag(30) as? UILabel {
        seatPassengerName.text =
          section == 0 ? FDLocalized("Upcoming flights") : FDLocalized("Past flights")
      }
      return cell.contentView
    }
    return UITableViewHeaderFooterView()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return openBookings.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FDMyBookingTableViewCell") as! FDMyBookingTableViewCell
    let booking = openBookings[indexPath.row]
    cell.labelPNR.text = booking.recordLocator
    let originStation = FDResourceManager.sharedInstance.getStation(booking.origin)
    let destinationStation = FDResourceManager.sharedInstance.getStation(booking.destination)
    cell.labelOrigin.text = originStation?.name
    cell.labelDestination.text = destinationStation?.name
    cell.labelFlightTime.text = booking.std?.toStringUTC(format:.custom("HH:mm EEE, dd MMM yyyy"))
    cell.btnCheckin.isHidden = true
    cell.delegate = self
    SharedModel.shared.pnr = booking.recordLocator
    return cell
  }
  
}

// MARK: - FDMyBookingTableViewCellDelegate
extension FDMMBCheckinListVC: FDMyBookingTableViewCellDelegate {
  
  func btnViewClicked(_ cell: FDMyBookingTableViewCell) {
    btnCheckinClicked(cell)
  }
  
  func btnCheckinClicked(_ cell: FDMyBookingTableViewCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let booking = openBookings[indexPath.row]
      let pnr = booking.recordLocator
      SVProgressHUD.show()
      FDWebCheckinManager.sharedInstance.getBooking(pnr, departureStation: nil, email: userEmail, lastName: nil, success: { (wciBooking) -> Void in
        SVProgressHUD.dismiss()
        if wciBooking.journeys.count > 0 {
          self.pushViewController("WebCheckin", storyboardID: "FDWCiFlightListVC") { (vc) -> Void in
            //vc
            let c = vc as! FDWCiFlightListVC
            c.wciUserCheckin.wciBooking = wciBooking
            c.pnr = pnr
          }
        } else {
          self.errorHandle(FDErrorHandle.API_GENERAL_ERROR)
        }
      }, fail:{ (reason) -> Void in
        SVProgressHUD.dismiss()
        if reason == FDErrorHandle.API_ERROR_5XX {
          self.errorHandle(FDErrorHandle.API_GENERAL_ERROR)
        } else {
          self.errorHandle(reason)
        }
      })
    }
  }
  
}
