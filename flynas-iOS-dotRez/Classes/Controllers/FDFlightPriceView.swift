//
//  FDFlightPriceView.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 30/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightPriceView: UIView {
  
  static let clrEcomomy = FDColorFlynasGreen
  static let clrExpress = UIColor(red: 246/255.0, green: 137/255.0, blue: 52/255.0, alpha: 1.0)
  static let clrBusiness = UIColor(red: 100/255.0, green: 100/255.0, blue: 100/255.0, alpha: 1.0)
  static let clrUnselected = UIColor.white
  static let clrSelected = UIColor(red: 221/255.0, green: 59/255.0, blue: 109/255.0, alpha: 1.0)
  
  @IBOutlet var view: UIView!
  @IBOutlet var lbPrice1: UILabel!
  @IBOutlet var lbPrice2: UILabel!
  @IBOutlet var lbFareType: UILabel!
  @IBOutlet var btnSelectFare: UIButton!
  @IBOutlet weak var imvSelected: UIImageView!
  @IBOutlet weak var vwBackground: UIView!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    Bundle.main.loadNibNamed("FDFlightPriceView", owner: self, options: nil)
    addSubview(view)
    view.frame.size = self.frame.size
  }
  
  var fare : FDFare? {
    didSet {
      if fare != nil {
        isHidden = false
        let currencySymbol = FDResourceManager.sharedInstance.selectedCurrencySymbol
        if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
          lbPrice1.text = "\(unwrap(str: fare?.smilePointsRedeemable)) \(FDLocalized("points"))"
          lbPrice2.text = "+ \(currencySymbol) \(unwrap(str: fare?.remainingAmount))"
        } else { // show in cash
          lbPrice1.text = "\(unwrap(str: FDUtility.formatCurrency(fare!.price as NSNumber))) \(currencySymbol)"
          lbPrice2.superview?.isHidden = true
        }
        switch fare!.fareType {
        case PriceOption.Economy.rawValue:
          lbFareType.text = FDLocalized(PriceOption.Economy.rawValue)
          vwBackground.backgroundColor = FDFlightPriceView.clrEcomomy
        case PriceOption.Business.rawValue:
          lbFareType.text = FDLocalized(PriceOption.Business.rawValue)
          vwBackground.backgroundColor = FDFlightPriceView.clrBusiness
        case PriceOption.StaffStandby.rawValue:
          lbFareType.text = FDLocalized(PriceOption.StaffStandby.rawValue)
          vwBackground.backgroundColor = FDFlightPriceView.clrEcomomy
        case PriceOption.StaffConfirm.rawValue:
          lbFareType.text = FDLocalized(PriceOption.StaffConfirm.rawValue)
          vwBackground.backgroundColor = FDFlightPriceView.clrBusiness
        default: break
        }
      } else {
        isHidden = true
        selected = false
      }
    }
  }
  
  var selected = false {
    didSet {
      imvSelected.isHidden = !selected
      vwBackground.layer.borderWidth = selected ? 3.0 : 0.0
      vwBackground.layer.borderColor = UIColor(red: 225.0/255.0, green: 57.0/255.0, blue: 109.0/255.0, alpha: 1.0).cgColor
    }
  }
  
}
