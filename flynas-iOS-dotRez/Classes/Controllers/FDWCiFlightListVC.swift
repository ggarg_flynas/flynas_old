//
//  FDWCiFlightListVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWCiFlightListVC: FDBookingCommonVC, UITableViewDelegate, UITableViewDataSource, FDWciFlightInfoCellDelegate {
  
  var wciUserCheckin = FDWciUserCheckinInfo()
  var agreeToTerms = false
  var pnr = ""
  
  var expandedIndex = 0
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var btnAgreeToTerms: UIButton!
  
  @IBOutlet var labelTermEn: UILabel!
  @IBOutlet var labelTermAr: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("web_checkin_flight_select_page")
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 145
    tableView.delegate = self
    tableView.dataSource = self
    labelTermEn.isHidden = self.isAr()
    labelTermAr.isHidden = self.isEn()
    updateUI()
  }
  
  func updateUI() {
    btnAgreeToTerms.isSelected = agreeToTerms
  }
  
  @IBAction func onBtnAgreeToTermsClicked(_ sender: UIButton) {
    agreeToTerms = !agreeToTerms
    updateUI()
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    if agreeToTerms {
      
      if expandedIndex >= 0 && wciUserCheckin.wciBooking.journeys[expandedIndex].segments.count > 0 {
        
        let j = wciUserCheckin.wciBooking.journeys[expandedIndex]
        let segment = j.segments[0]
        let checkinPassengers = segment.passengers.filter({$0.prepareToCheckin})
        
        var checkinSegments = [[String:AnyObject]]()
        if checkinPassengers.count > 0 {
          let ps = checkinPassengers.map({$0.passengerNumber})
          
          // sync web checkin status to all segment
          for i in 0..<j.segments.count {
            let sg = j.segments[i]
            
            if i != 0 {
              for p in sg.passengers {
                for cp in checkinPassengers {
                  if cp.passengerNumber == p.passengerNumber {
                    p.prepareToCheckin = true
                  }
                }
              }
            }
            
            checkinSegments.append([
              "JourneyNumber":sg.journeyNumber as AnyObject,
              "SegmentNumber":sg.segmentNumber as AnyObject,
              "PassengerNumbers":ps as AnyObject,
              ])
          }
          
          
          SVProgressHUD.show()
          
          let parameters =
            ["WciCheckInPassengers":
              ["CheckInPassengers":checkinSegments]
          ]
          
          FDWebCheckinManager.sharedInstance.checkinPassenger(parameters, success: { () -> Void in
            SVProgressHUD.dismiss()
            self.pushViewController("WebCheckin", storyboardID: "FDWCiPassengerDetailVC") { (vc) -> Void in
              let c = vc as! FDWCiPassengerDetailVC
              c.checkinJourney = self.wciUserCheckin.wciBooking.journeys[self.expandedIndex]
              c.checkinJourney.pnr = self.pnr
            }
            }, fail:{ (reason) -> Void in
              SVProgressHUD.dismiss()
              self.errorHandle(reason)
          })
          
          return
        }
      }
      
      let errorMsg = FDLocalized("Please select passenger.")
      UIAlertView.showWithTitle("", message: errorMsg , cancelButtonTitle: FDLocalized("BACK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      
    } else  {
      let errorMsg = FDLocalized("Please read and accept the Terms and Conditions.")
      UIAlertView.showWithTitle("", message: errorMsg , cancelButtonTitle: FDLocalized("BACK"), completionHandler: { (alertView, buttonIndex) -> Void in
      })
    }
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return wciUserCheckin.wciBooking.journeys.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell:FDWciFlightInfoCell = self.tableView.dequeueReusableCell(withIdentifier: "FDWciFlightInfoCell") as! FDWciFlightInfoCell
    
    cell.journey = wciUserCheckin.wciBooking.journeys[indexPath.row]
    cell.delegate = self
    cell.expanded = expandedIndex == indexPath.row
    
    cell.layoutIfNeeded()
    
    return cell
  }
  
  // MARK: - FDWciFlightInfoCellDelegate
  func btnExpandClicked(_ cell:FDWciFlightInfoCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      if expandedIndex == indexPath.row {
        expandedIndex = -1
      } else {
        expandedIndex = indexPath.row
      }
      
      tableView.reloadData()
    }
  }
  
  @IBAction func onBtnTermClicked(_ sender: AnyObject) {
    FDUtility.onBtnTermClicked(self)
  }
}
