//
//  FDMyBoardingPassesTableViewCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDMyBoardingPassesTableViewCellDelegate : class  {
    func btnViewClicked(_ cell:FDMyBoardingPassesTableViewCell)
}

class FDMyBoardingPassesTableViewCell: UITableViewCell {

    @IBOutlet var labelOrigin: UILabel!
    @IBOutlet var labelDestination: UILabel!
    @IBOutlet var labelFlightTime: UILabel!

    weak var delegate: FDMyBoardingPassesTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onBtnViewClicked(_ sender: UIButton) {
        delegate?.btnViewClicked(self)
    }
}
