//
//  SettingsVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 15/9/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var lbVersion: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addObservers()
    showVersion()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableView.reloadWithAnimation()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func addObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(settingsLanguage), name: NSNotification.Name(rawValue: Settings.getItems()[0].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsCurrency), name: NSNotification.Name(rawValue: Settings.getItems()[1].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsContactUs), name: NSNotification.Name(rawValue: Settings.getItems()[2].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsAppFeedback), name: NSNotification.Name(rawValue: Settings.getItems()[3].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsRateApp), name: NSNotification.Name(rawValue: Settings.getItems()[4].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsTerms), name: NSNotification.Name(rawValue: Settings.getItems()[5].notification), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(settingsPrivacy), name: NSNotification.Name(rawValue: Settings.getItems()[6].notification), object: nil)
  }
  
  func showVersion() {
    lbVersion.text = "\(FDLocalized("Version")) \(FDUtility.getVersionNumber())"
  }
  
  @IBAction func onBtnBack(_ sender: UIButton?) {
    dismiss(animated: true, completion: nil)
  }
  
}

extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Settings.getItems().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
    let item = Settings.getItems()[indexPath.row]
    cell.lbTitle.text = FDLocalized(item.title)
    cell.accessoryType = item.arrow ? .disclosureIndicator : .none
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = Settings.getItems()[indexPath.row]
    postNotification(item.notification, userInfo: [:])
    if !item.arrow { tableView.deselectRow(at: indexPath, animated: true) }
  }
  
}

extension SettingsVC {
  
  func settingsLanguage() {
    let selLangTitle = FDLocalized("Select language")
    let enTitle = FDResourceManager.sharedInstance.enTitle
    let arTitle = FDResourceManager.sharedInstance.arTitle
    UIAlertView.showWithTitle("", message: selLangTitle, cancelButtonTitle: enTitle, otherButtonTitle: arTitle) { (alertView, buttonIndex) -> Void in
      let changeLangConfirm = FDLocalized("To change the language, you need to close and then restart the app. Are you sure you wish to proceed?")
      if buttonIndex == 1 && self.isEn() {
        UIAlertView.showWithTitle("", message: changeLangConfirm, cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitle: FDLocalized("YES")) { (alertView, buttonIndex) -> Void in
          if buttonIndex == 1 {
            UserDefaults.standard.set([IOS_LANGUAGE_CODE_AR], forKey:"AppleLanguages")
            FDResourceManager.sharedInstance.currentCulture = CULTURECODE_AR
            UserDefaults.standard.synchronize()
            exit(0)
          }
        }
      } else if buttonIndex == 0 && self.isAr() {
        UIAlertView.showWithTitle("", message: changeLangConfirm, cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitle: FDLocalized("YES")) { (alertView, buttonIndex) -> Void in
          if buttonIndex == 1 {
            UserDefaults.standard.set([IOS_LANGUAGE_CODE_EN], forKey:"AppleLanguages")
            FDResourceManager.sharedInstance.currentCulture = CULTURECODE_EN
            UserDefaults.standard.synchronize()
            exit(0)
          }
        }
      }
    }
  }
  
  func settingsCurrency() {
    let items = FDResourceManager.sharedInstance.currencies.map({$0.value!})
    let index = items.index(of: FDResourceManager.sharedInstance.selectedCurrencyCode)
    let strTitle = FDLocalized(Settings.getItems()[1].title)
    ActionSheetStringPicker.show(withTitle: strTitle, rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      FDResourceManager.sharedInstance.selectedCurrencyCode = selectedValue as! String
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: UIButton(frame: CGRect.zero))
  }
  
  func settingsContactUs() {
    pushViewController("Member", storyboardID: "FDContactUsVC", prepare: nil)
  }
  
  func settingsAppFeedback() {
    onBtnBack(nil)
    postNotification("#selector(onBtnAppFeedback)", userInfo: [:])
  }
  
  func settingsRateApp() {
    RateMyApp.sharedInstance.showRatingAlert()
  }
  
  func settingsTerms() {
    FDUtility.onBtnTermClicked(self)
  }
  
  func settingsPrivacy() {
    let url = isEn() ? "http://www.flynas.com/en/privacy-policy" : "http://www.flynas.com/ar/privacy-policy"
    FDUtility.showUrl(self, url: url)
  }
  
}
