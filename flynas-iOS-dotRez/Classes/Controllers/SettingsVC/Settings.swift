//
//  Settings.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 15/9/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import Foundation

struct SettingsStruct {
  let title: String
  let notification: String
  let arrow: Bool
}

class Settings {
  
  class func getItems() -> [SettingsStruct] {
    let language = SettingsStruct(title: "Language", notification: "#selector(settingsLanguage)", arrow: false)
    let currency = SettingsStruct(title: "Currency", notification: "#selector(settingsCurrency)", arrow: false)
    let contactUs = SettingsStruct(title: "Contact Us", notification: "#selector(settingsContactUs)", arrow: true)
    let appFeedback = SettingsStruct(title: "App Feedback", notification: "#selector(settingsAppFeedback)", arrow: false)
    let rateApp = SettingsStruct(title: "Rate Flynas App", notification: "#selector(settingsRateApp)", arrow: false)
    let terms = SettingsStruct(title: "Terms & Conditions", notification: "#selector(settingsTerms)", arrow: true)
    let privacy = SettingsStruct(title: "Privacy Policy", notification: "#selector(settingsPrivacy)", arrow: true)
    return [language, currency, contactUs, appFeedback, rateApp, terms, privacy]
  }
  
}
