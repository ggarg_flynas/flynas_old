//
//  FDExtraMealCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 22/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDExtraMealCellDelegate : class {
  func selectMeal(_ cell: FDExtraMealCell)
  func mealChanged()
  func mealErrorHandle(_ reason: String?)
}

class FDExtraMealCell: UITableViewCell/*, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout */{
  
  let MealImgTag = 30
  let MealTitle = 31
  let MealPrice = 32
  let MealSelected = 33
  
  @IBOutlet var labelPassengerFullName: UILabel!
  @IBOutlet var labelMealName: UILabel!
  @IBOutlet var labelMealPrice: UILabel!
  @IBOutlet var btnSelectMeal: UIButton!
  
  var mealService : FDFlightPartService? {
    didSet {
      updateUserSelectedMeal()
    }
  }
  var pn = 0
  var menuExpand = false {
    didSet {
      updateUI(false)
    }
  }
  weak var delegate : FDExtraMealCellDelegate?
  var canUpgrade = false
  var userBooking: FDUserBookingInfo?
  var section = 0
  
  func updateUserSelectedMeal () {
    let noMeal = FDLocalized("No meal")
    if let selectedMeal = mealService?.getSelectedMealItem() {
      let mealName = FDResourceManager.sharedInstance.getServiceItemName(selectedMeal.code, defaultName: noMeal)
      labelMealName.setTextWithAnimation(mealName)
      if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
        let strPoints = "\(selectedMeal.smilePointsRedeemable) \(FDLocalized("points"))"
        let strSAR = FDUtility.formatCurrencyWithCurrencySymbol(selectedMeal.remainingAmount as NSNumber)
        labelMealPrice.setTextWithAnimation("\(strPoints) + \(unwrap(str: strSAR))")
      } else { // show in cash
        labelMealPrice.setTextWithAnimation(unwrap(str: FDUtility.formatCurrencyWithCurrencySymbol(selectedMeal.price)))
      }
    } else {
      labelMealName.setTextWithAnimation(noMeal)
      labelMealPrice.setTextWithAnimation("")
    }
    layoutIfNeeded()
  }
  
  func updateUI (_ animated: Bool) {
    btnSelectMeal.isHidden = mealService?.services.count == 0
    canUpgrade = false
    if mealService?.getSelectedMealItem() == nil {
      btnSelectMeal.setTitleWithAnimation(FDLocalized("Select"))
    } else {
      btnSelectMeal.setTitleWithAnimation(FDLocalized("Remove"))
//      SharedModel.shared.bundleMealCode.append(unwrap(str: mealService?.serviceInBundle?.firstObject))
      let selectedMealCode = unwrap(str: mealService?.getSelectedMealItem()?.code)
      let isBundleMeal = unwrap(str: mealService?.serviceInBundle?.firstObject) == selectedMealCode
      labelMealPrice.isHidden = isBundleMeal
      if isBundleMeal {
        btnSelectMeal.setTitleWithAnimation(FDLocalized("Upgrade"))
        canUpgrade = true
      }
    }
    layoutIfNeeded()
    func updateLayoutConstraint() {
      layoutIfNeeded()
    }
    if animated {
      UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
        updateLayoutConstraint()
      }, completion: nil)
    } else {
      updateLayoutConstraint()
    }
  }
  
  // MARK : - UICollectionViewDataSource
//  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//    return mealService != nil ? mealService!.services.count : 0
//  }
//
//  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//    let meal = mealService!.services[indexPath.row]
//    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MealCell", for: indexPath)
//    let placeholder = UIImage(named: "icn-meal-no-pic")
//    if let mealImage = cell.viewWithTag(MealImgTag) as? UIImageView {
//      if let url = FDResourceManager.sharedInstance.getServiceItemUrl(meal.code) {
//        mealImage.sd_setImage(with: URL(string: url), placeholderImage:placeholder);
//      } else {
//        mealImage.image = placeholder
//      }
//    }
//    if let mealName = cell.viewWithTag(MealTitle) as? UILabel {
//      mealName.text = FDResourceManager.sharedInstance.getServiceItemName(meal.code, defaultName: FDLocalized("Name missing"))
//    }
//    if let mealPrice = cell.viewWithTag(MealPrice) as? UILabel {
//      mealPrice.text = FDUtility.formatCurrencyWithCurrencySymbol(meal.price)
//    }
//    if let mealSelected = cell.viewWithTag(MealSelected) {
//      mealSelected.isHidden = meal.count == 0
//    }
//    return cell
//  }
//
//  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//  }
//
//  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//    let meal = mealService!.services[indexPath.row]
//    if meal.count > 0 {
//      meal.count = 0
//      itemChanged(meal, delete: true, success: { (_) -> Void in
//        self.delegate?.mealChanged()
//      }) { (reason) -> Void in
//        self.delegate?.mealErrorHandle(reason)
//      }
//    } else {
//      let m = mealService?.getSelectedMealItem()
//      m?.count = 0
//      meal.count = 1
//      // add meal
//      itemChanged(meal, delete: false, success: { (_) -> Void in
//        if m != nil {
//          self.itemChanged(m!, delete: true, success: { (_) -> Void in
//            self.delegate?.mealChanged()
//          }) { (reason) -> Void in
//            self.delegate?.mealErrorHandle(reason)
//          }
//        } else {
//          self.delegate?.mealChanged()
//        }
//      }) { (reason) -> Void in
//        self.delegate?.mealErrorHandle(reason)
//      }
//    }
//    updateUserSelectedMeal()
//    updateUI(false)
//  }
  
  func itemChanged(_ b: FDServiceItem, delete: Bool, success: @escaping () -> Void, fail: @escaping (String?) -> Void) {
    //origin=RUH&&destination=DXB&&pn=2&&code=X20
    guard let myMealService = mealService else { return }
    let parameter = [
      "origin" : myMealService.origin,
      "destination" : myMealService.destination,
      "pn" : pn,
      "code" : b.code
      ] as [String : Any]
    FDBookingManager.sharedInstance.getMeal(parameter, delete: delete, success: { (m) -> Void in
      success()
    }) { (reason) -> Void in
      fail(reason)
    }
  }
  
  @IBAction func onBtnExpandMenuClicked(_ sender: UIButton) {
    delegate!.selectMeal(self)
  }
  
  @IBAction func onBtnSelectMeal(_ sender: UIButton) {
    if mealService?.getSelectedMealItem() == nil {
      delegate!.selectMeal(self)
    } else if canUpgrade {
      guard let m = mealService?.getSelectedMealItem() else { return }
      m.count = 0
      self.delegate!.selectMeal(self)
      SVProgressHUD.show()
      self.itemChanged(m, delete: true, success: { (_) -> Void in
        self.delegate?.mealChanged()
        delay(1.0, closure: {
          SVProgressHUD.dismiss()
        })
      }) { (reason) -> Void in
        SVProgressHUD.dismiss()
        self.delegate?.mealErrorHandle(reason)
      }
      updateUserSelectedMeal()
      updateUI(true)
    } else {
      guard let m = mealService?.getSelectedMealItem() else { return }
      m.count = 0
      SVProgressHUD.show()
      self.itemChanged(m, delete: true, success: { (_) -> Void in
        self.delegate?.mealChanged()
        delay(1.0, closure: {
          SVProgressHUD.dismiss()
          self.addBundleMeal()
        })
      }) { (reason) -> Void in
        SVProgressHUD.dismiss()
        self.delegate?.mealErrorHandle(reason)
      }
      updateUserSelectedMeal()
      updateUI(true)
    }
  }
  
  func addBundleMeal() {
    let bundleCode = unwrap(str: userBooking?.mealSsr?.passengerServices[section].flightPartServices.first?.bundleCode)
    if bundleCode == BundleCode.LIGH.rawValue || bundleCode == BundleCode.LIT1.rawValue || bundleCode == BundleCode.LIT2.rawValue { return }
    let hasDefault = mealService?.serviceInBundle?.contains("BBML")
    if unwrap(bool: hasDefault) == false { return }
    guard let bundleMeal = mealService?.services[0] else { return }
    bundleMeal.count = 1
    SVProgressHUD.show()
    itemChanged(bundleMeal, delete: false, success: {
      SVProgressHUD.dismiss()
      self.delegate!.mealChanged()
    }) { (error) in
      SVProgressHUD.dismiss()
      print("fail")
    }
  }
  
}
