//
//  FDSearchVC.swift
//  flynas-dotRez
//
//  Created by River Huang on 11/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit
import CoreLocation

enum FlightOption : Int {
  case `return` = 30
  case oneWay
}

class FDSearchVC: BaseVC, FDSelectCityVCDelegate,FDSelectDateVCDelegate {
  
  @IBOutlet var blockHeight: NSLayoutConstraint!
  @IBOutlet var dateDeparture: UIView!
  @IBOutlet var dateReturn: UIView!
  @IBOutlet weak var equalWidthReturnDateView: NSLayoutConstraint!
  @IBOutlet weak var widthReturnDateView: NSLayoutConstraint!
  @IBOutlet weak var trailingDepartureDateVw1: NSLayoutConstraint!
  @IBOutlet weak var trailingDepartureDateVw2: NSLayoutConstraint!
  @IBOutlet var labelOrigin: UILabel!
  @IBOutlet var labelDestination: UILabel!
  @IBOutlet weak var lbOriginShortName: UILabel!
  @IBOutlet weak var lbDestinationShortName: UILabel!
  @IBOutlet var labelDepartureDate: UILabel!
  @IBOutlet var labelRetureDate: UILabel!
  @IBOutlet var labelReturnDateTitle: UILabel!
  @IBOutlet var btnOriginCity: UIButton!
  @IBOutlet var btnSelDestCity: UIButton!
  @IBOutlet var btnSelDepartureDate: UIButton!
  @IBOutlet var btnSelReturnDate: UIButton!
  @IBOutlet var btnSearch: UIButton!
  @IBOutlet var btnReturn: DLRadioButton!
  @IBOutlet var btnOneWay: DLRadioButton!
  @IBOutlet var tfPromoCode: FDTextField!
  @IBOutlet var imvPayWithNaSmiles: UIImageView!
  @IBOutlet weak var imvPayWithCash: UIImageView!
  @IBOutlet weak var lbPassengers: UILabel!
  @IBOutlet weak var vwNaSmiles: UIView!
  
  var flightOption = FlightOption.return
  var stationDepart : FDResourceStation?
  var stationArrive : FDResourceStation?
  var departureDate : Date?
  var returnDate : Date?
  var adultNum: Int = 1
  var childNum: Int = 0
  var infantNum: Int = 0
  let WaitingForResourceInterval = 1
  var waitingForStationResTimer : Timer?
  let WaitingForResourceMaxWaiting = 30
  var waitingForResourceCount = 0
  var promoCode = ""
  let locationManager = CLLocationManager()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    SharedModel.shared.isMMB = false
    SharedModel.shared.isWCI = false
    log("flight_search_page")
    btnReturn.flipButton()
    btnOneWay.flipButton()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
    departureDate = FDRM.sharedInstance.getLatestSearcDepartureDate() as Date
    returnDate = FDRM.sharedInstance.getLatestSearcArriveDate() as Date
    if departureDate?.compare(returnDate!) == ComparisonResult.orderedDescending {
      returnDate = departureDate?.dateByAddingDays(1)
    }
    flightOption = FDResourceManager.sharedInstance.getFlightOption()
    (adultNum, childNum, infantNum) = FDRM.sharedInstance.getLatestSearcPassengers()
    promoCode = FDResourceManager.sharedInstance.getLatestSearcPromoCode()
    let naSmiles = FDResourceManager.sharedInstance.getPayWithNaSmiles() && FDResourceManager.sharedInstance.selectedCurrencyCode == "SAR"
    FDResourceManager.sharedInstance.setPayWithNaSmiles(naSmiles)
    (view.viewWithTag(flightOption.rawValue) as? UIButton)?.isSelected = true;
    updateFlightOption(false)
    updatePayWithNaSmiles()
    updateUI()
    NotificationCenter.default.addObserver(self, selector: #selector(sationResourceReady(_:)), name: NSNotification.Name(rawValue: FDRM.SationResourceReady), object: nil)
    tfPromoCode.addTarget(self, action: #selector(tfPromoCodeDidChange), for: .editingChanged)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    FDRM.sharedInstance.updateResourcesWhenExpire()
    updateAireport()
  }
  
  deinit {
    stopTimer()
  }
  
  @IBAction func onBtnSwtchFlights1(_ sender: UIButton) {
    let tempDepart = stationDepart
    let tempArrive = stationArrive
    stationArrive = tempDepart
    stationDepart = tempArrive
    FDResourceManager.sharedInstance.setLatestDepartCity(stationDepart?.code)
    FDResourceManager.sharedInstance.setLatestArriveCity(stationArrive?.code)
    UIView.animate(withDuration: 0.2, animations: {
      self.labelOrigin.alpha = 0.0
      self.labelDestination.alpha = 0.0
      self.lbOriginShortName.alpha = 0.0
      self.lbDestinationShortName.alpha = 0.0
    }, completion: { (_) in
      self.updateUI()
      UIView.animate(withDuration: 0.2, animations: {
        self.labelOrigin.alpha = 1.0
        self.labelDestination.alpha = 1.0
        self.lbOriginShortName.alpha = 1.0
        self.lbDestinationShortName.alpha = 1.0
      }) 
    }) 
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    stopTimer()
    switch segue.identifier! {
    case "SelCity" :
      let controller = segue.destination as! FDSelectCityVC
      controller.delegate = self
      switch sender as! UIButton {
      case btnOriginCity:
        controller.destinationOption =  DestinationOption.depart
        controller.stationDepart = stationDepart
        controller.stationArrive = stationArrive
        break
      case btnSelDestCity:
        controller.destinationOption =  DestinationOption.arrive
        controller.stationDepart = stationDepart
        controller.stationArrive = stationArrive
        break
      default: break
      }
      break
    case "SelDate" :
      let controller = segue.destination as! FDSelectDateVC
      controller.delegate = self
      controller.departureDate = departureDate
      controller.returnDate = returnDate
      if self.flightOption == .return {
        switch sender as! UIButton {
        case btnSelDepartureDate:
          controller.destinationOption = .depart
          break
        case btnSelReturnDate:
          controller.destinationOption = .arrive
          break
        default: break
        }
      } else {
        controller.departOnly = true
      }
      break;
    case "SearchFlights" :
      let controller = segue.destination as! FDSelFlightsVC
      controller.userBooking.departureDate = departureDate
      controller.userBooking.returnDate = flightOption == FlightOption.oneWay ? nil : returnDate
      controller.userBooking.stationDepart = stationDepart
      controller.userBooking.stationArrive = stationArrive
      controller.userBooking.currencyCode = FDResourceManager.sharedInstance.selectedCurrencyCode
      controller.userBooking.setPassengerNumber(adultNum, childNum: childNum, infantNum: infantNum)
      controller.userBooking.payWithNaSmiles = FDResourceManager.sharedInstance.getPayWithNaSmiles()
      controller.userBooking.promoCode = promoCode
      FDRM.sharedInstance.setLatestSearcPassengers(adultNum, child: childNum, infant: infantNum)
      FDResourceManager.sharedInstance.setFlightOption(flightOption)
      if let d = departureDate {
        FDRM.sharedInstance.setLatestSearcDepartureDate(d)
      }
      if let d = returnDate {
        FDRM.sharedInstance.setLatestSearcArriveDate(d)
      }
      FDRM.sharedInstance.setLatestSearcPromoCode(promoCode)
      FDEventTrackingSendEvent("Search", action:getGAAction(), label: getGALabel()) // send GA event
      break;
    default: break
    }
  }
  
  func getGAAction() -> String {
    var tripType = "OneWay"
    if flightOption == FlightOption.return {
      tripType = "Return"
    }
    return tripType
  }
  
  func getGALabel() -> String {
    var d1 = ""
    if let d = departureDate {
      d1 = d.toString(format: .custom("yyyy-MM-dd"))
    }
    var d2 = ""
    if let d = returnDate {
      d2 = d.toString(format: .custom("yyyy-MM-dd"))
    }
    var s1 = ""
    if let s = stationDepart {
      s1 = s.code
    }
    var s2 = ""
    if let s = stationArrive {
      s2 = s.code
    }
    let flight2 = ""
    return "\(s1)->\(s2),\(flight2),\(d1),\(d2),\(adultNum),\(childNum),\(infantNum),\(FDResourceManager.sharedInstance.selectedCurrencyCode)"
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
  @IBAction func onBtnFlightOptionClicked(_ sender: AnyObject) {
    let newOption = FlightOption(rawValue:(sender as! UIButton).tag)!
    if flightOption != newOption {
      flightOption = newOption
      updateFlightOption(true)
    }
  }
  
  func updateFlightOption(_ animated: Bool) {
    switch flightOption {
    case .return:
      widthReturnDateView.priority = 100
      equalWidthReturnDateView.priority = 900
      trailingDepartureDateVw1.priority = 900
      trailingDepartureDateVw2.priority = 100
      labelReturnDateTitle.text = FDLocalized("Return Date")
    case .oneWay:
      equalWidthReturnDateView.priority = 100
      widthReturnDateView.priority = 900
      trailingDepartureDateVw1.priority = 100
      trailingDepartureDateVw2.priority = 900
      labelReturnDateTitle.text = FDLocalized("Return Date")
    }
    if animated {
      UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
        self.view.layoutIfNeeded()
        self.dateReturn.alpha = self.flightOption == .return ? 1.0 : 0.0
        }, completion: nil)
    } else {
      self.view.layoutIfNeeded()
    }
    updateUI()
  }
  
  @IBAction func onBtnPassengers(_ sender: UIButton) {
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() { return }
    let searchPaxVC = storyboard?.instantiateViewController(withIdentifier: "FDSearchPaxVC") as! FDSearchPaxVC
    searchPaxVC.paxInfo = PaxStruct(adult: adultNum, child: childNum, infant: infantNum)
    searchPaxVC.delegate = self
    present(searchPaxVC, animated: true, completion: nil)
  }
  
  func updateUI() {
    labelOrigin.text = unwrap(str: stationDepart?.name)
    labelDestination.text = unwrap(str: stationArrive?.name)
    lbOriginShortName.text = unwrap(str: stationDepart?.code)
    lbDestinationShortName.text = unwrap(str: stationArrive?.code)
    labelDepartureDate.text = departureDate?.toStringUTC(format: .custom("EEEE, dd MMM yyyy"))
    labelRetureDate.text = returnDate?.toStringUTC(format: .custom("EEEE, dd MMM yyyy"))
    var strPassengers = "\(FDLocalizeInt(adultNum)) \(FDLocalized("Adult"))"
    strPassengers += childNum > 0 ? ", \(FDLocalizeInt(childNum)) \(FDLocalized("Child"))" : ""
    strPassengers += infantNum > 0 ? ", \(FDLocalizeInt(infantNum)) \(FDLocalized("Infant"))" : ""
    lbPassengers.text = strPassengers
    lbPassengers.alpha = FDResourceManager.sharedInstance.getPayWithNaSmiles() ? 0.5 : 1.0
    btnSearch.isEnabled = (stationDepart != nil && stationArrive != nil && departureDate != nil)
    tfPromoCode.text = promoCode
    vwNaSmiles.alpha = FDResourceManager.sharedInstance.selectedCurrencyCode == "SAR" ? 1.0 : 0.5
    imvPayWithNaSmiles.isHighlighted = FDResourceManager.sharedInstance.getPayWithNaSmiles()
    imvPayWithCash.isHighlighted = !FDResourceManager.sharedInstance.getPayWithNaSmiles()
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() { adultNum = 1; childNum = 0; infantNum = 0 }
  }
  
  @IBAction func onBtnSelCityClicked(_ sender: AnyObject) {
    view.endEditing(true)
    if FDRM.sharedInstance.isStationReady {
      performSegue(withIdentifier: "SelCity", sender: sender)
    } else {
      SVProgressHUD.show()
      FDRM.sharedInstance.updateResourcesWhenExpire()
      waitingForStationResTimer = Timer.scheduledTimer(timeInterval: TimeInterval( self.WaitingForResourceInterval), target: self, selector: #selector(selCityClickedTimeout), userInfo: sender, repeats: true)
      waitingForResourceCount = 0
    }
  }
  
  @IBAction func onBtnSelDateClicked(_ sender: AnyObject) {
    view.endEditing(true)
    performSegue(withIdentifier: "SelDate", sender: sender)
  }
  
  // MARK: - FDSelectCityVCDelegate
  func didSelect(_ stationDepart: FDResourceStation, stationArrive: FDResourceStation?) {
    self.stationArrive = stationArrive
    self.stationDepart = stationDepart
    FDResourceManager.sharedInstance.setLatestDepartCity(stationDepart.code)
    FDResourceManager.sharedInstance.setLatestArriveCity(stationArrive?.code)
    updateUI ()
  }
  
  func didSelect(_ dateDepart: Date, dateReturn: Date?) {
    departureDate = dateDepart
    returnDate = dateReturn
    updateUI()
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    view.endEditing(true)
    if checkIfTermsAndConditions() {
      let vc = storyboard?.instantiateViewController(withIdentifier: "FDHajjUmrahTermsVC") as! FDHajjUmrahTermsVC
      vc.delegate = self
      present(vc, animated: true, completion: nil)
      return
    }
    performSegue(withIdentifier: "SearchFlights", sender: sender)
  }
  
  func checkIfTermsAndConditions() -> Bool {
    if unwrap(str: stationArrive?.code) != "JED" { return false }
    if unwrap(str: stationDepart?.code) == "LHE" { return true }
    if unwrap(str: stationDepart?.code) == "AUH" { return true }
    if unwrap(str: stationDepart?.code) == "AMM" { return true }
    if unwrap(str: stationDepart?.code) == "IST" { return true }
    if unwrap(str: stationDepart?.code) == "BEY" { return true }
    return false
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
  func sationResourceReady(_ notification: Notification){
    selCityClickedTimeout()
    updateAireport()
  }
  
  func stopTimer() {
    waitingForStationResTimer?.invalidate()
    waitingForStationResTimer = nil
    waitingForResourceCount = 0
  }
  
  func selCityClickedTimeout() {
    if let t = self.waitingForStationResTimer {
      if FDRM.sharedInstance.isStationReady {
        SVProgressHUD.dismiss()
        performSegue(withIdentifier: "SelCity", sender: t.userInfo)
        stopTimer()
      } else {
        waitingForResourceCount += 1
        if waitingForResourceCount > WaitingForResourceMaxWaiting {
          SVProgressHUD.dismiss()
          stopTimer()
          let msg = FDLocalized("Unable to retrieve flight stations")
          UIAlertView.showWithTitle("", message: msg, cancelButtonTitle: FDLocalized("CANCEL")) { (alertView, buttonIndex) -> Void in
          }
        }
      }
    }
  }
  
  func updatePayWithNaSmiles() {
    imvPayWithNaSmiles.isHighlighted = FDResourceManager.sharedInstance.getPayWithNaSmiles()
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() {
      adultNum = 1
      childNum = 0
      infantNum = 0
      FDResourceManager.sharedInstance.selectedCurrencyCode = "SAR"
    }
  }
  
  @IBAction func onBtnPayWithNaSmilesClicked(_ sender: AnyObject) {
    if FDResourceManager.sharedInstance.selectedCurrencyCode != "SAR" { return }
    FDResourceManager.sharedInstance.setPayWithNaSmiles(true)
    updatePayWithNaSmiles()
    updateUI()
  }
  
  @IBAction func onBtnPayWithCash(_ sender: UIButton) {
    FDResourceManager.sharedInstance.setPayWithNaSmiles(false)
    updatePayWithNaSmiles()
    updateUI()
  }
  
  func tfPromoCodeDidChange() {
    promoCode = unwrap(str: tfPromoCode.text)
  }
  
}

extension FDSearchVC: FDSearchPaxVCDelegate {
  
  func didPressDone(_ paxInfo: PaxStruct) {
    adultNum = paxInfo.adult
    childNum = paxInfo.child
    infantNum = paxInfo.infant
    updateUI()
  }
  
}

extension FDSearchVC: CLLocationManagerDelegate {
  
  func updateAireport() {
    if let latestDepartCity = FDResourceManager.sharedInstance.getLatestDepartCity() {
      stationDepart = latestDepartCity
      stationArrive = FDResourceManager.sharedInstance.getLatestArriveCity()
    } else {
      stationDepart = nil
      stationArrive = nil
      getNearestAirport()
    }
    updateUI()
  }
  
  
  func getNearestAirport() {
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Error while updating location " + error.localizedDescription)
    locationManager.stopUpdatingLocation()
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    locationManager.stopUpdatingLocation()
    if let location = locations.first {
      if stationDepart == nil {
        stationDepart = FDResourceManager.sharedInstance.getNearestStation(location.coordinate.latitude, lon: location.coordinate.longitude)
        updateUI()
      }
      print("Found user's location: \(location)")
    }
  }
  
}

extension FDSearchVC: FDHajjUmrahTermsVCDelegate {
  
  func didPressButton(agreed: Bool) {
    if !agreed { return }
    performSegue(withIdentifier: "SearchFlights", sender: nil)
  }
  
}
