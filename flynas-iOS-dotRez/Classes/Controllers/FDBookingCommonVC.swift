//
//  FDBookingCommonVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDBookingCommonActionDelegate : class {
  func onBtnContinueClicked()
  func onBtnBackClicked()
  func updatePage()
}

class FDBookingCommonVC: BaseVC {
  
  var TableHeaderHeight: CGFloat = 30.0
  let TableCellHeight : CGFloat = 58.0
  
  static let StoryBoard1 = "Main"
  static let StoryBoard2 = "BookingFlow2"
  
  weak var actionDelegate: FDBookingCommonActionDelegate?
  
  var userBooking = FDUserBookingInfo()
  @IBOutlet var btnNext: UIButton!
  
  @IBOutlet var viewTripSummary: FDTripSummaryView?
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.destination .isKind(of: FDBookingCommonVC.self) {
      let bookingVC = segue.destination as! FDBookingCommonVC
      bookingVC.userBooking = self.userBooking
    }
  }
  
  func updatePriceSummary(_ withSellKeys:Bool = false, success:(()->Void)? = nil) {
    if viewTripSummary != nil {
      self.viewTripSummary?.update(userBooking)
      userBooking.updatePriceSummary(withSellKeys, success: { (priceSummary) -> Void in
        PassengerLoungeServices.getLounge() { (loungeSsr) in
          self.userBooking.loungeSsrStruct = loungeSsr
          self.userBooking.priceSummary = priceSummary
          self.viewTripSummary?.update(self.userBooking)
          if let success = success {
            success()
          }
        }
      }) { (reason) -> Void in
        FDErrorHandle.apiCallError(reason)
      }
    }
  }
  
  override func pushViewController(_ storyboardName: String, storyboardID:String, prepare:((UIViewController) -> Void)? ) {
    super.pushViewController(storyboardName, storyboardID: storyboardID) { (targetVC) -> Void in
      if targetVC.isKind(of: FDBookingCommonVC.self) {
        (targetVC as! FDBookingCommonVC).userBooking = self.userBooking
      }
      prepare?(targetVC)
    }
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  func gotoExtraPage() {
    func gotoNextPage() {
      if (self.userBooking.baggageSsr != nil && self.userBooking.baggageSsr!.needUserAction()) || (self.userBooking.mealSsr != nil && self.userBooking.mealSsr!.needUserAction()) || (self.userBooking.sportsSsr != nil && self.userBooking.sportsSsr!.needUserAction()) {
        pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDExtraVC", prepare: nil)
      } else {
        gotoSeatMapPage()
      }
    }
    self.userBooking.baggageSsr = nil
    self.userBooking.mealSsr = nil
    self.userBooking.sportsSsr = nil
    self.userBooking.addBagage = false
    self.userBooking.selectMeals = false
    self.userBooking.currentExpandedMealCell = nil
    func fetchBaggage(_ completion: @escaping () -> Void) {
      FDBookingManager.sharedInstance.getBaggage (nil, delete: false, success: { (baggage) -> Void in
        self.userBooking.baggageSsr = baggage
        completion()
      }) { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        completion()
      }
    }
    func fetchMeal(_ completion: @escaping () -> Void) {
      FDBookingManager.sharedInstance.getMeal(nil, delete: false,success: { (meal) -> Void in
        self.userBooking.mealSsr = meal
        completion()
      }) { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        completion()
      }
    }
    func fetchSports(_ completion: @escaping () -> Void) {
      FDBookingManager.sharedInstance.getSportsEquipment(nil, delete: false, success: { (sports) in
        self.userBooking.sportsSsr = sports
        completion()
      }) { (reason) in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        completion()
      }
    }
    func fetchLounge(_ completion: @escaping () -> Void) {
      PassengerLoungeServices.getLounge { (loungeSsr) in
        self.userBooking.loungeSsrStruct = loungeSsr
        completion()
      }
    }
    SVProgressHUD.show()
    fetchBaggage() {
      fetchMeal() {
        fetchSports() {
          fetchLounge() {
            SVProgressHUD.dismiss()
            gotoNextPage()
          }
        }
      }
    }
  }
  
  func gotoSeatMapPage () {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.seatMap(nil, success: { (flightSeatMap, errorMsg) -> Void in
      SVProgressHUD.dismiss()
      var hasSeatOption = false
      if let fm = flightSeatMap {
        self.userBooking.flightSeatMap = fm
        if fm.seatMaps.count > 0 {
          hasSeatOption = true
          self.pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDSelSeatVC") { targetVC in
            let s = targetVC as! FDSelSeatVC
            s.seatMap = fm
          }
        }
      }
      if !hasSeatOption {
        self.gotoPayment()
      }
      FDErrorHandle.apiCallErrorWithAler(errorMsg)
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
      _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
    }
  }
  
  func gotoPayment(_ prepare:((UIViewController) -> Void)? = nil) {
    self.pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDPaymentVC") { targetVC in
      prepare?(targetVC)
    }
  }
  
  func setShowPriceChanged(_ show:Bool) {
    viewTripSummary?.showPriceChanged = show
  }
  
}
