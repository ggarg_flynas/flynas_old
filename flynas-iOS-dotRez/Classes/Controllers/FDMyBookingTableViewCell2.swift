//
//  FDMyBookingTableViewCell2.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 12/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMyBookingTableViewCell2: UITableViewCell {


    @IBOutlet var labelOrigin: UILabel!
    @IBOutlet var labelDestination: UILabel!
    @IBOutlet var labelFlightTime: UILabel!
    @IBOutlet var imgTickBg: UIImageView!
    @IBOutlet var imgTicked: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
    }
}
