//
//  FDLoginVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDLoginVCDelegate : class {
    func loginDidSuccess(_ memberLogin: FDMemberLogin)
    func loginResetPass(_ userEmail:String?)
}

class FDLoginVC: BaseVC {

    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var labelTermsEn: UILabel!
    @IBOutlet var labelTermsAr: UILabel!
  
  weak var delegate : FDLoginVCDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
    log("login_popup")
    labelTermsEn.isHidden = isAr()
    labelTermsAr.isHidden = isEn()
    //        FDReplaceLableAttributedTitle(labelForgotPass, title: FDLocalized("Forgot Password"))
  }
  
  @IBAction func onBtnShowPassword(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    tfPassword.isSecureTextEntry = !sender.isSelected
    sender.setTitle(FDLocalized("Show"), for: UIControlState())
    sender.setTitle(FDLocalized("Hide"), for: .selected)
  }

    @IBAction func onBtnCloseClicked(_ sender: UIButton) {
      view.endEditing(true)
      dismiss(animated: true, completion: nil)
    }

  @IBAction func onBtnLoginClicked(_ sender: UIButton) {
    self.view.endEditing(true)
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.loginUser(tfEmail.text, password: tfPassword.text, forceLogin:true, success: { (memberLogin) -> Void in
      SVProgressHUD.dismiss()
      FDEventTrackingSendEvent("Login", action:"Success")
      ArchiveManager.saveArray([], key: SearchItem.kMMBSearchDefaults)
      self.dismiss(animated: true) {
        self.delegate?.loginDidSuccess(memberLogin)
      }
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
      FDLoginManager.sharedInstance.clearUserNamePass()
      if reason == FDErrorHandle.API_GENERAL_ERROR {
        self.errorHandle(FDErrorHandle.API_LOGIN_FAIL)
      } else {
        self.errorHandle(reason)
      }
      FDEventTrackingSendEvent("Login", action:"Fail",label: reason != nil ? reason! : "" )
    }
  }

  @IBAction func onBtnRegisterClicked(_ sender: UIButton) {
    view.endEditing(true)
    dismiss(animated: true, completion: nil)
    postNotification("#selector(onBtnRegister)", userInfo: [:])
  }

    @IBAction func onBtnTermClicked(_ sender: UIButton) {
        FDUtility.onBtnTermClicked(self)
    }

    @IBAction func onBtnForgotPassClicked(_ sender: UIButton) {
      dismiss(animated: true) { 
        self.delegate?.loginResetPass(self.tfEmail.text)
      }
    }
}
