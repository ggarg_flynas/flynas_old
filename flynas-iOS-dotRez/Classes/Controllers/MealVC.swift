//
//  MealVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 14/6/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

class MealVC: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var mealService = FDFlightPartService()
  var tempArray: [FDPassengerService.FlightPartService.Service] = []
  weak var delegate: FDExtraMealCellDelegate?
  var pn = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    for service in mealService.services {
      if service.code == "BBML" {
        continue
      }
      tempArray.append(service)
    }
    collectionView.reloadData()
    delay(0.5) {
      self.scrollToSelected()
    }
  }
  
  @IBAction func onBtnDismiss(_ sender: UIButton) {
    addBundleMeal()
  }
  
  @IBAction func onBtnDismissFromBg(_ sender: UIButton) {
    addBundleMeal()
  }
  
  func itemChanged(_ b:FDServiceItem, delete:Bool, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    //origin=RUH&&destination=DXB&&pn=2&&code=X20
    let parameter = [
      "origin":mealService.origin,
      "destination":mealService.destination,
      "pn":pn,
      "code":b.code
    ] as [String : Any]
    FDBookingManager.sharedInstance.getMeal(parameter, delete: delete, success: { (m) -> Void in
      success()
    }) { (reason) -> Void in
      fail(reason)
    }
  }
  
  func scrollToSelected() {
    for i in 0..<tempArray.count {
      let meal = tempArray[i]
      if meal.count > 0 {
        let indexPath = IndexPath(row: i, section: 0)
        collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
        return
      }
    }
  }
  
}

extension MealVC: UICollectionViewDataSource, UICollectionViewDelegate {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return tempArray.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MealCollectionCell", for: indexPath) as! MealCollectionCell
    let meal = tempArray[indexPath.row]
    let placeholder = UIImage(named: "icn-meal-no-pic")
    if let url = FDResourceManager.sharedInstance.getServiceItemUrl(meal.code) {
      cell.cellImageView.sd_setImage(with: URL(string: url), placeholderImage: placeholder);
    } else {
      cell.cellImageView.image = placeholder
    }
    cell.cellTitleLabel.text = FDResourceManager.sharedInstance.getServiceItemName(meal.code, defaultName: FDLocalized("Name missing"))
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
      let strPoints = "\(meal.smilePointsRedeemable) \(FDLocalized("points"))"
      let strSAR = FDUtility.formatCurrencyWithCurrencySymbol(meal.remainingAmount as NSNumber)
      cell.cellPriceLabel.text = "\(strPoints) + \(unwrap(str: strSAR))"
    } else { // show in cash
      cell.cellPriceLabel.text = FDUtility.formatCurrencyWithCurrencySymbol(meal.price)
    }
    cell.cellSelectedView.isHidden = meal.count == 0
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let selectedMeal = tempArray[indexPath.row]
    if let previousMeal = mealService.getSelectedMealItem() {
      deleteMeal(meal: previousMeal) {
        self.addMeal(meal: selectedMeal)
      }
    } else {
      addMeal(meal: selectedMeal)
    }
    collectionView.reloadData()
    delay(0.5) { 
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  func deleteMeal(meal: FDPassengerService.FlightPartService.Service, completion: @escaping (() -> Void)) {
    meal.count = 0
    itemChanged(meal, delete: true, success: { (_) -> Void in
      completion()
    }) { (reason) -> Void in
      completion()
    }
  }
  
  func addMeal(meal: FDPassengerService.FlightPartService.Service) {
    meal.count = 1
    itemChanged(meal, delete: false, success: { (_) -> Void in
      self.delegate?.mealChanged()
    }) { (reason) -> Void in
      self.delegate?.mealErrorHandle(reason)
    }
  }
  
  func addBundleMeal() {
    if unwrap(str: mealService.serviceInBundle?.firstObject).isEmpty || unwrap(str: mealService.serviceInBundle?.firstObject) == "<null>" {
      dismiss(animated: true, completion: nil)
      return
    }
    let bundleMeal = mealService.services[0]
    bundleMeal.count = 1
    SVProgressHUD.show()
    itemChanged(bundleMeal, delete: false, success: {
      SVProgressHUD.dismiss()
      self.delegate!.mealChanged()
      self.dismiss(animated: true, completion: nil)
    }) { (error) in
      SVProgressHUD.dismiss()
      self.dismiss(animated: true, completion: nil)
      print("fail")
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let imageView = scrollView.subviews[scrollView.subviews.count - 1] as! UIImageView
    imageView.backgroundColor = UIColor(red: 35.0/255.0, green: 188.0/255.0, blue: 185.0/255.0, alpha: 1.0)
  }
  
}

class MealCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var cellImageView: UIImageView!
  @IBOutlet weak var cellTitleLabel: UILabel!
  @IBOutlet weak var cellPriceLabel: UILabel!
  @IBOutlet weak var cellSelectedView: UIView!
  
}
