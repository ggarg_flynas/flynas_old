//
//  BannerCollectionCell.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 14/8/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

class BannerCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var imgBanner: UIImageView!
  @IBOutlet weak var lbTitle: UILabel!
  
}
