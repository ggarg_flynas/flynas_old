//
//  FDNaSmileLoginVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/05/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

protocol FDNaSmileLoginVCDelegate : class {
    func naSmileLoginDidCancel()
    func naSmileLoginDidSuccess(_ memberLogin: FDNaSmileLoyalty)
    func naSmileLoginJoinNow()
}

class FDNaSmileLoginVC: BaseVC {

    weak var delegate : FDNaSmileLoginVCDelegate?
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!

    @IBOutlet var bgName: UIButton!
    @IBOutlet var bgPass: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("naSmile_login_page")
        let imageHighlight = UIImage(named: "bg-btn-highlight")
        bgName.setBackgroundImage(imageHighlight, for: .highlighted)
        bgName.setBackgroundImage(imageHighlight, for: .highlighted)

        tfEmail.addTarget(self, action: #selector(FDNaSmileLoginVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfEmail.addTarget(self, action: #selector(FDNaSmileLoginVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)

        #if DEBUG
            testData()
        #endif
    }
    #if DEBUG
    func testData () {
        tfEmail.text = "3333333333"
        tfPassword.text = "nas12345"

    }
    #endif

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        delegate?.naSmileLoginDidCancel()
    }

    @IBAction func onBtnLoginClicked(_ sender: UIButton) {
        self.view.endEditing(true)

        if let nasMemberAccountId = validateNasMemberAccountId(), let pass = validatePass() {

            SVProgressHUD.show()
            FDNaSmileManager.sharedInstance.manualLogin(nasMemberAccountId, password: pass, success: { (memberLogin) in
                SVProgressHUD.dismiss()
                self.delegate?.naSmileLoginDidSuccess(memberLogin)
                }) { (reason) in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
            }

        }
    }

    @IBAction func onBtnRegisterClicked(_ sender: UIButton) {
        // join now
        self.view.endEditing(true)
        delegate?.naSmileLoginJoinNow()
    }

    @IBAction func onBtnForgotPassClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        if let nasMemberAccountId = validateNasMemberAccountId() {

            SVProgressHUD.show()
            FDNaSmileManager.sharedInstance.forgetPassword(nasMemberAccountId, success: { () in

                SVProgressHUD.dismiss()
                let msg = FDLocalized("Your naSmile password has been sent to your email")
                UIAlertView.showWithTitle("", message: msg, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) -> Void in
                }

                }, fail: { (reason) in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
            })

        }
    }

    func resetHighLight() {
        bgName.isHighlighted = false
        bgPass.isHighlighted = false
    }

    func validateNasMemberAccountId() -> String? {
        resetHighLight()
        if let nasMemberAccountId = tfEmail.text {
            if !nasMemberAccountId.isEmpty {
                return nasMemberAccountId
            }
        }
        bgName.isHighlighted = true
        return nil

    }

    func validatePass() -> String? {
        resetHighLight()
        if let pass = tfPassword.text {
            if !pass.isEmpty {
                return pass
            }
        }
        bgPass.isHighlighted = true
        return nil
        
    }

    func textFieldEditingDidEnd(_ sender:UITextField) {
        resetHighLight()
    }

}
