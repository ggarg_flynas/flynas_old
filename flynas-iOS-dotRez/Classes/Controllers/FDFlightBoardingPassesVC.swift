//
//  FDFlightBoardingPassesVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import PassKit

class FDFlightBoardingPassesVC: BaseVC, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FDBoardingPassCellDelegate, PKAddPassesViewControllerDelegate {

    var boardingPasses : FDBoardingPasses?
    var wciCheckin = false

    @IBOutlet var pageControl: SMPageControl!
    @IBOutlet var labelNoBoardingPass: UILabel!
    @IBOutlet var collection: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("flight_boarding_passes_page")
        collection.dataSource = self
        collection.delegate = self
        pageControl.pageIndicatorImage = UIImage(named: "icn-page-indicator-inactive")
        pageControl.currentPageIndicatorImage = UIImage(named: "icn-page-indicator-active")
        pageControl.isEnabled = false
       let pnr = FDRM.sharedInstance.latestWciPNR
        if !pnr.isEmpty {
            if wciCheckin {
                SVProgressHUD.show()
                FDWebCheckinManager.sharedInstance.getWciBoardingPasses(pnr, departureStation: nil, success: { (boardingPasses) -> Void in
                    SVProgressHUD.dismiss()
                    self.boardingPasses = boardingPasses
                    self.collection.reloadData()

                    self.updateUI ()
                    }) { (r) -> Void in
                        SVProgressHUD.dismiss()
                        self.updateUI ()
                        FDErrorHandle.apiCallErrorWithAler(r)
                }

            } else {

                SVProgressHUD.show()
                    FDWebCheckinManager.sharedInstance.getBoardingPasses(pnr, departureStation: nil, success: { (boardingPasses) -> Void in
                        SVProgressHUD.dismiss()
                        self.boardingPasses = boardingPasses
                        self.collection.reloadData()
                        self.updateUI ()
                        }) { (r) -> Void in
                            // dirty solution since first try will fail
                            FDWebCheckinManager.sharedInstance.getBoardingPasses(pnr, departureStation: nil, success: { (boardingPasses) -> Void in
                                SVProgressHUD.dismiss()
                                self.boardingPasses = boardingPasses
                                self.collection.reloadData()
                                self.updateUI ()
                                }) { (r) -> Void in
                                    SVProgressHUD.dismiss()
                                    self.updateUI ()
                                    FDErrorHandle.apiCallErrorWithAler(r)
                            }
                    }
            }
        }

        boardingPasses = FDWebCheckinManager.sharedInstance.getCachedBoardingPasses()

        self.updateUI ()
    }

    func updateUI () {
        guard let boardingPasses = boardingPasses else {return}

        pageControl.numberOfPages = boardingPasses.items.count
        pageControl.currentPage = 0


        if boardingPasses.items.count > 0 {
            labelNoBoardingPass.isHidden = true
            collection.isHidden = false
            pageControl.isHidden = boardingPasses.items.count < 2
        } else {
            labelNoBoardingPass.isHidden = false
            collection.isHidden = true
            pageControl.isHidden = true
        }
    }

  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        postNotification(ContainerVC.kBtnHome, userInfo: [:])
    }

    // MARK : - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let boardingPasses = boardingPasses else { return 0 }

        return boardingPasses.items.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FDBoardingPassCell", for: indexPath) as! FDBoardingPassCell

        guard let boardingPasses = boardingPasses else { return cell }

        cell.boardingPass = boardingPasses.items[indexPath.row]
        cell.delegate = self

        if indexPath.row < boardingPasses.serialNumbers.count {
            cell.serialNumber = boardingPasses.serialNumbers[indexPath.row]
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }


    func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
    }

    var pass : PKPass?
    dynamic func addPassToPassbook(_ passBookParameters:[AnyHashable: Any]) {

//        SVProgressHUD.show()
//        let requestURL = "https://www.passsource.com/pass/create.php?hashedSerialNumber=eNortjIysVIyNAt09dHX9zHz1853csrISzEpzQ4st7VVsgZcMIxyCPM,&download=true"
//        let manager = RKObjectManager.sharedManager()
//        let downloadRequest = manager.requestWithObject(nil, method: .GET, path: requestURL, parameters: nil)
//
//        let requestOperation = AFHTTPRequestOperation(request: downloadRequest)
//
//        requestOperation.setCompletionBlockWithSuccess({ (operation, result) -> Void in
//            SVProgressHUD.dismiss()
//            if let data = operation.responseData {
//                var error: NSError?
//                let pass = PKPass(data: data, error: &error)
//
//                let passLib = PKPassLibrary()
//
////                if passLib.containsPass(pass) {
////                    // update
////                    self.errorHandle(FDLocalized("Boarding pass has added."))
////                } else {
//                    let addPassVC = PKAddPassesViewController(pass: pass)
//                    //            addPassVC.delegate = self
//                    self.presentViewController(addPassVC, animated: true, completion: nil)
////                }
//            }
//            }) { (operation, ErrorType) -> Void in
//                SVProgressHUD.dismiss()
//                self.errorHandle(FDErrorHandle.API_GENERAL_ERROR)
//            }
//
//            manager.HTTPClient.enqueueHTTPRequestOperation(requestOperation)

//        if let path = NSBundle.mainBundle().pathForResource("flynas", ofType: "pkpass") {
//
//            if let boardingPassData = NSData(contentsOfFile: path) {
//
//                var error: NSError?
//                let pass = PKPass(data: boardingPassData, error: &error)
//
////                let passLib = PKPassLibrary()
//
////                if passLib.containsPass(pass) {
////                    // update
////                    self.errorHandle(FDLocalized("Boarding pass has added."))
////                } else {
//                    let addPassVC = PKAddPassesViewController(pass: pass)
//                    //                    {
//                    //            addPassVC.delegate = self
//                    self.presentViewController(addPassVC, animated: true, completion: nil)
//                    //                } else {
//                    //                    self.errorHandle(FDLocalized("Can't not retrive Boarding pass."))
//                    //                }
////                }
//            }
//
//        }


        pass = nil
        SVProgressHUD.show()
        FDWebCheckinManager.sharedInstance.getWciPassbookPassData(passBookParameters, success: { (boardingPassData) -> Void in

            SVProgressHUD.dismiss()

            var error: NSError?
            self.pass = PKPass(data: boardingPassData, error: &error)

            if let error = error {
                self.pass = nil
                self.errorHandle(error.localizedDescription)
            } else {
                if let pass = self.pass {
                    let passLib = PKPassLibrary()
                    if passLib.containsPass(pass) {
                        if let passURL = pass.passURL {
                            UIApplication.shared.openURL(passURL)
                        } else {
                            self.errorHandle(FDErrorHandle.API_GENERAL_ERROR)
                        }
                    } else {
                        let addPassVC = PKAddPassesViewController(pass: pass)

                        addPassVC.delegate = self
                        self.present(addPassVC, animated: true, completion: nil)
                    }
                }
            }

            }) { (r) -> Void in
                SVProgressHUD.dismiss()
                self.errorHandle(r)
        }
    }

    func addPassesViewControllerDidFinish(_ controller: PKAddPassesViewController) {
        self.dismiss(animated: true) { () -> Void in
            if let pass = self.pass {
                UIApplication.shared.openURL(pass.passURL!)
            }
        }
    }

    func testBoardingPass() {

        let passBookParameters = [
            "Passbook": [
                "PassbookRequest": [
                    "BarcodeMessage": "M1ALESHAIWI/MANAYIR   EGY9Z3A RUHDXBXY 0209 119C002D0005 147>1182  6118BXY 000000000000029999000L12193800                          ",
                    "Fields": [
                    [
                    "Key": "depart",
                    "Label": "Riyadh",
                    "Section": "primary",
                    "Value": "RUH"
                    ],
                    [
                    "Key": "arrive",
                    "Label": "Dubai",
                    "Section": "primary",
                    "Value": "DXB"
                    ],
                    [
                    "Key": "passenger",
                    "Label": "PASSENGER",
                    "Section": "secondary",
                    "Value": "MANAYIR ALESHAIWI"
                    ],
                    [
                    "Key": "seat",
                    "Label": "SEAT",
                    "Section": "secondary",
                    "Value": "2D"
                    ],
                    [
                    "Key": "boardingTime",
                    "Label": "DEPARTS",
                    "Section": "auxiliary",
                    "Value": "28 Apr, 19:00"
                    ],
                    [
                    "Key": "flightNumber",
                    "Label": "FLIGHT",
                    "Section": "auxiliary",
                    "Value": "XY 209"
                    ],
                    [
                    "Key": "seqNum",
                    "Label": "SEQ",
                    "Section": "auxiliary",
                    "Value": "5"
                    ]
                    ],
                    "SerialNumber": "MBGY9Z3AXY20905"
                ]
            ]
        ]

        pass = nil
        SVProgressHUD.show()
        FDWebCheckinManager.sharedInstance.getWciPassbookPassData(passBookParameters, success: { (boardingPassData) -> Void in

            SVProgressHUD.dismiss()

            var error: NSError?
            self.pass = PKPass(data: boardingPassData, error: &error)

            if let error = error {
                self.pass = nil
                self.errorHandle(error.localizedDescription)
            } else {
                if let pass = self.pass {

                    if let depart = pass.localizedValue(forFieldKey: "depart") {
                        print (depart)
                    }

//                    let passLib = PKPassLibrary()
//                    if passLib.containsPass(pass) {
//                        UIApplication.sharedApplication().openURL(pass.passURL)
//                    } else {
//                        let addPassVC = PKAddPassesViewController(pass: pass)
//
//                        addPassVC.delegate = self
//                        self.presentViewController(addPassVC, animated: true, completion: nil)
//                    }
                }
            }

        }) { (r) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(r)
        }
    }
}
