//
//  FDTripSummaryView.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 5/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDTripSummaryView: UIView,UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet var view: UIView!
  @IBOutlet var viewHandle: UIView!
  @IBOutlet var labelTotlePrice: UILabel!
  @IBOutlet var labelTotle: UILabel!
  @IBOutlet weak var lbPointsRedeemed: UILabel!
  @IBOutlet weak var lbViewSummary: UILabel!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightTable: NSLayoutConstraint!
  @IBOutlet var heightHandle: NSLayoutConstraint!
  @IBOutlet var imgHandle: UIImageView!
  
  enum ViewOpenState {
    case opened
    case middle
    case closed
  }
  
  enum TripSummaryTableViewCellItemTag : Int {
    case icon = 41
    case label1 = 42
    case label2 = 43
    case label3 = 44
    case label4 = 45
    case btn = 46
    case btnLabel = 47
  }
  
  
  enum TripSummaryTableViewCellIdentifier : String {
    case Header = "FDTripSummaryTableViewCellHeader"
    case Item = "FDTripSummaryTableViewCellItem"
    case Footer = "FDTripSummaryTableViewCellFooter"
  }
  
  struct Flight {
    var image = ""
    var title = ""
    var flightDate = ""
    var flightCity = ""
    var flightTime = ""
    var feeList: [FeeList] = []
    struct FeeList {
      var feeName = ""
      var feeValue = ""
      var hasTax = false
      var isTax = false
      var expanded = false
    }
  }
  
  static let heightNavbar: CGFloat = 74.0
  var showPriceChanged = false
  var openState = ViewOpenState.closed {
    didSet {
      if openState == .opened {
        imgHandle.image = UIImage(named: "icn-arrow-up-white")
      } else if openState == ViewOpenState.closed {
        imgHandle.image = UIImage(named: "icn-arrow-down-white")
      }
    }
  }
  var origPosPan : CGFloat = 0
  var lastPosPan : CGFloat = 0
  var origH : CGFloat = 0
  var minSize : CGFloat = 30
  var maxSize : CGFloat = 180 {
    didSet {
      maxSize = min(heightTable.constant + heightHandle.constant, UIScreen.main.bounds.height-FDTripSummaryView.heightNavbar)
    }
  }
  var opening = false
  var timer: Timer?
  var heightConstraint = NSLayoutConstraint()
  var priceSummayr : FDPriceSummary?
  var flights: [Flight] = []
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    Bundle.main.loadNibNamed("FDTripSummaryView", owner: self, options: nil)
    lbPointsRedeemed.text = ""
    labelTotle.text = FDLocalized("Total:")
    lbViewSummary.text = FDLocalized("View Summary")
    tableView.register( UINib(nibName: TripSummaryTableViewCellIdentifier.Item.rawValue, bundle: nil) , forCellReuseIdentifier: TripSummaryTableViewCellIdentifier.Item.rawValue)
    tableView.register(UINib(nibName: TripSummaryTableViewCellIdentifier.Header.rawValue, bundle: nil), forCellReuseIdentifier: TripSummaryTableViewCellIdentifier.Header.rawValue)
    tableView.register(UINib(nibName: TripSummaryTableViewCellIdentifier.Footer.rawValue, bundle: nil), forCellReuseIdentifier: TripSummaryTableViewCellIdentifier.Footer.rawValue)
    view.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(view)
    heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.frame.size.height)
    self.addConstraint(heightConstraint)
    heightConstraint.constant = minSize
    openState = .closed
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0))
    // Pan gesture recognizer for moving
    let p = UIPanGestureRecognizer(target: self, action: #selector(FDTripSummaryView.handlePan(_:)))
    p.minimumNumberOfTouches = 1
    p.maximumNumberOfTouches = 1
    viewHandle.addGestureRecognizer(p)
    let t = UITapGestureRecognizer(target: self, action: #selector(FDTripSummaryView.handleTap(_:)))
    viewHandle.addGestureRecognizer(t)
    tableView.delegate = self
    tableView.dataSource = self
    update(nil)
  }
  
  func endOpening() {
    openState = opening ? ViewOpenState.opened : ViewOpenState.closed
    timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(self.animateOpening), userInfo: nil, repeats: true)
  }
  
  func animateOpening() {
    if opening {
      if round(heightConstraint.constant) == maxSize { timer?.invalidate(); return }
      if round(heightConstraint.constant) <= maxSize {
        heightConstraint.constant += 1
      } else {
        heightConstraint.constant -= 1
      }
    } else {
      if round(heightConstraint.constant) == minSize { timer?.invalidate(); return }
      if round(heightConstraint.constant) >= minSize {
        heightConstraint.constant -= 1
      } else {
        heightConstraint.constant += 1
      }
    }
  }
  
  func handleTap(_ recognizer: UITapGestureRecognizer) {
    opening = openState == .closed
    endOpening()
  }
  
  func handlePan(_ recognizer: UIPanGestureRecognizer) {
    switch (recognizer.state) {
    case .began:
      origPosPan = recognizer.location(in: self).y
      origH = heightConstraint.constant
      opening = openState == .closed
      lastPosPan = origPosPan
      break
    case .changed:
      var newH = recognizer.location(in: self).y - origPosPan + origH
      let delta = recognizer.location(in: self).y - lastPosPan
      if delta > 0 {
        opening = true
      } else if delta < 0 {
        opening = false
      }
      lastPosPan = recognizer.location(in: self).y
      
      newH = fmax(newH, minSize)
      newH = fmin(newH, maxSize)
      
      heightConstraint.constant = newH
      if newH == minSize {
        openState = .closed
      } else if newH == maxSize {
        openState = .opened
      } else {
        openState = .middle
      }
      self.layoutIfNeeded()
      break
    case .ended:
      
      if openState == .middle {
        endOpening()
      }
      break
    default: break
    }
  }
  
  func update(_ userBooking : FDUserBookingInfo?) {
    priceSummayr = userBooking?.priceSummary
    SharedModel.shared.journeys = []
    flights.removeAll()
    if let priceSummayr = priceSummayr { // show in points
      if FDResourceManager.sharedInstance.getPayWithNaSmiles() && priceSummayr.smilePointsRedeemableTotal > 0 {
        lbPointsRedeemed.text = "+ \(priceSummayr.smilePointsRedeemableTotal) \(FDLocalized("points redeemed"))"
        labelTotlePrice.text = FDUtility.formatCurrencyWithCurrencySymbol(priceSummayr.remainingAmountTotal as NSNumber)
      } else { // show in cash
        labelTotlePrice.text = priceSummayr.currencyCode + " " + FDUtility.formatCurrency(priceSummayr.total)!
      }
      var originCityCode : String? = nil
      SharedModel.shared.journeys = priceSummayr.journeyCharges
      for journeyCharge in priceSummayr.journeyCharges {
        SharedModel.shared.numberOfStops = journeyCharge.numberOfStops.intValue
        var feeList: [Flight.FeeList] = []
        var discount : Float = 0
        for paxCharge in journeyCharge.paxCharges {
          var bundleStr = ""
          switch paxCharge.bundleCode {
          case BundleCode.LIGH.rawValue, BundleCode.LIT1.rawValue, BundleCode.LIT2.rawValue:
            bundleStr = FDLocalized("Light")
          case BundleCode.PLUS.rawValue, BundleCode.PLS1.rawValue, BundleCode.PLS2.rawValue:
            bundleStr = FDLocalized("Plus")
          case BundleCode.PRUM.rawValue, BundleCode.PRM1.rawValue, BundleCode.PRM2.rawValue:
            bundleStr = FDLocalized("Premium")
          default:
            break
          }
          var title = ""
          if isEn() {
            title = "\(FDLocalized(paxCharge.fareType)) \(bundleStr) x \(paxCharge.paxCount)"
          } else {
            title = "\(paxCharge.paxCount) x \(bundleStr) \(FDLocalized(paxCharge.fareType))"
          }
          let amount = paxCharge.totalAmount.doubleValue + paxCharge.bundlePrice.doubleValue
          let value = FDUtility.formatCurrencyWithCurrencySymbol(amount as NSNumber)
          let bundleItem = Flight.FeeList(feeName: title, feeValue: value!, hasTax: false, isTax: false, expanded: false)
          feeList.append(bundleItem)
          //          feeList.append(paxCharge.getNameValue())
          discount = discount + paxCharge.paxFareTotalDiscountAmount.floatValue
        }
        
        // discount
        if discount > 0 {
          let item = Flight.FeeList(feeName: FDLocalized("Discount"),
                                    feeValue: FDUtility.formatCurrencyWithCurrencySymbol(discount as NSNumber)!,
                                    hasTax: false,
                                    isTax: false,
                                    expanded: false)
          feeList.append(item)
        }
        
        // baggages
        let (baggageCount, baggagePrice, baggageTax) = getBaggage(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if baggageCount > 0.0 {
          let item = Flight.FeeList(feeName: "\(FDLocalizeInt(Int(baggageCount)))x \(FDLocalized("Baggage"))",
            feeValue: FDUtility.formatCurrencyWithCurrencySymbol(baggagePrice as NSNumber)!,
            hasTax: baggageTax > 0.0,
            isTax: false,
            expanded: false)
          feeList.append(item)
          if baggageTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(baggageTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        
        // meals
        let (mealCount, mealPrice, mealTax) = getMeal(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if mealCount > 0.0 {
          let item = Flight.FeeList(feeName: "\(FDLocalizeInt(Int(mealCount)))x \(FDLocalized("Meal"))",
            feeValue: FDUtility.formatCurrencyWithCurrencySymbol(mealPrice as NSNumber)!,
            hasTax: mealTax > 0.0,
            isTax: false,
            expanded: false)
          feeList.append(item)
          if mealTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(mealTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        
        // sports
        let (sportsCount, sportsPrice, sportsTax) = getSports(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if sportsCount > 0.0 {
          let item = Flight.FeeList(feeName: "\(FDLocalizeInt(Int(sportsCount)))x \(FDLocalized("Sports Equipment"))",
            feeValue: FDUtility.formatCurrencyWithCurrencySymbol(sportsPrice as NSNumber)!,
            hasTax: sportsTax > 0.0,
            isTax: false,
            expanded: false)
          feeList.append(item)
          if sportsTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(sportsTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        
        // lounge
        let (loungeCount, loungePrice, loungeTax) = getLounge(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if loungeCount > 0.0 {
          let item = Flight.FeeList(feeName: "\(FDLocalizeInt(Int(loungeCount)))x \(FDLocalized("Business Class Lounge"))",
            feeValue: FDUtility.formatCurrencyWithCurrencySymbol(loungePrice as NSNumber)!,
            hasTax: loungeTax > 0.0,
            isTax: false,
            expanded: false)
          feeList.append(item)
          if loungeTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(loungeTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        
        // seats
        let (seatCount, seatPrice, seatTax) = getSeats(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if seatCount > 0.0 {
          let item = Flight.FeeList(feeName: "\(FDLocalizeInt(Int(seatCount)))x \(FDLocalized("Seat"))",
            feeValue: FDUtility.formatCurrencyWithCurrencySymbol(seatPrice as NSNumber)!,
            hasTax: seatTax > 0.0,
            isTax: false,
            expanded: false)
          feeList.append(item)
          if seatTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(seatTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        
        // serviceCharges
        let (servicePrice, serviceTax) = getServiceCharges(unwrap(str: journeyCharge.origin), dest: unwrap(str: journeyCharge.destination))
        if servicePrice > 0.0 {
          let item = Flight.FeeList(feeName: FDLocalized("Taxes and Fees"),
                                    feeValue: FDUtility.formatCurrencyWithCurrencySymbol(servicePrice as NSNumber)!,
                                    hasTax: serviceTax > 0.0,
                                    isTax: false,
                                    expanded: false)
          feeList.append(item)
          if serviceTax > 0.0 {
            let item = Flight.FeeList(feeName: "   \(FDLocalized("KSA (5%) VAT"))",
              feeValue: FDUtility.formatCurrencyWithCurrencySymbol(serviceTax as NSNumber)!,
              hasTax: false,
              isTax: true,
              expanded: false)
            feeList.append(item)
          }
        }
        // smilePointsEarnedTotal
        if !FDResourceManager.sharedInstance.getPayWithNaSmiles() && priceSummayr.smilePointsEarnedTotal > 0 {
          let value = "\(FDLocalized("Total eligible earning points")): \(priceSummayr.smilePointsEarnedTotal)"
          let item = Flight.FeeList(feeName: "",
                                    feeValue: value,
                                    hasTax: false,
                                    isTax: false,
                                    expanded: false)
          feeList.append(item)
        }
        //        let taxFeeValue = FDUtility.formatCurrencyWithCurrencySymbol(Double(priceSummayr.taxAmount)! as NSNumber)
        //        let taxAmountItem = Flight.FeeList(feeName: FDLocalized("Taxes and Fees"),
        //                                           feeValue: taxFeeValue!,
        //                                           hasTax: false,
        //                                           isTax: false,
        //                                           expanded: false)
        //        feeList.append(taxAmountItem)
        var title = FDLocalized("Departing Flight")
        var image = "icn-flight"
        if originCityCode == nil {
          originCityCode = journeyCharge.origin
        } else if originCityCode! == journeyCharge.destination! {
          title = FDLocalized("Returning Flight")
          
          image = "icn-flight-return-s"
        }
        
        let flight = Flight(image: image,
                            title: title,
                            flightDate: journeyCharge.std!.toStringUTC(format:.custom("EEE, dd MMM yyyy")),
                            flightCity: journeyCharge.origin! + " -> " + journeyCharge.destination!,
                            flightTime: journeyCharge.std!.toStringUTC(format:.custom("HH:mm")) + " -> " + journeyCharge.sta!.toStringUTC(format:.custom("HH:mm")),
                            feeList: feeList)
        flights.append(flight)
      }
      
      
      if priceSummayr.bookingCharges.count > 0 {
        var feeList: [Flight.FeeList] = []
        for serviceCharge in priceSummayr.bookingCharges {
          let title = getFeeName(serviceCharge.code)
          let item = Flight.FeeList(feeName: title,
                                    feeValue: priceSummayr.currencyCode + " " + FDUtility.formatCurrency(serviceCharge.amount)!,
                                    hasTax: false,
                                    isTax: false,
                                    expanded: false)
          feeList.append(item)
        }
        let flight = Flight(image: "",
                            title: (FDLocalized("Other Fees")),
                            flightDate: "",
                            flightCity: "",
                            flightTime: "",
                            feeList: feeList)
        flights.append(flight)
      }
      if priceSummayr.hasUncommittedChanges {
        let feeList: [Flight.FeeList] = []
        let priceBefore = priceSummayr.currencyCode + " " + FDUtility.formatCurrency(priceSummayr.total.floatValue-priceSummayr.balanceDue.floatValue as NSNumber)!
        let priceAfter = priceSummayr.currencyCode + " " + FDUtility.formatCurrency(priceSummayr.total)!
        if showPriceChanged {
          let rowPriceBefore = Flight(image: "",
                                      title: (FDLocalized("Price before change")),
                                      flightDate: priceBefore,
                                      flightCity: "",
                                      flightTime: "",
                                      feeList: feeList)
          flights.append(rowPriceBefore)
          let rowPriceAfter = Flight(image: "",
                                     title: (FDLocalized("Price after change")),
                                     flightDate: priceAfter,
                                     flightCity: "",
                                     flightTime: "",
                                     feeList: feeList)
          flights.append(rowPriceAfter)
        }
      }
    } else {
      if let userBooking = userBooking {
        labelTotlePrice.text = "\(unwrap(str: userBooking.currencyCode)) \(unwrap(str: FDUtility.formatCurrency(0)))"
      } else {
        labelTotlePrice.text = "\(FDResourceManager.sharedInstance.selectedCurrencySymbol) \(FDUtility.formatCurrency(0)!)"
      }
    }
    tableView.reloadData()
    heightTable.constant = min(tableView.contentSize.height, UIScreen.main.bounds.height-FDTripSummaryView.heightNavbar-30)
    maxSize = min(heightTable.constant + heightHandle.constant, UIScreen.main.bounds.height-FDTripSummaryView.heightNavbar)
    view.layoutIfNeeded()
    if openState == .opened {
      let newH = heightConstraint.constant
      if newH == minSize {
        openState = .closed
      } else if newH == maxSize {
        openState = .opened
        imgHandle.image = UIImage(named: "icn-arrow-up-white")
      } else {
        openState = .middle
      }
      if openState == .middle {
        endOpening()
      }
    }
  }
  
  func getBaggage(_ orig: String, dest: String) -> (count: Double, price: Double, tax: Double) {
    var count: Double = 0.0
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (count, price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for ssr in jCharge.ssrCharges {
          if ssr.ssrType == "Baggage" && ssr.feeCode == "K7" {
            //            tax += ssr.count.doubleValue * ssr.amount.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue // temp
          } else if ssr.ssrType == "Baggage" {
            count += ssr.count.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue
          }
        }
      }
    }
    return (count, price, tax)
  }
  
  func getMeal(_ orig: String, dest: String) -> (count: Double, price: Double, tax: Double) {
    var count: Double = 0.0
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (count, price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for ssr in jCharge.ssrCharges {
          if ssr.ssrType == "Meals" && ssr.feeCode == "K7" {
            //            tax += ssr.count.doubleValue * ssr.amount.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue // temp
          } else if ssr.ssrType == "Meals" {
            count += ssr.count.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue
          }
        }
      }
    }
    return (count, price, tax)
  }
  
  func getSports(_ orig: String, dest: String) -> (count: Double, price: Double, tax: Double) {
    var count: Double = 0.0
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (count, price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for ssr in jCharge.ssrCharges {
          if ssr.ssrType == "SportEquipment" && ssr.feeCode == "K7" {
            //            tax += ssr.count.doubleValue * ssr.amount.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue // temp
          } else if ssr.ssrType == "SportEquipment" {
            count += ssr.count.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue
          }
        }
      }
    }
    return (count, price, tax)
  }
  
  func getLounge(_ orig: String, dest: String) -> (count: Double, price: Double, tax: Double) {
    var count: Double = 0.0
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (count, price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for ssr in jCharge.ssrCharges {
          if ssr.ssrType == "Lounge" && ssr.feeCode == "K7" {
            //            tax += ssr.count.doubleValue * ssr.amount.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue // temp
          } else if ssr.ssrType == "Lounge" {
            count += ssr.count.doubleValue
            price += ssr.count.doubleValue * ssr.amount.doubleValue
          }
        }
      }
    }
    return (count, price, tax)
  }
  
  func getSeats(_ orig: String, dest: String) -> (count: Double, price: Double, tax: Double) {
    var count: Double = 0.0
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (count, price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for seat in jCharge.seatCharges {
          if seat.feeCode == "K7" {
            //            tax += seat.count.doubleValue * seat.amount.doubleValue
            price += seat.count.doubleValue * seat.amount.doubleValue // temp
          } else {
            count += seat.count.doubleValue
            price += seat.count.doubleValue * seat.amount.doubleValue
          }
        }
      }
    }
    return (count, price, tax)
  }
  
  func getServiceCharges(_ orig: String, dest: String) -> (price: Double, tax: Double) {
    var price: Double = 0.0
    let tax: Double = 0.0
    guard let summary = priceSummayr else { return (price, tax) }
    for jCharge in summary.journeyCharges {
      if jCharge.origin == orig && jCharge.destination == dest {
        for sCharge in jCharge.serviceCharges {
          if sCharge.code == "K7" {
            //            tax = sCharge.amount!.doubleValue
            price += sCharge.amount!.doubleValue // temp
          } else {
            price += sCharge.amount!.doubleValue
          }
        }
      }
    }
    return (price, tax)
  }
  
  func getFeeName(_ code:String) -> String {
    return FDRM.sharedInstance.getFeeName(code)
  }
  
  // --- UITableViewDelegate/DataSource
  func numberOfSections(in tableView: UITableView) -> Int {
    return flights.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return flights[section].feeList.count + 1
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return nil
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.row == 0 && !flights[indexPath.section].image.isEmpty  {
      return 46
    } else {
      let flight = flights.indices.contains(indexPath.section) ? flights[indexPath.section] : Flight()
      let feeListItem = flight.feeList.indices.contains(indexPath.row-1) ? flight.feeList[indexPath.row-1] : Flight.FeeList()
      if feeListItem.isTax {
        return feeListItem.expanded ? 20 : 0
      } else {
        return 20
      }
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      if priceSummayr != nil {
        let section = indexPath.section
        if flights[section].image.isEmpty {
          let cell = tableView.dequeueReusableCell(withIdentifier: TripSummaryTableViewCellIdentifier.Footer.rawValue)
          let label1 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label1.rawValue) as! UILabel?
          let label2 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label2.rawValue) as! UILabel?
          label1?.text = flights[section].title
          label2?.text = flights[section].flightDate
          return cell!
        } else {
          let cell = tableView.dequeueReusableCell(withIdentifier: TripSummaryTableViewCellIdentifier.Header.rawValue)
          let icon = cell?.viewWithTag(TripSummaryTableViewCellItemTag.icon.rawValue) as! UIImageView?
          let label1 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label1.rawValue) as! UILabel?
          let label2 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label2.rawValue) as! UILabel?
          let label3 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label3.rawValue) as! UILabel?
          let label4 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label4.rawValue) as! UILabel?
          icon?.image = UIImage(named: flights[section].image)
          label1?.text = flights[section].title
          label2?.text = flights[section].flightDate
          label3?.text = flights[section].flightCity
          label4?.text = flights[section].flightTime
          return cell!
        }
      }
    } else {
      let cell = self.tableView.dequeueReusableCell(withIdentifier: TripSummaryTableViewCellIdentifier.Item.rawValue)
      let label1 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label1.rawValue) as! UILabel
      let label2 = cell?.viewWithTag(TripSummaryTableViewCellItemTag.label2.rawValue) as! UILabel
      let btn = cell?.viewWithTag(TripSummaryTableViewCellItemTag.btn.rawValue) as! UIButton
      let btnLabel = cell?.viewWithTag(TripSummaryTableViewCellItemTag.btnLabel.rawValue) as! UILabel
      btn.addTarget(self, action: #selector(expandTax), for: .touchUpInside)
      btnLabel.text = flights[indexPath.section].feeList[indexPath.row-1].expanded ? "-" : "+"
      label1.text = flights[indexPath.section].feeList[indexPath.row-1].feeName
      label2.text = flights[indexPath.section].feeList[(indexPath.row-1)].feeValue
      let vwStack = label1.superview!.superview as! UIStackView
      vwStack.subviews[1].isHidden = !flights[indexPath.section].feeList[(indexPath.row-1)].hasTax || flights[indexPath.section].feeList[(indexPath.row-1)].isTax
      return cell!
    }
    return UITableViewCell()
  }
  
  func expandTax(_ sender: UIButton) {
    let point = sender.convert(CGPoint.zero, to: tableView)
    let indexPath = tableView.indexPathForRow(at: point)
    flights[indexPath!.section].feeList[indexPath!.row-1].expanded = !flights[indexPath!.section].feeList[indexPath!.row-1].expanded
    flights[indexPath!.section].feeList[indexPath!.row].expanded = !flights[indexPath!.section].feeList[indexPath!.row].expanded
    tableView.reloadWithAnimation()
    delay(0.5) {
      self.heightTable.constant = min(self.tableView.contentSize.height, UIScreen.main.bounds.height-FDTripSummaryView.heightNavbar-30)
      self.maxSize = min(self.heightTable.constant + self.heightHandle.constant, UIScreen.main.bounds.height-FDTripSummaryView.heightNavbar)
      self.view.layoutIfNeeded()
      self.endOpening()
    }
  }
  
}
