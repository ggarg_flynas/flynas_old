//
//  TestViewController.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 14/12/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    @IBOutlet var pickerView: UIView!

    let paymentNSVC = PaymentNSVC(nibName:"PaymentNSVC", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        paymentNSVC.view.frame.size = pickerView.frame.size
        pickerView.addSubview(paymentNSVC.view)

    }


    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
