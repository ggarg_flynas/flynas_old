//
//  SidebarMenuTable2VC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 29/04/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class SidebarMenuTable2VC: UIViewController {
  
    @IBOutlet var tableView: UITableView!
  
    let MenuItemButtonTag = 30
    let VersionLabelTag = 31
    let vm = SidebarMenuTable2VM()

    override func viewDidLoad() {
      super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 36
        tableView.delegate = self
        tableView.dataSource = self
        updateMenuItemes()
    }

    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
        updateMenuItemes()
    }

    func updateMenuItemes() {
        vm.updateMenuItemes()
        self.tableView.reloadData()
    }
  
}

extension SidebarMenuTable2VC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.menuItems.count

    }

    func getMenuItemCell(_ item:SidebarMenuItem) -> UITableViewCell {

        var cell : UITableViewCell?
        switch item.getType() {
        case .nameLabel:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "NameLabel")
            break
        case .socialMedia:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "SocialMedia")
            break
        case .logout:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "Logout")
            break
        case .groupHeader:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "GroupHeader")
            break
        default:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "MenuButton")
        }

        if let cell = cell {
            if let btn = cell.viewWithTag(MenuItemButtonTag) as? FDButton {
                btn.setTitle(vm.btnTitle(item), for: UIControlState())
                if vm.btnUserInteractionEnabled(item) {
                    btn.isUserInteractionEnabled = true

                    btn.action = { [weak self] sender in
                        self?.menuItemClicked(item, sender: sender)
                    }
                } else {
                    btn.isUserInteractionEnabled = false
                }

                if item.getType() == .groupHeader {
                    if item == .flynas {
                        btn.setImage(UIImage(named: item.imageName()))
                    } else {
                        btn.setAutoFlipImage(UIImage(named: item.imageName()))
                    }
                }

                if item.getType() == .logout {
                    btn.isHidden = !vm.isMemberLogin
                }
            }

            if let versionLabel = cell.viewWithTag(VersionLabelTag) as? UILabel {
                versionLabel.text = vm.getVersionNumber()
            }

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        return getMenuItemCell(vm.menuItems[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

  func menuItemClicked(_ item:SidebarMenuItem, sender: UIButton) {
    self.revealToggle(sender)
    switch item {
    case .NameLabel:
      return postNotification("#selector(onBtnLogin)", userInfo: [:])
    case .MyProfile:
      return postNotification("#selector(onBtnMyProfile)", userInfo: [:])
    case .Register:
      return postNotification("#selector(onBtnRegister)", userInfo: [:])
    case .MyBooking:
      return postNotification(ContainerVC.kBtnManage, userInfo: [:])
    case .BookFlight:
      return postNotification(ContainerVC.kBtnBook, userInfo: [:])
    case .CheckIn:
      return postNotification(ContainerVC.kBtnCheckin, userInfo: [:])
    case .MyBoardingPasses:
      return postNotification(ContainerVC.kBtnPasses, userInfo: [:])
    case .ManageMyBooking, .PurchaseAdditionalBags, .PickYourFavoriteSeat, .ChooseYourFavoriteMeal:
      return postNotification(ContainerVC.kBtnManage, userInfo: [:])
    case .MyNotifications:
      return postNotification("#selector(onBtnMyNotifications)", userInfo: [:])
    case .BookAHotel:
      return postNotification("#selector(bookAHotel)", userInfo: [:])
    case .RentACar:
      return postNotification("#selector(rentACar)", userInfo: [:])
    case .AirportTransfer:
      return postNotification("#selector(airportTransfer)", userInfo: [:])
    case .Deals:
      return getFDHomeVC().comingSoon()
    case .FlightStatus:
      return postNotification("#selector(onBtnFlightStatusClick)", userInfo: [:])
    case .FlightSchedule:
      return postNotification("#selector(onBtnFlightSchedule)", userInfo: [:])
    case .Logout:
      return postNotification("#selector(onBtnLogout)", userInfo: [:])
    case .naSmilesMembershipCard:
      return postNotification("#selector(onBtnMyNaSmileCard)", userInfo: [:])
    case .AboutNaSmiles:
      return postNotification("#selector(naSmileAbout)", userInfo: [:])
    case .FAQs:
      return postNotification("#selector(faqs)", userInfo: [:])
    case .EarningSMILEPoints:
      return postNotification("#selector(naSmileEarningSMILEPoints)", userInfo: [:])
    case .TierBenefits:
      return postNotification("#selector(naSmileTierBenefits)", userInfo: [:])
    case .SmilePointCalculator:
      return postNotification("#selector(smilePointCalculator)", userInfo: [:])
    case .PartnersCalculator:
      return postNotification("#selector(partnersCalculator)", userInfo: [:])
    default:
      break
    }
    
  }
  
}

extension SidebarMenuTable2VC {
  
    func showURL(_ url:String) {
        if let url = URL(string: url) {
            UIApplication.shared.openURL(url)
        }
    }

    @IBAction func onBtnFb(_ sender: UIButton) {
        self.revealToggle(sender)
        showURL("https://www.facebook.com/flynas/")
    }

    @IBAction func onBtnTwitter(_ sender: UIButton) {
        self.revealToggle(sender)
        showURL("https://twitter.com/flynas/")
    }

    @IBAction func onBtnInstagram(_ sender: UIButton) {
        self.revealToggle(sender)
        showURL("https://www.instagram.com/flynas")
    }

    @IBAction func onBtnYoutube(_ sender: UIButton) {
        self.revealToggle(sender)
        showURL("https://www.youtube.com/user/flynasdotcom/")
    }

    @IBAction func onBtnLinkedin(_ sender: UIButton) {
        self.revealToggle(sender)
        showURL("https://www.linkedin.com/company/flynas/")
    }

}
