//
//  ContainerVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 17/8/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {
  
  @IBOutlet weak var stvTabbar: UIStackView!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var bottomTabbar: NSLayoutConstraint!
  @IBOutlet weak var imvCheckin: UIImageView!
  @IBOutlet weak var imvBook: UIImageView!
  @IBOutlet weak var imvHome: UIImageView!
  @IBOutlet weak var imvManage: UIImageView!
  @IBOutlet weak var imvPasses: UIImageView!
  
  static let staticBottomTabbar: CGFloat = -70.0
  static let kBtnCheckin = "#selector(onBtnCheckin)"
  static let kBtnBook = "#selector(onBtnBook)"
  static let kBtnHome = "#selector(onBtnHome)"
  static let kBtnManage = "#selector(onBtnManage)"
  static let kBtnPasses = "#selector(onBtnPasses)"
  static let kHideTabbar = "#selector(hideTabbar)"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addObservers()
    setColors(2)
  }
  
  func addObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnCheckin), name: NSNotification.Name(rawValue: ContainerVC.kBtnCheckin), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnBook), name: NSNotification.Name(rawValue: ContainerVC.kBtnBook), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnHome), name: NSNotification.Name(rawValue: ContainerVC.kBtnHome), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnManage), name: NSNotification.Name(rawValue: ContainerVC.kBtnManage), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(onBtnPasses), name: NSNotification.Name(rawValue: ContainerVC.kBtnPasses), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(hideTabbar), name: NSNotification.Name(rawValue: ContainerVC.kHideTabbar), object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  class func shouldHideTabbar(_ vc: UIViewController) {
    switch vc {
    case is FDMMBCheckinListVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is FDWebCheckinVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is FDSearchVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is FDHomePageVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is FDMMBBookingListVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is FDFlightBoardingPassesVC: // tabbar item
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : false])
    case is SettingsVC:
      return
    case is FDLoginVC:
      return
    case is ApplePaySplashVC:
      return
    case is FDSearchPaxVC:
      return
    default:
      postNotification(ContainerVC.kHideTabbar, userInfo: ["hide" : true])
    }
  }
  
  func hideTabbar(_ notification: Notification) {
    let hide = notification.userInfo!["hide"] as! Bool
    stvTabbar.isUserInteractionEnabled = !hide
    delay(1.0) {
      self.bottomTabbar.constant = hide ? ContainerVC.staticBottomTabbar : 0.0
      UIView.animate(withDuration: 0.2, animations: {
        self.view.layoutIfNeeded()
      }) 
    }
  }
  
  @IBAction func onBtnCheckin(_ sender: UIButton) {
    var vc: UIViewController?
    if FDResourceManager.sharedInstance.isSessionLogined {
      let story = UIStoryboard(name: "ManageMyBooking", bundle: nil)
      vc = story.instantiateViewController(withIdentifier: "FDMMBCheckinListNC")
    } else {
      let story = UIStoryboard(name: "WebCheckin", bundle: nil)
      vc = story.instantiateViewController(withIdentifier: "FDWebCheckinNC")
    }
    addSuby(vc)
    setColors(0)
    SharedModel.shared.isWCI = true
  }
  
  @IBAction func onBtnBook(_ sender: UIButton) {
    let vc = storyboard?.instantiateViewController(withIdentifier: "FDSearchNC")
    addSuby(vc)
    setColors(1)
    SharedModel.shared.isWCI = false
  }
  
  @IBAction func onBtnHome(_ sender: UIButton) {
    addSuby(nil)
    setColors(2)
  }
  
  @IBAction func onBtnManage(_ sender: UIButton) {
    let story = UIStoryboard(name: "ManageMyBooking", bundle: nil)
//    var vc: UIViewController?
//    if FDResourceManager.sharedInstance.isSessionLogined {
      let vc = story.instantiateViewController(withIdentifier: "FDMMBBookingListNC")
//    } else {
//      vc = story.instantiateViewControllerWithIdentifier("FDMMBSearchNC")
//    }
    addSuby(vc)
    setColors(3)
  }
  
  @IBAction func onBtnPasses(_ sender: UIButton) {
    let story = UIStoryboard(name: "WebCheckin", bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: "FDFlightBoardingPassesVC")
    addSuby(vc)
    setColors(4)
  }
  
  func addSuby(_ vc: UIViewController?) {
    let homeVC = containerView.subviews.first!
    for subview in containerView.subviews {
      if subview == homeVC { continue }
      subview.removeFromSuperview()
      subview.willMove(toSuperview: nil)
    }
    if let myVC = vc {
      addChildViewController(myVC)
      myVC.view.frame = containerView.frame
      containerView.addSubview(myVC.view)
      myVC.didMove(toParentViewController: self)
    }
  }
  
  func setColors(_ btnNo: Int) {
    imvCheckin.isHighlighted = false
    imvBook.isHighlighted = false
    imvHome.isHighlighted = false
    imvManage.isHighlighted = false
    imvPasses.isHighlighted = false
    switch btnNo {
    case 0:
      imvCheckin.isHighlighted = true
    case 1:
      imvBook.isHighlighted = true
    case 2:
      imvHome.isHighlighted = true
    case 3:
      imvManage.isHighlighted = true
    case 4:
      imvPasses.isHighlighted = true
    default:
      return
    }
  }
  
}
