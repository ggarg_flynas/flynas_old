//
//  FDConfirmationVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum ManagingBookingType {
  case none
  case cancelling
  case changing
}

class FDConfirmationVC: FDBookingCommonVC, UITableViewDelegate, UITableViewDataSource, FDPassengerExtraDetailDelegate,FDBookingCommonActionDelegate, FDSelectFlightVCDelegate, FDSelectDateVCDelegate {
  
  @IBOutlet var labelTitle: UILabel!
  @IBOutlet var labelPNR: UILabel!
  @IBOutlet var labelStatus: UILabel!
  @IBOutlet var labelMMBPNR: UILabel!
  @IBOutlet var labelMMBStatus: UILabel!
  @IBOutlet var labelMMBPrice: UILabel!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightTableView: NSLayoutConstraint!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var imgHomeAd: UIImageView!
  @IBOutlet var labelSadadBillNumber: UILabel!
  @IBOutlet var labelSadadBillCode: UILabel!
  @IBOutlet var labelSadadExpiryDate: UILabel!
  @IBOutlet weak var vwBookingPnr: UIView!
  @IBOutlet weak var vwBookingSadad: UIView!
  @IBOutlet var labelBalance: UILabel!
  @IBOutlet var btnBack: FDButton!
  @IBOutlet var btnShareRight: FDButton!
  @IBOutlet var btnShareLeft: FDButton!
  @IBOutlet var btnPurchase: UIButton!
  @IBOutlet var btnReset: FDButton!
  @IBOutlet var btnHome: FDButton!
  @IBOutlet var btnChangeFlight: FDButton!
  @IBOutlet var btnCancelFlight: FDButton!
  @IBOutlet var btnExtra: FDButton!
  @IBOutlet var btnSeat: FDButton!
  @IBOutlet weak var vwMmbButtons: UIView!
  @IBOutlet weak var vwBookingDetail: UIView!
  @IBOutlet weak var vwMmbPurchaseButton: UIView!
  @IBOutlet weak var vwAds: UIView!
  @IBOutlet weak var lbBookingDate: UILabel!
  @IBOutlet weak var lbEmail: UILabel!
  @IBOutlet weak var vwLightFarePolicy: UIView!
  
  static let HeightFDFlightInfoCell2: CGFloat = 142.0
  static let HeightPassengerCellTitle: CGFloat = 53.0 + 1.0
  static let HeightPassengerCellDetail: CGFloat = 286.0 + 1.0
  var manageBooking = false
  var showBackBtn = false
  static let TableCellExpandHeight = CGFloat(287)
  var currentExpandedCell : IndexPath?
  var managingBookingType = ManagingBookingType.none
  var bookingFlights: FDBookingFlights?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    vwLightFarePolicy.isHidden = true
    viewTripSummary?.isHidden = userBooking.bookingDetails.isGdsBooking && (SharedModel.shared.isMMB || SharedModel.shared.isWCI)
    btnBack.isHidden = !showBackBtn
    btnHome.isHidden = showBackBtn
    btnShareRight.isHidden = true
    btnShareLeft.isHidden = showBackBtn
    PassengerLoungeServices.getLounge { (loungeSsr) in
      self.userBooking.loungeSsrStruct = loungeSsr
      self.tableView.reloadData()
    }
    heightTableView.constant = getTableViewHeight()
    if manageBooking {
      log("mmb_page")
      userBooking.bookingFlow = BookingFlow.manageMyBooking
      labelTitle.text = FDLocalized("Manage my booking")
      vwBookingPnr.isHidden = true
      vwBookingSadad.isHidden = true
      vwAds.isHidden = true
      vwMmbButtons.isHidden = false
      btnReset.isHidden = !self.userBooking.priceSummary.hasUncommittedChanges
      btnShareRight.isHidden = self.userBooking.priceSummary.hasUncommittedChanges
      setShowPriceChanged(true)
      if userBooking.priceSummary.hasUncommittedChanges {
        vwMmbPurchaseButton.isHidden = false
      } else {
        vwMmbPurchaseButton.isHidden = true
      }
      updateUI()
      updateBookingDetail()
      view.layoutIfNeeded()
    } else {
      log("confirmation_page")
      userBooking.bookingFlow = BookingFlow.normalBooking
      labelTitle.text = FDLocalized("Confirmation")
      vwBookingPnr.isHidden = false
      vwBookingSadad.isHidden = false
      vwAds.isHidden = false
      loadAds()
      NotificationCenter.default.addObserver(self, selector: #selector(FDConfirmationVC.handleEnterForeground(_:)), name: NSNotification.Name(rawValue: "UIApplicationWillEnterForegroundNotification"), object: nil)
      vwMmbButtons.isHidden = true
      vwMmbPurchaseButton.isHidden = true
      setShowPriceChanged(false)
      updateBookingDetail()
      updateUI()
      viewTripSummary?.update(userBooking)
      FDEventTrackingSendEvent("Booking", action:"Confirmation", label: userBooking.bookingDetails.recordLocator, value: userBooking.paymentMethods.balanceDue)
      FDEventTrackingBooking("FlightBooking", pnr: userBooking.bookingDetails.recordLocator, price: userBooking.paymentMethods.balanceDue)
      view.layoutIfNeeded()
    }
  }
  
  func updateSadadPayment () {
    var show = false
    let bookingDetails = self.userBooking.bookingDetails
    let bookingPayments = self.userBooking.bookingPayments
    if !bookingDetails.recordLocator.isEmpty && bookingDetails.status == FDBookingDetails.StatusConfirmed {
      if bookingPayments != nil {
        if let payment = bookingPayments!.payments.last {
          if payment.authorizationStatus == FDPayment.StatusPending && payment.status == FDPayment.StatusPendingCustomerAction {
            show = true
            labelSadadBillNumber.text = payment.xReferenceNumber
            labelSadadBillCode.text = FDPayment.SADADBillerCode
            let expireDateTime = bookingDetails.bookingDate .dateByAddingHours(4)
            labelSadadExpiryDate.text = expireDateTime.toString(format:.custom("HH:mm dd MMM yyyy"))
            labelStatus.text = FDLocalized("PENDING")
          }
        }
      }
    }
    vwBookingSadad.isHidden = !show
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  @IBAction func onBtnAppleCalendar(_ sender: UIButton) {
    for legNo in 0..<userBooking.legNumber {
      let leg = userBooking.getLegInfo(legNo).0
      if let destName = leg?.destination, let depDate = leg?.utcDepartureDate, let arrDate = leg?.utcArrivalDate {
        let eventTitle = "\(FDLocalized("My flight with flynas to")) \(FDLocalized(destName))"
        CalendarHelper.addToCalendar(eventTitle, startDate: depDate, endDate: arrDate) { (success) in
          if success {
            showMessageOnly(message: FDLocalized("The flight event is added to your calendar successfully."))
            sender.superview!.isHidden = true
          } else {
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
          }
        }
      }
    }
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onConfirmationBtnHomeClicked(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
    delay(1.0) {
      RateMyApp.sharedInstance.trackEventUsage()
    }
  }
  
  func updateUI() {
    labelPNR.text = userBooking.bookingDetails.recordLocator
    labelStatus.text = FDLocalized(userBooking.bookingDetails.status)
    lbBookingDate.text = userBooking.bookingDetails.bookingDate.toStringUTC(format: .custom("EEE, dd MMM yyyy"))
    lbEmail.text = userBooking.contactInfo.emailAddress
    updateSadadPayment()
    labelMMBPNR.text = userBooking.bookingDetails.recordLocator
    labelMMBStatus.text = FDLocalized(userBooking.bookingDetails.status)
    let flights = userBooking.getFlights()
    var cancellationAllowed = false
    var changeAllowed = false
    var extraAllowed = false
    var seatAllowed = false
    for flight in flights {
      extraAllowed = true
      seatAllowed = true
      if flight.cancellationAllowed {
        cancellationAllowed = true
      }
      if flight.changeAllowed {
        changeAllowed = true
      }
    }
    if userBooking.priceSummary.hasLoyaltyPayment {
      cancellationAllowed = false
    }
    btnCancelFlight.isEnabled = cancellationAllowed
    btnChangeFlight.isEnabled = changeAllowed
    btnExtra.isEnabled = extraAllowed
    btnSeat.isEnabled = seatAllowed
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func numberOfSections(in tableView: UITableView) -> Int {
    return userBooking.legNumber
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FDFlightInfoCell2") as! FDFlightInfoCell2
    let (leg,direction) = userBooking.getLegInfo(section)
    if leg != nil {
      cell.leg = leg
      cell.labelFlightDirection.text = direction == .arrive ? FDLocalized("Return flight") : FDLocalized("Departing flight")
      if isEn() {
        cell.imvFlightDirection.rotate(direction == .arrive ? 180.0 : 0.0)
      } else {
        cell.imvFlightDirection.rotate(direction == .depart ? 180.0 : 0.0)
      }
    }
    cell.flight = userBooking.getFlightInfo(section).0
    return cell.contentView
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return FDConfirmationVC.HeightFDFlightInfoCell2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return userBooking.passengers.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if self.currentExpandedCell == indexPath {
      return FDConfirmationVC.HeightPassengerCellTitle + FDConfirmationVC.HeightPassengerCellDetail
    }
    return FDConfirmationVC.HeightPassengerCellTitle
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var passenger: FDPassenger?
    if userBooking.passengers.indices.contains(indexPath.row) {
      passenger = userBooking.passengers[indexPath.row]
    }
    let cell = tableView.dequeueReusableCell(withIdentifier: "FDConfirmationPassengerCell") as! FDConfirmationPassengerCell
    cell.labelPassengerFullName.text = unwrap(str: passenger?.fullName)
    let flightIndex = userBooking.getFlightIndexByLegIndex(indexPath.section)
    // bundle code
    var journey: FDBookingFlights.Journey?
    if let myBookingFlights = bookingFlights {
      if userBooking.legNumber > myBookingFlights.journeys.count {
        journey = myBookingFlights.journeys[indexPath.section / 2]
      } else {
        journey = myBookingFlights.journeys[indexPath.section]
      }
    }
    let bundleCode = unwrap(str: journey?.bundleCode)
    cell.lbBundleCode.superview!.isHidden = false
    switch bundleCode {
    case BundleCode.LIGH.rawValue, BundleCode.LIT1.rawValue, BundleCode.LIT2.rawValue:
      cell.lbBundleCode.text = FDLocalized("Light")
      if manageBooking { vwLightFarePolicy.isHidden = false }
    case BundleCode.PLUS.rawValue, BundleCode.PLS1.rawValue, BundleCode.PLS2.rawValue:
      cell.lbBundleCode.text = FDLocalized("Plus")
    case BundleCode.PRUM.rawValue, BundleCode.PRM1.rawValue, BundleCode.PRM2.rawValue:
      cell.lbBundleCode.text = FDLocalized("Premium")
    default:
      cell.lbBundleCode.superview!.isHidden = true
    }
    if userBooking.bookingFlow == .normalBooking {
      AdjustSDKMan.confirmTicket(isBusiness: cell.lbBundleCode.superview!.isHidden)
    }
    // included baggage
    cell.lbIncludedBaggage.superview!.isHidden = false
    var bundleBaggageStr = [FDLocalized("7kg cabin")]
    if let serviceInBundle = journey?.serviceInBundle as? [String] {
      if passenger?.paxType == "INFT" {
        bundleBaggageStr = [FDLocalized("10kg")]
      } else {
        for item in serviceInBundle {
          switch item {
          case "XBAG":
            bundleBaggageStr.append(FDLocalized("20kg"))
            if let index = bundleBaggageStr.index(of: "30kg") {
              bundleBaggageStr.remove(at: index)
            }
            break
          case "BBML":
            bundleBaggageStr.append(FDLocalized("30kg"))
          default:
            continue
          }
        }
      }
      let str = bundleBaggageStr.joined(separator: ", ")
      cell.lbIncludedBaggage.text = str
    } else {
      cell.lbIncludedBaggage.superview!.isHidden = true
    }
    // baggages
    let noBag = FDLocalized("No Extra Bag")
    cell.labelBaggage.text = noBag
    if let baggages = userBooking.getBaggageService(flightIndex, passengerNumber: indexPath.row)?.getSelectedBaggageItem() {
      var allBags: [FDPassengerService.FlightPartService.Service] = []
      for baggage in baggages { for _ in 0..<baggage.count { allBags.append(baggage) } }
      let joinedBags = allBags.map({FDResourceManager.sharedInstance.getServiceItemName($0.code, defaultName: "")}).joined(separator: ", ")
      cell.labelBaggage.text = baggages.count == 0 ? noBag : joinedBags
    }
    // sports
    let sports = userBooking.getSportsService(flightIndex, passengerNumber: indexPath.row)?.getSelectedSportsItems()
    let noEquipment = FDLocalized("No Sports Equipment")
    let joinedSports = sports?.map({FDResourceManager.sharedInstance.getServiceItemName($0.code, defaultName: "")}).joined(separator: ", ")
    cell.lblSports.text = sports?.count == 0 ? noEquipment : joinedSports
    // meals
    cell.labelMeal.adjustsFontSizeToFitWidth = true
    var isBusiness = false
    if userBooking.priceSummary.journeyCharges.indices.contains(indexPath.section) {
      let journeyCharge = userBooking.priceSummary.journeyCharges[indexPath.section]
      if journeyCharge.paxCharges.indices.contains(indexPath.row) {
        let paxCharge = journeyCharge.paxCharges[indexPath.row]
        isBusiness = paxCharge.fareType == "Business"
      }
    }
    cell.labelMeal.text = FDResourceManager.sharedInstance.getServiceItemName(userBooking.getMealService(indexPath.section, passengerNumber: indexPath.row)?.getSelectedMealItem()?.code, defaultName: isBusiness ? FDLocalized("Yes") : FDLocalized("No Meal"))
    // lounge
    var lounges: [String] = []
    for passengerService in userBooking.loungeSsrStruct.passengerServices {
      let leg = userBooking.getLegInfo(indexPath.section).0
      if unwrap(str: leg?.origin) == passengerService.origin {
        if passengerService.passengerLoungeServices.indices.contains(indexPath.row) {
          let passengerLoungeService = passengerService.passengerLoungeServices[indexPath.row]
          for service in passengerLoungeService.services {
            if service.code == "VIP" && service.count != "0" {
              lounges.append(passengerService.origin)
            }
          }
        }
      }
    }
//    let loungeServices = userBooking.loungeSsrStruct.passengerServices
//    let passengerService = services.indices.contains(indexPath.section) ? services[indexPath.section] : PassengerServiceStruct()
//    let passengerLoungeService = passengerService.passengerLoungeServices.indices.contains(indexPath.row) ? passengerService.passengerLoungeServices[indexPath.row] : PassengerLoungeServicesStruct()
//    let count = Int(unwrap(str: passengerLoungeService.services.first?.count))
    //      cell.lblLounge.text = count > 0 ? "\(FDLocalized("Airport")) \(flightIndex + 1) - \(unwrap(str: passengerService?.origin))" : noLounge
    //      let isBusiness = SharedModel.shared.journeys.indices.contains(flightIndex) ? SharedModel.shared.journeys[flightIndex].paxCharges[0].fareType == "Business" : false
    
//    var lounges: [String] = []
//    for service in loungeServices {
//      if unwrap(str: leg?.origin) == service.origin {
//        if service.passengerLoungeServices.indices.contains(indexPath.section) {
//          let passengerLoungeService = service.passengerLoungeServices[indexPath.section]
////          passengerLoungeService.
//        }
//        service.passengerLoungeServices.first
//        lounges.append(service.origin)
//      }
//    }
    cell.lblLounge.text = lounges.count > 0 ? lounges.joined(separator: ", ") : FDLocalized("NO")
    // seats
    var seatNumber = FDLocalized("--")
    if indexPath.section < self.userBooking.flightSeatMap.seatMaps.count {
      let sm = self.userBooking.flightSeatMap.seatMaps[indexPath.section]
      if indexPath.row < sm.passengerSeatInfos.count {
        let p = sm.passengerSeatInfos[indexPath.row]
        if p.selectedSeatNumber != nil {
          seatNumber = p.selectedSeatNumber!
        }
      }
    }
    cell.labelSeat.text = seatNumber
    cell.expanded = self.currentExpandedCell == indexPath
    cell.delegate = self
    return cell
  }
  
  // MARK: - FDPassengerExtraDetailDelegate
  func onBtnExpandClicked(_ cell: FDConfirmationPassengerCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      CATransaction.begin()
      tableView.beginUpdates()
      CATransaction.setCompletionBlock({ () -> Void in
        if self.currentExpandedCell != nil {
          let rect = cell.convert(cell.bounds, to:self.scrollView)
          let bottomRt = CGRect(x: rect.origin.x, y: rect.origin.y + rect.height + FDConfirmationVC.TableCellExpandHeight - 1, width: rect.width, height: 1)
          self.scrollView.scrollRectToVisible(bottomRt, animated: true)
        }
      })
      if currentExpandedCell == indexPath {
        currentExpandedCell = nil
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
      } else {
        var rows = [indexPath]
        if currentExpandedCell != nil {
          rows.append(currentExpandedCell!)
        }
        currentExpandedCell = indexPath
        tableView.reloadRows(at: rows, with: UITableViewRowAnimation.automatic)
      }
      heightTableView.constant = getTableViewHeight()
      tableView.endUpdates()
      CATransaction.commit()
    }
  }
  
  func getTableViewHeight() -> CGFloat {
    let numberOfSections = CGFloat(self.numberOfSections(in: tableView))
    let numberOfPassengers = CGFloat(userBooking.passengers.count)
    return (numberOfSections * FDConfirmationVC.HeightFDFlightInfoCell2) + numberOfSections * numberOfPassengers * FDConfirmationVC.HeightPassengerCellTitle + (currentExpandedCell != nil ? FDConfirmationVC.HeightPassengerCellDetail : 0)
  }
  
  // MARK: - MMB
  @IBAction func onBtnChangeExtraClicked(_ sender: AnyObject) {
    self.pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDExtraVC" ) { targetVC in
      let f = targetVC as! FDExtraVC
      f.actionDelegate = self
    }
  }
  
  @IBAction func onBtnChangeSeatClicked(_ sender: AnyObject) {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.seatMap(nil, success: { (flightSeatMap, errorMsg) -> Void in
      SVProgressHUD.dismiss()
      if let fm = flightSeatMap {
        self.userBooking.flightSeatMap = fm
        self.pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDSelSeatVC") { targetVC in
          let s = targetVC as! FDSelSeatVC
          s.seatMap = fm
          s.actionDelegate = self
        }
      }
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    }
  }
  
  @IBAction func onBtnPaymentClicked(_ sender: AnyObject) {
    
    if self.userBooking.priceSummary.balanceDue.floatValue > 0 {
      // have to pay due to cancellation fee bigger than flight price
      // go to payment but with a back to confirmation page tag
      self.pushViewController(FDBookingCommonVC.StoryBoard2, storyboardID: "FDPaymentVC") { targetVC in
        let vc = targetVC as! FDPaymentVC
        vc.actionDelegate = self
      }
    } else {
      
      var confirmMsg = FDLocalized("You are about to commit your changes. The changes could not be rolled back after you click \"Ok\" button. Would you still like to proceed?")
      
      if self.userBooking.priceSummary.balanceDue.floatValue < 0 {
        
        if self.userBooking.priceSummary.hasLoyaltyPayment {
          if !userBooking.hasJourney {
            let smilePoint = self.userBooking.priceSummary.currencyCode  + " " + FDUtility.formatCurrency(-self.userBooking.priceSummary.loyaltyAmount.floatValue as NSNumber)!
            let credit = self.userBooking.priceSummary.currencyCode  + " " + FDUtility.formatCurrency(-self.userBooking.priceSummary.creditShellAmount.floatValue as NSNumber)!
            
            confirmMsg = FDLocalized("You are about to commit your changes. Smile points of {0} will be credited on your nas account. The credit shell of {1} will be created on your profile. The changes could not be rolled back after you click \"Ok\" button. Would you still like to proceed?").replacingOccurrences(of: "{0}", with: smilePoint).replacingOccurrences(of: "{1}", with: credit)
            
          } else {
            var confirmMsg = FDLocalized("Booking with loyalty payment doesn’t allow this change.")
            
            UIAlertView.showWithTitle("", message: confirmMsg, cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitle: nil) { (alertView, buttonIndex) -> Void in
            }
            return
          }
          
        } else {
          let credit = self.userBooking.priceSummary.currencyCode  + " " + FDUtility.formatCurrency((-self.userBooking.priceSummary.balanceDue.floatValue) as NSNumber)!
          // confirm
          confirmMsg = FDLocalized("You are about to commit your changes. The credit shell of {0} will be created on your profile. The changes could not be rolled back after you click \"Ok\" button. Would you still like to proceed?").replacingOccurrences(of: "{0}", with: credit)
        }
      }
      
      UIAlertView.showWithTitle("", message: confirmMsg, cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) -> Void in
        if buttonIndex == 1 {
          // refund
          SVProgressHUD.show()
          
          func submitBooking () {
            FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, bookingFlights, contactInput, passengersInput, baggageSsr, mealSsr, sportSsr, flightSeatMap) -> Void in
              SVProgressHUD.dismiss()
              self.updateBookingDetail()
              }, fail: { (reason) -> Void in
                SVProgressHUD.dismiss()
                self.errorHandle(reason)
            })
          }
          if self.userBooking.priceSummary.balanceDue.floatValue < 0 {
            // PaymentMethods
            FDBookingManager.sharedInstance.getPaymentMethods({ (paymentMethods) -> Void in
              if self.userBooking.priceSummary.hasLoyaltyPayment && !self.userBooking.hasJourney {
                FDBookingManager.sharedInstance.makeRefundMixedPayment({ (paymentResult, errorMessage) -> Void in
                  submitBooking()
                  }, fail: { (reason) -> Void in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
                })
              } else {
                FDBookingManager.sharedInstance.makeCreditShellPayments({ (paymentResult, errorMessage) -> Void in
                  submitBooking()
                  }, fail: { (reason) -> Void in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
                })
              }
              }, fail: { (reason) -> Void in
                SVProgressHUD.dismiss()
                self.errorHandle(reason)
            })
          } else {
            submitBooking()
          }
        }
      }
    }
  }
  
  @IBAction func onBtnCancelFlightClicked(_ sender: AnyObject) {
    managingBookingType = ManagingBookingType.cancelling
    AdjustSDKMan.cancelFlight()
    popupSelectFlights(self.userBooking.bookingDetails.hasLoyaltyPayment)
  }
  
  func popupSelectFlights(_ forceSelectAll:Bool = false) {
    let storyboard = UIStoryboard(name: "ManageMyBooking", bundle: nil)
    let selFlightVC = storyboard.instantiateViewController(withIdentifier: "FDSelectFlightVC") as! FDSelectFlightVC
    selFlightVC.delegate = self
    selFlightVC.flights = userBooking.getFlights()
    selFlightVC.managingBookingType = managingBookingType
    selFlightVC.forceSelectAll = forceSelectAll
    self.presentPopupViewController(selFlightVC, animationType: MJPopupViewAnimationFade)
  }
  
  func doCancelFlight(_ flights: [Int]) {
    SVProgressHUD.show()
    // submit flight to cancel
    FDBookingManager.sharedInstance.cancelFlights(flights, success: {() -> Void in
      SVProgressHUD.dismiss()
      self.updateBookingDetail ()
      },fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
    })
  }
  
  @IBAction func onBtnChangeFlightClicked(_ sender: UIButton) {
    managingBookingType = ManagingBookingType.changing
    popupSelectFlights()
  }
  
  func doChangeFlight(_ flights: [Int]) {
    
    userBooking.managingFlights = flights
    self.pushViewController(FDBookingCommonVC.StoryBoard1, storyboardID: "FDSelectDateVC") { targetVC in
      let controller = targetVC as! FDSelectDateVC
      
      controller.delegate = self
      
      if flights.count == 1 {
        if flights[0] == 0 {
          controller.departureDate = self.userBooking.departureDate
        } else {
          controller.departureDate = self.userBooking.returnDate
        }
        
        controller.departOnly = true
      } else {
        controller.departureDate = self.userBooking.departureDate
        controller.returnDate = self.userBooking.returnDate
        
        controller.departOnly = false
      }
      
      controller.destinationOption = .depart
      controller.backAnimation = false
    }
  }
  
  // MARK: - FDBookingCommonActionDelegate
  func onBtnBackClicked() {
    //        updateBookingDetail ()
  }
  
  func onBtnContinueClicked() {
    //        updateBookingDetail ()
  }
  
  func updatePage() {
    updateBookingDetail ()
  }
  
  func updateBookingDetail() {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getBookingDetail(false, success: { (bookingDetails, bookingPayments, bookingFlights : FDBookingFlights?, contactInput : FDContactInput?, passengersInput : FDPassengersInput?, baggageSsr : FDBaggageSsr?,mealSsr : FDMealSsr?, sportSsr: FDSportsSsr?, flightSeatMap : FDFlightSeatMap?) -> Void in
      self.bookingFlights = bookingFlights
      if self.userBooking.bookingFlow == .manageMyBooking && !FDResourceManager.sharedInstance.isSessionLogined {
        var searchItems = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
        let searchItem = SearchItem(pnr: bookingDetails.recordLocator, lastname: unwrap(str: passengersInput?.passengers.first?.lastName), origin: unwrap(str: bookingFlights?.journeys.first?.origin), destination: unwrap(str: bookingFlights?.journeys.first?.destination), departDate: bookingFlights?.journeys.first?.departureDate ?? Date().dateByAddingDays(-1))
        if !searchItems.map({$0.pnr}).contains(searchItem.pnr) {
          searchItems.append(searchItem)
          ArchiveManager.saveArray(searchItems, key: SearchItem.kMMBSearchDefaults)
        }
      }
      SVProgressHUD.dismiss()
      self.userBooking.bookingDetails = bookingDetails
      self.userBooking.bookingPayments = bookingPayments
      self.userBooking.mealSsr = mealSsr
      self.userBooking.baggageSsr = baggageSsr
      self.userBooking.sportsSsr = sportSsr
      if let contactInput = contactInput {
        self.userBooking.contactInfo = contactInput.contactItemInfo
      }
      if let bookingFlights = bookingFlights {
        if bookingFlights.journeys.count > 0 {
          self.userBooking.flightOption = .oneWay
          let j = bookingFlights.journeys[0]
          self.userBooking.departureFlight = j.getFlight()
          self.userBooking.stationDepart = FDResourceManager.sharedInstance.getStation(j.origin)
          self.userBooking.stationArrive = FDResourceManager.sharedInstance.getStation(j.destination)
          self.userBooking.departureDate = j.departureDate
        } else {
          self.userBooking.departureFlight = nil
        }
        if bookingFlights.journeys.count > 1 {
          let j = bookingFlights.journeys[1]
          self.userBooking.returnFlight = j.getFlight()
          self.userBooking.returnDate = j.departureDate
          self.userBooking.stationDepart2 = FDResourceManager.sharedInstance.getStation(j.origin)
          self.userBooking.stationArrive2 = FDResourceManager.sharedInstance.getStation(j.destination)
          if self.userBooking.stationArrive?.code == self.userBooking.stationDepart2?.code {
            self.userBooking.flightOption = .return
          } else {
          }
        } else {
          self.userBooking.returnFlight = nil
        }
      }
      if let passengersInput = passengersInput {
        self.userBooking.passengers = passengersInput.passengers
      } else {
        self.userBooking.passengers.removeAll()
      }
      if let flightSeatMap = flightSeatMap {
        self.userBooking.flightSeatMap = flightSeatMap
      }
      self.tableView.reloadData()
      self.heightTableView.constant = self.getTableViewHeight()
      self.vwBookingDetail.isHidden = self.numberOfSections(in: self.tableView) == 0
      self.updateUI()
      self.updatePriceSummary() {
        let currencySymbol = self.userBooking.priceSummary.currencyCode
        if let priceString = FDUtility.formatCurrency(self.userBooking.priceSummary.balanceDue) {
          self.labelMMBPrice.text = currencySymbol + " " + priceString
          if self.userBooking.priceSummary.balanceDue.floatValue > 0 {
            self.btnPurchase.setTitle(FDLocalized("Purchase"), for: UIControlState())
          } else {
            self.btnPurchase.setTitle(FDLocalized("Confirm Changes"), for: UIControlState())
          }
        } else {
          self.btnPurchase.setTitle(FDLocalized("Confirm Changes"), for: UIControlState())
          self.labelMMBPrice.text = currencySymbol + " " + FDUtility.formatCurrency(0)!
        }
        self.btnReset.isHidden = !self.userBooking.priceSummary.hasUncommittedChanges
        if self.btnHome.isHidden {
          self.btnShareRight.isHidden = self.userBooking.priceSummary.hasUncommittedChanges
        }
        if self.userBooking.bookingFlow == .manageMyBooking && self.userBooking.priceSummary.hasUncommittedChanges {
          self.vwMmbPurchaseButton.isHidden = false
        } else {
          self.vwMmbPurchaseButton.isHidden = true
        }
        self.tableView.reloadData()
      }
      self.view.layoutIfNeeded()
      },fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
    })
  }
  
  // MARK: - FDSelectFlightVCDelegate
  func selectFlightCancel() {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    managingBookingType = .none
  }
  
  func selectedFlight(_ flights: [Int]) {
    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    if managingBookingType == .changing {
      doChangeFlight(flights)
    } else if managingBookingType == .cancelling {
      doCancelFlight(flights)
    }
  }
  
  func resetFlight() {
    FDBookingManager.sharedInstance.getBooking(userBooking.pnr, departureStation: nil, email: userBooking.userEmail, lastName: userBooking.userLastName, success: { () -> Void in
      SVProgressHUD.dismiss()
      self.updateUI()
      // retrieve booking info
      self.updateBookingDetail()
      self.view.layoutIfNeeded()
      }, fail:{ (reason) -> Void in
        SVProgressHUD.dismiss()
        if reason == FDErrorHandle.API_ERROR_5XX {
          self.errorHandle(FDLocalized("Unable to find the booking"))
        } else {
          self.errorHandle(reason)
        }
    })
  }
  
  //MARK: - FDSelectDateVCDelegate
  func didSelect(_ dateDepart: Date, dateReturn:Date?) {
    if userBooking.managingFlights.count == 1 {
      if userBooking.managingFlights[0] == 0 {
        userBooking.departureDate = dateDepart
      } else {
        userBooking.returnDate = dateDepart
      }
    } else {
      userBooking.departureDate = dateDepart
      userBooking.returnDate = dateReturn
    }
    self.pushViewController(FDBookingCommonVC.StoryBoard1, storyboardID: "FDSelFlightsVC" ) { targetVC in
      let f = targetVC as! FDSelFlightsVC
      f.actionDelegate = self
    }
  }
  
  @IBAction func onBtnResetChangesClicked(_ sender: UIButton) {
    resetFlight()
  }
  
  func loadAds () {
    FDRM.sharedInstance.getHomePageADImage({ (image) -> Void in
      self.imgHomeAd.image = image
    }) { (r) -> Void in
    }
  }
  
  func handleEnterForeground(_ sender:Notification) {
    loadAds()
  }
  
  @IBAction func onBtnShareClicked(_ sender: UIButton) {
    //Set the default sharing message.
    let message = "" //"PNR:\(userBooking.bookingDetails.recordLocator)"
    UIGraphicsBeginImageContextWithOptions(tableView.bounds.size, tableView.isOpaque, 0.0)
    tableView.drawHierarchy(in: tableView.bounds, afterScreenUpdates: false)
    if let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext() {
      UIGraphicsEndImageContext()
      //Set the link to share.
      let objectsToShare : [AnyObject] = [message as AnyObject,snapshotImageFromMyView]
      let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
      activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
      self.present(activityVC, animated: true, completion: nil)
    } else {
      UIGraphicsEndImageContext()
    }
  }
}
