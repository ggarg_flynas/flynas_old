//
//  FDWebCheckinVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWebCheckinVC: BaseVC/*, FDSelectCityVCDelegate*/ {
  
  @IBOutlet var btnNext: UIButton!
  //    @IBOutlet var tfPnr: UITextField!
  //    @IBOutlet var tfEmail: UITextField!
  //    @IBOutlet var tfLastName: UITextField!
  //    @IBOutlet var btnPortOfDeparture: UIButton!
  @IBOutlet weak var btnHome: FDButton!
  @IBOutlet weak var tfPNR: UITextField!
  @IBOutlet weak var tfLastname: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    SharedModel.shared.isMMB = false
    SharedModel.shared.isWCI = true
  }
  
  @IBAction func onBtnPNR(_ sender: UIButton) {
    tfPNR.becomeFirstResponder()
  }
  
  @IBAction func onBtnLastname(_ sender: UIButton) {
    tfLastname.becomeFirstResponder()
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    view.endEditing(true)
    dismiss(animated: true, completion: nil)
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    log("web_checkin_page")
    view.endEditing(true)
    if (tfPNR.text != nil && !tfPNR.text!.isEmpty) && (tfLastname.text != nil && !tfLastname.text!.isEmpty) {
      SVProgressHUD.show()
      FDWebCheckinManager.sharedInstance.getBooking(unwrap(str: tfPNR.text), departureStation: nil, email: "", lastName: unwrap(str: tfLastname?.text), success: { (wciBooking) -> Void in
        SVProgressHUD.dismiss()
        if wciBooking.journeys.count > 0 {
          self.pushViewController("WebCheckin", storyboardID: "FDWCiFlightListVC") { (vc) -> Void in
            //vc
            let c = vc as! FDWCiFlightListVC
            c.wciUserCheckin.wciBooking = wciBooking
            c.pnr = unwrap(str: self.tfPNR.text)
          }
        } else {
          self.errorHandle(FDLocalized("You can check-in online 48 hours before your departure. Online check-in closes 4 hours before departure."))
        }
        }, fail:{ (reason) -> Void in
          SVProgressHUD.dismiss()
          if reason == FDErrorHandle.API_ERROR_5XX {
            self.errorHandle(FDLocalized("You can check-in online 48 hours before your departure. Online check-in closes 4 hours before departure."))
          } else {
            self.errorHandle(reason)
          }
      })
      
    } else  {
      let errorMsg = FDLocalized("Please enter your booking number with your last name")
      UIAlertView.showWithTitle("", message: errorMsg , cancelButtonTitle: FDLocalized("BACK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
    }
  }
  
  //    func didSelect(stationDepart: FDResourceStation, stationArrive: FDResourceStation?) {
  //        self.stationDepart = stationDepart
  //        btnPortOfDeparture.setTitle(stationDepart.name, forState: .Normal)
  //    }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    view.endEditing(true)
    dismiss(animated: true, completion: nil)
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
}
