//
//  FDSelectCityVC.swift
//  flynas-dotRez
//
//  Created by River Huang on 14/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit

public protocol FDSelectCityVCDelegate : NSObjectProtocol {
  func didSelect(_ stationDepart: FDResourceStation, stationArrive:FDResourceStation?)
}

enum FDSelectCityVCOption {
  case departureAndArrive
  case onlyDeparture
}

class FDSelectCityVC: BaseVC,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
  
  @IBOutlet var btnDeparture: DLRadioButton!
  @IBOutlet var btnArrive: DLRadioButton!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var searchBar: CustomSearchBar!
  @IBOutlet var labelArrive: UILabel!
  @IBOutlet var heightStationTab: NSLayoutConstraint!
  
  var selectCityVCOption = FDSelectCityVCOption.departureAndArrive
  var destinationOption = DestinationOption.depart
  var stationDepart : FDResourceStation?
  var stationArrive : FDResourceStation?
  var isFiltered = false
  var filterStations = FDResourceManager.sharedInstance.stations
  var stations = FDResourceManager.sharedInstance.stations
  var latestDepart : FDResourceStation?
  var latestArrive : FDResourceStation?
  weak var delegate: FDSelectCityVCDelegate?
  
  var lastestStationDepart : FDResourceStation?
  var lastestStationArrive : FDResourceStation?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("select_airport_page")
    
    btnDeparture.flipButton()
    btnArrive.flipButton()
    
    btnDeparture.tag = DestinationOption.depart.rawValue
    btnArrive.tag = DestinationOption.arrive.rawValue
    
    tableView.delegate = self
    tableView.dataSource = self
    
    // set Search Bar Search icon
    CustomSearchBar.setAppearance()
    searchBar.setImage(UIImage(named: "icn-search-green"), for: UISearchBarIcon.search, state: UIControlState())
    searchBar.setSearchIcon("icn-search-green")
    searchBar.delegate = self
    
    lastestStationDepart = FDResourceManager.sharedInstance.getLatestDepartCity()
    lastestStationArrive = FDResourceManager.sharedInstance.getLatestArriveCity()
    
    if selectCityVCOption == .onlyDeparture {
      destinationOption = .depart
    }
    updateOption(false)
    
  }
  
  @IBAction func onBtnDestinationOptionClicked(_ sender: AnyObject) {
    let newOption = DestinationOption(rawValue:(sender as! UIButton).tag)!
    
    if destinationOption != newOption {
      destinationOption = newOption
      resetSearchBar()
      updateOption(true)
      updateTableView()
    }
  }
  
  func updateOption(_ animated: Bool) {
    if selectCityVCOption == .onlyDeparture {
      heightStationTab.priority = 900
    } else {
      heightStationTab.priority = 100
    }
    if stationDepart == nil {
      destinationOption = .depart
      btnArrive.isEnabled = false
      btnArrive.setTitle("", for: UIControlState())
    } else {
      btnArrive.isEnabled = true
      btnArrive.setTitle(stationArrive?.name, for: UIControlState())
    }
    (view.viewWithTag(destinationOption.rawValue) as? UIButton)?.isSelected = true
    
    btnDeparture.setTitle(stationDepart?.name, for: UIControlState())
    btnArrive.setTitle(stationArrive?.name, for: UIControlState())
    
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    
  }
  // --- UITableViewDelegate/DataSource
  func numberOfSections(in tableView: UITableView) -> Int {
    if isFiltered || (lastestStationDepart == nil && destinationOption == .depart) || (lastestStationArrive == nil && destinationOption == .arrive) {
      return 1
    } else {
      return 2
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if destinationOption == .depart {
      if isFiltered {
        return filterStations.count
      } else {
        if section == 0 && lastestStationDepart != nil  {
          return 1
        } else {
          return stations.count
        }
      }
    } else {
      if isFiltered {
        return filterStations.count
      } else {
        if section == 0 && lastestStationArrive != nil {
          return 1
        } else {
          return (stationDepart?.markets.count)!
        }
        
      }
    }
  }
  
  func getStationWithIndexPath(_ indexPath: IndexPath) -> FDResourceStation? {
    if destinationOption == .depart {
      if isFiltered {
        return filterStations[indexPath.row]
      } else {
        //return stations[indexPath.row]
        if indexPath.section == 0 && lastestStationDepart != nil {
          return lastestStationDepart
        } else {
          return stations[indexPath.row]
        }
        
      }
    } else {
      if isFiltered {
        return filterStations[indexPath.row]
      } else {
        if indexPath.section == 0 && lastestStationArrive != nil {
          return lastestStationArrive
        } else {
          return stationDepart?.markets[indexPath.row]
        }
      }
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let station = getStationWithIndexPath(indexPath)
    
    let cell:FDStationTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "StationCell") as! FDStationTableViewCell
    
    cell.labelName.text = station?.name
    cell.labelCode.text = station?.code
    cell.labelCountryName.text = FDRM.sharedInstance.countryNameByCode(station?.country)
    
    return cell
    
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "StationListHearder")
    
    let label = cell?.viewWithTag(40) as? UILabel
    
    if section == 0 && !isFiltered && ((lastestStationDepart != nil && destinationOption == .depart) || (lastestStationArrive != nil && destinationOption == .arrive)) {
      label?.text = FDLocalized("Recent search")
    } else {
      label?.text = FDLocalized("ALL")
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if destinationOption == .depart {
      stationDepart = getStationWithIndexPath(indexPath)
      if stationDepart != nil {
        
        if selectCityVCOption == .onlyDeparture {
          delegate?.didSelect(stationDepart!, stationArrive:nil)
          self.navigationController?.popViewController(animated: true)
        } else {
          searchBar.text = ""
          destinationOption = .arrive
          isFiltered = false
          stationArrive = nil
          
          updateOption(true)
          updateTableView()
        }
      }
    } else { // .Arrive
      stationArrive = getStationWithIndexPath(indexPath)
      if stationArrive != nil {
        updateOption(true)
        
        // back
        delegate?.didSelect(stationDepart!, stationArrive:stationArrive!)
        self.navigationController?.popViewController(animated: true)
      }
    }
  }
  
  //
  func updateTableView() {
    tableView.reloadData()
  }
  
  // MARK: - searchBar
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    if  searchText.lengthOfBytes(using: String.Encoding.utf8) > 0 {
      isFiltered = true
      let ss = searchText.lowercased()
      filterStations = stations.filter(){ ($0.name != nil && $0.name.lowercased().hasPrefix(ss)) || $0.code.lowercased().hasPrefix(ss)}
    } else {
      resetSearchBar()
    }
    
    updateTableView()
  }
  
  func resetSearchBar() {
    searchBar.text = ""
    searchBar.resignFirstResponder()
    view.endEditing(true)
    isFiltered = false
  }
  
}
