//
//  FDMMBSearchVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 4/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMMBSearchVC: BaseVC {
  
  @IBOutlet var tfPnr: UITextField!
  @IBOutlet var tfLastName: UITextField!
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onBtnPnr(_ sender: UIButton) {
    tfPnr.becomeFirstResponder()
  }
  
  @IBAction func onBtnLastname(_ sender: UIButton) {
    tfLastName.becomeFirstResponder()
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    log("search_booking_page")
    view.endEditing(true)
    if (tfPnr.text != nil && !tfPnr.text!.isEmpty) && (tfLastName.text != nil && !tfLastName.text!.isEmpty) {
      SVProgressHUD.show()
      FDBookingManager.sharedInstance.getBooking(unwrap(str: tfPnr?.text), departureStation: nil, email: "", lastName: tfLastName.text, success: { () -> Void in
        SVProgressHUD.dismiss()
        self.pushViewController("BookingFlow2", storyboardID: "FDConfirmationVC") { (vc) -> Void in
          let c = vc as! FDConfirmationVC
          c.manageBooking = true
          c.showBackBtn = true
          c.userBooking.pnr = unwrap(str: self.tfPnr?.text)
          c.userBooking.userEmail = ""
          c.userBooking.userLastName = self.tfLastName.text
          AdjustSDKMan.searchTicket()
        }
        }, fail:{ (reason) -> Void in
          SVProgressHUD.dismiss()
          if reason == FDErrorHandle.API_ERROR_5XX {
            self.errorHandle(FDLocalized("Unable to find the booking"))
          } else {
            self.errorHandle(reason)
          }
      })
    } else  {
      let errorMsg = FDLocalized("Please enter your booking number with your last name")
      UIAlertView.showWithTitle("", message: errorMsg , cancelButtonTitle: FDLocalized("BACK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
    }
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
}
