//
//  FDTextField.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 11/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDTextField: UITextField {
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if isRTL() {
            if self.textAlignment == .left {
                self.textAlignment = .right
            } else if self.textAlignment == .right {
                self.textAlignment = .left
            }
        }
    }

}
