//
//  FDNaSmileCardVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/05/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDNaSmileCardVC: BaseVC {
  var refresh : Bool = true
  
  
  @IBOutlet var userTitle: UILabel!
  @IBOutlet var name: UILabel!
  @IBOutlet var number: UILabel!
  @IBOutlet var tier: UILabel!
  @IBOutlet var expiryDate: UILabel!
  @IBOutlet var balance: UIButton!
  @IBOutlet var imgTier: UIImageView!
  @IBOutlet var imgBarcode: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("my_naSmile_car_page")
    updateUI ()
    
    if refresh {
      FDNaSmileManager.sharedInstance.autoLogin({ (memberLogin) in
        self.updateUI()
        }, fail: { (_) in
          // todo
      })
    }
    
    if !FDLoginManager.sharedInstance.naSmilesNumber.isEmpty {
      FDLoginManager.sharedInstance.retrieveMemberProfile(nil, success: { (memberProfile) in
        self.updateUI()
        }, fail: { (_) in
          //
      })
      
    }
  }
  
  func updateUI () {
    
    userTitle.isHidden = true
    
    if let memberLogin = FDNaSmileManager.sharedInstance.memberLogin {
      userTitle.text = memberLogin.title
      name.text = memberLogin.fullName
      tier.text = memberLogin.tier
      number.text = memberLogin.formatedNasLoyaltyNumber
      expiryDate.text = "\(FDLocalized("Expiry date")) \(memberLogin.formatedExpiryDate)"
      balance.setTitle(
        FDLocalizeInt(memberLogin.balancePoints),
        for: UIControlState())
      imgTier.image = UIImage(named: memberLogin.tierImage)
      
      //            imgBarcode.image =
      
      let code = memberLogin.getBarcodePayload()
      print (code)
      let data = code.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
      if let filter = CIFilter(name: "CIAztecCodeGenerator") {
        filter.setValue(data, forKey: "inputMessage")
        if let qrcodeImage = filter.outputImage {
          displayQRCodeImage(qrcodeImage, barcodeImageView: imgBarcode)
        }
      }
    }
  }
  
  func displayQRCodeImage(_ qrcodeImage:CIImage, barcodeImageView: UIImageView) {
    view.layoutIfNeeded()
    let w = min (barcodeImageView.frame.size.height, barcodeImageView.frame.size.width)
    
    let scale = w / qrcodeImage.extent.size.width
    let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scale, y: scale))
    
    barcodeImageView.image = UIImage(ciImage: transformedImage)
    view.layoutIfNeeded()
  }
  
  @IBAction func onBtnTierBenefits(_ sender: AnyObject) {
    FDUtility.naSmileTierBenefits(self)
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    //        self.navigationController?.popToRootViewControllerAnimated(true)
    dismiss(animated: true, completion: nil)
  }
}
