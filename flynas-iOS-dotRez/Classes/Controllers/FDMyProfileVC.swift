//
//  FDMyProfileVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 20/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDMyProfileVC: BaseVC, UITableViewDelegate, UITableViewDataSource,FDMyProfileTableCellDelegate {
  
  @IBOutlet var tfEmail: UITextField!
  @IBOutlet var tfCurrentPassword: UITextField!
  @IBOutlet var tfNewPassowrd: UITextField!
  @IBOutlet var tfMobile: UITextField!
  @IBOutlet var tfPhone: UITextField!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightTable: NSLayoutConstraint!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var btnMobileCountryCode: UIButton!
  @IBOutlet var btnPhoneCountryCode: UIButton!
  @IBOutlet var tfMemberNaSmiles: UITextField!
  @IBOutlet var hideRetrieveNaSmilesBtn: NSLayoutConstraint!
  
  let TableCellHeight = 58
  let TableCellDetailHeight = 308
  let HeightTableViewCellExpireRowHeight = 53
  var currentExpandedCell = 0
  var userEmail = ""
  var memberProfile = FDMemberProfile()
  var documentTypes = FDDocumentTypes()
  var currentPass = ""
  var newPass = ""
  var newPassenger = FDPassenger(paxType: .Adult, passengerNumber: 0)
  var alertVC = UIAlertController()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("my_profile_page")
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = CGFloat(TableCellHeight)
    tableView.delegate = self
    tableView.dataSource = self
    SVProgressHUD.show()
    // get document types
    FDBookingManager.sharedInstance.getDocumentTypes({ (documentTypes) -> Void in
      SVProgressHUD.dismiss()
      self.documentTypes = documentTypes
      self.tableView.reloadData()
      self.getMemberProfile()
    }, fail: { (reason) -> Void in
      SVProgressHUD.dismiss()
      _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
    })
    tfEmail.addTarget(self, action: #selector(FDMyProfileVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfCurrentPassword.addTarget(self, action: #selector(FDMyProfileVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfNewPassowrd.addTarget(self, action: #selector(FDMyProfileVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfMobile.addTarget(self, action: #selector(FDMyProfileVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfMobile.addTarget(self, action: #selector(FDMyProfileVC.textFieldDidChange(_:)), for: .editingChanged)
    tfPhone.addTarget(self, action: #selector(FDMyProfileVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfPhone.addTarget(self, action: #selector(FDMyProfileVC.textFieldDidChange(_:)), for: .editingChanged)
    tfMemberNaSmiles.addTarget(self, action: #selector(FDMyProfileVC.textFieldDidChange(_:)), for: .editingChanged)
  }
  
  func loginAgain (_ userEmail:String, password:String) {
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.loginUser(userEmail, password: password, forceLogin: false, success: { (memberLogin) -> Void in
      SVProgressHUD.dismiss()
      self.getMemberProfile ()
    }, fail: { (r) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(r)
    })
  }
  
  func getMemberProfile () {
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.clearAutoLoginTime()
    FDLoginManager.sharedInstance.retrieveMemberProfile(nil, success: { (memberProfile) -> Void in
      SVProgressHUD.dismiss()
      if let e = FDLoginManager.sharedInstance.userEmail {
        self.userEmail = e
      }
      //            self.memberLogin.copyFrom(FDLoginManager.sharedInstance.memberLogin)
      self.updateMemberProfileForManager()
    }) { (r) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(r)
      if FDLoginManager.sharedInstance.hasSavedUserCredential && !FDResourceManager.sharedInstance.isSessionLogined {
        FDLoginManager.sharedInstance.clearUserNamePass()
      }
    }
  }
  
  func updateMemberProfileForManager() {
    memberProfile.copyFrom(FDLoginManager.sharedInstance.memberProfile)
    updateTableViewHeight()
    updateUI()
    tableView.reloadData()
  }
  
  func textFieldDidChange(_ sender:UITextField) {
    sender.text = FDConvertArNumberToEn(sender.text)
  }
  
  func textFieldEditingDidEnd(_ sender:UITextField) {
    self.view.endEditing(true)
    switch (sender) {
    case tfEmail:
      self.userEmail = sender.text != nil ? sender.text! : ""
      break
    case tfCurrentPassword:
      currentPass = sender.text != nil ? sender.text! : ""
      break
    case tfNewPassowrd:
      newPass = sender.text != nil ? sender.text! : ""
      break
    case tfMobile:
      let moible = sender.text != nil ? sender.text! : ""
      memberProfile.profilePhones.setMobilePhone(moible)
      break
    case tfPhone:
      let phone = sender.text != nil ? sender.text! : ""
      memberProfile.profilePhones.setHomePhone(phone)
      break
    case tfMemberNaSmiles:
      //            let phone = sender.text != nil ? sender.text! : ""
      //            memberProfile.profilePhones.setHomePhone(phone)
      break
    default:break
    }
    updateUI()
  }
  
  func updateUI() {
    //        let pl = memberLogin
    //        let ca = memberProfile.profileAddresses
    let ph = memberProfile.profilePhones
    //        btnUserEmail.setTitle(pl.username, forState: .Normal)
    tfEmail.text = self.userEmail
    tfMobile.text = ph.mobile
    tfPhone.text = ph.homePhone
    tfMemberNaSmiles.text = memberProfile.profileDetails.details.naSmilesLoyalty.nasLoyaltyNumber
    btnMobileCountryCode.setTitle(FDResourceManager.sharedInstance.countryPhoneCodeByCode(memberProfile.profilePhones.mobileCode), for: UIControlState())
    btnPhoneCountryCode.setTitle(FDResourceManager.sharedInstance.countryPhoneCodeByCode(memberProfile.profilePhones.homePhoneCode), for: UIControlState())
    //        if ca.addresses.count > 0 {
    //            let add = ca.addresses[0]
    //            tfCity.text = add.city
    //            btnContactCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(add.countryCode), forState: .Normal)
    ////            btnContactAddress.setTitle(add.addressLine1, forState: .Normal)
    //            tfAddress.text = add.addressLine1
    //        }
    hideRetrieveNaSmilesBtn.priority = memberProfile.profileDetails.details.naSmilesLoyalty.nasLoyaltyNumber.isEmpty ? 100 : 900
  }
  
  func updateProfile(_ parameters:[AnyHashable: Any]?, success:@escaping () -> Void) {
    self.view.endEditing(true)
    // push object back
    FDLoginManager.sharedInstance.memberProfile.copyFrom(self.memberProfile)
    
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.retrieveMemberProfile(parameters, success: { (memberProfile) -> Void in
      
      SVProgressHUD.dismiss()
      UIAlertView.showWithTitle("", message:FDLocalized("Profile update successfully") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      
      success()
      //            self.memberLogin.copyFrom(FDLoginManager.sharedInstance.memberLogin)
      //            self.memberProfile.copyFrom(FDLoginManager.sharedInstance.memberProfile)
      self.updateUI()
      
    }) { (r) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(r)
    }
  }
  
  func updateFamily(_ parameters:[AnyHashable: Any]?, success:@escaping () -> Void) {
    self.view.endEditing(true)
    // push object back
    FDLoginManager.sharedInstance.memberProfile.copyFrom(self.memberProfile)
    
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.retrieveMemberFamily(parameters, success: { (memberProfile) -> Void in
      
      SVProgressHUD.dismiss()
      UIAlertView.showWithTitle("", message:FDLocalized("Profile update successfully") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      
      success()
      //            self.memberLogin.copyFrom(FDLoginManager.sharedInstance.memberLogin)
      //            self.memberProfile.copyFrom(FDLoginManager.sharedInstance.memberProfile)
      self.updateUI()
      
    }) { (r) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(r)
    }
  }
  
  
  @IBAction func onBtnUpdateRegisterDetailsClicked(_ sender: UIButton) {
    view.endEditing(true)
    let validator = SHEmailValidator()
    do {
      try validator.validateSyntax(ofEmailAddress: self.userEmail)
    } catch {
      FDErrorHandle.validateErrorHandle(.emailNotValidate)
      return
    }
    let changingPass = !currentPass.isEmpty || !newPass.isEmpty
    if changingPass {
      if currentPass.isEmpty || currentPass != FDLoginManager.sharedInstance.password {
        currentPass = ""
        tfCurrentPassword.text = ""
        FDErrorHandle.validateErrorHandle(.currentPasswordNotMatching)
        tfCurrentPassword.becomeFirstResponder()
        return
      }
      
      if newPass.isEmpty {
        UIAlertView.showWithTitle("", message: FDLocalized("Please input new password.") , cancelButtonTitle: FDLocalized("BACK") , completionHandler: { (alertView, buttonIndex) -> Void in
        })
        tfNewPassowrd.becomeFirstResponder()
        return
      }
    }
    
    let parameter = [
      "profileDetails": [
        "details": [
          "dateOfBirth": memberProfile.profileDetails.details.dateOfBirth != nil ? memberProfile.profileDetails.details.dateOfBirth!.toString(format: .custom("yyyy-MM-dd")) : "",
          "first": memberProfile.profileDetails.details.firstName,
          "gender": memberProfile.profileDetails.details.gender,
          "last": memberProfile.profileDetails.details.lastName,
          "middle": memberProfile.profileDetails.details.middleName,
          "nationality": memberProfile.profileDetails.details.nationality,
          "title": memberProfile.profileDetails.details.title,
          "password": changingPass ? newPass : "",
          
          "email" : self.userEmail
        ]
      ]
    ]
    
    updateProfile(parameter) { () -> Void in
      self.newPass = ""
      self.currentPass = ""
      self.tfCurrentPassword.text = ""
      self.tfNewPassowrd.text = ""
      self.updateMemberProfileForManager()
    }
  }
  
  @IBAction func onBtnUpdateContractDetailsClicked(_ sender: UIButton) {
    
    self.view.endEditing(true)
    
    var phones =  [[
      "number":memberProfile.profilePhones.mobileWithCountryCode,
      "typeCode":FDPhoneTypeCode.Mobile.rawValue
      ]
    ]
    
    if !memberProfile.profilePhones.homePhone.isEmpty {
      phones.append(
        [
          "number":memberProfile.profilePhones.homePhoneWithCountryCode.replacingOccurrences(of: "null ", with: ""),
          "typeCode":FDPhoneTypeCode.Home.rawValue
        ])
    }
    
    let parameter = [
      "profileAddresses":[
        "addresses":[
          [
            "addressLine1":memberProfile.profileAddresses.addresses[0].addressLine1,
            "addressLine2":"",
            "addressLine3":"",
            "city":memberProfile.profileAddresses.addresses[0].city,
            "countryCode":memberProfile.profileAddresses.addresses[0].countryCode,
            "postalCode":memberProfile.profileAddresses.addresses[0].postalCode,
            "provinceState":memberProfile.profileAddresses.addresses[0].provinceState
          ]
        ]
      ],
      "profilePhones":[
        "phones":phones
      ]
    ]
    
    updateProfile(parameter) {
      
    }
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    //        self.navigationController?.popToRootViewControllerAnimated(true)
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    if let myNavigation = navigationController {
      myNavigation.popViewController(animated: true)
    } else {
      dismiss(animated: true, completion: nil)
    }
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return memberProfile.memberFamily.members.count + 2
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell:FDMyProfileTableCell = self.tableView.dequeueReusableCell(withIdentifier: "FDMyProfileTableCell") as! FDMyProfileTableCell
    cell.documentTypes = documentTypes
    cell.tfFirstName.isEnabled = indexPath.row != 0
    cell.tfLastName.isEnabled = indexPath.row != 0
    cell.tfFirstName.alpha = indexPath.row == 0 ? 0.5 : 1.0
    cell.tfLastName.alpha = indexPath.row == 0 ? 0.5 : 1.0
    if indexPath.row == 0 {
      cell.passenger = memberProfile.profileDetails.details
      cell.newMember = false
      cell.canDelete = false
      cell.viewRelationship.isHidden = true
    } else if indexPath.row < memberProfile.memberFamily.members.count + 1 {
      cell.passenger = memberProfile.memberFamily.members[indexPath.row - 1]
      cell.newMember = false
      cell.canDelete = true
      cell.viewRelationship.isHidden = false
    } else {
      cell.passenger = newPassenger
      cell.newMember = true
      cell.canDelete = false
      cell.viewRelationship.isHidden = false
    }
    cell.expand = indexPath.row == currentExpandedCell
    cell.delegate = self
    return cell
  }
  
  func updateTableViewHeight() {
    var hideExpire = false
    if currentExpandedCell > -1 {
      if currentExpandedCell == 0 {
        hideExpire = memberProfile.profileDetails.details.travelDocument.hideExpire
      } else if currentExpandedCell < memberProfile.memberFamily.members.count + 1 {
        hideExpire = memberProfile.memberFamily.members[currentExpandedCell - 1].travelDocument.hideExpire
      } else {
        hideExpire = newPassenger.travelDocument.hideExpire
      }
    }
    heightTable.constant = CGFloat(tableView(tableView, numberOfRowsInSection: 0) * TableCellHeight) + CGFloat(currentExpandedCell >= 0 ? TableCellDetailHeight : 0)  - CGFloat(hideExpire ? HeightTableViewCellExpireRowHeight : 0)
    print(heightTable.constant)
    view.layoutIfNeeded()
  }
  
  // MARK: - FDMyProfileTableCellDelegate
  func updateTableCellHeight(_ cell: FDMyProfileTableCell) {
    if let i = tableView.indexPath(for: cell) {
      tableView.reloadRows(at: [i], with: UITableViewRowAnimation.automatic)
      self.updateTableViewHeight()
    }
  }
  
  func validateError(_ cell: FDMyProfileTableCell, errCode: ValidateErrorCode) {
    //
  }
  
  func pushHijiri(_ cell: FDMyProfileTableCell) {
    FDLoginManager.sharedInstance.memberProfile.copyHijiriFlagFrom(self.memberProfile)
  }
  
  func btnExpandClicked(_ cell:FDMyProfileTableCell) {
    if let i = tableView.indexPath(for: cell) {
      togglePassengerDetail(i)
    }
  }
  
  func togglePassengerDetail(_ indexPath: IndexPath) {
    if currentExpandedCell == indexPath.row {
      currentExpandedCell = -1
      updateTableViewHeight()
      tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
    } else {
      var rows = [indexPath]
      if currentExpandedCell != -1 {
        rows.append(IndexPath(row: currentExpandedCell, section: 0))
      }
      
      currentExpandedCell = indexPath.row
      tableView.reloadRows(at: rows, with: UITableViewRowAnimation.automatic)
      
      updateTableViewHeight()
      //            scrollToCurrentPassenger()
    }
    
    tableView.reloadData()
  }
  
  func btnUpdateClicked(_ cell: FDMyProfileTableCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      if indexPath.row == 0 {
        // update self
        let parameter = [
          "profileTravelDocument":[
            "documents":[
              [
                "birthCountry":memberProfile.profileDetails.details.travelDocument.birthCountry,
                "default":memberProfile.profileDetails.details.travelDocument.isDefault,
                "expirationDate":memberProfile.profileDetails.details.travelDocument.expirationDate != nil ? memberProfile.profileDetails.details.travelDocument.expirationDate!.toString(format: .custom("yyyy-MM-dd")) : "",
                "first":memberProfile.profileDetails.details.firstName,
                "issueDate":memberProfile.profileDetails.details.travelDocument.issueDate != nil ? memberProfile.profileDetails.details.travelDocument.issueDate!.toString(format: .custom("yyyy-MM-dd")) : "",
                "last":memberProfile.profileDetails.details.lastName,
                "middle":memberProfile.profileDetails.details.middleName,
                "nationality":memberProfile.profileDetails.details.nationality,
                "number":memberProfile.profileDetails.details.travelDocument.docNumber,
                "title":memberProfile.profileDetails.details.title,
                "typeCode":memberProfile.profileDetails.details.travelDocument.docTypeCode
              ]
            ]
          ],
          "profileDetails": [
            "details": [
              "dateOfBirth": memberProfile.profileDetails.details.dateOfBirth != nil ? memberProfile.profileDetails.details.dateOfBirth!.toString(format: .custom("yyyy-MM-dd")) : "",
              "first": memberProfile.profileDetails.details.firstName,
              "gender": memberProfile.profileDetails.details.gender,
              "last": memberProfile.profileDetails.details.lastName,
              "middle": memberProfile.profileDetails.details.middleName,
              "nationality": memberProfile.profileDetails.details.nationality,
              "title": memberProfile.profileDetails.details.title,
              "password": "",
              "email" : self.userEmail
            ]
          ]
        ]
        
        updateProfile(parameter) { () -> Void in
          self.updateMemberProfileForManager()
        }
        
      } else
        
        if (cell.newMember) {
          
          updateProfile(self.getMemberFamilyObject(self.newPassenger)) { () -> Void in
            //              self.updateFamily(self.getMemberFamilyObject(self.newPassenger)) { () -> Void in
            self.newPassenger = FDPassenger(paxType: .Adult, passengerNumber: 0)
            self.updateMemberProfileForManager()
          }
        } else {
          updateProfile(self.getMemberFamilyObject(nil)) { () -> Void in
            //                self.updateFamily(self.getMemberFamilyObject(nil)) { () -> Void in
            self.updateMemberProfileForManager()
          }
          //                }
      }
      
    }
  }
  
  func getMemberFamilyObject (_ newMember:FDPassenger?) -> [AnyHashable: Any]? {
    
    var memberList = [[AnyHashable: Any]]()
    var ml = memberProfile.memberFamily.members
    if newMember != nil {
      ml.append(newMember!)
    }
    
    for m in ml {
      let mo: [String : Any] = [
        "dateOfBirth": m.dateOfBirth != nil ? m.dateOfBirth!.toString(format: .custom("yyyy-MM-dd")) : "",
        "document":[
          "birthCountry":m.travelDocument.birthCountry,
          "expirationDate":m.travelDocument.expirationDate != nil ? m.travelDocument.expirationDate!.toString(format: .custom("yyyy-MM-dd")) : "",
          "issuingCountry":m.travelDocument.docIssuingCountry,
          "number":m.travelDocument.docNumber,
          "type":m.travelDocument.docTypeCode
        ],
        "firstName":m.firstName,
        "gender":m.gender,
        "lastName":m.lastName,
        "middleName":m.middleName,
        "nasmiles":m.nasmiles,
        "nationality":m.nationality,
        "relationship":m.relationship,
        "title":m.title
      ]
      
      memberList.append(mo as [AnyHashable: Any])
    }
    
    if memberList.isEmpty {
      return ["memberFamily":["members":"null"]]
    } else {
      let parameter = ["memberFamily":[
        "agentName":"",
        "members":memberList
        ]
      ]
      
      return parameter
    }
  }
  
  func btnDeleteClicked(_ cell: FDMyProfileTableCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      memberProfile.memberFamily.members.remove(at: indexPath.row - 1)
      updateProfile(getMemberFamilyObject(nil)) { () -> Void in
        self.updateMemberProfileForManager()
      }
    }
  }
  
  @IBAction func onMobileCountryCodeClicked(_ sender: UIView) {
    self.view.endEditing(true)
    
    FDUtility.selectCountryPhoneCode(FDLocalized("Mobile Country Code"), initValue: memberProfile.profilePhones.mobileCode, origin:sender) { (selectedValue) -> Void in
      self.memberProfile.profilePhones.setMobilePhoneCode(selectedValue)
      self.updateUI()
    }
  }
  
  @IBAction func onPhoneCountryCodeClicked(_ sender: UIView) {
    self.view.endEditing(true)
    
    FDUtility.selectCountryPhoneCode(FDLocalized("Phone Country Code"), initValue: memberProfile.profilePhones.homePhoneCode, origin:sender) { (selectedValue) -> Void in
      self.memberProfile.profilePhones.setHomePhoneCode(selectedValue)
      self.updateUI()
    }
  }
  
  @IBAction func onCountryClicked(_ sender: UIView) {
    self.view.endEditing(true)
    
    let ca = memberProfile.profileAddresses
    if ca.addresses.count > 0 {
      let add = ca.addresses[0]
      //            tfCity.text = add.city
      //            btnContactCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(add.countryCode), forState: .Normal)
      
      FDUtility.selectCountry(FDLocalized("Country"), initValue: add.countryCode, empty: true, origin:sender) { (selectedValue) -> Void in
        add.countryCode = selectedValue
        self.updateUI()
      }
    }
  }
  
  @IBAction func onBtnNaSmilesUpdateClicked(_ sender: AnyObject) {
    self.view.endEditing(true)
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.memberNaSmileNumberRetrieve({ (_) -> Void in
      SVProgressHUD.dismiss()
      UIAlertView.showWithTitle("", message:FDLocalized("Profile update successfully") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      self.getMemberProfile()
    }) { (r) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(r)
    }
  }
  
  @IBAction func onBtnJoinNasmiles(_ sender: UIButton) {
    view.endEditing(true)
    alertVC = UIAlertController(title: "", message: "Please select a password for your nasmiles account", preferredStyle: .alert)
    alertVC.view.tintColor = clrTurquoise
    alertVC.addTextField { (textField) in
      textField.placeholder = FDLocalized("Password")
      textField.isSecureTextEntry = true
    }
    alertVC.addTextField { (textField) in
      textField.placeholder = FDLocalized("Confirm password")
      textField.isSecureTextEntry = true
    }
    let cancelAction = UIAlertAction(title: FDLocalized("Cancel"), style: .default, handler: nil)
    alertVC.addAction(cancelAction)
    let submitAction = UIAlertAction(title: FDLocalized("Submit"), style: .default) { (action) in
      SVProgressHUD.show()
      FDLoginManager.sharedInstance.memberNaSmileNumberJoin(unwrap(str: self.alertVC.textFields?[0].text), success: { (message) in
        SVProgressHUD.dismiss()
        UIAlertView.showWithTitle("", message: message, cancelButtonTitle: FDLocalized("OK"))
        self.getMemberProfile()
      }) { (error) in
        SVProgressHUD.dismiss()
        UIAlertView.showWithTitle("", message: error, cancelButtonTitle: FDLocalized("OK"))
        self.getMemberProfile()
      }
    }
    submitAction.isEnabled = false
    alertVC.addAction(submitAction)
    alertVC.textFields![0].addTarget(self, action: #selector(checkPasswords), for: .editingChanged)
    alertVC.textFields![1].addTarget(self, action: #selector(checkPasswords), for: .editingChanged)
    present(alertVC, animated: true, completion: nil)
  }
  
  func checkPasswords() {
    let tfPassword1 = alertVC.textFields![0] as UITextField
    let tfPassword2 = alertVC.textFields![1] as UITextField
    alertVC.actions[1].isEnabled = unwrap(str: tfPassword1.text) == unwrap(str: tfPassword2.text) && (!unwrap(str: tfPassword1.text).isEmpty && !unwrap(str: tfPassword2.text).isEmpty)
  }
  
}
