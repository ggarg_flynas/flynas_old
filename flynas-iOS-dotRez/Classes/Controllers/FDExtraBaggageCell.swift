//
//  FDExtraBaggageCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 22/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

protocol FDExtraBaggageCellDelegate {
  func baggageErrorHandle(_ reason:String?)
  func itemChanged()
  func didExpandButtonPressed(_ key: String)
}

class FDExtraBaggageCell: UITableViewCell {
  
  @IBOutlet var lbPaxName: UILabel!
  @IBOutlet weak var imgArrow: UIImageView!
  @IBOutlet weak var btnExpand: UIButton!
  @IBOutlet weak var btnBaggage1Plus: UIButton!
  @IBOutlet weak var btnBaggage2Plus: UIButton!
  @IBOutlet weak var btnBaggage3Plus: UIButton!
  @IBOutlet weak var btnBaggage1Minus: UIButton!
  @IBOutlet weak var btnBaggage2Minus: UIButton!
  @IBOutlet weak var btnBaggage3Minus: UIButton!
  @IBOutlet weak var lblBaggage1Count: UILabel!
  @IBOutlet weak var lblBaggage2Count: UILabel!
  @IBOutlet weak var lblBaggage3Count: UILabel!
  @IBOutlet weak var lblBaggage1Cost: UILabel!
  @IBOutlet weak var lblBaggage2Cost: UILabel!
  @IBOutlet weak var lblBaggage3Cost: UILabel!
  @IBOutlet weak var lblBicycleTitle: UILabel!
  @IBOutlet weak var lblOtherTitle: UILabel!
  @IBOutlet weak var lblBicycleCost: UILabel!
  @IBOutlet weak var lblOtherCost: UILabel!
  @IBOutlet weak var btnBicycle: UIButton!
  @IBOutlet weak var btnOther: UIButton!
  
  var baggage1Code = ""
  var baggage2Code = ""
  var baggage3Code = ""
  static let sportsBikeCode = "BIKE"
  static let sportsOthersCode = "SPEQ"
  
  var key = ""
  var delegate : FDExtraBaggageCellDelegate?
  var pn = 0
  let clrActive = UIColor(hex: "E0396D", alpha: 1)
  let clrDeactive = UIColor(hex: "BEBEBE", alpha: 1)
  var baggageService : FDFlightPartService? {
    didSet {
      _ = checkBaggageCount(false)
      updateBaggage()
    }
  }
  var sportsService: FDFlightPartService? {
    didSet {
      updateSports()
    }
  }
  
  func shouldAllowChanges(_ flight: FDFlight) {
    let allow = flight.changeAllowed
    btnBaggage1Plus.isEnabled = allow
    btnBaggage1Minus.isEnabled = allow
    btnBaggage2Plus.isEnabled = allow
    btnBaggage2Minus.isEnabled = allow
    btnBaggage3Plus.isEnabled = allow
    btnBaggage3Minus.isEnabled = allow
    btnBicycle.isEnabled = allow
    btnOther.isEnabled = allow
  }
  
  @IBAction func onBtnExpand(_ sender: UIButton) {
    delegate?.didExpandButtonPressed(key)
  }
  
}

//MARK: - Baggage
extension FDExtraBaggageCell {
  
  func updateBaggage() {
    lblBaggage1Count.text = "0"
    lblBaggage2Count.text = "0"
    lblBaggage3Count.text = "0"
    guard let myBaggageService = baggageService else { return }
    for service in myBaggageService.services {
      switch service.code {
      case baggage1Code:
        if !myBaggageService.services.indices.contains(0) { continue }
        if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
          let points = "\(myBaggageService.services[0].smilePointsRedeemable) \(FDLocalized("points"))"
          let sar = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[0].remainingAmount as NSNumber)
          lblBaggage1Cost.text = "\(points) + \(unwrap(str: sar))"
        } else { // show in cash
          lblBaggage1Cost.text = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[0].price)
        }
        lblBaggage1Count.text = "\(myBaggageService.services[0].count)"
      case baggage2Code:
        if !myBaggageService.services.indices.contains(1) { continue }
        if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
          let points = "\(myBaggageService.services[1].smilePointsRedeemable) \(FDLocalized("points"))"
          let sar = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[1].remainingAmount as NSNumber)
          lblBaggage2Cost.text = "\(points) + \(unwrap(str: sar))"
        } else { // show in cash
          lblBaggage2Cost.text = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[1].price)
        }
        lblBaggage2Count.text = "\(myBaggageService.services[1].count)"
      case baggage3Code:
        if !myBaggageService.services.indices.contains(2) { continue }
        if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
          let points = "\(myBaggageService.services[2].smilePointsRedeemable) \(FDLocalized("points"))"
          let sar = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[2].remainingAmount as NSNumber)
          lblBaggage3Cost.text = "\(points) + \(unwrap(str: sar))"
        } else { // show in cash
          lblBaggage3Cost.text = FDUtility.formatCurrencyWithCurrencySymbol(myBaggageService.services[2].price)
        }
        lblBaggage3Count.text = "\(myBaggageService.services[2].count)"
      default:
        continue
      }
    }
    colourButtons()
  }
  
  func checkBaggageCount(_ increse: Bool) -> Bool {
    var baggage10 = FDPassengerService.FlightPartService.Service()
    var baggage15 = FDPassengerService.FlightPartService.Service()
    var baggage20 = FDPassengerService.FlightPartService.Service()
    guard let myBaggageService = baggageService else { return false }
    for i in 0..<myBaggageService.services.count {
      let service = myBaggageService.services[i]
      switch i {
      case 0:
        baggage1Code = service.code
        baggage10 = service
      case 1:
        baggage2Code = service.code
        baggage15 = service
      case 2:
        baggage3Code = service.code
        baggage20 = service
      default:
        continue
      }
    }
    let total = baggage10.count + baggage15.count + baggage20.count
    if increse {
      if total == 2 { return false }
    } else {
      if total == 0 { return false }
    }
    return true
  }
  
  func colourButtons() {
    btnBaggage1Plus.backgroundColor = clrDeactive
    btnBaggage2Plus.backgroundColor = clrDeactive
    btnBaggage3Plus.backgroundColor = clrDeactive
    btnBaggage1Minus.backgroundColor = clrDeactive
    btnBaggage2Minus.backgroundColor = clrDeactive
    btnBaggage3Minus.backgroundColor = clrDeactive
    var baggageService1 = FDPassengerService.FlightPartService.Service()
    var baggageService2 = FDPassengerService.FlightPartService.Service()
    var baggageService3 = FDPassengerService.FlightPartService.Service()
    guard let myBaggageService = baggageService else { return }
    for service in myBaggageService.services {
      switch service.code {
      case baggage1Code:
        baggageService1 = service
      case baggage2Code:
        baggageService2 = service
      case baggage3Code:
        baggageService3 = service
      default:
        continue
      }
    }
    let total = baggageService1.count + baggageService2.count + baggageService3.count
    btnBaggage1Plus.backgroundColor = baggageService1.count > 1 || total > 1 ? clrDeactive : clrActive
    btnBaggage2Plus.backgroundColor = baggageService2.count > 1 || total > 1 ? clrDeactive : clrActive
    btnBaggage3Plus.backgroundColor = baggageService3.count > 1 || total > 1 ? clrDeactive : clrActive
    btnBaggage1Minus.backgroundColor = baggageService1.count < 1 || total < 1 ? clrDeactive : clrActive
    btnBaggage2Minus.backgroundColor = baggageService2.count < 1 || total < 1 ? clrDeactive : clrActive
    btnBaggage3Minus.backgroundColor = baggageService3.count < 1 || total < 1 ? clrDeactive : clrActive
    btnBaggage1Plus.isEnabled = baggageService1.count > 1 || total > 1 ? false : true
    btnBaggage2Plus.isEnabled = baggageService2.count > 1 || total > 1 ? false : true
    btnBaggage3Plus.isEnabled = baggageService3.count > 1 || total > 1 ? false : true
    btnBaggage1Minus.isEnabled = baggageService1.count < 1 || total < 1 ? false : true
    btnBaggage2Minus.isEnabled = baggageService2.count < 1 || total < 1 ? false : true
    btnBaggage3Minus.isEnabled = baggageService3.count < 1 || total < 1 ? false : true
  }
  
  @IBAction func onBtnBaggage1Plus(_ sender: UIButton) {
    increaseBaggage(0)
  }
  
  @IBAction func onBtnBaggage2Plus(_ sender: UIButton) {
    increaseBaggage(1)
  }
  
  @IBAction func onBtnBaggage3Plus(_ sender: UIButton) {
    increaseBaggage(2)
  }
  
  @IBAction func onBtnBaggage1Minus(_ sender: UIButton) {
    decreaseBaggage(0)
  }
  
  @IBAction func onBtnBaggage2Minus(_ sender: UIButton) {
    decreaseBaggage(1)
  }
  
  @IBAction func onBtnBaggage3Minus(_ sender: UIButton) {
    decreaseBaggage(2)
  }
  
  func increaseBaggage(_ type: Int) {
    guard let myBaggageService = baggageService else { return }
//    guard myBaggageService.services.count == 3 else { return }
    guard checkBaggageCount(true) == true else { return }
    let baggage = myBaggageService.services[type]
    baggage.count += 1
    baggage.count = min(2, baggage.count)
    itemChanged(baggage, delete:false)
  }
  
  func decreaseBaggage(_ type: Int) {
    guard let myBaggage = baggageService else { return }
//    guard myBaggage.services.count == 3 else { return }
    guard checkBaggageCount(false) == true else { return }
    let baggage = myBaggage.services[type]
    baggage.count -= 1
    baggage.count = max(0, baggage.count)
    itemChanged(baggage, delete:true)
  }
  
  func itemChanged(_ b:FDServiceItem, delete:Bool) {
    if baggageService != nil {
      let parameter = [
        "origin":baggageService!.origin,
        "destination":baggageService!.destination,
        "pn":pn,
        "code":b.code
        ] as [String : Any]
      SVProgressHUD.show()
      FDBookingManager.sharedInstance.getBaggage(parameter, delete: delete, success: { (baggage) -> Void in
        SVProgressHUD.dismiss()
        self.delegate?.itemChanged()
      }) { (reason) -> Void in
        SVProgressHUD.dismiss()
        self.delegate?.baggageErrorHandle(reason)
      }
    }
    updateBaggage()
  }
  
}

//MARK: - Sports equipment
extension FDExtraBaggageCell {
  
  @IBAction func onBtnBicycle(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    self.sportsBicycle(sender.isSelected)
  }
  
  @IBAction func onBtnOther(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    self.sportsOther(sender.isSelected)
  }
  
  func sportsBicycle(_ enable: Bool) {
    SVProgressHUD.show()
    sportsItemChange(FDExtraBaggageCell.sportsBikeCode, delete: !enable) { (success) in
      let bicycle = self.unwrapServices(self.sportsService).bikeService
      bicycle.count = success ? (enable ? 1 : 0) : (!enable ? 1 : 0)
      self.updateSports()
    }
  }
  
  func sportsOther(_ enable: Bool) {
    SVProgressHUD.show()
    sportsItemChange(FDExtraBaggageCell.sportsOthersCode, delete: !enable) { (success) in
      let other = self.unwrapServices(self.sportsService).otherService
      other.count = success ? (enable ? 1 : 0) : (!enable ? 1 : 0)
      self.updateSports()
    }
  }
  
  func sportsItemChange(_ code: String, delete: Bool, success: @escaping (Bool) -> Void) {
    let parameters: [String: Any] = [
      "origin":      unwrap(str: baggageService?.origin),
      "destination": unwrap(str: baggageService?.destination),
      "pn":          pn,
      "code":        code
    ]
    FDBookingManager.sharedInstance.getSportsEquipment(parameters as [String : AnyObject], delete: delete, success: { (sports) in
      self.delegate?.itemChanged()
      success(true)
    }) { (reason) in
      self.delegate?.baggageErrorHandle(reason)
      success(false)
    }
  }
  
  func updateSports() {
    SVProgressHUD.dismiss()
    let (bikeService, otherService) = unwrapServices(sportsService)
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() {
      let pointsBike = bikeService.smilePointsRedeemable
      let sarBike = FDUtility.formatCurrencyWithCurrencySymbol(bikeService.remainingAmount as NSNumber)
      lblBicycleCost.text = "\(pointsBike) \(FDLocalized("points")) + \(unwrap(str: sarBike))"
      let pointsOther = otherService.smilePointsRedeemable
      let sarOther = FDUtility.formatCurrencyWithCurrencySymbol(otherService.remainingAmount as NSNumber)
      lblOtherCost.text = "\(pointsOther) \(FDLocalized("points")) + \(unwrap(str: sarOther))"
    } else {
      lblBicycleCost.text = FDUtility.formatCurrencyWithCurrencySymbol(bikeService.price)
      lblOtherCost.text = FDUtility.formatCurrencyWithCurrencySymbol(otherService.price)
    }
    btnBicycle.isSelected = bikeService.count > 0
    btnOther.isSelected = otherService.count > 0
    lblBicycleCost.superview?.isUserInteractionEnabled = !btnOther.isSelected && Int(bikeService.price) > 0
    lblBicycleCost.superview?.alpha = !btnOther.isSelected && Int(bikeService.price) > 0 ? 1.0 : 0.5
    lblOtherCost.superview?.isUserInteractionEnabled = !btnBicycle.isSelected && Int(otherService.price) > 0
    lblOtherCost.superview?.alpha = !btnBicycle.isSelected && Int(otherService.price) > 0 ? 1.0 : 0.5
  }
  
  func unwrapServices(_ sportsService: FDFlightPartService?) -> (bikeService: FDPassengerService.FlightPartService.Service, otherService: FDPassengerService.FlightPartService.Service) {
    var bikeService = FDFlightPartService.Service()
    var otherService = FDFlightPartService.Service()
    guard let mySportsService = sportsService else { return (bikeService, otherService) }
    for service in mySportsService.services {
      switch service.code {
      case FDExtraBaggageCell.sportsBikeCode:
        bikeService = service
      case FDExtraBaggageCell.sportsOthersCode:
        otherService = service
      default:
        continue
      }
    }
    return (bikeService, otherService)
  }
  
  @IBAction func onBtnSportsInfo(_ sender: UIButton) {
    let message = FDLocalized("Items included in other equipments are: Golf, Diving/Scuba Equipment, Fishing Equipment. Only one other equipment item allowed per passenger.")
    FDUtility.popInfo(FDLocalized("Sports Equipment"), message: message)
  }
  
}
