//
//  NSObject+FDExtension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 27/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import Foundation

public let IOS_LANGUAGE_CODE_AR = "ar"
public let IOS_LANGUAGE_CODE_EN = "en-US"

var isRtl :Bool?

extension NSObject {

    public func LANGUAGE_CODE_AR() -> String {
        return IOS_LANGUAGE_CODE_AR
    }

    public func LANGUAGE_CODE_EN() -> String {
        return IOS_LANGUAGE_CODE_EN
    }

    public func isRTL() -> Bool {

        if let l = isRtl {
            return l
        }

        if let appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as? NSArray {
            if let fl = appleLanguages.firstObject as? String {
                isRtl = fl.substring(to: fl.characters.index(fl.startIndex, offsetBy: 2)) == IOS_LANGUAGE_CODE_AR

                return isRtl!
            }
        }

        isRtl = false
        return isRtl!
    }

    public func isAr() -> Bool {
        return isRTL()
    }

    public func isEn() -> Bool {
        return !isRTL()
    }

    func flipImage(_ image:UIImage?) -> UIImage? {
        if let i = image {
            //            if #available(iOS 9.0, *) {
            //                i.imageFlippedForRightToLeftLayoutDirection()
            //                return i
            //            } else {
            //Fallback on earlier versions
            if let c = i.cgImage {
                return UIImage(cgImage: c, scale: i.scale, orientation: .upMirrored)
            } else {
                return i
            }
            //            }
        }
        
        return image
    }

    func SYSTEM_VERSION_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
            options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }

    func SYSTEM_VERSION_GREATER_THAN(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
            options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }

    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
            options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }

    func SYSTEM_VERSION_LESS_THAN(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
            options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }

    func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
            options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }
}
