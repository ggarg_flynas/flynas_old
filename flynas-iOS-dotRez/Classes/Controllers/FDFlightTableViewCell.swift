//
//  FDFlightTableViewCell.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 29/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDFlightTableViewCellDelegate : class {
  func selectFlightFare(_ selectedFlight: FDFlight, selectedFare: FDFare, expand: Bool)
  func btnExpandClicked(_ cell:FDFlightTableViewCell)
  func btnStopInfoClicked(_ cell:FDFlightTableViewCell)
  func btnFareInfoClicked(_ cell:FDFlightTableViewCell, priceOption:PriceOption)
}

class FDFlightTableViewCell: UITableViewCell {
  
  @IBOutlet weak var lbFlightType: UILabel!
  @IBOutlet weak var lbFlightDuration: UILabel!
  @IBOutlet var price1: FDFlightPriceView!
  @IBOutlet var price3: FDFlightPriceView!
  @IBOutlet weak var imvAirplane: UIImageView!
  @IBOutlet weak var leadingAirplaneImv: NSLayoutConstraint!
  @IBOutlet weak var trailingAirplaneImv: NSLayoutConstraint!
  @IBOutlet weak var lbFlightNumber: UILabel!
  @IBOutlet weak var lbDepartureTime: UILabel!
  @IBOutlet weak var lbArrivalTime: UILabel!
  @IBOutlet weak var lbDepartureCity: UILabel!
  @IBOutlet weak var lbArrivalCity: UILabel!
  @IBOutlet weak var lbCurrency: UILabel!
  @IBOutlet weak var lbPrice: UILabel!
  @IBOutlet weak var imvExpand: UIImageView!
  @IBOutlet weak var imvAirline: UIImageView!
  @IBOutlet weak var btnInfo: UIButton!
  @IBOutlet weak var imvLightSelect: UIImageView!
  @IBOutlet weak var imvPlusSelect: UIImageView!
  @IBOutlet weak var imvFlexSelect: UIImageView!
  @IBOutlet weak var lbBundle1Price1: UILabel!
  @IBOutlet weak var lbBundle1Price2: UILabel!
  @IBOutlet weak var lbBundle2Price1: UILabel!
  @IBOutlet weak var lbBundle2Price2: UILabel!
  @IBOutlet weak var lbBundle3Price1: UILabel!
  @IBOutlet weak var lbBundle3Price2: UILabel!
  @IBOutlet weak var lbLightDetails: UILabel!
  @IBOutlet weak var lbPlusDetails: UILabel!
  @IBOutlet weak var lbPremiumDetails: UILabel!
  @IBOutlet weak var imvLightBg: UIImageView!
  @IBOutlet weak var imvPlusBg: UIImageView!
  @IBOutlet weak var imvPremiumBg: UIImageView!
  
  var withDetails = false
  var priceOption = PriceOption.none
  var tripIndex = 0
  weak var delegate : FDFlightTableViewCellDelegate?
  var flight : FDFlight? {
    didSet {
      imvAirplane.rotate(isEn() ? 0.0 : 180.0)
      lbDepartureTime.text = flight!.departureDate?.toStringUTC(format:.custom("HH:mm"))
      lbArrivalTime.text = FDUtility.arrivalDateHHMMString(flight!.departureDate, arrivalDate: flight!.arrivalDate)
      let originStation = FDResourceManager.sharedInstance.getStation(unwrap(str: flight?.origin))
      let destinationStation = FDResourceManager.sharedInstance.getStation(unwrap(str: flight?.destination))
      lbDepartureCity.text = unwrap(str: originStation?.code)
      lbArrivalCity.text = unwrap(str: destinationStation?.code)
      lbFlightNumber.text = "\(FDLocalized("Flight")) \(flight!.flightNumber!)"
      lbFlightDuration.text = FDUtility.formatDurationShort(flight!.utcDepartureDate!, end: flight!.utcArrivalDate!)
      var lowestPrice = MAXFLOAT
      price1.isHidden = false
      price3.isHidden = false
      price1.fare = nil
      price3.fare = nil
      for fare in flight!.fares {
        var igorneFare = false // to ignore staff fares
        let price = fare.price
        if price > 0 {
          switch fare.fareType {
          case PriceOption.Economy.rawValue:
            price1.fare = fare
            price1.isHidden = false
            price1.btnSelectFare.addTarget(self, action: #selector(FDFlightTableViewCell.onEconomyPriceClicked(_:)), for: .touchUpInside)
            lbPrice.text = fare.price == 0.0 ? FDLocalized("SOLD OUT") : FDUtility.formatCurrency(fare.price as NSNumber)
            if unwrap(int: fare.bundleSet?.bundleOffers.count) > 0 {
              for i in 0..<unwrap(int: fare.bundleSet?.bundleOffers.count) {
                let bundleOffer = fare.bundleSet!.bundleOffers[i]
                switch i {
                case 0:
                  if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
                    lbBundle1Price1.text = "\(bundleOffer.smilePointsRedeemableTotal) \(FDLocalized("points"))"
                    lbBundle1Price2.text = "+ \(FDResourceManager.sharedInstance.selectedCurrencySymbol) \(unwrap(str: FDUtility.formatCurrency(bundleOffer.remainingAmountTotal as NSNumber)))"
                  } else { // show in cash
                    lbBundle1Price1.text = FDUtility.formatCurrency((bundleOffer.price + fare.price) as NSNumber)
                    lbBundle1Price2.text = FDResourceManager.sharedInstance.selectedCurrencySymbol
                  }
                  lbLightDetails.text = FDLocalized(getDetailsText(bundleCode: unwrap(str: bundleOffer.bundleCode)))
                case 1:
                  if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
                    lbBundle2Price1.text = "\(bundleOffer.smilePointsRedeemableTotal) \(FDLocalized("points"))"
                    lbBundle2Price2.text = "+ \(FDResourceManager.sharedInstance.selectedCurrencySymbol) \(unwrap(str: FDUtility.formatCurrency(bundleOffer.remainingAmountTotal as NSNumber)))"
                  } else { // show in cash
                    lbBundle2Price1.text = FDUtility.formatCurrency((bundleOffer.price + fare.price) as NSNumber)
                    lbBundle2Price2.text = FDResourceManager.sharedInstance.selectedCurrencySymbol
                  }
                  lbPlusDetails.text = FDLocalized(getDetailsText(bundleCode: unwrap(str: bundleOffer.bundleCode)))
                case 2:
                  if FDResourceManager.sharedInstance.getPayWithNaSmiles() { // show in points
                    lbBundle3Price1.text = "\(bundleOffer.smilePointsRedeemableTotal) \(FDLocalized("points"))"
                    lbBundle3Price2.text = "+ \(FDResourceManager.sharedInstance.selectedCurrencySymbol) \(unwrap(str: FDUtility.formatCurrency(bundleOffer.remainingAmountTotal as NSNumber)))"
                  } else { // show in cash
                    lbBundle3Price1.text = FDUtility.formatCurrency((bundleOffer.price + fare.price) as NSNumber)
                    lbBundle3Price2.text = FDResourceManager.sharedInstance.selectedCurrencySymbol
                  }
                  lbPremiumDetails.text = FDLocalized(getDetailsText(bundleCode: unwrap(str: bundleOffer.bundleCode)))
                default:
                  break
                }
              }
            }
            break
          case PriceOption.Business.rawValue:
            price3.fare = fare
            price3.isHidden = false
            price3.btnSelectFare.addTarget(self, action: #selector(FDFlightTableViewCell.onBussinessPriceClicked(_:)), for: .touchUpInside)
            break
          case PriceOption.StaffStandby.rawValue:
            price1.fare = fare
            price1.isHidden = false
            price1.btnSelectFare.addTarget(self, action: #selector(FDFlightTableViewCell.onStaffStandbyPriceClicked(_:)), for: .touchUpInside)
          case PriceOption.StaffConfirm.rawValue:
            price3.fare = fare
            price3.isHidden = false
            price3.btnSelectFare.addTarget(self, action: #selector(FDFlightTableViewCell.onStaffConfirmPriceClicked(_:)), for: .touchUpInside)
          default:
            igorneFare = true
            break
          }
          if !igorneFare {
            lowestPrice = min(price,lowestPrice)
          }
        }
      }
      lbCurrency.text = FDResourceManager.sharedInstance.selectedCurrencySymbol
      imvExpand.isHidden = !(lowestPrice < MAXFLOAT)
      lbCurrency.isHidden = !(lowestPrice < MAXFLOAT)
      let operatorCode = getOperatorCode(unwrap(str: flight?.operatedByFlightNumber))
      imvAirline.image = getOperatedBy(operatorCode)
    }
  }
  
  @IBAction func onBtnFareRules(_ sender: UIButton) {
    let url = isEn() ? "http://www.flynas.com/en/booking-flynas/fare-types" : "http://www.flynas.com/ar/book-flynas/fare-types"
    FDUtility.showUrl(getTopmostVC()!, url: url)
  }
  
  func getDetailsText(bundleCode: String) -> String {
    switch bundleCode {
    case BundleCode.LIGH.rawValue:
      return "7Kg carry-on bag, No change/cancelation"
    case BundleCode.LIT1.rawValue:
      return "7Kg carry-on bag, No change, No cancelation"
    case BundleCode.LIT2.rawValue:
      return "7Kg carry-on bag, 20kg bag, No change/cancelation"
    case BundleCode.PLUS.rawValue:
      return "7Kg carry-on bag, 20kg bag, standard seat, change/cancelation for fee"
    case BundleCode.PLS1.rawValue:
      return "7Kg carry on-bag, 20kg bag, standard seat, snack, change/cancelation For Fee."
    case BundleCode.PLS2.rawValue:
      return "7Kg carry on-bag, 30kg bag, standard seat, snack, change/cancelation For Fee."
    case BundleCode.PRUM.rawValue:
      return "7Kg carry-on bag, 30kg bag, premium seat, snack, change for free, cancelation for a fee."
    case BundleCode.PRM1.rawValue:
      return "7Kg carry on bag, 30kg bag, premium seat, snack, change for free, cancelation For a Fee."
    case BundleCode.PRM2.rawValue:
      return "7Kg carry on bag, 20kg bag*2, premium seat, snack, change for free, cancelation For a Fee."
    default:
      return ""
    }
  }
  
  func getOperatorCode(_ operatedByFlightNumber: String) -> String {
    let operatedByFlightNumber = operatedByFlightNumber.trimmingCharacters(in: CharacterSet.whitespaces)
    if operatedByFlightNumber.isEmpty || operatedByFlightNumber == " " || operatedByFlightNumber == "/" || operatedByFlightNumber.lengthOfBytes(using: String.Encoding.utf8) < 2  {
      return ""
    } else {
      return operatedByFlightNumber.substring(to: operatedByFlightNumber.index(operatedByFlightNumber.startIndex, offsetBy: 2))
    }
  }
  
  func getOperatedBy(_ code: String) -> UIImage {
    switch code {
    case "EY":
      return UIImage(named: "icn-air-etihad")!
    case "PC":
      return UIImage(named: "icn-air-pegasus")!
    default:
      return UIImage(named: "icn-air-flynas")!
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    updatePrice()
    imvLightSelect.superview!.superview!.superview!.isHidden = true
    if isAr() {
      imvLightBg.rotate(180.0)
      imvPlusBg.rotate(180.0)
      imvPremiumBg.rotate(180.0)
    }
  }
  
  func setFare(_ fare:FDFare?) {
    if fare != nil {
      switch fare!.fareType {
      case "Economy":
        priceOption = .Economy
        imvLightSelect.superview!.superview!.superview!.isHidden = false
      case "Business":
        priceOption = .Business
        imvLightSelect.superview!.superview!.superview!.isHidden = true
      case "StaffStandby":
        priceOption = .StaffStandby
        imvLightSelect.superview!.superview!.superview!.isHidden = true
      case "StaffConfirm":
        priceOption = .StaffConfirm
        imvLightSelect.superview!.superview!.superview!.isHidden = true
      default:
        break
      }
    } else {
      priceOption = .none
    }
    updatePrice()
  }
  
  func setDetails(_ withDetails: Bool) {
    self.withDetails = withDetails
    btnInfo.isHidden = true
    let stopsStr = FDUtility.formatStops(flight!.numberOfStops)
    lbFlightType.attributedText = nil
    lbFlightType.text = stopsStr
    if withDetails {
      let shouldExpand = unwrap(int: price1.fare?.bundleSet?.bundleOffers.count) > 0
      if !shouldExpand {
        imvLightSelect.superview!.superview!.superview!.isHidden = true
      }
      leadingAirplaneImv.priority = 100
      trailingAirplaneImv.priority = 900
      updateExpandImv(true)
      if flight!.numberOfStops > 0 {
        btnInfo.isHidden = false
        lbFlightType.text = nil
        lbFlightType.attributedText = NSAttributedString(string: stopsStr, attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
      }
    } else {
      imvLightSelect.superview!.superview!.superview!.isHidden = true
      trailingAirplaneImv.priority = 100
      leadingAirplaneImv.priority = 900
      updateExpandImv(false)
    }
    price1.superview!.superview!.isHidden = !withDetails
  }
  
  func updateExpandImv(_ up: Bool) {
    let scale = up ? CGAffineTransform(scaleX: 1.0, y: -1.0) : CGAffineTransform(scaleX: -1.0, y: 1.0)
    imvExpand.layer.setAffineTransform(scale)
  }
  
  @IBAction func onEconomyPriceClicked(_ sender: AnyObject) {
    priceOption = .Economy
    updatePrice()
    let shouldExpand = unwrap(int: price1.fare?.bundleSet?.bundleOffers.count) > 0
    if !shouldExpand {
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = ""
      } else {
        SharedModel.shared.economyFareCodeReturn = ""
      }
    }
    delegate!.selectFlightFare(flight!, selectedFare: price1.fare!, expand: shouldExpand)
  }
  
  @IBAction func onBussinessPriceClicked(_ sender: AnyObject) {
    priceOption = .Business
    if tripIndex == 0 {
      SharedModel.shared.economyFareCodeDepart = ""
    } else {
      SharedModel.shared.economyFareCodeReturn = ""
    }
    updatePrice()
    delegate!.selectFlightFare(flight!, selectedFare: price3.fare!, expand: false)
  }
  
  @IBAction func onStaffStandbyPriceClicked(_ sender: AnyObject) {
    priceOption = .StaffStandby
    updatePrice()
    let shouldExpand = unwrap(int: price1.fare?.bundleSet?.bundleOffers.count) > 0
    if !shouldExpand {
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = ""
      } else {
        SharedModel.shared.economyFareCodeReturn = ""
      }
    }
    delegate!.selectFlightFare(flight!, selectedFare: price1.fare!, expand: false)
  }
  
  @IBAction func onStaffConfirmPriceClicked(_ sender: AnyObject) {
    priceOption = .StaffConfirm
    if tripIndex == 0 {
      SharedModel.shared.economyFareCodeDepart = ""
    } else {
      SharedModel.shared.economyFareCodeReturn = ""
    }
    updatePrice()
    delegate!.selectFlightFare(flight!, selectedFare: price3.fare!, expand: false)
  }
  
  func updatePrice() {
    price1.selected = priceOption == .Economy
    price3.selected = priceOption == .Business
  }
  
  @IBAction func onBtnExpandClicked(_ sender: AnyObject) {
    if imvExpand.isHidden { return }
    delegate!.btnExpandClicked(self)
  }
  
  @IBAction func onStopInfoClicked(_ sender: UIButton) {
    delegate!.btnStopInfoClicked(self)
  }
  
  @IBAction func onBtnEconomyOption(_ sender: UIButton) {
    imvLightSelect.isHighlighted = false
    imvPlusSelect.isHighlighted = false
    imvFlexSelect.isHighlighted = false
    switch sender.tag {
    case 209:
      imvLightSelect.isHighlighted = true
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[0].bundleCode)
      } else {
        SharedModel.shared.economyFareCodeReturn = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[0].bundleCode)
      }
    case 259:
      imvPlusSelect.isHighlighted = true
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[1].bundleCode)
      } else {
        SharedModel.shared.economyFareCodeReturn = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[1].bundleCode)
      }
    case 359:
      imvFlexSelect.isHighlighted = true
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[2].bundleCode)
      } else {
        SharedModel.shared.economyFareCodeReturn = "|" + unwrap(str: flight?.fares[0].bundleSet?.bundleOffers[2].bundleCode)
      }
    default:
      if tripIndex == 0 {
        SharedModel.shared.economyFareCodeDepart = ""
      } else {
        SharedModel.shared.economyFareCodeReturn = ""
      }
    }
    delegate!.selectFlightFare(flight!, selectedFare: price1.fare!, expand: false)
  }
  
}
