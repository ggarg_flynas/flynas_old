//
//  FDHajjUmrahTermsVC.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 26/6/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import UIKit

protocol FDHajjUmrahTermsVCDelegate {
  func didPressButton(agreed: Bool)
}

class FDHajjUmrahTermsVC: UIViewController {
  
  @IBOutlet weak var webView: UIWebView!
  
  var delegate: FDHajjUmrahTermsVCDelegate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let url = isEn()
      ? "http://www.flynas.com/images/terms_and_conditions/Feb-7-2016-flynas-TERMS-AND-CONDITIONS-OF-CARRIAGE-2016-E.pdf"
      : "http://www.flynas.com/images/terms_and_conditions/Feb-7-2016-flynas-TERMS-AND-CONDITIONS-OF-CARRIAGE-2016-A.pdf"
    let request = URLRequest(url: URL(string: url)!)
    webView.loadRequest(request)
  }
  
  @IBAction func onBtnBack(_ sender: FDButton) {
    dismiss(animated: true, completion: nil)
    dismiss(animated: true) {
      self.delegate.didPressButton(agreed: false)
    }
  }
  
  @IBAction func onBtnAgree(_ sender: UIButton) {
    dismiss(animated: true) {
      self.delegate.didPressButton(agreed: true)
    }
  }
  
}


extension FDHajjUmrahTermsVC: UIWebViewDelegate {
  
  func webViewDidStartLoad(_ webView: UIWebView) {
    SVProgressHUD.show()
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    SVProgressHUD.dismiss()
  }
  
}
