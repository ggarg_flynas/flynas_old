//
//  FDSelectPassengerVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 21/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDSelectPassengerVCDelegate : class {
  func selectPassenger(_ passenger:FDPassenger)
  func selectPassengerCancel()
}

class FDSelectPassengerVC: BaseVC {
  
  @IBOutlet var heightTableView: NSLayoutConstraint!
  
  weak var delegate : FDSelectPassengerVCDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("select_passenger_popup")
    heightTableView.constant = CGFloat (46.0 * Double(FDResourceManager.sharedInstance.savedPassenger.count) )
  }
  
  @IBAction func onBtnClose(_ sender: UIButton) {
    delegate?.selectPassengerCancel()
  }
  
}

extension FDSelectPassengerVC: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return FDResourceManager.sharedInstance.savedPassenger.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SelPassengerCell", for: indexPath) as! FDSelPassengerCell
    let passenger = FDResourceManager.sharedInstance.savedPassenger[indexPath.row]
    cell.lbFullName.text = passenger.fullName
    cell.lbPaxType.text = passenger.typeString
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let passenger = FDResourceManager.sharedInstance.savedPassenger[indexPath.row]
    delegate?.selectPassenger(passenger)
  }
  
}
