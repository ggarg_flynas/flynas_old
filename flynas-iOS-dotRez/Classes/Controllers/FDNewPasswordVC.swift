//
//  FDNewPasswordVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 24/08/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDNewPasswordVC: BaseVC {

    var userEmail = ""
    var oldPassword = ""

    @IBOutlet var tfPass: UITextField!
    @IBOutlet var tfConfirmPass: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("new_password_page")
        userEmail = FDLoginManager.sharedInstance.userEmail!
        oldPassword = FDLoginManager.sharedInstance.password!
        FDLoginManager.sharedInstance.clearUserNamePass()
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
        FDLoginManager.sharedInstance.logoutUser({
            }) { (_) in
        }
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onBtnNextClicked(_ sender: UIButton) {
        self.view.endEditing(true)

        if tfPass.text != tfConfirmPass.text  {
            UIAlertView.showWithTitle("", message:  FDErrorHandle.PASS_NOT_SAME, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (_, _) -> Void in
            })

            return
        }

        let password = tfPass.text != nil ? tfPass.text! : ""

        SVProgressHUD.show()
        FDLoginManager.sharedInstance.memberPasswordReset(userEmail, oldPassword: oldPassword, password: password, success: { (memberLogin) -> Void in
            SVProgressHUD.dismiss()
            self.navigationController?.popToRootViewController(animated: true)
            }, fail: { (r) -> Void in
                SVProgressHUD.dismiss()
                self.errorHandle(r)
        })

    }

}
