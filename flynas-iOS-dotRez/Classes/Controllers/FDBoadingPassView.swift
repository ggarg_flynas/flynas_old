//
//  FDBoadingPassView.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDBoadingPassView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet var view: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initView()
        
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)

        initView()
    }

    func initView() {
//        NSBundle.mainBundle().loadNibNamed("FDBoadingPassView", owner: self, options: nil)

//        view.translatesAutoresizingMaskIntoConstraints = false
//
//        self.addSubview(view)
//
//        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))
//
//        self.layoutIfNeeded()
    }
}
