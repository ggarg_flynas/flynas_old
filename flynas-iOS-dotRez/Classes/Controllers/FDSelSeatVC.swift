//
//  FDSelSeatVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 13/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum FDSelSeatApi {
  case booking
  case webCheckin
}

class FDSelSeatVC: FDBookingCommonVC, UITableViewDelegate, UITableViewDataSource {
  
  let SeatAddIconTag = 30
  let SeatPassengerNaem = 31
  let SeatPrice = 32
  let SeatNumber = 33
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightTableView: NSLayoutConstraint!
  @IBOutlet var heightSummary: NSLayoutConstraint!
  @IBOutlet weak var btnGoToPayment: UIButton!
  
  var seatMap : FDFlightSeatMap?
  var seatMapApi = FDSelSeatApi.booking
  var checkinJourney = FDWciJourney()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("seat_select_page")
    viewTripSummary?.isHidden = userBooking.bookingDetails.isGdsBooking && (SharedModel.shared.isMMB || SharedModel.shared.isWCI)
    if self.seatMapApi == .webCheckin {
      log("web_checkin_seat_select_page")
      //            lbInfo.text = FDLocalized("Select your preferred seats now, so that you can stretch out on the plane or sit with your family. You can get a free seat assignment. Click CONTINUE without selecting a seat from the seat map.")
      setShowPriceChanged(true)
    }
    
    if seatMapApi != .booking {
      self.viewTripSummary?.isHidden = true
      heightSummary.constant = 0
    } else {
      self.viewTripSummary?.isHidden = false
      heightSummary.constant = 30
    }
    
    if userBooking.bookingFlow == .manageMyBooking {
      setShowPriceChanged(true)
    }
    
    self.updateUI()
    if userBooking.bookingFlow == .manageMyBooking {
      btnGoToPayment.setTitle(FDLocalized("Continue"), for: UIControlState())
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    updateUI()
  }
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    switch segue.identifier! {
    case "SeatMap" :
      if let indexPath = sender as? IndexPath {
        let controller = segue.destination as! FDSeatMapVC
        
        controller.seatMaps = self.seatMap!
        controller.seatMapApi = self.seatMapApi
        controller.passengerIndex = indexPath.row
        controller.legIndex = indexPath.section
      }
      break
    case "Payment":
      let c = segue.destination as! FDPaymentVC
      c.seatMapApi = self.seatMapApi
      c.checkinJourney = self.checkinJourney
      break
    default:break
    }
  }
  
  func updateUI() {
    tableView.reloadWithAnimation()
    if seatMapApi == .booking {
      updatePriceSummary()
    }
    let sections = CGFloat(numberOfSections(in: tableView))
    let rows = CGFloat(tableView(tableView, numberOfRowsInSection: 0))
    heightTableView.constant = (sections * TableHeaderHeight) + (sections * rows * TableCellHeight)
    //        heightTableView.constant = CGFloat(numberOfSectionsInTableView(tableView)) * TableHeaderHeight + CGFloat(numberOfSectionsInTableView(tableView))*CGFloat(tableView(tableView, numberOfRowsInSection: 0)) * TableCellHeight
  }
  
  @IBAction func onBtnAddSeatClicked(_ sender: UIButton) {
    let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
    if let indexPath = tableView.indexPathForRow(at: buttonPosition) {
      if seatMap != nil {
        if let _ = seatMap?.seatMaps[indexPath.section] {
//          if sm.changeAllowed {
            let passenger = seatMap!.seatMaps[indexPath.section].passengerSeatInfos[indexPath.row]
            
            if passenger.selectedSeatNumber != nil {
              if seatMapApi == .booking {
                SVProgressHUD.show()
                let remvoeSeatObject = passenger.removePassengerSeat()
                
                FDBookingManager.sharedInstance.seatMap(remvoeSeatObject, success: { (flightSeatMap,errorMsg) -> Void in
                  SVProgressHUD.dismiss()
                  if self.userBooking.updateUserSeatSelection(flightSeatMap) {
                    FDErrorHandle.apiCallErrorWithAler(FDLocalized("Error assigning seat"))
                  }
                  
                  self.updateUI()
                  
                }) { (reason) -> Void in
                  SVProgressHUD.dismiss()
                  FDErrorHandle.apiCallErrorWithAler(reason)
                }
              } else {
                SVProgressHUD.show()
                
                let remvoeSeatObject = passenger.removePassengerSeat()
                
                FDWebCheckinManager.sharedInstance.passengerSeats(remvoeSeatObject, success: { (wsiFlightSeatMap) -> Void in
                  SVProgressHUD.dismiss()
                  self.seatMap!.updateUserSeatSelection(wsiFlightSeatMap)
                  self.updateUI()
                }) { (reason) -> Void in
                  SVProgressHUD.dismiss()
                  FDErrorHandle.apiCallErrorWithAler(reason)
                }
              }
            } else {
              self.performSegue(withIdentifier: "SeatMap", sender: indexPath)
            }
//          }
        }
      }
    }
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    
    if let actionDelegate = actionDelegate {
      actionDelegate.updatePage()
      actionDelegate.onBtnBackClicked()
    }
  }
  
  func syncWciSeat() {
    for seatMap in self.seatMap!.seatMaps {
      for passengerSeatInfo in seatMap.passengerSeatInfos {
        if passengerSeatInfo.selectedSeat != nil {
          for sg in checkinJourney.segments {
            if sg.departureStation == seatMap.origin && sg.arrivalStation == seatMap.destination {
              for p in sg.passengers {
                if p.passengerNumber == passengerSeatInfo.passengerNumber {
                  p.unitDesignator = passengerSeatInfo.selectedSeat
                }
              }
            }
          }
        }
      }
    }
  }
  
  func wciPayment() {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getPaymentMethods({ (paymentMethods) -> Void in
      if paymentMethods.balanceDue.floatValue > 0 {
        SVProgressHUD.dismiss()
        self.performSegue(withIdentifier: "Payment", sender: nil)
      } else {
        FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, bookingFlights, contactInput, passengersInput, baggageSsr, mealSsr, sportSsr, flightSeatMap) -> Void in
          self.seatMap!.updateUserSeatSelection(flightSeatMap)
          self.syncWciSeat()
          //                    SVProgressHUD.dismiss()
          FDWebCheckinManager.sharedInstance.checkIn(nil, success: { () -> Void in
            SVProgressHUD.dismiss()
            // go to complete
            self.pushViewController("WebCheckin", storyboardID: "FDWCiCompleteVC") { (vc) -> Void in
              let c = vc as! FDWCiCompleteVC
              c.checkinJourney = self.checkinJourney
            }
            }, fail:{ (reason) -> Void in
              SVProgressHUD.dismiss()
              self.errorHandle(reason)
          })
          }, fail: { (reason) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(reason)
        })
      }
      self.updateUI()
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
    })
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    if userBooking.bookingFlow == .normalBooking {
      if self.seatMapApi == FDSelSeatApi.webCheckin && self.seatMap != nil {
        // check all seat selected
        if !self.seatMap!.isAllPassengersHavaSeat {
          let msg = FDLocalized("Are you sure you want to continue with Random seating?")
          UIAlertView.showWithTitle("", message: msg, cancelButtonTitle: FDLocalized("Cancel") , otherButtonTitle: FDLocalized("Continue"), completionHandler: { (alertView, buttonIndex) -> Void in
            if buttonIndex == 1 {
              SVProgressHUD.show()
              FDWebCheckinManager.sharedInstance.passengerAutoSeats({ (wsiFlightSeatMap) -> Void in
                FDBookingManager.sharedInstance.getBookingDetail(false, success: { (bookingDetails, bookingPayments, bookingFlights, contactInput, passengersInput, baggageSsr, mealSsr, sportSsr, flightSeatMap) -> Void in
                  self.seatMap!.updateUserSeatSelection(flightSeatMap)
                  self.updateUI()
                  self.wciPayment()
                  }, fail: { (reason) -> Void in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
                })
              }) { (reason) -> Void in
                SVProgressHUD.dismiss()
                FDErrorHandle.apiCallErrorWithAler(reason)
              }
            }
          })
        } else {
          wciPayment()
        }
      } else {
        self.performSegue(withIdentifier: "Payment", sender: sender)
      }
    } else { // MMB
      self.navigationController?.popViewController(animated: true)
      if let actionDelegate = actionDelegate {
        actionDelegate.updatePage()
        actionDelegate.onBtnContinueClicked()
      }
    }
  }
  
  // MARK: - seat map table
  func numberOfSections(in tableView: UITableView) -> Int {
    if seatMap != nil {
      return seatMap!.seatMaps.count
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FDFlightInfoCell") as! FDFlightInfoCell
    if seatMapApi == .booking {
      if let sm = seatMap?.seatMaps[section] {
        let direction = sm.diretion
        cell.lbDirection.text = direction == .arrive ? FDLocalized("Return Flight:") : FDLocalized("Departing Flight:")
        cell.lbFlight.text = sm.origin + " - " + sm.destination
      }
    } else {
      if let sm = seatMap?.seatMaps[section] {
        cell.lbDirection.text = FDLocalized("Flight:")
        cell.lbFlight.text = sm.origin + " - " + sm.destination
      }
    }
    return cell.contentView
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if unwrap(int: seatMap?.seatMaps.count) > 0 {
      if let sm = seatMap?.seatMaps[section] {
        return sm.passengerSeatInfos.count
      }
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SelSeatTableCell") as! SelSeatTableCell
    if let sm = seatMap?.seatMaps[indexPath.section] {
      let passenger = sm.passengerSeatInfos[indexPath.row]
      cell.lbFullName.text = passenger.fullName
//      if sm.changeAllowed {
//        cell.btnSelect.isHidden = false
        let titleStr = passenger.selectedSeatNumber != nil ? FDLocalized("Remove") : FDLocalized("Select")
        cell.btnSelect.setTitle(titleStr, for: UIControlState())
//      } else {
//        cell.btnSelect.isHidden = true
//      }
      cell.lbSeat.text = passenger.selectedSeatNumber == nil ? FDLocalized("Not selected") : unwrap(str: passenger.selectedSeatNumber)
      cell.lbPrice.text = FDUtility.formatCurrencyWithCurrencySymbol(passenger.selectedSeatPrice)
      cell.lbPrice.isHidden = passenger.selectedSeatPrice == nil
      if self.seatMapApi == .webCheckin {
        if nil == passenger.selectedSeatNumber {
          cell.lbSeat.text = FDLocalized("Random seat")
        }
      }
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let _ = seatMap?.seatMaps[indexPath.section] {
//      if sm.changeAllowed {
        performSegue(withIdentifier: "SeatMap", sender: indexPath)
//      }
    }
  }
  
}

class SelSeatTableCell: UITableViewCell {
  
  @IBOutlet weak var lbFullName: UILabel!
  @IBOutlet weak var lbSeat: UILabel!
  @IBOutlet weak var btnSelect: UIButton!
  @IBOutlet weak var lbPrice: UILabel!
  
}
