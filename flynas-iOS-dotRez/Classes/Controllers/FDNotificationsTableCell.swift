//
//  FDNotificationsTableCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 6/07/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDNotificationsTableCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var receiveDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
