//
//  FDResetPasswordVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 24/08/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDResetPasswordVC: BaseVC {
  
  var userEmail : String = ""
  
  @IBOutlet var tfEmail: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("reset_password_page")
    tfEmail.text = userEmail
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.view.endEditing(true)
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    self.view.endEditing(true)
    
    userEmail = tfEmail.text != nil ? tfEmail.text! : ""
    
    let validator = SHEmailValidator()
    do {
      try validator.validateSyntax(ofEmailAddress: userEmail)
    } catch {
      FDErrorHandle.validateErrorHandle(.emailNotValidate)
      return
    }
    
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.forgetPassword(userEmail, success: { (memberLogin) -> Void in
      SVProgressHUD.dismiss()
      
      let confirmMsg = FDLocalized("The temporary password has been emailed to {0}. Please use it next time you login!").replacingOccurrences(of: "{0}", with: self.userEmail)
      
      UIAlertView.showWithTitle(FDLocalized("Password reset successful") , message:  confirmMsg, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (_, _) -> Void in
        
        //                self.navigationController?.popViewControllerAnimated(true)
        self.navigationController?.popToRootViewController(animated: true)
      })
      
      }, fail: { (r) -> Void in
        SVProgressHUD.dismiss()
        self.errorHandle(r)
    })
    
    
  }
  
}
