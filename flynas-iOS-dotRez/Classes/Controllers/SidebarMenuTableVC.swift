//
//  SidebarMenuTableVC.swift
//  flynas-dotRez
//
//  Created by River Huang on 9/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit

class SidebarMenuTableVC: UIViewController     {

    @IBOutlet var heightMyProfile: NSLayoutConstraint!
    @IBOutlet var heightRegister: NSLayoutConstraint!
    @IBOutlet var viewLogout: UIView!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var labelInternalVersion: UILabel!
    @IBOutlet var btnManagerMyBooking: FDButton!
    @IBOutlet var labelInternalVersion2: UILabel!

  override func viewWillAppear(_ animated: Bool) {
    #if PRO
      labelInternalVersion.hidden = true
    #else
      if let appBuildString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") {
        labelInternalVersion.isHidden = false
        labelInternalVersion.text = "b\(appBuildString)"
      }
    #endif
    //        btnManagerMyBooking.enabled = false
    let label = labelInternalVersion2
    let rotate = CGAffineTransform(rotationAngle: CGFloat(-(Double.pi / 2)))
    let translate = CGAffineTransform(translationX: ((label?.bounds.height)! / 2)-((label?.bounds.width)! / 2), y: -((label?.bounds.width)! / 2)+((label?.bounds.height)! / 2))
    label?.transform = rotate.concatenating(translate)
    label?.textColor = UIColor(red: 71/255.0, green: 145/255.0, blue: 177/255.0, alpha: 1.0)
    labelInternalVersion2.isHidden = true
    label?.text = FDVersionNumber
    updateUI()
  }
    
    func updateUI () {
        if FDResourceManager.sharedInstance.isSessionLogined {
            heightMyProfile.priority = 100
            heightRegister.priority = 900
            viewLogout.isHidden = false
            btnLogin.setTitle(FDLoginManager.sharedInstance.userName, for: .disabled)
            btnLogin.isEnabled = false
        } else {
            heightMyProfile.priority = 900
            heightRegister.priority = 100
            viewLogout.isHidden = true
            btnLogin.isEnabled = true
        }

    }

  @IBAction func onBtnBookFlightClicked(_ sender: UIButton) {
    self.revealToggle(sender)
    //        getFDHomeVC().onBtnTabBook(sender)
    postNotification(ContainerVC.kBtnBook, userInfo: [:])
  }

    @IBAction func onBtnOnlineCheckInClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnOnlineCheckin(sender)
    }

    @IBAction func onBtnManagerMyBookingClicked(_ sender: UIButton) {
    }

    @IBAction func onBtnMyBoardingPassClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnMyBoardingPassClicked(sender)
    }

    @IBAction func onBtnFlightStatusClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnFlightStatusClick(sender)
    }

    @IBAction func onBtnFlightScheduleClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnFlightSchedule(sender)
    }

    @IBAction func onBtnCloseClicked(_ sender: UIButton) {
        self.revealToggle(sender)
    }

    @IBAction func onBtnLogin(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnLogin(sender)
    }

    @IBAction func onBtnLogout(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnLogout(sender)
    }

    @IBAction func onBtnMyProfile(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnMyProfile(sender)
    }

    @IBAction func onBtnRegisterClicked(_ sender: UIButton) {
        self.revealToggle(sender)
//        getFDHomeVC().loginSwitchToRegister()
    }

    @IBAction func onBtnContactUsClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnContactUsClicked(sender)
    }

    @IBAction func onBtnManageMyBookingClicked(_ sender: UIButton) {
        self.revealToggle(sender)
        getFDHomeVC().onBtnManagerMyBookingClicked(sender)
    }
}
