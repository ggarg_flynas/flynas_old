//
//  SidebarMenuTable2VM.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 29/04/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation

enum SidebarMenuItemType {
    case nameLabel
    case groupHeader
    case menuButton
    case socialMedia
    case logout
}

enum SidebarMenuItem : String {
  
  case NameLabel = "Login"
  case Welcome
  case MyProfile
  case Register
  case MyBooking = "My bookings"
  case flynas
  case BookFlight = "Book a flight"
  case CheckIn = "Check-in"
  case MyBoardingPasses = "My boarding passes"
  case ManageMyBooking = "Manage my booking"
  case PurchaseAdditionalBags = "Purchase additional bags"
  case PickYourFavoriteSeat = "Pick your favorite seat"
  case ChooseYourFavoriteMeal = "Choose your favorite meal"
  case Deals = "Deals (coming soon)"
  case HotelsAndCars = "Hotels and Cars"
  case BookAHotel = "Book a Hotel"
  case RentACar = "Rent a Car"
  case AirportTransfer = "Airport Transfer"
  case FlightInfo = "Flight Info"
  case FlightStatus = "Flight Status"
  case FlightSchedule = "Flight Schedule"
  case naSmiles
  case naSmilesMembershipCard = "My Membership Card"
  case AboutNaSmiles = "About naSmiles"
  case EarningSMILEPoints = "Earning SMILE Points"
  case TierBenefits = "Tier Benefits"
  case SmilePointCalculator = "Smile Point Calculator"
  case PartnersCalculator = "Partners Calculator"
  case AboutFlynas = "About flynas"
  case FAQs = "FAQs"
  case MyNotifications  = "My notifications"
  case SocialMedia
  case Logout
  
  static func menuItems (_ member:Bool, nasSmileMmeber: Bool, hasNaSmileLogOut:Bool) -> [SidebarMenuItem] {
    var menu = [SidebarMenuItem]()
    if member {
      menu.append(contentsOf: [
        NameLabel,
        Welcome,
        MyProfile,
        MyBooking,
        
        flynas,
        BookFlight,
        CheckIn,
        MyBoardingPasses,
        ManageMyBooking,
        MyNotifications,
        
        HotelsAndCars,
        BookAHotel,
        RentACar,
        AirportTransfer,
        
        FlightInfo,
        FlightStatus,
        FlightSchedule,
        
        ])
    } else {
      menu.append(contentsOf: [
        NameLabel,
        Welcome,
        Register,
        
        flynas,
        BookFlight,
        CheckIn,
        MyBoardingPasses,
        ManageMyBooking,
        MyNotifications,
        
        HotelsAndCars,
        BookAHotel,
        RentACar,
        AirportTransfer,
        
        FlightInfo,
        FlightStatus,
        FlightSchedule,
        ])
    }
    
    if nasSmileMmeber {
      menu.append(contentsOf: [
        naSmiles,
        naSmilesMembershipCard,
        AboutNaSmiles,
        EarningSMILEPoints,
        TierBenefits,
        SmilePointCalculator,
        PartnersCalculator
        ])
    } else {
      menu.append(contentsOf: [
        naSmiles,
        AboutNaSmiles,
        EarningSMILEPoints,
        TierBenefits,
        SmilePointCalculator,
        PartnersCalculator
        ])
    }
    if member {
      menu.append(contentsOf: [
        AboutFlynas,
        FAQs,
        SocialMedia,
        Logout
        ])
    } else {
      menu.append(contentsOf: [
        AboutFlynas,
        FAQs,
        SocialMedia,
        Logout])
    }
    
    return menu
  }
  
  func getType () -> SidebarMenuItemType {
    switch self {
    case .NameLabel:
      return .nameLabel
    case .Welcome, .flynas, .HotelsAndCars, .FlightInfo, .naSmiles, .AboutFlynas:
      return .groupHeader
    case .SocialMedia:
      return .socialMedia
    case .Logout:
      return .logout
    default:
      return .menuButton
    }
  }
  
  func getString () -> String {
    switch self {
    case .NameLabel:
      return FDLocalized("Login")
      
    case .Welcome:
      return FDLocalized("Welcome")
    case .MyProfile:
      return FDLocalized("My profile")
    case .Register:
      return FDLocalized("Register")
    case .MyBooking:
      return FDLocalized("My bookings")
      
    case .flynas:
      return FDLocalized("flynas")
    case .BookFlight:
      return FDLocalized("Book a flight")
    case .CheckIn:
      return FDLocalized("Check-in")
    case .MyBoardingPasses:
      return FDLocalized("My boarding passes")
    case .ManageMyBooking:
      return FDLocalized("Manage my booking")
    case .PurchaseAdditionalBags:
      return FDLocalized("Purchase additional bags")
    case .PickYourFavoriteSeat:
      return FDLocalized("Pick your favorite seat")
    case .ChooseYourFavoriteMeal:
      return FDLocalized("Choose your favorite meal")
    case .Deals:
      return FDLocalized("Deals")
      
    case .HotelsAndCars:
      return FDLocalized("Hotels and Cars")
    case .BookAHotel:
      return FDLocalized("Book a Hotel")
    case .RentACar:
      return FDLocalized("Rent a Car")
    case .AirportTransfer:
      return FDLocalized("Airport Transfer")
      
    case .FlightInfo:
      return FDLocalized("Flight Info")
    case .FlightStatus:
      return FDLocalized("Flight Status")
    case .FlightSchedule:
      return FDLocalized("Flight Schedule")
      
    case .naSmiles:
      return FDLocalized("naSmiles")
    case .naSmilesMembershipCard:
      return FDLocalized("My Membership Card")
    case .AboutNaSmiles:
      return FDLocalized("About naSmiles")
    case .EarningSMILEPoints:
      return FDLocalized("Earning SMILE Points")
    case .TierBenefits:
      return FDLocalized("Tier Benefits")
    case .SmilePointCalculator:
      return FDLocalized("Smile Point Calculator")
    case .PartnersCalculator:
      return FDLocalized("Partners Calculator")
    case .AboutFlynas:
      return FDLocalized("About flynas")
    case .FAQs:
      return FDLocalized("FAQs")
    case .MyNotifications :
      return FDLocalized("My notifications")
    case .Logout:
      return FDLocalized("Logout")
    default:
      return ""
    }
  }
  
  func imageName() -> String {
    switch self {
    case .flynas:
      return "icn-menu-flynas"
    case .HotelsAndCars:
      return "icn-menu-hotelsandcars"
    case .FlightInfo:
      return "icn-menu-flightInfo"
    case .naSmiles:
      return "icn-menu-naSmile"
    case .AboutFlynas:
      return "icn-menu-info"
    default:
      return "icn-menu-welcome"
    }
  }
  
}

class SidebarMenuTable2VM {
  
  var isMemberLogin = false
  var isNaSmileLogin = false
  var menuItems = [SidebarMenuItem]()
  
  func updateMenuItemes() {
    isMemberLogin = FDLoginManager.sharedInstance.hasSavedUserCredential
    isNaSmileLogin = FDLoginManager.sharedInstance.hasNaSmiles
    let hasNaSmileLogOut = FDNaSmileManager.sharedInstance.isNaSmileLogin
    menuItems = SidebarMenuItem.menuItems(isMemberLogin, nasSmileMmeber: isNaSmileLogin, hasNaSmileLogOut: hasNaSmileLogOut)
  }
  
  func btnUserInteractionEnabled(_ item:SidebarMenuItem) -> Bool {
    switch item {
    case .NameLabel:
      return !isMemberLogin
    default:
      if item.getType() == .groupHeader {
        return false
      } else {
        return true
      }
    }
  }
  
  func btnTitle(_ item:SidebarMenuItem) -> String {
    if isMemberLogin {
      switch item {
      case .NameLabel:
        if let userName = FDLoginManager.sharedInstance.userName {
          return userName
        } else {
          return ""
        }
      default:
        break
      }
      
    }
    
    return item.getString()
  }
  
  func getVersionNumber() -> String {
    return ""//"\(FDLocalized("Version")) \(FDUtility.getVersionNumber())"
  }
}
