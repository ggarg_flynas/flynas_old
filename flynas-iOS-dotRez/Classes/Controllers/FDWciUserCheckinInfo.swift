//
//  FDWciUserCheckinInfo.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWciUserCheckinInfo: NSObject {
    var wciBooking = FDWciBooking() {
        didSet {
//            segments.removeAll()
            for i in 0..<wciBooking.journeys.count {
                let journey = wciBooking.journeys[i]
                for j in 0..<journey.segments.count {
                    let segment = journey.segments[j]
//                    segments.append(segment)
                    segment.journeyNumber = journey.journeyNumber
                    for passenger in segment.passengers {
                        if passenger.unitDesignator != nil && !passenger.unitDesignator!.isEmpty {
                            passenger.isEdiable = false
                        } else {
                            passenger.isEdiable = true
                        }
                    }
                }
            }
        }
    }

//    var segments = [FDWciSegment]()
}
