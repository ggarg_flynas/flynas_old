//
//  FDSelPassengerCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 21/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDSelPassengerCell: UITableViewCell {
  
  @IBOutlet weak var lbFullName: UILabel!
  @IBOutlet weak var lbPaxType: UILabel!
  
}
