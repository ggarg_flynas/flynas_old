//
//  FDFlightConnectionInfoTableViewCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 25/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightConnectionInfoTableViewCell: UITableViewCell {

    @IBOutlet var lableFlightInfoTitle: UILabel!
    @IBOutlet var lableFlightInfoDeparting: UILabel!
    @IBOutlet var lableFlightInfoArrives: UILabel!
    @IBOutlet var lableFlightInfoDuration: UILabel!
    @IBOutlet var lableFlightInfoTransit: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
