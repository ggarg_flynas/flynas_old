//
//  FDStationTableViewCell.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 24/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDStationTableViewCell: UITableViewCell {

    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelCountryName: UILabel!
    @IBOutlet var labelCode: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
