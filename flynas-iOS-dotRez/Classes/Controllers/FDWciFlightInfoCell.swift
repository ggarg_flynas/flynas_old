//
//  FDWciFlightInfoCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDWciFlightInfoCellDelegate : class {
    func btnExpandClicked(_ cell:FDWciFlightInfoCell)
}

class FDWciFlightInfoCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    var journey : FDWciJourney?{
        didSet {
            if journey != nil {
                heighflightInfoTableView.constant = CGFloat(58 * (journey!.segments.count))

                if journey!.segments.count > 0 {
                    heighPassengerTableView.constant = CGFloat(58 * (journey!.segments[0].passengers.count))
                }

                labelDepartDate.text = journey!.departureDate?.toStringUTC(format:.custom("EEEE, dd MMM yyyy"))

            }
        }
    }

    var expanded = false {
        didSet {
            if expanded {
                heightPassengerList.priority = 100
                expandBtn.isSelected = true
            } else {
                heightPassengerList.priority = 999
                expandBtn.isSelected = false
            }
        }
    }
    weak var delegate : FDWciFlightInfoCellDelegate?
    
    @IBOutlet var heightPassengerList: NSLayoutConstraint!
    @IBOutlet var expandBtn: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var heighPassengerTableView: NSLayoutConstraint!

    @IBOutlet var flightInfoTableView: UITableView!
    @IBOutlet var heighflightInfoTableView: NSLayoutConstraint!

    @IBOutlet var labelDepartDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.dataSource = self
        tableView.delegate = self

        flightInfoTableView.dataSource = self
        flightInfoTableView.delegate = self

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func onBtnExpandClicked(_ sender: UIButton) {
        delegate?.btnExpandClicked(self)
    }

    @IBAction func onBtnCheckinClicked(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: buttonPosition) {
            tableView(tableView, didSelectRowAt: indexPath)
        }
    }

    // MARK: - UITableViewDelegate/DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let j = journey {
            if tableView == flightInfoTableView {
                return j.segments.count
            } else {
                if j.segments.count > 0 {
                    return j.segments[0].passengers.count
                }
            }
        }
        return 0
    }

    let BtnCheckin = 30
    let SeatPassengerNaem = 31
    let SeatNumber = 33

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == flightInfoTableView {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "FDWciLegInfoCell") as? FDWciLegInfoCell {

                cell.segment = journey!.segments[indexPath.row]
                return cell
            }

            return UITableViewCell()
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "FDWciPassengerInfoCell")


            if let passenger = journey?.segments[0].passengers[indexPath.row] {
                if let seatPassengerName = cell!.viewWithTag(SeatPassengerNaem) as? UILabel {
                    seatPassengerName.text = passenger.fullName
                }

                if let btnCheckin = cell!.viewWithTag(BtnCheckin) as? UIButton {
                    btnCheckin.isSelected = passenger.prepareToCheckin
                    btnCheckin.isHidden = passenger.isCheckedIn
                }
            }

            var seatNumberString = ""
            if let journey = journey {
                for i in 0..<journey.segments.count {
                    let s = journey.segments[i]
                    if indexPath.row < s.passengers.count {
                        var sn = "--"
                        if let unitDesignator = s.passengers[indexPath.row].unitDesignator {
                            sn = unitDesignator
                        }

                        if i == 0 {
                            seatNumberString = sn
                        } else {
                            seatNumberString = "\(seatNumberString),\(sn)"
                        }
                    }
                }
            }

            if let seatNumber = cell?.viewWithTag(SeatNumber) as? UILabel {
                seatNumber.text = seatNumberString
            }
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == self.tableView {
            if let passenger = journey?.segments[0].passengers[indexPath.row] {
                if !passenger.isCheckedIn {
                    passenger.prepareToCheckin = !passenger.prepareToCheckin

                    for p in journey!.segments[0].passengers {
                        if p != passenger && p.passengerNumber == passenger.passengerNumber {
                            p.prepareToCheckin = passenger.prepareToCheckin
                        }
                    }
                }
            }
            tableView.reloadData()
        }
    }

}
