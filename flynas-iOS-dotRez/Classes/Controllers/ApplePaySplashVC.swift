//
//  ApplePaySplashVC.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 31/8/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

class ApplePaySplashVC: BaseVC {
  
  @IBOutlet weak var vwSetup: UIView!
  @IBOutlet weak var btnDismiss: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSetupButtonIfNeeded()
  }
  
  func addSetupButtonIfNeeded() {
    if ApplePayHandler.status().hasAddedCards { return }
    let btnApplePay = ApplePayHandler.getPaymentButton()
    btnApplePay.frame = CGRect(x: 0.0, y: 0.0, width: vwSetup.bounds.size.width, height: vwSetup.bounds.size.height)
    btnApplePay.addTarget(self, action: #selector(self.onBtnSetup), for: .touchUpInside)
    vwSetup.addSubview(btnApplePay)
    btnDismiss.isSelected = true
    btnDismiss.setBackgroundImage(UIImage(named: "btn-nothanks"), for: UIControlState())
    btnDismiss.setTitle(FDLocalized("NO THANKS"), for: UIControlState())
  }
  
  func onBtnSetup() {
    ApplePayHandler.setupCards()
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onBtnDismiss(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
  }
  
}
