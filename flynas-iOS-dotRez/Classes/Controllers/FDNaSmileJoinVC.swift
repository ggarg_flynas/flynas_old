//
//  FDNaSmileJoinVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 4/01/2017.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

protocol FDNaSmileJoinVCDelegate : class {
    func naSmileJoinSuccess()
    func naSmileJoinBack()
}

class FDNaSmileJoinVC: BaseVC {

    var naSmilesID : String?

    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfConfirmPassword: UITextField!

    @IBOutlet var bgName: UIButton!
    @IBOutlet var bgPass: UIButton!
    @IBOutlet var bgConfirmPass: UIButton!

    weak var delegate : FDNaSmileJoinVCDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
      log("naSmile_join_page")
        tfEmail.text = naSmilesID

    }

    @IBAction func onBtnJoinClicked(_ sender: AnyObject) {

        self.view.endEditing(true)

        if tfPassword.text == nil || tfPassword.text!.isEmpty   {
            UIAlertView.showWithTitle("", message:  FDErrorHandle.PASS_EMPTY, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (_, _) -> Void in
            })

            return
        }
        
        if tfPassword.text != tfConfirmPassword.text  {
            UIAlertView.showWithTitle("", message:  FDErrorHandle.PASS_NOT_SAME, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (_, _) -> Void in
            })

            return
        }

        SVProgressHUD.show()
        FDLoginManager.sharedInstance.memberNaSmileNumberJoin(tfPassword.text!, success: { (msg) -> Void in
            SVProgressHUD.dismiss()
            UIAlertView.showWithTitle("", message:FDLocalized(msg) , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
                self.delegate?.naSmileJoinSuccess()
            })
        }) { (r) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(r)
        }
    }

    @IBAction func onBtnBackClicked(_ sender: AnyObject) {
        self.delegate?.naSmileJoinBack()

    }
}
