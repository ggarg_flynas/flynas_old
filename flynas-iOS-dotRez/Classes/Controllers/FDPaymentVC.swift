//
//  FDPaymentVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPaymentVC: FDBookingCommonVC {
  
  enum PaymentMethod {
    case creditCard
    case sadad
    case voucher
    case nasCredit
    case naSmile
    case applePay
    case sadadOLP
  }
  
  @IBOutlet weak var vwNextButton: UIView!
  @IBOutlet weak var vwOtherPaymentButtons: UIView!
  @IBOutlet weak var vwOtherPaymentDetails: UIView!
  @IBOutlet weak var vwCreditCardButton: UIView!
  @IBOutlet weak var vwNasCreditButton: UIView!
  @IBOutlet weak var vwNaSmileButton: UIView!
  @IBOutlet weak var vwSadadButton: UIView!
  @IBOutlet weak var lbSubtotal: UILabel!
  @IBOutlet var tfTotlePrice: UILabel!
  @IBOutlet weak var tfKsaVat: UILabel!
  @IBOutlet var tfCreditCardNumber: BKCardNumberField!
  @IBOutlet var tfCardHolderName: UITextField!
  @IBOutlet var tfCreditCardExpiryDate: BKCardExpiryField!
  @IBOutlet var tfCVV: UITextField!
  @IBOutlet var labelTerms: UILabel!
  @IBOutlet var btnAgreeToTerms: UIButton!
  @IBOutlet var btnPaymentChannels: UIButton!
  @IBOutlet var btnPaymentCreditCard: UIButton!
  @IBOutlet var btnPaymentSADAD: UIButton!
  @IBOutlet weak var btnSadadOLP: UIButton!
  @IBOutlet var btnPaymentNasCredit: UIButton!
  @IBOutlet var btnPaymentNaSmile: UIButton!
  @IBOutlet var viewPaymentMethod1: UIView!
  @IBOutlet var viewPaymentMethod2: UIView!
  @IBOutlet var viewPaymentMethod3: UIView!
  @IBOutlet var viewPaymentMethodCF: UIView!
  @IBOutlet var viewPaymentMethodNS: UIView!
  @IBOutlet weak var vwPaymentOLP: UIView!
  
  @IBOutlet var labelWhatIsCvv: UILabel!
  @IBOutlet var btnWhatIsCvv: UIButton!
  @IBOutlet var labelCreditShellApplied: UILabel!
  @IBOutlet var btnRemoveCreditShellPayment: UIButton!
  @IBOutlet var heightCreditShellApplied: NSLayoutConstraint!
  @IBOutlet var labelNSApplied: UILabel!
  @IBOutlet var heightNSApplied: NSLayoutConstraint!
  @IBOutlet var labelCreditShellBalance: UILabel!
  @IBOutlet var tfCreditShellAmount: UITextField!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightPaymentSummaryTable: NSLayoutConstraint!
  @IBOutlet weak var vwOtherPaymentsHeader: UIView!
  @IBOutlet weak var vwApplePayButtonBase: UIView!
  @IBOutlet weak var vwPayMethods: UIView!
  @IBOutlet weak var tfSadadOlp: UITextField!
  
  let paymentNSVC = PaymentNSVC(nibName:"PaymentNSVC", bundle: nil)
  let defaultBookingPollingInterval = 3
  let paymentTimeOutTime = 60*5 // 2 minutes
  var paymentTimeOutTimer : Timer?
  var sessionPaymentQueryCount = 0
  var paymentMethod = PaymentMethod.creditCard
  var seatMapApi = FDSelSeatApi.booking
  var checkinJourney = FDWciJourney()
  var agreeToTerms = false
  var paymentDetail = FDPayment()
  var paymentMethods = FDAvailablePaymentMethods()
  var paymentResult = FDPaymentResult()
  var bookingPolling = FDBookingPolling()
  var bookingDetails = FDBookingDetails()
  var bookingPayments : FDBookingPayments?
  var creditFileInfo = FDCreditFileInfo()
  var webView : SVModalWebViewController?
  let paymentVM = FDPaymentVM()
  let PaymentSummaryLabelLeft = 41
  let PaymentSummaryLabelRight = 42
  let PaymentSummaryImage = 40
  let applePayHandler = ApplePayHandler()
  let successUrl = "/polling"
  let failUrl = "/payment"
  var isBusiness = false
  var hasBag = false
  var hasMeal = false
  var canPaybyNS : Bool {
    // can only pay by naSmile only once
    if nil != bookingPayments?.getPendingNS() {
      return false
    }
    if !userBooking.payWithNaSmiles {
      return false
    }
    return paymentMethods.canPayByNaSmileRedemption
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("payment_page")
    tfCreditCardNumber.clearButtonMode = .never
    tfCreditCardExpiryDate.clearButtonMode = .never
    tfCreditShellAmount.keyboardType = UIKeyboardType.decimalPad
    if self.seatMapApi == .webCheckin {
      log("web_checkin_payment_page")
    }
    view.layoutIfNeeded()
    viewPaymentMethodNS.addSubview(paymentNSVC.view)
    paymentNSVC.view.frame.size = viewPaymentMethodNS.frame.size
    paymentNSVC.initPanelPosition()
    paymentNSVC.delegate = self
    //        paymentNSVC.nsMin = 5000
    //        if userBooking.isTwoWay {
    //            paymentNSVC.nsMin = 10000
    //        }
    if self.bookingDetails.hasLoyaltyPayment {
      return paymentNSVC.nsMin = 0
    }
    if isRTL() {
      FDReplaceBtnAttributedTitle(btnPaymentChannels, title: FDLocalized("Payment Channels"), state: UIControlState())
      FDReplaceLableAttributedTitle(labelWhatIsCvv,title: FDLocalized("What is CW2/CVV?"))
      FDReplaceLableAttributedTitle(labelTerms,title: FDLocalized("Terms and Conditions of Carriage"))
    }
    agreeToTerms = true
    updatePriceSummary(){}
    if seatMapApi == FDSelSeatApi.booking {
      self.viewTripSummary?.isHidden = false
    } else {
      self.viewTripSummary?.isHidden = true
    }
    getPaymentMethods();
    updateUI()
    tfCreditCardNumber.addTarget(self, action: #selector(FDPaymentVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfCardHolderName.addTarget(self, action: #selector(FDPaymentVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfCreditCardExpiryDate.addTarget(self, action: #selector(FDPaymentVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfCVV.addTarget(self, action: #selector(FDPaymentVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    // payment summary table
    paymentVM.userBooking = self.userBooking
    paymentVM.updatePaymentSummaryTableItems()
    updatePaymentSummaryTableHeight()
    setupApplePayButton()
    NotificationCenter.default.addObserver(self, selector: #selector(self.setupApplePayWithDelay), name: NSNotification.Name(rawValue: kFDAppDidEnterForeground), object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    switch segue.identifier! {
    case "Confirmation":
      let c = segue.destination as! FDConfirmationVC
      c.manageBooking = false
      userBooking.agreeToTerms = agreeToTerms
      userBooking.paymentDetail = paymentDetail
      userBooking.paymentMethods = paymentMethods
      userBooking.paymentResult = paymentResult
      userBooking.bookingPolling = bookingPolling
      userBooking.bookingDetails = bookingDetails
      userBooking.bookingPayments = bookingPayments
      break
    default:break
    }
  }
  
  func updateUI() {
    tfTotlePrice.text = paymentMethods.getBalnceDue(inPoints: FDResourceManager.sharedInstance.getPayWithNaSmiles())
    if SharedModel.shared.isWCI {
      tfKsaVat.text = paymentMethods.getSeatVat(userBooking).0
      lbSubtotal.text = paymentMethods.getSubtotal(unwrap(str: paymentMethods.getSeatVat(userBooking).1))
    } else {
      tfKsaVat.text = paymentMethods.getKsaVatDue(unwrap(str: userBooking.priceSummary.taxAmount))
      lbSubtotal.text = paymentMethods.getSubtotal(unwrap(str: userBooking.priceSummary.taxAmount))
    }
    btnAgreeToTerms.isSelected = agreeToTerms
    // credit card payment info
    tfCardHolderName.text = paymentDetail.accountName
    tfCreditCardNumber.cardNumber = paymentDetail.accountNumber
    tfCreditCardExpiryDate.text = FDUtility.getExpiryDateString(paymentDetail.expiryDate)
    tfCVV.text = paymentDetail.verificationCode
    let pay1 = paymentMethod == .creditCard
    let pay2 = paymentMethod == .sadad
    let pay3 = paymentMethod == .voucher
    let payCF = paymentMethod == .nasCredit
    let payNS = paymentMethod == .naSmile
    let payOLP = paymentMethod == .sadadOLP
    btnPaymentCreditCard.isSelected = pay1
    btnPaymentSADAD.isSelected = pay2
    btnPaymentNasCredit.isSelected = payCF
    btnPaymentNaSmile.isSelected = payNS
    btnSadadOLP.isSelected = payOLP
    updateTabPositon()
    viewPaymentMethod1.isHidden = !pay1
    viewPaymentMethod2.isHidden = !pay2
    viewPaymentMethod3.isHidden = !pay3
    viewPaymentMethodCF.isHidden = !payCF
    viewPaymentMethodNS.isHidden = !payNS
    vwPaymentOLP.isHidden = !payOLP
    // update nas credit balance
    if self.creditFileInfo.creditFileAvailable {
      self.labelCreditShellBalance.text = "\(self.bookingDetails.currencyCode) \(FDUtility.formatCurrency(self.creditFileInfo.availableAmount)!)"
    }
    // update remove cf line
    if let pendingCF = self.bookingPayments?.getPendingCF() {
      heightCreditShellApplied.constant = 25
      labelCreditShellApplied.text = "\(pendingCF.quotedCurrencyCode) \(FDUtility.formatCurrency(pendingCF.quotedAmount)!)"
      labelCreditShellApplied.text = "\(pendingCF.quotedCurrencyCode) \(FDUtility.formatCurrency(creditFileInfo.amountAlreadyUsed)!)"
    } else {
      heightCreditShellApplied.constant = 0
      labelCreditShellApplied.text = ""
    }
    // update remove NS line
    if let pendingNS = self.bookingPayments?.getPendingNS() {
      heightNSApplied.constant = 25
      labelNSApplied.text = "\(pendingNS.quotedCurrencyCode) \(FDUtility.formatCurrency(pendingNS.quotedAmount)!)"
    } else {
      heightNSApplied.constant = 0
      labelNSApplied.text = ""
    }
    tableView.superview?.isHidden = SharedModel.shared.isWCI
    view.layoutIfNeeded()
  }
  
  func updateTabPositon() {
    var canPaybyCC = paymentMethods.canPayByCreditCard
    var canPaybySA = paymentMethods.canPayBySADAD
    var canPaybyOLP = paymentMethods.canPayBySadadOLP
    var canPaybyCF = paymentMethods.canPayByNasCredit && FDResourceManager.sharedInstance.isSessionLogined
    var canPaybyNasSmiles = canPaybyNS
    if usingBDPromo() {
      canPaybyCC = true
      canPaybySA = false
      canPaybyCF = false
      canPaybyNasSmiles = false
      canPaybyOLP = false
    }
    var btnNumber = 0
    if canPaybyCC { btnNumber += 1 }
    if canPaybySA { btnNumber += 1 }
    if canPaybyCF { btnNumber += 1 }
    if canPaybyNasSmiles { btnNumber += 1 }
    if canPaybyOLP { btnNumber += 1 }
    vwCreditCardButton.isHidden = !canPaybyCC
    vwSadadButton.isHidden = !canPaybySA
    vwNasCreditButton.isHidden = !canPaybyCF
    vwNaSmileButton.isHidden = !canPaybyNasSmiles
    btnSadadOLP.superview!.isHidden = !canPaybyOLP
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    actionDelegate?.onBtnBackClicked()
    self.navigationController?.popViewController(animated: true)
  }
  
  func doCreditCardPayment () {
    if let cardCompanyName = tfCreditCardNumber.cardCompanyName{
      switch cardCompanyName {
      case "Visa":
        paymentDetail.code = "VI"
        break
      case "MasterCard":
        paymentDetail.code = "MC"
        break
      case "American Express":
        paymentDetail.code = "AX"
        break
      default:
        paymentDetail.code = "MC"
        break
      }
    } else {
      paymentDetail.code = "MC"
    }
    if usingAXPromo() && paymentDetail.code != "AX" {
      var message = FDLocalized("Only Amex card can be used for the promo code XXXXX, please enter an Amex card number.")
      message = message.replacingOccurrences(of: "XXXXX", with: userBooking.promoCode)
      UIAlertView.showWithTitle("", message: message, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) in
        self.tfCreditCardNumber.text = ""
        self.tfCardHolderName.text = ""
        self.tfCreditCardExpiryDate.text = ""
        self.tfCVV.text = ""
        self.tfCreditCardNumber.becomeFirstResponder()
      }
      return
    }
    if usingBDPromo() && !isBDCard() {
      var message = FDLocalized("Only Al-Bilad bank card can be used for the promo code XXXXX, please enter an Al-Bilad card number.")
      message = message.replacingOccurrences(of: "XXXXX", with: userBooking.promoCode)
      UIAlertView.showWithTitle("", message: message, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) in
        self.tfCreditCardNumber.text = ""
        self.tfCardHolderName.text = ""
        self.tfCreditCardExpiryDate.text = ""
        self.tfCVV.text = ""
        self.tfCreditCardNumber.becomeFirstResponder()
      }
      return
    }
    vwNextButton.isHidden = true
    //        btnNext.enabled = false
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makePayments(paymentDetail, success:  { (paymentResult, errorMessage) -> Void in
      self.paymentResult = paymentResult
      //                self.btnNext.enabled = true
      self.updateUI()
      self.vwNextButton.isHidden = false
      //            self.btnNext.enabled = true
      if errorMessage != nil {
        SVProgressHUD.dismiss()
        FDErrorHandle.apiCallErrorWithAler(errorMessage!)
      } else {
        // open webview
        if paymentResult.tdsRedirectUrl != nil {
          SVProgressHUD.dismiss()
          self.webView = SVModalWebViewController(address: paymentResult.tdsRedirectUrl!)
          if let webView = self.webView {
            self.clearWebViewEnv ()
            webView.webViewDelegate = self
            self.navigationController!.present(webView, animated: true, completion: { () -> Void in
              self.paymentTimeOutTimer?.invalidate()
              // set Timeout to dissmiss
              self.paymentTimeOutTimer = Timer.scheduledTimer(timeInterval: TimeInterval( self.paymentTimeOutTime), target: self, selector: #selector(FDPaymentVC.paymentTimeOut), userInfo: nil, repeats: false)
            })
          }
        } else {
          // post booking before Polling
          // submit payment
          FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
            self.doBookingPolling()
            }, fail: {(reason) -> Void in
              self.doBookingPolling()
            }
          )
        }
      }
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        self.vwNextButton.isHidden = false
        //                self.btnNext.enabled = true
    })
  }
  
  func doSADADPayment() {
    vwNextButton.isHidden = true
    //        btnNext.enabled = false
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makeSADADPayments({ (paymentResult, errorMessage) -> Void in
      self.paymentResult = paymentResult
      if paymentResult.authorizationStatus == FDPaymentResult.AuthorizationStatusPending && paymentResult.status == FDPaymentResult.StatusReceived{
        // submit payment
        FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
          self.bookingDetails = bookingDetails
          self.bookingPayments = bookingPayments
          if let payment = bookingPayments?.payments.last {
            if !bookingDetails.recordLocator.isEmpty && bookingDetails.status == FDBookingDetails.StatusConfirmed && payment.status == FDPayment.StatusPending && payment.authorizationStatus == FDPayment.StatusPending {
              self.doBookingPolling()
              return
            } else {
              //                            errorMsg.appendContentsOf(" \(FDLocalized("with status"))(\(payment.status)))")
            }
          }
          self.handlePaymentFail ();
          }, fail: {(reason) -> Void in
            self.errorHandle(reason)
          }
        )
      }
      SVProgressHUD.dismiss()
      self.updateUI()
      if errorMessage != nil {
        FDErrorHandle.apiCallErrorWithAler(errorMessage!)
      }
      self.vwNextButton.isHidden = false
      //            self.btnNext.enabled = true
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        self.vwNextButton.isHidden = false
        //                self.btnNext.enabled = true
    })
  }
  
  func doSadadOLPPayment() {
    let olp = unwrap(str: tfSadadOlp.text)
    if olp.isEmpty { tfSadadOlp.becomeFirstResponder(); return }
    let page = FDApiBaseUrl + FDApiVersion + "BookingPolling"
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makeSadadOLPPayments(olpId: olp, returnPage: page, { (paymentResult) in
      var strBody = "customer_email=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.customerEmail))&"
      strBody.append("payment_option=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.paymentOption))&")
      strBody.append("return_url=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.returnUrl).replacingOccurrences(of: "\\", with: ""))&")
      strBody.append("language=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.language))&")
      strBody.append("merchant_identifier=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.merchantIdentifier))&")
      strBody.append("amount=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.amount))&")
      strBody.append("signature=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.signature))&")
      strBody.append("merchant_reference=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.merchantReference))&")
      strBody.append("sadad_olp=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.sadadOlp))&")
      strBody.append("currency=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.currency))&")
      strBody.append("command=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.command))&")
      strBody.append("access_code=\(unwrap(str: paymentResult?.sadadOnlinePaymentData.accessCode))")
      let url = unwrap(str: paymentResult?.sadadOnlinePaymentData.payFortLink)
      var request = URLRequest(url: URL(string: url)!)
      request.httpMethod = "POST"
      request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
      request.httpBody = strBody.data(using: .utf8)
      ConnectionHandler.requestConnection(request: request) { (result) in
        SVProgressHUD.dismiss()
        let source = unwrap(str: result?["raw"])
        let webVC = self.storyboard?.instantiateViewController(withIdentifier: "SadadOlpWebViewVC") as! SadadOlpWebViewVC
        webVC.sourceStr = source
        webVC.delegate = self
        self.present(webVC, animated: true, completion: nil)
      }
    }) { (reason) in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    }
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    view.endEditing(true)
    if agreeToTerms {
      switch paymentMethod {
      case .creditCard:
        doCreditCardPayment()
        break
      case .sadad:
        doSADADPayment()
        break
      case .sadadOLP:
        doSadadOLPPayment()
        break
      case .voucher:
        paymentComplete()
        break
      case .nasCredit:
        doNasCreditPayment()
        break
      case .naSmile:
        doNaSmile()
        break
      case .applePay:
        break
      }
    } else {
      UIAlertView.showWithTitle("", message: FDLocalized("Please accept the Terms and conditions.") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
    }
  }
  
  @IBAction func onBtnAgreeToTermsClicked(_ sender: UIButton) {
    view.endEditing(true)
    agreeToTerms = !agreeToTerms
    updateUI()
  }
  
  @IBAction func onBtnWhatIsCVVClicked(_ sender: UIButton) {
    FDUtility.popInfo(FDLocalized("What is CW2/CVV?"), message: FDLocalized("The CVV Number (Card Verification Value) on your credit card or debit card is a 3 digit number on VISA®, MasterCard®. For American Express cards, CW is 4 digits instead of 3."))
  }
  
  func textFieldEditingDidEnd(_ sender:UITextField) {
    switch (sender) {
    case tfCardHolderName:
      paymentDetail.accountName = sender.text != nil ? sender.text! : ""
      break
    case tfCreditCardNumber:
      if let cardNumber = tfCreditCardNumber.cardNumber {
        paymentDetail.accountNumber = cardNumber
      } else {
        paymentDetail.accountNumber = ""
      }
      break
    case tfCVV:
      paymentDetail.verificationCode = sender.text != nil ? sender.text! : ""
      break
    case tfCreditCardExpiryDate:
      if unwrap(str: sender.text).isEmpty { break }
      paymentDetail.expiryDate = String(format: "%4d-%02d", tfCreditCardExpiryDate.dateComponents.year!,tfCreditCardExpiryDate.dateComponents.month!)
      break
    default:break
    }
    updateUI()
  }
  
  func paymentTimeOut() {
    // todo - check ar
    UIAlertView.showWithTitle("", message: FDLocalized("Payment timeout.") , cancelButtonTitle: FDLocalized("OK") , completionHandler: nil)
    dissmissWebview()
  }
  
  func compareURL(_ originUrl:String, destUrl:String) -> Bool {
    let destLen = destUrl.lengthOfBytes(using: String.Encoding.utf8)
    if originUrl.lengthOfBytes(using: String.Encoding.utf8) >= destLen {
      return originUrl.substring(from: originUrl.index(originUrl.endIndex, offsetBy: -destLen)) == destUrl
    }
    return false
  }
  
  func doBookingPolling() {
    var adt = 0
    var chd = 0
    for passenger in userBooking.passengers {
      if passenger.paxType == "ADT" {
        adt += 1
      } else if passenger.paxType == "CHD" {
        chd += 1
      }
    }
    AdjustSDKMan.ticketPurchase(revenue: paymentMethods.balanceDue.doubleValue,
                                currency: paymentMethods.currencyCode,
                                isBusiness: isBusiness,
                                adults: adt,
                                kids: chd,
                                bag: hasBag,
                                meal: hasMeal)
    self.paymentTimeOutTimer?.invalidate()
    SVProgressHUD.show()
    self.vwNextButton.isHidden = true
    sessionPaymentQueryCount += 1
    FDBookingManager.sharedInstance.getBookingPolling({ (bookingPolling) -> Void in
      self.bookingPolling = bookingPolling
      if bookingPolling.shouldContinuePolling {
        if bookingPolling.maxQueryCount >= bookingPolling.sessionPaymentQueryCount && bookingPolling.maxQueryCount >= self.sessionPaymentQueryCount {
          let pollingInterval = max(self.defaultBookingPollingInterval, bookingPolling.pollingWaitTime)
          self.paymentTimeOutTimer = Timer.scheduledTimer(timeInterval: TimeInterval(pollingInterval*2), target: self, selector: #selector(FDPaymentVC.doBookingPolling), userInfo: nil, repeats: false)
        } else {
          // payment submit but still pending after maxQueryCount
          UIAlertView.showWithTitle("", message: FDLocalized("Payment submitted but still pending.") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
          })
          SVProgressHUD.dismiss()
          self.vwNextButton.isHidden = false
        }
      } else {
        self.checkingBookingStatus()
      }
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        self.vwNextButton.isHidden = false
    })
  }
  
  func checkingBookingStatus() {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getBookingDetail(false, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
      self.vwNextButton.isHidden = false
      SVProgressHUD.dismiss()
      self.bookingDetails = bookingDetails
      self.bookingPayments = bookingPayments
      // credit card status
      if self.paymentMethod == .creditCard {
        if !bookingDetails.recordLocator.isEmpty && bookingDetails.status == FDBookingDetails.StatusConfirmed {
          self.paymentComplete()
          FDEventTrackingSendEvent("Payment", action:"Success", label: "CC")
          return;
        } else {
          // payment fail
          self.handlePaymentFail()
          FDEventTrackingSendEvent("Payment", action:"Fail", label: "CC")
          return
        }
      } else if self.paymentMethod == .sadad {
        if !bookingDetails.recordLocator.isEmpty && bookingDetails.status == FDBookingDetails.StatusConfirmed {
          if bookingPayments != nil {
            if let payment = bookingPayments!.payments.last {
              if payment.authorizationStatus == FDPayment.StatusPending && payment.status == FDPayment.StatusPendingCustomerAction {
                self.paymentComplete()
                FDEventTrackingSendEvent("Payment", action:"Success", label: "SA")
                return
              }
            }
          }
        }
        self.handlePaymentFail ();
        FDEventTrackingSendEvent("Payment", action:"Fail", label: "SA")
      } else {
        self.paymentMethod = .voucher
        self.paymentComplete()
      }
      },fail: { (reason) -> Void in
        self.vwNextButton.isHidden = false
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
    })
  }
  
  @IBAction func onBtnPaymentMethodClicked(_ sender: UIButton) {
    switch sender {
    case btnPaymentCreditCard:
      paymentMethod = .creditCard
      break
    case btnPaymentSADAD:
      paymentMethod = .sadad
      break
    case btnPaymentNasCredit:
      paymentMethod = .nasCredit
      break
    case btnPaymentNaSmile:
      paymentMethod = .naSmile
      break
    case btnSadadOLP:
      paymentMethod = .sadadOLP
    default:
      paymentMethod = .creditCard
    }
    updateUI()
  }
  
  @IBAction func onBtnSadadInfoClicked(_ sender: UIButton) {
    FDUtility.popInfo(FDLocalized("Payment Channels"), message: FDLocalized("1-Automated Teller Machine (ATM)\n2- Internet Banking\n3- Bank Branch\n4- Phone Banking(IVR)"))
  }
  
  func paymentComplete() {
    if self.userBooking.bookingFlow == BookingFlow.normalBooking {
      if seatMapApi == .booking {
        self.performSegue(withIdentifier: "Confirmation", sender: nil)
      } else {
        FDWebCheckinManager.sharedInstance.checkIn(nil, success: { () -> Void in
          SVProgressHUD.dismiss()
          // go to complete
          self.pushViewController("WebCheckin", storyboardID: "FDWCiCompleteVC") { (vc) -> Void in
            let c = vc as! FDWCiCompleteVC
            c.checkinJourney = self.checkinJourney
          }
          }, fail:{ (reason) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(reason)
        })
      }
    } else {
      // mmb
      self.navigationController?.popViewController(animated: true)
      if let actionDelegate = actionDelegate {
        actionDelegate.updatePage()
        actionDelegate.onBtnContinueClicked()
      }
    }
  }
  
  @IBAction func onBtnTermClicked(_ sender: UIButton) {
    onBtnAgreeToTermsClicked(sender)
    FDUtility.onBtnTermClicked(self)
  }
  
  func handlePaymentFail () {
    let errorMsg = FDLocalized("The payment was not successful, please try again")
    if !self.errorHandleAndSwitchToSearchIfExpired(errorMsg) {
      // call payment method again to reset the payment
      getPaymentMethods();
    }
  }
  
  func getPaymentMethods () {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getPaymentMethods({ (paymentMethods) -> Void in
      self.paymentMethods = paymentMethods
      // payment button and views
      if !paymentMethods.canPayByCreditCard && self.paymentMethod == .creditCard {
        self.paymentMethod = .sadad
      }
      FDBookingManager.sharedInstance.getBookingDetail(false, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
        self.bookingDetails = bookingDetails
        self.bookingPayments = bookingPayments
        if self.bookingDetails.hasLoyaltyPayment && self.canPaybyNS {
          self.paymentNSVC.nsMin = 0
        }
        if self.paymentMethods.canPayByNasCredit && FDResourceManager.sharedInstance.isSessionLogined {
          FDBookingManager.sharedInstance.getCreditFileInfo({ (creditFileInfo) in
            self.creditFileInfo = creditFileInfo
            SVProgressHUD.dismiss()
            self.updateUI()
            }, fail: { (reason) in
              self.errorHandle(reason)
              SVProgressHUD.dismiss()
              self.updateUI()
          })
        } else {
          SVProgressHUD.dismiss()
          self.updateUI()
        }
      
        }, fail: {(reason) -> Void in
          SVProgressHUD.dismiss()
          self.updateUI()
        }
      )
      }, fail: { (reason) -> Void in
        self.updateUI()
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
    })
  }
  
  func doNasCreditPayment () {
    // check amount input
    guard let amountStr = tfCreditShellAmount.text, let cfAmount = Double(amountStr), self.creditFileInfo.creditFileAvailable && cfAmount > 0 && cfAmount <= self.creditFileInfo.availableAmount.doubleValue else {
      let errorMsg = FDLocalized("Please input correct amount.")
      UIAlertView.showWithTitle("", message: errorMsg, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      return
    }
    
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makeCFPayments(cfAmount, success: { (paymentResult, errorMessage) -> Void in
      
      self.tfCreditShellAmount.text = ""
      self.paymentResult = paymentResult
      
      if paymentResult.balanceDue == 0 {
        // submit payment
        FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
          self.bookingDetails = bookingDetails
          self.bookingPayments = bookingPayments
          
          if !bookingDetails.recordLocator.isEmpty {
            self.doBookingPolling()
            return
          }
          
          SVProgressHUD.dismiss()
          self.handlePaymentFail ();
          
          }, fail: {(reason) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(reason)
          }
        )
      } else {
        SVProgressHUD.dismiss()
        self.paymentMethod = .creditCard
        self.getPaymentMethods()
      }
      
      if errorMessage != nil {
        FDErrorHandle.apiCallErrorWithAler(errorMessage!)
      }
      
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        self.vwNextButton.isHidden = false
        //                self.btnNext.enabled = true
    })
    
    return
    
  }
  
  func doNaSmile () {
    // check amount input
    guard let redeemAmount = paymentNSVC.redeemAmount, let nasMemberAccountId = paymentNSVC.memberLogin?.nasLoyaltyNumber else {
      let errorMsg = FDLocalized("Invalid loyalty details.")
      UIAlertView.showWithTitle("", message: errorMsg, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      
      return
    }
    
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makeNSPayments(redeemAmount, nasMemberAccountId:nasMemberAccountId, success: { (paymentResult, errorMessage) -> Void in
      self.paymentResult = paymentResult
      if paymentResult.balanceDue == 0 {
        // submit payment
        FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
          self.bookingDetails = bookingDetails
          self.bookingPayments = bookingPayments
          if !bookingDetails.recordLocator.isEmpty {
            self.doBookingPolling()
            return
          }
          SVProgressHUD.dismiss()
          self.handlePaymentFail ();
          }, fail: {(reason) -> Void in
            SVProgressHUD.dismiss()
            self.errorHandle(reason)
          }
        )
      } else {
        SVProgressHUD.dismiss()
        self.paymentMethod = .creditCard
        self.getPaymentMethods()
      }
      if errorMessage != nil {
        FDErrorHandle.apiCallErrorWithAler(errorMessage!)
      }
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
        _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        self.vwNextButton.isHidden = false
        //                self.btnNext.enabled = true
    })
    return
  }
  
  @IBAction func onBtnNSRemovePayment(_ sender: AnyObject) {
    // remove CF payment
    SVProgressHUD.show()
    if let pendingNS = self.bookingPayments?.getPendingNS() {
      FDBookingManager.sharedInstance.removePayment(pendingNS.paymentNumber.intValue, success: { () in
        self.getPaymentMethods()
        }, fail: { (reason) in
          self.errorHandle(reason)
          SVProgressHUD.dismiss()
          self.updateUI()
      })
    }
  }
  
  @IBAction func onBtnCFRemovePayment(_ sender: UIButton) {
    // remove CF payment
    SVProgressHUD.show()
    if let pendingCF = self.bookingPayments?.getPendingCF() {
      FDBookingManager.sharedInstance.removePayment(pendingCF.paymentNumber.intValue, success: { () in
        self.getPaymentMethods()
        }, fail: { (reason) in
          self.errorHandle(reason)
          SVProgressHUD.dismiss()
          self.updateUI()
      })
    }
  }
  
  @IBAction func onBtnNASCreditAmount(_ sender: UIButton) {
    tfCreditShellAmount.becomeFirstResponder()
  }
  
  @IBAction func onBtnCCCardNumberField(_ sender: UIButton) {
    tfCreditCardNumber.becomeFirstResponder()
  }
  
  @IBAction func onBtnCCHolderNameField(_ sender: UIButton) {
    tfCardHolderName.becomeFirstResponder()
  }
  
  @IBAction func onBtnCCExpiryDateField(_ sender: UIButton) {
    tfCreditCardExpiryDate.becomeFirstResponder()
  }
  
  @IBAction func onBtnCCVField(_ sender: UIButton) {
    tfCVV.becomeFirstResponder()
  }
  
}

// MARK: - WEB VIEW
extension FDPaymentVC: UIWebViewDelegate {
  
  func clearWebViewEnv () {
    print("clearWebViewEnv")
    URLCache.shared.removeAllCachedResponses()
    FDClearCookies()
  }
  
  func webViewDidStartLoad(_ webView: UIWebView) {
  }
  
  func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    //        webView.contentMode = UIViewContentMode.ScaleAspectFit
    webView.scalesPageToFit = true
    FDPrintCookies()
    if let url = request.url?.absoluteString {
      print("\n Will load url=\(url)\n")
      if url.contains(successUrl) {
        dissmissWebview()
        self.paymentTimeOutTimer?.invalidate()
        sessionPaymentQueryCount = 0
        doBookingPolling()
      } else if url.contains(failUrl) {
        dissmissWebview()
        self.paymentTimeOutTimer?.invalidate()
        doBookingPolling()
      }
    }
    SVProgressHUD.show()
    return true
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    SVProgressHUD.dismiss()
  }
  
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    errorHandle(error.localizedDescription)
    self.dissmissWebview()
  }
  
  func dissmissWebview() {
    self.webView = nil
    dismiss(animated: true, completion: nil)
    SVProgressHUD.dismiss()
  }
  
}

//MARK: CARD IO
extension FDPaymentVC : CardIOPaymentViewControllerDelegate {
  
  @IBAction func onBtnScanCreditCard(_ sender: AnyObject) {
    let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
    cardIOVC?.modalPresentationStyle = .formSheet
    present(cardIOVC!, animated: true, completion: nil)
  }
  
  func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
    //        resultLabel.text = "user canceled"
    paymentViewController?.dismiss(animated: true, completion: nil)
  }
  
  func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
    if let info = cardInfo {
      //            tfCreditCardNumber.text = info.cardNumber
      //            tfCVV.text = info.cvv
      //            tfCreditCardExpiryDate.dateComponents.year = Int(info.expiryYear)
      //            tfCreditCardExpiryDate.dateComponents.month = Int(info.expiryMonth)
      paymentDetail.accountNumber = info.cardNumber
      paymentDetail.verificationCode = info.cvv
      paymentDetail.expiryDate = String(format: "%4d-%02d", info.expiryYear,info.expiryMonth)
      updateUI()
      //            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
      //            let result = str as String
      print (info)
    }
    paymentViewController?.dismiss(animated: true, completion: nil)
  }
  
}

//MARK: FPaymentNSVC
extension FDPaymentVC : FPaymentNSVCDelegate {
  
  func ApplyClicked() {
    doNaSmile()
  }
  
  func NaSmilesTermClicked() {
    let url = isEn()
      ? "http://www.flynas.com/en/nasmiles-terms-and-conditions"
      : "http://www.flynas.com/ar/nasmiles-terms-and-conditions"
    let webView = SVModalWebViewController(address: url)
    self.present(webView!, animated: true, completion: { () -> Void in
    })
  }
  
}

//MARK: TABLE VIEW
extension FDPaymentVC : UITableViewDelegate, UITableViewDataSource {
  
  func updatePaymentSummaryTableHeight() {
    heightPaymentSummaryTable.constant = (tableView.sectionFooterHeight + tableView.sectionHeaderHeight) * CGFloat(numberOfSections(in: tableView)) + tableView.rowHeight * CGFloat(paymentVM.getTotalRowNumber())
    view.layoutIfNeeded()
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return paymentVM.getSectionNumber()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return paymentVM.getRowNumberInSection(section)
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDPaymentSummaryCell") {
      if let feeLabel = cell.viewWithTag(PaymentSummaryLabelLeft) as? UILabel {
        feeLabel.text = paymentVM.getFeeLabelString(indexPath.section, row:indexPath.row)
        if unwrap(str: feeLabel.text).contains(FDLocalized("Meal")) {
          AdjustSDKMan.mealPurchase()
          hasMeal = true
        } else if unwrap(str: feeLabel.text).contains(FDLocalized("Seat")) {
          AdjustSDKMan.seatPurchase()
        } else if unwrap(str: feeLabel.text).contains(FDLocalized("Business")) {
          isBusiness = true
        } else if unwrap(str: feeLabel.text).contains(FDLocalized("Baggage")) {
          hasBag = true
        }
      }
      if let feeAmountLabel = cell.viewWithTag(PaymentSummaryLabelRight) as? UILabel {
        feeAmountLabel.text = paymentVM.getFeeAmountString(indexPath.section, row:indexPath.row)
      }
      return cell
    }
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDPaymentSummaryHeader") {
      if let headerTitleLable = cell.viewWithTag(PaymentSummaryLabelLeft) as? UILabel {
        headerTitleLable.text = paymentVM.getSectionTitle(section)
      }
      return cell.contentView
    }
    return nil
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDPaymentSummaryCell") {
      if let feeLabel = cell.viewWithTag(PaymentSummaryLabelLeft) as? UILabel {
        feeLabel.text = paymentVM.getFooterFeeLabelString(section)
        feeLabel.font = fontBold(16.0)
      }
      if let feeAmountLabel = cell.viewWithTag(PaymentSummaryLabelRight) as? UILabel {
        feeAmountLabel.text = paymentVM.getFooterFeeAmountString(section, ub: userBooking)
        feeAmountLabel.font = fontBold(16.0)
      }
      return cell.contentView
    }
    return nil
  }
  
}

//MARK: PROMO CODE
extension FDPaymentVC {
  
  func isBDCard() -> Bool {
    let bdCardInitials = ["428334", "428335", "433072", "424261", "457796", "424262", "424260", "481207", "481206", "446392", "475078", "400795", "475100", "475153", "475109"]
    for cardInitial in bdCardInitials {
      if tfCreditCardNumber.cardNumber.hasPrefix(cardInitial) {
        return true
      }
    }
    return false
  }
  
  func usingAXPromo() -> Bool {
    let hasAX = userBooking.promoCode.uppercased().hasPrefix("AX")
    //    var isPromoFlight = false
    //    for fare in userBooking.getFlights()[0].fares {
    //      if fare.promoFare {
    //        isPromoFlight = true
    //        break
    //      }
    //    }
    return hasAX /*&& isPromoFlight*/
  }
  
  func usingBDPromo() -> Bool {
    let hasBD = userBooking.promoCode.uppercased().hasPrefix("BD")
    return hasBD
  }
  
}

//MARK: - APPLE PAY
extension FDPaymentVC {
  
  func setupApplePayWithDelay() {
    SVProgressHUD.show()
    delay(3.0) {
      SVProgressHUD.dismiss()
      self.addApplePayButon()
    }
  }
  
  func setupApplePayButton() {
    addApplePayButon()
    if ApplePayHandler.status().canMakePayments {
      vwNextButton.isHidden = true
    } else {
      vwApplePayButtonBase.isHidden = true
      vwOtherPaymentsHeader.isHidden = true
      onBtnShowOtherPaymentOptions(UIButton())
    }
    vwPayMethods.layoutIfNeeded()
  }
  
  func addApplePayButon() {
    let btnApplePay = ApplePayHandler.getPaymentButton()
    btnApplePay.frame = CGRect(x: 0.0, y: 0.0, width: vwApplePayButtonBase.bounds.size.width, height: 44.0)
    if ApplePayHandler.status().canMakePayments && ApplePayHandler.status().hasAddedCards {
      btnApplePay.addTarget(self, action: #selector(self.onBtnApplePay), for: .touchUpInside)
    } else if ApplePayHandler.status().canMakePayments {
      btnApplePay.addTarget(self, action: #selector(self.onBtnSetup), for: .touchUpInside)
    }
    for subview in vwApplePayButtonBase.subviews { subview.removeFromSuperview() }
    vwApplePayButtonBase.addSubview(btnApplePay)
  }
  
  func onBtnApplePay() {
    if !agreeToTerms {
      UIAlertView.showWithTitle("", message: FDLocalized("Please accept the Terms and conditions.") , cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
      })
      return
    }
    let totalPrice = String(describing: paymentMethods.balanceDue)
    applePayHandler.startPayment(totalPrice) { (success, item) in
      let vc = getTopmostVC()
      vc?.dismiss(animated: true, completion: nil)
      if success {
        self.doApplePayPayment(item)
      }
    }
  }
  
  func doApplePayPayment(_ applePayItem: APStruct) {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.makeApplePayPayments(applePayItem, success: { (paymentResult, errorMessage) in
      self.paymentResult = paymentResult
      self.updateUI()
      if errorMessage != nil {
        SVProgressHUD.dismiss()
        FDErrorHandle.apiCallErrorWithAler(errorMessage!)
      } else {
        FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
          self.doBookingPolling()
          }, fail: {(reason) -> Void in
            self.doBookingPolling()
          }
        )
      }
    }) { (error) in
      SVProgressHUD.dismiss()
      _ = self.errorHandleAndSwitchToSearchIfExpired(error)
    }
  }
  
  func onBtnSetup() {
    ApplePayHandler.setupCards()
  }
  
  @IBAction func onBtnShowOtherPaymentOptions(_ sender: UIButton) {
    vwOtherPaymentsHeader.isHidden = true
    vwOtherPaymentButtons.isHidden = false
    vwOtherPaymentDetails.isHidden = false
    vwNextButton.isHidden = false
    //    btnNext.hidden = false
    //    heightPayMethods.constant = 329.0//304.0
    vwPayMethods.layoutIfNeeded()
    updateUI()
  }
  
}

//MARK: - SadadOlpVCDelegate
extension FDPaymentVC: SadadOlpVCDelegate {
  
  func shouldDoPolling() {
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getBookingDetail(true, success: { (bookingDetails, bookingPayments, _,_,_,_,_,_,_) -> Void in
      self.doBookingPolling()
    }, fail: {(reason) -> Void in
      self.doBookingPolling()
    }
    )
  }
  
}
