//
//  String+FDExtension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 1/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import Foundation
import UIKit


extension String {

    var is10Alphanumeric: Bool {
        // (10 alphanumeric)
        if self.range(of: "^[a-z0-9A-Z]{10}$", options: .regularExpression) != nil {
            return true
        }

        return false
    }

    var isAlphanumeric: Bool {
        // (10 alphanumeric)
        if self.range(of: "^[a-z0-9A-Z]+$", options: .regularExpression) != nil {
            return true
        }

        return false
    }

    var isValidateName: Bool {
        // (10 alphanumeric)
        if self.range(of: "^[a-zA-Z ]+$", options: .regularExpression) != nil {
            return true
        }

        return false
    }

    func substringToIndex(_ i: Int) -> String? {
        if self.characters.count > i {
            return self.substring(to: self.characters.index(self.startIndex, offsetBy: i))
        }

        return nil
    }

    func substringFromIndex(_ i: Int, toIndex:Int) -> String? {
        if self.characters.count > i {
            return self.substring( with: self.characters.index(self.startIndex, offsetBy: i)..<self.characters.index(self.startIndex, offsetBy: toIndex) )
        }

        return nil
    }
}
