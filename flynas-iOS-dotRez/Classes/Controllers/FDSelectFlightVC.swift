//
//  FDSelectFlightVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 12/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

protocol FDSelectFlightVCDelegate : class {
    func selectedFlight(_ flights:[Int])
    func selectFlightCancel()
}

class FDSelectFlightVC: BaseVC, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet var tableView: UITableView!
    @IBOutlet var heightTableView: NSLayoutConstraint!

    weak var delegate : FDSelectFlightVCDelegate?

    var managingBookingType = ManagingBookingType.cancelling
    var forceSelectAll = false

    var selectedFligh = [Int]()
    var flights = [FDFlight]() {
//    var legs = [FDLeg]() {
        didSet {
            selectedFligh = [Int]()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
      log("select_flight_popup")
        tableView.delegate = self
        tableView.dataSource = self

        heightTableView.constant = CGFloat (58 * flights.count)

    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flights.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FDMyBookingTableViewCell2", for: indexPath) as! FDMyBookingTableViewCell2

        let flight = flights[indexPath.row]
        let originStation = FDResourceManager.sharedInstance.getStation(flight.origin)
        let destinationStation = FDResourceManager.sharedInstance.getStation(flight.destination)

        cell.labelOrigin.text = originStation?.name
        cell.labelDestination.text = destinationStation?.name

        if let utcDepartureDate = flight.departureDate {
            cell.labelFlightTime.text = utcDepartureDate.toStringUTC(format:.custom("HH:mm EEE, dd MMM yyyy"))
        } else {
            cell.labelFlightTime.text = ""
        }

        if editAllowed(indexPath:indexPath) {
            cell.imgTickBg.isHidden = false
            cell.imgTicked.isHidden = !selectedFligh.contains(indexPath.row)
        } else {
            cell.imgTickBg.isHidden = true
            cell.imgTicked.isHidden = true
        }

        return cell
    }

    func editAllowed(indexPath: IndexPath) -> Bool {
        return editAllowed(index:indexPath.row)
    }

    func editAllowed(index: Int) -> Bool {
        let flight = flights[index]
        var allowed = flight.cancellationAllowed
        if managingBookingType == .changing {
            allowed = flight.changeAllowed
        }
        return allowed
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if editAllowed(indexPath:indexPath) {
            if selectedFligh.contains(indexPath.row) {

                if forceSelectAll {
                    selectedFligh.removeAll()
                } else {
                    selectedFligh.remove(at: selectedFligh.index(of: Int(indexPath.row))!)
                }
            } else {
                if forceSelectAll {
                    selectedFligh.removeAll()
                    for i in 0..<flights.count {
                        if editAllowed(index:i) {
                            selectedFligh.append(i)
                        }
                    }
                } else {
                    selectedFligh.append(indexPath.row)
                }
            }

            if forceSelectAll {
                tableView.reloadData()
            } else {
                if let cell = tableView.cellForRow(at: indexPath) as? FDMyBookingTableViewCell2 {
                    cell.imgTicked.isHidden = !selectedFligh.contains(indexPath.row)
                }
            }
        }

    }
    
    @IBAction func onBtnContinueClicked(_ sender: UIButton) {
        if selectedFligh.count > 0 {
            delegate?.selectedFlight(selectedFligh)
        } else {
            delegate?.selectFlightCancel()
        }
    }

    @IBAction func onBtnCloseClicked(_ sender: UIButton) {
        delegate?.selectFlightCancel()
    }

}
