//
//  FDImageView.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 28/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDImageView: UIImageView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setImageAutoFlip(image)
    }

    func setImageAutoFlip(_ image:UIImage?) {
        if isRTL() {
            self.image = flipImage(image)
        } else {
            self.image = image
        }
    }
}
