//
//  FDWCiCompleteVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWCiCompleteVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
  
  var checkinJourney = FDWciJourney() {
    didSet {
      if checkinJourney.segments.count > 0 {
        checkinPassengers = checkinJourney.segments[0].passengers.filter({$0.prepareToCheckin})
      }
    }
  }
  var checkinPassengers = [FDWciPassenger]()
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heighPassengerTableView: NSLayoutConstraint!
  @IBOutlet var labelDepartDate: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("web_checkin_confirmation_page")
    FDRM.sharedInstance.latestWciPNR = checkinJourney.pnr
    tableView.dataSource = self
    tableView.delegate = self
    updateUI ()
  }
  
  func updateUI () {
    heighPassengerTableView.constant = CGFloat((58 * (checkinPassengers.count + 1)) * (checkinJourney.segments.count))
    labelDepartDate.text = checkinJourney.departureDate?.toStringUTC(format:.custom("EEEE, dd MMM yyyy"))
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func numberOfSections(in tableView: UITableView) -> Int {
    return checkinJourney.segments.count
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDWciLegInfoCell") as? FDWciLegInfoCell {
      
      cell.segment = checkinJourney.segments[section]
      return cell.contentView
    }
    
    return UITableViewHeaderFooterView()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return checkinPassengers.count
  }
  let SeatPassengerName = 31
  let SeatNumber = 33
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = self.tableView.dequeueReusableCell(withIdentifier: "FDWciPassengerInfoCell") {
      let passengers = checkinJourney.segments[indexPath.section].passengers.filter({$0.prepareToCheckin})
      let passenger = passengers[indexPath.row]
      if let seatPassengerName = cell.viewWithTag(SeatPassengerName) as? UILabel {
        seatPassengerName.text = passenger.fullName
      }
      if let seatNumber = cell.viewWithTag(SeatNumber) as? UILabel {
        seatNumber.text = passenger.unitDesignator
      }
      return cell
    }
    return UITableViewCell()
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    AdjustSDKMan.confirmTicket(isBusiness: true)
    navigationController?.popToRootViewController(animated: true)
    delay(1.0) {
      postNotification(ContainerVC.kBtnPasses, userInfo: [:])
    }
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
}
