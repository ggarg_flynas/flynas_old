//
//  FDFlightConnectionInfoVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 25/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightConnectionInfoVC: BaseVC, UITableViewDelegate, UITableViewDataSource {

    let heightFlightInfo = 64
    let heightFlightTrasiInfo = 36
    weak var delegate : FDPopOutVCDelegate?
    var flight = FDFlight()
    @IBOutlet var tableView: UITableView!
    @IBOutlet var heightTableView: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("connection_popup")
        tableView.delegate = self
        tableView.dataSource = self
        heightTableView.constant = CGFloat ((heightFlightInfo + heightFlightTrasiInfo) * self.tableView(tableView, numberOfRowsInSection: 0) - heightFlightTrasiInfo)
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flight.legs.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FDFlightConnectionInfoTableViewCell", for: indexPath) as! FDFlightConnectionInfoTableViewCell

        let leg = flight.legs[indexPath.row]
        let originStation = FDResourceManager.sharedInstance.getStation(leg.origin)
        let destinationStation = FDResourceManager.sharedInstance.getStation(leg.destination)

        if (originStation != nil && destinationStation != nil && leg.departureDate != nil && leg.arrivalDate != nil && leg.utcArrivalDate != nil && leg.utcDepartureDate != nil) {
            cell.lableFlightInfoTitle.text = "\(FDLocalized("Departing flight")) \(indexPath.row+1) \(leg.carrierCode) \(leg.flightNumber)"

            cell.lableFlightInfoDeparting.text = "\(FDLocalized("Departing:")) \(leg.departureDate!.toStringUTC(format: .custom("HH:mm, EEE, dd MMM yyyy"))) - \(originStation!.name)"
            //Departing  2:25PM, Wed, 15 Feb 2015 - Dubai

            cell.lableFlightInfoArrives.text = "\(FDLocalized("Arrives:")) \(leg.arrivalDate!.toStringUTC(format: .custom("HH:mm, EEE, dd MMM yyyy"))) - \(destinationStation!.name)"
            //"Arrives: 3:00 PM, Wed 15, 2015 - Abu Dhabi"

            cell.lableFlightInfoDuration.text = "\(FDLocalized("Flight duration:")) \(FDUtility.formatDuration(leg.utcDepartureDate!, end: leg.utcArrivalDate!))"
            //Flight duration: 45 min
        }

        if indexPath.row < flight.legs.count - 1 {
            // transition info
            let leg2 = flight.legs[indexPath.row+1]

            if let originStation = FDResourceManager.sharedInstance.getStation(leg2.origin) {
                cell.lableFlightInfoTransit.text = "\(FDLocalized("Transit in")) \(originStation.name) \(FDLocalized("for")) \(FDUtility.formatDuration(leg.utcArrivalDate!, end: leg2.utcDepartureDate!))"
            }
            //"Transit in Abu Dhabi for 45 mins"
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.row == self.tableView(tableView, numberOfRowsInSection: 0) - 1 {
            return CGFloat (heightFlightInfo)
        }

        return CGFloat (heightFlightInfo + heightFlightTrasiInfo)
    }

    @IBAction func onBtnCancel(_ sender: UIButton) {
        self.view.endEditing(true)
        delegate?.dismissInfo()
    }

}
