//
//  PaymentNSVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 13/12/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

protocol FPaymentNSVCDelegate : class {
  func ApplyClicked()
  func NaSmilesTermClicked()
}

class PaymentNSVC: UIViewController, AKPickerViewDataSource, AKPickerViewDelegate {
  
  @IBOutlet var tfEmail: UITextField!
  @IBOutlet var tfPassword: UITextField!
  
  @IBOutlet var viewLoginLeft: NSLayoutConstraint!
  @IBOutlet var viewSliderLeft: NSLayoutConstraint!
  
  @IBOutlet var pickerView: AKPickerView!
  
  @IBOutlet var lbNsBalance: UILabel!
  @IBOutlet var lbRedeemAmount: UILabel!
  
  @IBOutlet var lbTermsEn: UILabel!
  @IBOutlet var lbTermsAr: UILabel!
  
  weak var delegate : FPaymentNSVCDelegate?
  
  var nsMin : Int = 2000
  var nsMax : Int = 10000
  var nsStep : Int = 50
  var selectedValue: Int = 5000
  
  //    let BalanceLimit = 10000
  
  var memberLogin: FDNaSmileLoyalty?
  
  var redeemAmount : Float? {
    guard let memberLogin = memberLogin else {return nil}
    return memberLogin.getValue(selectedValue)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    #if DEBUG
      testData()
    #endif
    
    self.pickerView.delegate = self
    self.pickerView.dataSource = self
    
    self.pickerView.font = UIFont(name: "HelveticaNeue-Bold", size: 11)!
    self.pickerView.textColor = FDColorFlynasBlack
    self.pickerView.highlightedFont = UIFont(name: "HelveticaNeue-Bold", size: 11)!
    self.pickerView.highlightedTextColor = FDColorFlynasGreenText
    self.pickerView.pickerViewStyle = .flat
    self.pickerView.maskDisabled = true
    self.pickerView.reloadData()
    
    self.lbTermsEn.isHidden = isAr()
    self.lbTermsAr.isHidden = isEn()
    
    view.layoutIfNeeded()
    let _ = validateNasMemberAccountId()
    let _ = validatePass()
  }
  #if DEBUG
  func testData () {
    tfEmail.text = "1874635263"
    tfPassword.text = "Test1234"
  }
  #endif
  
  func initPanelPosition() {
//    view.layoutIfNeeded()
    viewSliderLeft.constant = view.frame.width
  }
  
  @IBAction func onBtnLoginClicked(_ sender: UIButton) {
    retriveNaSmileBalance()
  }
  
  func retriveNaSmileBalance() {
    
    self.view.endEditing(true)
    if let nasMemberAccountId = validateNasMemberAccountId(), let pass = validatePass() {
      
      SVProgressHUD.show()
      FDNaSmileManager.sharedInstance.manualLogin(nasMemberAccountId, password: pass, accountInfo: false, success: { (memberLogin) in
        SVProgressHUD.dismiss()
        
        //                if memberLogin.balancePoints >= self.BalanceLimit {
        if memberLogin.maxRedeemablePoint >= memberLogin.minRedeemablePoint {
          self.memberLogin = memberLogin
          self.nsMax = memberLogin.maxRedeemablePoint
          self.nsMin = memberLogin.minimumLoyaltyThreshold
          self.pickerView.reloadData()
          
          UIView.animate(withDuration: 0.5, animations: {
            self.viewLoginLeft.constant = -self.view.frame.width
            self.viewSliderLeft.constant = 0
            self.view.layoutIfNeeded()
          })
          
          self.onBtnMaxClicked(nil)
          //                    } else {
          //                        self.errorHandle(FDErrorHandle.API_CANNOT_PAY_BY_NASMILE)
          //                    }
        } else {
          self.errorHandle(FDErrorHandle.API_NASMILE_BALANCE_LIMITE)
        }
      }) { (reason) in
        SVProgressHUD.dismiss()
        self.errorHandle(reason)
      }
    }
    
  }
  
  func resetHighLight() {
    tfEmail.superview!.markError(false)
    tfPassword.superview!.markError(false)
  }
  
  func validateNasMemberAccountId() -> String? {
    resetHighLight()
    if let nasMemberAccountId = tfEmail.text {
      if !nasMemberAccountId.isEmpty {
        return nasMemberAccountId
      }
    }
    tfEmail.superview!.markError(true)
    return nil
    
  }
  
  func validatePass() -> String? {
    resetHighLight()
    if let pass = tfPassword.text {
      if !pass.isEmpty {
        return pass
      }
    }
    tfPassword.superview!.markError(true)
    return nil
    
  }
  
  @IBAction func onBtnMaxClicked(_ sender: AnyObject?) {
    let lastItemm = numberOfItemsInPickerView(pickerView)
    if lastItemm > 0 {
      pickerView.scrollToEnd()
      selectedValue = nsMin + nsStep * (lastItemm - 1)
      updateUI()
    }
  }
  
  @IBAction func onBtnMinClicked(_ sender: AnyObject?) {
    if numberOfItemsInPickerView(pickerView) > 0 {
      pickerView.scrollToItem(0)
      selectedValue = nsMin
      self.updateUI()
    }
  }
  
  @IBAction func onBtnApplyClicked(_ sender: AnyObject?) {
    delegate?.ApplyClicked()
  }
  
  // MARK: - AKPickerViewDataSource
  func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
    return (nsMax - nsMin) / nsStep
  }
  
  func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
    
    let value = nsMin + nsStep * item
    return value.description
  }
  
  func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
    selectedValue = nsMin + nsStep * item
    updateUI()
  }
  
  func updateUI () {
    if let memberLogin = memberLogin {
      lbNsBalance.text = memberLogin.balancePoints.description
      lbRedeemAmount.text = FDUtility.formatCurrencyWithCurrencySymbol(redeemAmount as! NSNumber)
    } else {
      lbNsBalance.text = ""
      lbRedeemAmount.text = ""
    }
  }
  
  @IBAction func onBtnTermsClicked(_ sender: AnyObject?) {
    
    delegate?.NaSmilesTermClicked()
    
  }
  
  @IBAction func onBtnEmailField(_ sender: UIButton) {
    tfEmail.becomeFirstResponder()
  }
  
  @IBAction func onBtnPasswordField(_ sender: UIButton) {
    tfPassword.becomeFirstResponder()
  }
  
}

extension PaymentNSVC: UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    let _ = validateNasMemberAccountId()
    let _ = validatePass()
  }
  
}
