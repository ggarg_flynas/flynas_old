//
//  FDWciLegInfoCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 21/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWciLegInfoCell: UITableViewCell {

    @IBOutlet var labelFlightDirection: UILabel!
    @IBOutlet var imgFlightDirection: FDImageView!
    @IBOutlet var labelDepartTime: UILabel!
    @IBOutlet var labelArriveTime: UILabel!
    @IBOutlet var labelOriginCityName: UILabel!
    @IBOutlet var labelDestinationCityName: UILabel!
    @IBOutlet var labelFlightCode: UILabel!
    @IBOutlet var labelDuration: UILabel!
    @IBOutlet var labelStops: UILabel!

    var segment : FDWciSegment? {
        didSet {
            if segment != nil {
                labelDepartTime.text = segment!.departureDate?.toStringUTC(format:.custom("HH:mm"))
                labelArriveTime.text = FDUtility.arrivalDateHHMMString(segment!.departureDate!, arrivalDate: segment!.arrivalDate!)

                let originStation = FDResourceManager.sharedInstance.getStation(segment!.departureStation)
                let destinationStation = FDResourceManager.sharedInstance.getStation(segment!.arrivalStation)

                labelOriginCityName.text = originStation?.name
                labelDestinationCityName.text = destinationStation?.name
//                labelOriginAirportName.text = FDRM.sharedInstance.countryNameByCode(originStation?.country)
//                labelDestinationAirprotName.text = FDRM.sharedInstance.countryNameByCode(destinationStation?.country)
                labelFlightCode.text = FDLocalized("Flight ") + segment!.flightDesignator
                labelStops.text = ""
                labelDuration.text = FDUtility.getDurationString(segment?.duration)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
