//
//  FDPassengerTableCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDPassengerTableCellDelegate : class  {
  func btnExpandClicked(_ cell:FDPassengerTableCell)
  func btnSelPassengerClicked(_ cell:FDPassengerTableCell)
  func updateTableCellHeight(_ cell:FDPassengerTableCell)
}

class FDPassengerTableCell: UITableViewCell {
  
  @IBOutlet var btnToggleExpand: UIButton!
  @IBOutlet var viewPassengerDetail: UIView!
  @IBOutlet var labelPassengeTitle: UILabel!
  @IBOutlet weak var tfTitle: UITextField!
  @IBOutlet weak var imvHijri: UIImageView!
  @IBOutlet var tfFirstName: UITextField!
  @IBOutlet var tfLastName: UITextField!
  @IBOutlet weak var tfDOB: UITextField!
  @IBOutlet var tfDocumentNumber: UITextField!
  @IBOutlet weak var tfExpirationDate: UITextField!
  @IBOutlet var btnSelPassenger: UIButton!
  @IBOutlet var tfDiscountCode: UITextField!
  @IBOutlet weak var tfDocumentType: UITextField!
  @IBOutlet weak var tfIssuingCountry: UITextField!
  @IBOutlet weak var tfNationality: UITextField!
  @IBOutlet weak var imvExpiryDateHijri: UIImageView!
  
  static let HeightNaSmiles: CGFloat = 45.0
  var isDomestic = false {
    didSet {
      if isDomestic && !SharedModel.shared.isDocTypeSet {
        passenger.travelDocument.docTypeCode = "I"
        updatePassengersLayout()
        SharedModel.shared.isDocTypeSet = true
      }
    }
  }
  var expanded = true {
    didSet {
      updatePassengersLayout()
    }
  }
  var documentTypes = FDDocumentTypes()
  var passenger = FDPassenger(paxType: .Adult, passengerNumber: 0) {
    didSet {
      updatePassengersDetail()
    }
  }
  weak var delegate: FDPassengerTableCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    tfDocumentNumber.addTarget(self, action: #selector(FDPassengerTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfDocumentNumber.addTarget(self, action: #selector(FDPassengerTableCell.textFieldDidChange(_:)), for: .editingChanged)
    tfFirstName.addTarget(self, action: #selector(FDPassengerTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfLastName.addTarget(self, action: #selector(FDPassengerTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfDiscountCode.addTarget(self, action: #selector(FDPassengerTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
    tfDiscountCode.addTarget(self, action: #selector(FDPassengerTableCell.textFieldDidChange(_:)), for: .editingChanged)
    tfDiscountCode.textColor = FDResourceManager.sharedInstance.isSessionLogined ? UIColor.lightGray : UIColor.black
  }
  
  func textFieldEditingDidEnd(_ sender: UITextField) {
    switch (sender) {
    case tfFirstName:
      passenger.firstName = unwrap(str: sender.text)
      break
    case tfLastName:
      passenger.lastName = unwrap(str: sender.text)
      break
    case tfDocumentNumber:
      passenger.travelDocument.docNumber = unwrap(str: sender.text)
      break
    case tfDiscountCode:
      passenger.frequentFlyer.docNumber = unwrap(str: sender.text)
      break
    default:break
    }
    updatePassengersDetail()
  }
  
  func textFieldDidChange(_ sender:UITextField) {
    sender.text = FDConvertArNumberToEn(sender.text)
  }
  
  @IBAction func onBtnToggleExpandClicked(_ sender: UIButton) {
    endEditing(true)
    delegate?.btnExpandClicked(self)
  }
  
  func updatePassengersDetail() {
    tfTitle.text = FDResourceManager.sharedInstance.titleByKey(passenger.title)
    labelPassengeTitle.text = passenger.typeNumberString
    tfFirstName.text = passenger.firstName
    tfLastName.text = passenger.lastName
    tfDOB.text = passenger.dateOfBirth?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar: passenger.dateOfBirthHijri)
    imvHijri.isHighlighted = passenger.dateOfBirthHijri
    tfIssuingCountry.text = FDResourceManager.sharedInstance.countryNameByCode(passenger.travelDocument.docIssuingCountry)
    tfNationality.text = FDResourceManager.sharedInstance.countryNameByCode(passenger.travelDocument.nationality)
    tfDocumentNumber.text = passenger.travelDocument.docNumber
    tfExpirationDate.text = passenger.travelDocument.expirationDate?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar: passenger.travelDocument.expirationDateHijri)
    imvExpiryDateHijri.isHighlighted = passenger.travelDocument.expirationDateHijri
    passenger.frequentFlyer.docNumber = passenger.frequentFlyer.docNumber.isEmpty ? passenger.nasmiles : passenger.frequentFlyer.docNumber
    tfDiscountCode.text = passenger.frequentFlyer.docNumber
    tfExpirationDate.superview!.isHidden = shouldHideExpiryDate()
    tfIssuingCountry.superview!.isHidden = passenger.travelDocument.hideIssuingCountry
    highlightErrorField()
    btnSelPassenger.isHidden = !FDResourceManager.sharedInstance.isSessionLogined || FDResourceManager.sharedInstance.savedPassenger.count == 0
    tfDocumentType.text = documentTypes.getDocumentTypeName(passenger.travelDocument.docTypeCode)
  }
  
  func shouldHideExpiryDate() -> Bool {
    if isDomestic {
      if passenger.travelDocument.docTypeCode == "P" {
        return false
      } else {
        return true
      }
    } else {
      return false
    }
  }
  
  func highlightErrorField() {
    DispatchQueue.main.async {
      self.tfTitle.superview!.markError(self.passenger.title.isEmpty)
      self.tfFirstName.superview!.markError(self.passenger.firstName.isEmpty || !self.passenger.firstName.isValidateName)
      self.tfLastName.superview!.markError(self.passenger.lastName.isEmpty || !self.passenger.lastName.isValidateName)
      self.tfDOB.superview!.markError(self.passenger.dateOfBirth == nil)
      self.tfDocumentNumber.superview!.markError(self.passenger.travelDocument.validateDocumentNumber != .none)
      self.tfExpirationDate.superview!.markError(self.passenger.travelDocument.expirationDate == nil)
    }
  }
  
  func updatePassengersLayout() {
    //        btnToggleExpand.setImage(UIImage(named: expanded ? "icn-minus" : "icn-add"), forState: .Normal)
    layoutIfNeeded()
  }
  
  @IBAction func onBtnNationality(_ sender: UIButton) {
    endEditing(true)
    FDUtility.selectCountry(FDLocalized("Nationality"), initValue: passenger.travelDocument.nationality, empty: false, origin:sender) { (selectedValue) -> Void in
      self.passenger.travelDocument.nationality = selectedValue
      self.updatePassengersDetail()
    }
  }
  
  @IBAction func onBtnCountryOfBirth(_ sender: UIButton) {
    endEditing(true)
    FDUtility.selectCountry(FDLocalized("Country of Birth"), initValue: passenger.travelDocument.birthCountry, empty: false, origin: sender) { (selectedValue) in
      self.passenger.travelDocument.birthCountry = selectedValue
      self.updatePassengersDetail()
    }
  }
  
  @IBAction func onIssuingCountryClicked(_ sender: UIView) {
    endEditing(true)
    FDUtility.selectCountry(FDLocalized("Issuing Country"), initValue: passenger.travelDocument.docIssuingCountry, empty: false, origin:sender) { (selectedValue) -> Void in
      self.passenger.travelDocument.docIssuingCountry = selectedValue
      self.updatePassengersDetail()
    }
  }
  
  @IBAction func onDobClicked(_ sender: UIView) {
    endEditing(true)
    let maximumDate = Date().dateByAddingDays(-1)
    let selectedDate = passenger.dateOfBirth != nil ? passenger.dateOfBirth : Date()
    FDUtility.showPickerWithTitle(FDLocalized("Date of birth"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: passenger.dateOfBirthHijri,
                                  minimumDate: nil,
                                  maximumDate: maximumDate,
                                  doneBlock: { (picker, value, index) -> Void in
                                    self.passenger.dateOfBirth = value as? Date
                                    self.updatePassengersDetail()
      }, cancelBlock: { (_) -> Void in
      }, origin: sender)
  }
  
  @IBAction func onDobHijriClicked(_ sender: AnyObject) {
    endEditing(true)
    passenger.dateOfBirthHijri = !passenger.dateOfBirthHijri
    updatePassengersDetail()
  }
  
  @IBAction func onIDExpirationDateClicked(_ sender: UIView) {
    endEditing(true)
    let selectedDate = passenger.travelDocument.expirationDate != nil ? passenger.travelDocument.expirationDate : Date()
    FDUtility.showPickerWithTitle(FDLocalized("Expiry Date"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: passenger.travelDocument.expirationDateHijri, minimumDate:Date(),maximumDate:nil,  doneBlock: { (picker, value, index) -> Void in
      self.passenger.travelDocument.expirationDate = value as? Date
      self.updatePassengersDetail()
      }, cancelBlock: { (_) -> Void in
      }, origin: sender)
  }
  
  @IBAction func onIDExpirationDateHijriClicked(_ sender: AnyObject) {
    endEditing(true)
    passenger.travelDocument.expirationDateHijri = !passenger.travelDocument.expirationDateHijri
    updatePassengersDetail()
  }
  
  @IBAction func onTitleClicked(_ sender: UIView) {
    endEditing(true)
    let isAdult = passenger.paxType == PaxType.Adult.rawValue
    let allTitleDes = isAdult ? FDResourceManager.sharedInstance.titlesDes : FDResourceManager.sharedInstance.titlesDesChildInfant
    let allTitleKeys = isAdult ? FDResourceManager.sharedInstance.titlesKey : FDResourceManager.sharedInstance.titlesKeyChildInfant
    let index = allTitleKeys.index(of: passenger.title)
    ActionSheetStringPicker.show(withTitle: FDLocalized("Title"), rows: allTitleDes, initialSelection: index==nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      self.passenger.title = allTitleKeys[selectedIndex]
      self.updatePassengersDetail()
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
  }
  
  @IBAction func onTravelDocTypeClicked(_ sender: UIView) {
    endEditing(true)
    let items = documentTypes.documentTypes.map({$0.name})
    var index = documentTypes.documentTypes.index(where: {$0.code == passenger.travelDocument.docTypeCode})
    if items.count > 0 {
      if unwrap(int: index) < 0 && unwrap(int: index) >= items.count {
        index = 0
      }
      ActionSheetStringPicker.show(withTitle: FDLocalized("Document Type"), rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
        picker, selectedIndex, selectedValue in
        self.passenger.travelDocument.docTypeCode = self.documentTypes.documentTypes[selectedIndex].code
        self.updatePassengersDetail()
        self.delegate?.updateTableCellHeight(self)
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
  }
  
  @IBAction func onBtnFirstname(_ sender: UIButton) {
    tfFirstName.becomeFirstResponder()
  }
  
  @IBAction func onBtnLastname(_ sender: UIButton) {
    tfLastName.becomeFirstResponder()
  }
  
  @IBAction func onBtnDocumentNumber(_ sender: UIButton) {
    tfDocumentNumber.becomeFirstResponder()
  }
  
  @IBAction func onBtnNaSmilesNumber(_ sender: UIButton) {
    if FDResourceManager.sharedInstance.isSessionLogined { return }
    tfDiscountCode.becomeFirstResponder()
  }
  
  @IBAction func onSelPassengerClicked(_ sender: UIView) {
    self.endEditing(true)
    delegate?.btnSelPassengerClicked(self)
  }
  
}
