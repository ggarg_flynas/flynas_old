//
//  FDMyBoardingPassesVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit


// Not used
class FDMyBoardingPassesVC: BaseVC, UITableViewDelegate, UITableViewDataSource {

    var boardingPasses = FDBoardingPasses()

    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewNoItem: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        log("my_boarding_passes_page")

        SVProgressHUD.show()

        FDWebCheckinManager.sharedInstance.getBoardingPasses("E5V27M", departureStation: nil, success: { (boardingPasses) -> Void in
            SVProgressHUD.dismiss()
            self.boardingPasses = boardingPasses
            self.updateUI ()
            }) { (r) -> Void in
                SVProgressHUD.dismiss()
                self.updateUI ()
                FDErrorHandle.apiCallErrorWithAler(r)
        }
    }

    func updateUI () {
        if boardingPasses.items.count > 0 {
            viewNoItem.isHidden = true
            tableView.isHidden = false
        } else {
            viewNoItem.isHidden = false
            tableView.isHidden = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UITableViewDelegate/DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boardingPasses.items.count
    }

    let SeatPassengerName = 31
    let SeatNumber = 33
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "FDMyBoardingPassesTableViewCell") {

//            let bp = boardingPasses.items[indexPath.row]


//            let passenger = checkinPassengers[indexPath.row]
//            if let seatPassengerName = cell.viewWithTag(SeatPassengerName) as? UILabel {
//                seatPassengerName.text = passenger.fullName
//            }
//
//            if let seatNumber = cell.viewWithTag(SeatNumber) as? UILabel {
//                seatNumber.text = passenger.unitDesignator
//            }

            return cell
        }
        
        return UITableViewCell()
    }

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }


}
