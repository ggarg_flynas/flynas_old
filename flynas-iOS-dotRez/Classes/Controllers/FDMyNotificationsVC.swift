//
//  FDMyNotificationsVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 4/07/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

let kFDAppDidEnterForeground = "kFDAppDidEnterForeground"

class FDMyNotificationsVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
  
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet weak var lbNoNotifications: UILabel!
  @IBOutlet weak var swcNotifications: UISwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("my_notifications_page")
    tableView.delegate = self
    tableView.dataSource = self
    
    //        getMemberProfile()
    //        let notifications = realm.objects(FDNotificationItem)
    //        for
    //
    //        let msg1 = FDNotificationItem()
    //        msg1.title = "msg1"
    //        let msg2 = FDNotificationItem()
    //        msg2.title = "msg1"
    //        notifications.append(msg1)
    //        notifications.append(msg2)
    
    NotificationCenter.default.addObserver( self, selector: #selector(handleNotification(_:)), name: NSNotification.Name(rawValue: FD_RECEIVE_NOTIFICATION), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name(rawValue: kFDAppDidEnterForeground), object: nil)
    updateUI()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  @IBAction func onSwcNotification(_ sender: UISwitch) {
    if sender.isOn {
      let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
      UIApplication.shared.registerUserNotificationSettings(settings)
      delay(1.0) {
        if !UIApplication.shared.isRegisteredForRemoteNotifications {
          UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }
      }
    } else {
      UIApplication.shared.unregisterForRemoteNotifications()
    }
  }
  
  func updateUI() {
    lbNoNotifications.isHidden = !(FDAPNManager.sharedInstance.notifications.count == 0)
    swcNotifications.isOn = UIApplication.shared.isRegisteredForRemoteNotifications
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return FDAPNManager.sharedInstance.notifications.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if let cell = self.tableView.dequeueReusableCell(withIdentifier: "FDNotificationsTableCell") as? FDNotificationsTableCell {
      
      let notification = FDAPNManager.sharedInstance.notifications[indexPath.row]
      cell.title.text = notification.message
      if let receiveDate = notification.receiveDate {
        cell.receiveDate.text = receiveDate.toString(format:.custom("HH:mm EEE, dd MMM yyyy"))
      } else {
        cell.receiveDate.text = ""
      }
      
      return cell
    }
    
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    showNotification(FDAPNManager.sharedInstance.notifications[indexPath.row])
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  func handleNotification(_ notification: Notification) {
    //        if let msg = notification.object as? FDNotificationItem {
    //            showNotification(msg)
    //        }
    
    self.refreshNotifications()
    
  }
  
  func refreshNotifications() {
    self.tableView.reloadData()
  }
  
  func showNotification(_ notification:FDNotificationItem) {
    //        UIAlertView.showWithTitle(notification.title, message: notification.message, cancelButtonTitle: FDLocalized("OK")) { (alertView, buttonIndex) -> Void in
    //        }
    
    FDUtility.popInfo(notification.title,message: notification.message)
    
  }
}
