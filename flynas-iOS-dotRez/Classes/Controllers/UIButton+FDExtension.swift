//
//  UIButton+FDExtension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 27/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import Foundation


extension UIButton {
  func setAlignmentByAppLanguage() {
    layer.cornerRadius = frame.size.width / 2
    clipsToBounds = true
  }
  
  func WBHighlightImage(_ image:UIImage) -> UIImage
  {
    let size = image.size
    let bnds = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
    let colr = UIColor(white: 0, alpha: 0.46)
    
    // begin image context
    UIGraphicsBeginImageContext(bnds.size)
    UIGraphicsBeginImageContextWithOptions(bnds.size, false, 0.0);
    if let ctxt = UIGraphicsGetCurrentContext() {
      
      // transform CG* coords to UI* coords
      ctxt.translateBy(x: 0.0, y: bnds.size.height)
      ctxt.scaleBy(x: 1.0, y: -1.0)
      
      // draw original image
      ctxt.draw(image.cgImage!, in: bnds);
      
      // draw highlight overlay
      ctxt.clip(to: bnds, mask: image.cgImage!);
      ctxt.setFillColor(colr.cgColor);
      ctxt.fill(bnds);
    }
    
    // finish image context
    if let copy = UIGraphicsGetImageFromCurrentImageContext() {
      UIGraphicsEndImageContext()
      return copy
    } else {
      UIGraphicsEndImageContext()
      return image
    }
  }
  
  func flipButton() {
    if isRTL() {
      if self.contentHorizontalAlignment == .left {
        self.contentHorizontalAlignment = .right
      } else if self.contentHorizontalAlignment == .right {
        self.contentHorizontalAlignment = .left
      }
      
      // sync the inset
      self.titleEdgeInsets = UIEdgeInsetsMake(titleEdgeInsets.top,titleEdgeInsets.right,titleEdgeInsets.bottom,titleEdgeInsets.left)
      self.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInsets.top,imageEdgeInsets.right,imageEdgeInsets.bottom,imageEdgeInsets.left)
      self.contentEdgeInsets = UIEdgeInsetsMake(contentEdgeInsets.top,contentEdgeInsets.right,contentEdgeInsets.bottom,contentEdgeInsets.left)
      
      if let img = image(for: UIControlState()) {
        self.setImage(flipImage(img), for: UIControlState())
      }
      if var img = image(for: .highlighted) {
        img = WBHighlightImage(img)
        self.setImage(flipImage(img), for: .highlighted)
      }
      if let img = image(for: .selected) {
        self.setImage(flipImage(img), for: .selected)
      }
      if var img = image(for: .disabled) {
        img = WBHighlightImage(img)
        self.setImage(flipImage(img), for: .disabled)
      }
      
    }
  }
  
  func setAutoFlipImage(_ image: UIImage?) {
    if isRTL() {
      if let img = flipImage(image) {
        let highlightImg = WBHighlightImage(img)
        self.setImage(image, for: UIControlState())
        self.setImage(highlightImg, for: .highlighted)
        self.setImage(img, for: .selected)
        self.setImage(highlightImg, for: .disabled)
      }
    } else {
      self.setImage(image, for: UIControlState())
    }
  }
  
  func setImage(_ image: UIImage?) {
    if isRTL() {
      if let img = image {
        let highlightImg = WBHighlightImage(img)
        self.setImage(image, for: UIControlState())
        self.setImage(highlightImg, for: .highlighted)
        self.setImage(img, for: .selected)
        self.setImage(highlightImg, for: .disabled)
      }
    } else {
      self.setImage(image, for: UIControlState())
    }
  }
  
  func setTitleWithAnimation(_ str: String) {
    UIView.transition(with: self,
                              duration: 0.2,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.setTitle(str, for: UIControlState())
      }, completion: nil)
  }
  
}
