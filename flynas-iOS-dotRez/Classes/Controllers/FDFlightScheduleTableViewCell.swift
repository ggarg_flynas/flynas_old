//
//  FDFlightScheduleTableViewCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 10/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightScheduleTableViewCell: UITableViewCell {

    @IBOutlet var labelTimePeriod: UILabel!
    @IBOutlet var labelDepartureTime: UILabel!
    @IBOutlet var labelArrivalTime: UILabel!
    @IBOutlet var labelFlightDesignator: UILabel!

    @IBOutlet var labelSun: UILabel!
    @IBOutlet var labelSat: UILabel!
    @IBOutlet var labelMon: UILabel!
    @IBOutlet var labelTue: UILabel!
    @IBOutlet var labelWed: UILabel!
    @IBOutlet var labelThu: UILabel!
    @IBOutlet var labelFri: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelSun.roundView()
        labelSat.roundView()
        labelMon.roundView()
        labelTue.roundView()
        labelWed.roundView()
        labelThu.roundView()
        labelFri.roundView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setFlightScheduleDetail(_ flightScheduleDetail:FDFlightScheduleDetail) {
        labelFlightDesignator.text = flightScheduleDetail.flightDesignator
        labelArrivalTime.text = flightScheduleDetail.arrivalTime!.toStringUTC(format: .custom("HH:mm"))
        labelDepartureTime.text = flightScheduleDetail.departureTime!.toStringUTC(format: .custom("HH:mm"))

        labelTimePeriod.text = "\(flightScheduleDetail.beginDate!.toStringUTC(format: .custom("dd MMM yyyy"))) - \(flightScheduleDetail.endDate!.toStringUTC(format: .custom("dd MMM yyyy")))"

        setFlightAvailable(labelSun,isAvailable: false)
        setFlightAvailable(labelSat,isAvailable: false)
        setFlightAvailable(labelMon,isAvailable: false)
        setFlightAvailable(labelTue,isAvailable: false)
        setFlightAvailable(labelWed,isAvailable: false)
        setFlightAvailable(labelThu,isAvailable: false)
        setFlightAvailable(labelFri,isAvailable: false)

        if flightScheduleDetail.flightFrequency == nil || flightScheduleDetail.flightFrequency!.contains("None") {
        } else {
            if flightScheduleDetail.flightFrequency!.contains("Daily") {
                setFlightAvailable(labelSun,isAvailable: true)
                setFlightAvailable(labelSat,isAvailable: true)
                setFlightAvailable(labelMon,isAvailable: true)
                setFlightAvailable(labelTue,isAvailable: true)
                setFlightAvailable(labelWed,isAvailable: true)
                setFlightAvailable(labelThu,isAvailable: true)
                setFlightAvailable(labelFri,isAvailable: true)
            }

            if flightScheduleDetail.flightFrequency!.contains("WeekEnd") {
                setFlightAvailable(labelSun,isAvailable: true)
                setFlightAvailable(labelSat,isAvailable: true)
            }

            if flightScheduleDetail.flightFrequency!.contains("WeekDay") {
                setFlightAvailable(labelMon,isAvailable: true)
                setFlightAvailable(labelTue,isAvailable: true)
                setFlightAvailable(labelWed,isAvailable: true)
                setFlightAvailable(labelThu,isAvailable: true)
                setFlightAvailable(labelFri,isAvailable: true)
            }

            if flightScheduleDetail.flightFrequency!.contains("Saturday") {
                setFlightAvailable(labelSun,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Sunday") {
                setFlightAvailable(labelSat,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Monday") {
                setFlightAvailable(labelMon,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Tuesday") {
                setFlightAvailable(labelTue,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Wednesday") {
                setFlightAvailable(labelWed,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Thursday") {
                setFlightAvailable(labelThu,isAvailable: true)
            }
            if flightScheduleDetail.flightFrequency!.contains("Friday") {
                setFlightAvailable(labelFri,isAvailable: true)
            }
        }

    }

    func setFlightAvailable(_ dayLabel:UILabel, isAvailable:Bool) {
        if isAvailable {
            dayLabel.backgroundColor = FDColorFlynasGreen
        } else {
            dayLabel.backgroundColor = UIColor(red: 200/255.0, green: 200/255.0, blue: 199/255.0, alpha: 1.0)
        }
    }
}
