//
//  FDFlightFareInfoVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 25/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightFareInfoVC: BaseVC,UIWebViewDelegate {
  
  @IBOutlet var labelTitle: UILabel!
  var priceOption = PriceOption.Economy
  weak var delegate : FDPopOutVCDelegate?
  
  @IBOutlet var viewHeader: UIView!
  @IBOutlet var webView: UIWebView!
  
  @IBOutlet var heightWebView: NSLayoutConstraint!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("fare_info_popup")
    var fareUrl = "mfares"
    
    switch (priceOption) {
//    case .Extra:
//      fareUrl += "2"
//      labelTitle.text = FDLocalized(PriceOption.Extra.rawValue)
//      viewHeader.backgroundColor = FDFlightPriceView.clrExpress
//      break
//    case .Express:
//      fareUrl += "2"
//      labelTitle.text = FDLocalized(PriceOption.Express.rawValue)
//      viewHeader.backgroundColor = FDFlightPriceView.clrExpress
//      break
    case .Business:
      labelTitle.text = FDLocalized(PriceOption.Business.rawValue)
      viewHeader.backgroundColor = FDFlightPriceView.clrBusiness
      fareUrl += "3"
      break
    default:
      labelTitle.text = FDLocalized(PriceOption.Economy.rawValue)
      viewHeader.backgroundColor = FDFlightPriceView.clrEcomomy
      fareUrl += "1"
      break
    }
    
    switch (FDRM.sharedInstance.currentCulture) {
    case "en-GB":
      fareUrl += "ar"
      break
    case "tr-TR":
      fareUrl += "tr"
      break
    default:
      fareUrl += "en"
      break
    }
    
    webView.delegate = self
    webView.scrollView.showsHorizontalScrollIndicator = false
    fareUrl = "\(FDApiBaseUrl)Fareshtml/\(fareUrl).html"
    //        webView.loadRequest(NSURLRequest(URL: NSURL(string: fareUrl)!))
    
    webView.loadRequest(URLRequest(url: URL(string: fareUrl)!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 10.0))
    
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
  
  @IBAction func onBtnCancel(_ sender: UIButton) {
    self.view.endEditing(true)
    webView.delegate = nil
    webView.stopLoading()
    delegate?.dismissInfo()
  }
  
  // MARK: - UIWebViewDelegate
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    let fittingSize = webView.sizeThatFits(CGSize.zero)
    
    UIView.animate(withDuration: 0.5, animations: { () -> Void in
      self.heightWebView.constant = fittingSize.height
      self.view.layoutIfNeeded()
    }) 
  }
}
