//
//  FDPageControl.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 22/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPageControl: UIPageControl {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    let activeImage = UIImage(named: "icn-seat-business-normal")
    let inactiveImage = UIImage(named: "icn-seat-eco-orange")

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
