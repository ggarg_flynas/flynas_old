//
//  FDMMBFlightInfoCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 12/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMMBFlightInfoCell: UITableViewCell {


//    @IBOutlet var labelFlightDirection: UILabel!
//    @IBOutlet var imgFlightDirection: FDImageView!
    @IBOutlet var labelDepartDate: UILabel!

    @IBOutlet var labelDepartTime: UILabel!
    @IBOutlet var labelArriveTime: UILabel!
    @IBOutlet var labelOriginCityName: UILabel!
    @IBOutlet var labelDestinationCityName: UILabel!
    @IBOutlet var labelOriginAirportName: UILabel!
    @IBOutlet var labelDestinationAirprotName: UILabel!

    @IBOutlet var labelFlightCode: UILabel!


    @IBOutlet var labelDuration: UILabel!
    @IBOutlet var labelStops: UILabel!

    var leg : FDLeg? {
        didSet {
            if leg != nil {
                labelDepartTime.text = leg!.departureDate?.toStringUTC(format:.custom("HH:mm"))
                labelArriveTime.text = FDUtility.arrivalDateHHMMString(leg!.departureDate!, arrivalDate: leg!.arrivalDate!)

                let originStation = FDResourceManager.sharedInstance.getStation(leg!.origin)
                let destinationStation = FDResourceManager.sharedInstance.getStation(leg!.destination)

                labelOriginCityName.text = originStation?.name
                labelDestinationCityName.text = destinationStation?.name
                labelOriginAirportName.text = originStation?.code
                labelDestinationAirprotName.text = destinationStation?.code
                labelFlightCode.text = leg!.carrierCode + " " + leg!.flightNumber
                labelStops.text = ""
                labelDuration.text = FDUtility.formatDuration(leg!.utcDepartureDate!, end: leg!.utcArrivalDate!)

                labelDepartDate.text = leg!.departureDate?.toStringUTC(format:.custom("EEEE, dd MMM yyyy"))
            }
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


}
