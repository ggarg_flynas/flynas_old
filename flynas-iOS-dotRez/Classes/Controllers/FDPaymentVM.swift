//
//  FDPaymentVM.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 6/09/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation

class FDPaymentVM {
  
  var userBooking : FDUserBookingInfo?
  
  var flightFeeList = [(feeName:String, feeValue:String)]()
  var flightFeeTotal : Float = 0
  var travelExtrasFeeList = [(feeName:String, feeValue:String)]()
  var travelExtrasFeeTotal : Float = 0
  
  struct ItemNameAmountValue {
    var itemName:String
    var itemNumber:Int
    var itemValue:Float
  }
  
  func clearPaymentSummaryList () {
    flightFeeList.removeAll()
    flightFeeTotal = 0
    travelExtrasFeeList.removeAll()
    travelExtrasFeeTotal = 0
  }
  
  func updatePaymentSummaryTableItems () {
    if let priceSummary = userBooking?.priceSummary {
      var itemList1 = [ItemNameAmountValue]()
      var itemList2 = [ItemNameAmountValue]()
      func appendToItemList(_ itemList:inout [ItemNameAmountValue], item:(itemName:String, itemNumber:Int, itemValue:Float), updateNumber:Bool) {
        var findMatchItem = false
        for index in 0..<itemList.count {
          if itemList[index].itemName == item.itemName {
            if updateNumber {
              itemList[index].itemNumber += item.itemNumber
            }
            itemList[index].itemValue += item.itemValue
            findMatchItem = true
            break
          }
        }
        if !findMatchItem {
          itemList.append(ItemNameAmountValue(itemName: item.itemName, itemNumber: item.itemNumber, itemValue: item.itemValue))
        }
      }
      var totalTax = 0.0
      for journeyCharge in priceSummary.journeyCharges {
        var discount : Float = 0
        var service :Double = 0
        for serviceCharge in journeyCharge.serviceCharges {
          service += (serviceCharge.amount?.doubleValue)!
        }
        for paxCharge in journeyCharge.paxCharges {
          var bundleStr = ""
          switch paxCharge.bundleCode {
          case BundleCode.LIGH.rawValue, BundleCode.LIT1.rawValue, BundleCode.LIT2.rawValue:
            bundleStr = FDLocalized("Light")
          case BundleCode.PLUS.rawValue, BundleCode.PLS1.rawValue, BundleCode.PLS2.rawValue:
            bundleStr = FDLocalized("Plus")
          case BundleCode.PRUM.rawValue, BundleCode.PRM1.rawValue, BundleCode.PRM2.rawValue:
            bundleStr = FDLocalized("Premium")
          default:
            continue
          }
          let title = "\(FDLocalized(paxCharge.fareType)) \(bundleStr)"
          let amount = paxCharge.totalAmount.doubleValue + paxCharge.bundlePrice.doubleValue
          appendToItemList(&itemList1, item: (title, Int(paxCharge.paxCount), Float(amount)), updateNumber: false)
          //          appendToItemList(&itemList1, item:paxCharge.getNameAmountValue(),updateNumber:false)
          discount = discount + paxCharge.paxFareTotalDiscountAmount.floatValue
        }
        // discount
        if discount > 0 {
          appendToItemList(&itemList1, item:(FDLocalized("Discount"), 0,-1*discount),updateNumber:false)
        }
        // baggages
        if let (baggageNumber, baggagePrice) = userBooking?.baggageSsr?.getBaggageServiceNumberAndPrice(journeyCharge.origin,destination: journeyCharge.destination), baggageNumber > 0 {
          appendToItemList(&itemList2, item:(FDLocalized("Baggage"), baggageNumber, baggagePrice), updateNumber:true)
        }
        // sports
        if let (sportNumber, sportPrice) = userBooking?.sportsSsr?.getSportsServiceNumberAndPrice(journeyCharge.origin, destination: journeyCharge.destination), sportNumber > 0 {
          appendToItemList(&itemList2, item: (FDLocalized("Sports Equipment"), sportNumber, sportPrice), updateNumber: true)
        }
        // meals
        var serviceNumber = 0
        var servicePrice : Float = 0
        for i in 0...journeyCharge.numberOfStops.intValue {
          if let leg = userBooking?.getLegInfoBy(journeyCharge.origin,destination: journeyCharge.destination, index:i) {
            if let (sn, sp) = userBooking?.mealSsr?.getMealServiceNumberAndPrice(leg.origin,destination: leg.destination) {
              serviceNumber += sn
              servicePrice += sp
            }
          }
        }
        if serviceNumber > 0 {
          appendToItemList(&itemList2, item:(FDLocalized("Meal"), serviceNumber, servicePrice), updateNumber:true)
        }
        // seats
        serviceNumber = 0
        servicePrice = 0
        for i in 0...journeyCharge.numberOfStops.intValue {
          if let leg = userBooking?.getLegInfoBy(journeyCharge.origin,destination: journeyCharge.destination, index:i) {
            if let (sn, sp) = userBooking?.flightSeatMap.getServiceNumberAndPrice(leg.origin,destination: leg.destination) {
              serviceNumber += sn
              servicePrice += sp
            }
          }
        }
        if serviceNumber > 0 {
          appendToItemList(&itemList2, item:(FDLocalized("Seat"), serviceNumber, servicePrice), updateNumber:true)
        }
        // serviceCharges
        if journeyCharge.serviceCharges.count > 0 {
          for serviceCharge in journeyCharge.serviceCharges {
            totalTax += (serviceCharge.amount?.doubleValue)!
          }
        }
        // Lounge
        let loungeCharge = journeyCharge.getLoungeCharge()
        if loungeCharge > 0 {
          appendToItemList(&itemList2, item:(FDLocalized("Business Class Lounge"), 0, loungeCharge), updateNumber:true)
        }
      }
      appendToItemList(&itemList1, item:(FDLocalized("Taxes and Fees"), 0, Float(totalTax)), updateNumber:false)
      func loadItemListToFeeList(_ feeList:inout [(feeName:String, feeValue:String)], feeTotal:inout Float, itemList:[ItemNameAmountValue]) {
        for item in itemList {
          if item.itemNumber > 0 {
            feeList.append(("\(item.itemName) x \(FDLocalizeInt(item.itemNumber))",FDUtility.formatCurrencyWithCurrencySymbol(item.itemValue as NSNumber)!))
          } else {
            feeList.append((item.itemName,FDUtility.formatCurrencyWithCurrencySymbol(item.itemValue as NSNumber)!))
          }
          feeTotal += item.itemValue
        }
      }
      loadItemListToFeeList(&flightFeeList, feeTotal: &flightFeeTotal, itemList: itemList1)
      loadItemListToFeeList(&travelExtrasFeeList, feeTotal: &travelExtrasFeeTotal, itemList: itemList2)
    }
  }
  
  func getRowNumberInSection(_ section:Int) -> Int {
    if section == 1 {
      return travelExtrasFeeList.count
    } else {
      return flightFeeList.count
    }
  }
  
  func getTotalRowNumber() -> Int {
    var totalRowNumber = 0
    for i in 0..<getSectionNumber() {
      totalRowNumber += getRowNumberInSection(i)
    }
    return totalRowNumber
  }
  
  func getSectionNumber() -> Int {
    return travelExtrasFeeList.count > 0 ? 2 : 1
  }
  
  func getSectionTitle(_ section:Int) -> String {
    if section == 1 {
      return FDLocalized("Travel extras")
    } else {
      return FDLocalized("Flights")
    }
  }
  
  func getSectionImageName(_ section:Int) -> String {
    if section == 1 {
      return "icn-baggage"
    } else {
      return "icn-flight-origin-m"
    }
  }
  
  func getFeeLabelString(_ section:Int, row:Int) -> String {
    if section == 1 {
      return travelExtrasFeeList[row].feeName
    } else {
      return flightFeeList[row].feeName
    }
  }
  
  func getFeeAmountString(_ section:Int, row:Int) -> String {
    if section == 1 {
      return travelExtrasFeeList[row].feeValue
    } else {
      return flightFeeList[row].feeValue
    }
  }
  
  func getFooterFeeLabelString(_ section:Int) -> String {
    if section == 1 {
      return FDLocalized("Total travel extras")
    } else {
      return FDLocalized("Fare total")
    }
  }
  
  func getFooterFeeAmountString(_ section:Int, ub: FDUserBookingInfo) -> String {
    if section == 1 {
      return FDUtility.formatCurrencyWithCurrencySymbol(travelExtrasFeeTotal as NSNumber)!
    } else {
      return FDUtility.formatCurrencyWithCurrencySymbol(ub.priceSummary.total)!
    }
  }
}
