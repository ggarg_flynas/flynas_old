//
//  FDFlightInfoCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 22/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightInfoCell: UITableViewCell {
  
  @IBOutlet weak var lbDirection: UILabel!
  @IBOutlet weak var lbFlight: UILabel!
  
}
