//
//  FDPassengerVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 6/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPassengerVC: FDBookingCommonVC, FDPassengerTableCellDelegate, FDSelectPassengerVCDelegate {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var heightPassengerTable: NSLayoutConstraint!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet weak var tfCountryCode: UITextField!
  @IBOutlet weak var tfMobileNumber: UITextField!
  @IBOutlet weak var tfEmail1: UITextField!
  @IBOutlet weak var tfPassword1: UITextField!
  @IBOutlet weak var tfPassword2: UITextField!
  @IBOutlet weak var imvJoinNow: UIImageView!
  @IBOutlet weak var vwNasmilesJoin: UIView!
  @IBOutlet weak var vwNasmilesPasswords: UIView!
  @IBOutlet weak var btnLogin: UIButton!
  
  static let HeightTableViewCellHeader: CGFloat = 48.0
  static let HeightTableViewCellDetail: CGFloat = 276.0
  var currentExpandedCell = 0
  var isDomestic = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("passenger_details_page")
    updatePriceSummary(false)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = FDPassengerVC.HeightTableViewCellHeader
    tableView.delegate = self
    tableView.dataSource = self
    tableView.reloadData()
    SharedModel.shared.isDocTypeSet = false
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tfMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    updateTableViewHeight()
    updateTableView()
    updateUI()
    updatePassengerDetailFromBackend()
    updateJoinNow()
    btnLogin.isHidden = FDResourceManager.sharedInstance.isSessionLogined
  }
  
  func loadRecentPassengerAndContact() {
    var passengerNumber = min(self.userBooking.passengers.count, FDResourceManager.sharedInstance.savedPassenger.count)
    passengerNumber = min(passengerNumber,1)
    for i in 0..<passengerNumber {
      userBooking.passengers[i].copyFrom(FDResourceManager.sharedInstance.savedPassenger[i])
    }
    userBooking.contactInfo.copyContact(FDResourceManager.sharedInstance.savedContactInfo)
    userBooking.contactInfo.emailAddressConfirm = userBooking.contactInfo.emailAddress
  }
  
  @IBAction func onBtnLogin(_ sender: UIButton) {
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FDLoginVC")
    present(vc, animated: true, completion: nil)
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton) {
    view.endEditing(true)
    let (errorCode,index) = validateAll()
    if errorCode == .none {
      SVProgressHUD.show()
      if !FDResourceManager.sharedInstance.isSessionLogined {
        FDResourceManager.sharedInstance.savedPassenger = userBooking.passengers
        FDResourceManager.sharedInstance.saveRecentContactInfo(userBooking.contactInfo)
        FDResourceManager.sharedInstance.saveRecentPassengers(userBooking.passengers)
      }
      FDBookingManager.sharedInstance.bookingPassengers(userBooking.passengersObject, success: { (_) -> Void in
        FDBookingManager.sharedInstance.bookingContacts(self.userBooking.contactObject, success: { () -> Void in
          self.userBooking.updatePassengerLegExtraItem()
          if !FDLoginManager.sharedInstance.hasSavedUserCredential && self.imvJoinNow.isHighlighted, let pass = self.tfPassword1.text {
            FDLoginManager.sharedInstance.registerUser(self.userBooking.contactInfo.emailAddress, password: pass, parameters: self.getRegisterObject(pass), success: { (memberLogin) in
              SVProgressHUD.dismiss()
              self.gotoExtraPage()
              }, fail: { (reason) in
                SVProgressHUD.dismiss()
                self.errorHandle(reason)
            })
          } else {
            SVProgressHUD.dismiss()
            self.gotoExtraPage()
          }
          }, fail: { (reason) -> Void in
            SVProgressHUD.dismiss()
            _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
        })
        }, fail: { (reason) -> Void in
          SVProgressHUD.dismiss()
          _ = self.errorHandleAndSwitchToSearchIfExpired(reason)
      })
    } else {
      if index < 0 {
        scrollToErrorField(errorCode)
      } else {
        if currentExpandedCell != index {
          togglePassengerDetail(IndexPath(row: index, section: 0))
        } else {
          scrollToCurrentPassenger()
        }
      }
      FDErrorHandle.validateErrorHandle(errorCode)
    }
  }
  
  func getRegisterObject(_ password: String) -> [AnyHashable: Any] {
    guard let passenger = self.userBooking.passengers.first else { return ["": ""] }
    let mobileCountryCode = userBooking.contactInfo.mobilePhone.code
    let moible = userBooking.contactInfo.mobilePhone.number
    let phoneCountryCode = userBooking.contactInfo.workPhone.code
    let phone = userBooking.contactInfo.workPhone.number
    let fax = ""
    let address = ""
    let city = ""
    let userEmail = self.userBooking.contactInfo.emailAddress
    var mobileCountryCodeNumber = ""
    if let c = FDResourceManager.sharedInstance.countryPhoneNumberByCode(mobileCountryCode) {
      mobileCountryCodeNumber = c
    }
    var phones = [
      [
        "number": "+\(mobileCountryCodeNumber) \(moible)",
        "typeCode":FDPhoneTypeCode.Mobile.rawValue
      ]
    ]
    if !phone.isEmpty {
      var phoneCountryCodeNumber = ""
      if let c = FDResourceManager.sharedInstance.countryPhoneNumberByCode(phoneCountryCode) {
        phoneCountryCodeNumber = c
      }
      phones.append(
        [
          "number":"+\(phoneCountryCodeNumber) \(phone)",
          "typeCode":FDPhoneTypeCode.Home.rawValue
        ])
    }
    if !fax.isEmpty {
      phones.append([
        "number":   fax,
        "typeCode": "F"
        ]
      )
    }
    let register : [AnyHashable: Any] = [
      "profileAddresses": [
        "addresses": [
          [
            "addressLine1": address,
            "city":         city,
            "countryCode":  passenger.residentCountry
          ]
        ]
      ],
      "profileDetails": [
        "details": [
          "dateOfBirth": passenger.dateOfBirth != nil ? passenger.dateOfBirth!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false) : "",
          "email":       userEmail,
          "first":       passenger.firstName,
          "last":        passenger.lastName,
          "nationality": passenger.travelDocument.nationality,
          "title":       passenger.title
        ]
      ],
      "profilePhones": [
        "phones": phones
      ],
      "profileRegister": [
        "password": password
      ],
      "profileTravelDocument": [
        "documents": [
          [
            "birthCountry":   passenger.travelDocument.birthCountry,
            "expirationDate": passenger.travelDocument.expirationDate != nil ? passenger.travelDocument.expirationDate!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false) : "",
            "first":          passenger.firstName,
            "last":           passenger.lastName,
            "nationality":    passenger.travelDocument.nationality,
            "number":         passenger.travelDocument.docNumber,
            "title":          passenger.title,
            "typeCode":       passenger.travelDocument.docTypeCode
          ]
        ]
      ]
    ]
    return register
  }
  
  func updatePassengerDetailFromBackend() {
    getDocumentTypes()
//    if !FDResourceManager.sharedInstance.isSessionLogined { return }
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.bookingPassengers(nil, success: { (passengersInput) -> Void in
      SVProgressHUD.dismiss()
      if let passengersInput = passengersInput {
        self.userBooking.updatePassengerDiscountCode(passengersInput)
        if passengersInput.regionFlightType == "Domestic" {
          self.isDomestic = true
          self.userBooking.updatePassengerDefaultTravelDocType(.NationalIDCard)
        } else {
          self.userBooking.updatePassengerDefaultTravelDocType(.Passport)
        }
        self.tableView.reloadWithAnimation()
        self.loadRecentPassengerAndContact()
        //        if let passenger = FDResourceManager.sharedInstance.savedPassenger.first {
        //          self.selectPassenger(passenger)
        //        }
      }
      }, fail: { (reason) -> Void in
        SVProgressHUD.dismiss()
    })
  }
  
  func getDocumentTypes() {
    FDBookingManager.sharedInstance.getDocumentTypes({ (documentTypes) -> Void in
      self.userBooking.documentTypes = documentTypes
      self.updateTableView()
      self.updateUI()
      self.updateTableViewHeight()
      }, fail: { (reason) -> Void in
    })
  }
  
  func shouldHideNasmiles(_ index: Int) -> Bool {
    if userBooking.payWithNaSmiles {
      return true
    } else {
      return userBooking.passengers[index].paxType == PaxType.Infant.rawValue
    }
  }
  
  // MARK: - FDPassengerTableCellDelegate
  func btnExpandClicked(_ cell:FDPassengerTableCell) {
    if let i = tableView.indexPath(for: cell) {
      togglePassengerDetail(i)
    }
  }
  
  func btnSelPassengerClicked(_ cell: FDPassengerTableCell) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let selPassengerVC = storyboard.instantiateViewController(withIdentifier: "FDSelectPassengerVC") as! FDSelectPassengerVC
    selPassengerVC.delegate = self
    presentPopupViewController(selPassengerVC, animationType: MJPopupViewAnimationFade)
  }
  
  func updateTableCellHeight(_ cell: FDPassengerTableCell) {
    if let i = tableView.indexPath(for: cell) {
      tableView.reloadRows(at: [i], with: UITableViewRowAnimation.automatic)
      updateTableViewHeight()
    }
  }
  
  // MARK : - FDSelectPassengerVCDelegate
  func selectPassenger(_ passenger: FDPassenger) {
    dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    if currentExpandedCell >= 0 {
      userBooking.passengers[currentExpandedCell].copyFrom(passenger)
      updateTableView()
    }
    
  }
  
  func selectPassengerCancel() {
    dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
  }
  
  func scrollToCurrentPassenger () {
    if currentExpandedCell != -1 {
      if let cell = tableView.cellForRow(at: IndexPath(row: currentExpandedCell, section: 0)) {
        let rect = cell.convert(cell.bounds, to:self.scrollView)
        scrollView.scrollRectToVisible(rect, animated: true)
      }
    }
  }
  
  func scrollTo(_ item:UIView) {
    let rect = item.convert(item.bounds, to:self.scrollView)
    self.scrollView.scrollRectToVisible(rect, animated: true)
  }
  
  func togglePassengerDetail(_ indexPath: IndexPath) {
    if currentExpandedCell == indexPath.row {
      currentExpandedCell = -1
      updateTableViewHeight()
      updateTableView()
    } else {
      var rows = [indexPath]
      if currentExpandedCell != -1 {
        rows.append(IndexPath(row: currentExpandedCell, section: 0))
      }
      currentExpandedCell = indexPath.row
      updateTableViewHeight()
      updateTableView()
      scrollToCurrentPassenger()
    }
  }
  
  func updateTableView() {
    tableView.reloadWithAnimation()
    view.layoutIfNeeded()
  }
  
  // MARK: - Contact details
  @IBAction func onMobileCountryCodeClicked(_ sender: UIView) {
    self.view.endEditing(true)
    FDUtility.selectCountryPhoneCode(FDLocalized("Mobile Country Code"), initValue: userBooking.contactInfo.mobilePhone.code, origin:sender) { (selectedValue) -> Void in
      self.userBooking.contactInfo.mobilePhone.code = selectedValue
      self.updateUI()
    }
  }
  
  //    @IBAction func onPhoneCountryCodeClicked(sender: UIView) {
  //        self.view.endEditing(true)
  //        FDUtility.selectCountryPhoneCode(FDLocalized("Phone Country Code"), initValue: userBooking.contactInfo.workPhone.code, origin:sender) { (selectedValue) -> Void in
  //            self.userBooking.contactInfo.workPhone.code = selectedValue
  //            self.updateUI()
  //        }
  //    }
  
  func validateAll() -> (ValidateErrorCode,Int) {
    for i in 0..<userBooking.passengers.count {
      let passenger = userBooking.passengers[i]
      let errorCode = passenger.validateAll
      if errorCode != .none {
        return (errorCode,i)
      }
    }
    if !FDLoginManager.sharedInstance.hasSavedUserCredential && imvJoinNow.isHighlighted {
      if unwrap(str: tfPassword1.text).isEmpty {
        return (.passwordCannotEmpty, -1)
      }
      if unwrap(str: tfPassword1.text) != unwrap(str: tfPassword2.text)  {
        return (.confirmPasswordNotMatch, -1)
      }
    }
    return (userBooking.contactInfo.validateAll, -1)
  }
  
  func updateUI() {
    tfCountryCode.text = FDResourceManager.sharedInstance.countryPhoneCodeByCode(userBooking.contactInfo.mobilePhone.code)
    //TODO        btnPhoneCountryCode.setTitle(FDResourceManager.sharedInstance.countryPhoneCodeByCode(userBooking.contactInfo.workPhone.code), forState: .Normal)
    tfMobileNumber.text = userBooking.contactInfo.mobilePhone.number
    //TODO        tfPhoneNumber.text = userBooking.contactInfo.workPhone.number
    tfEmail1.text = userBooking.contactInfo.emailAddress
    //TODO        tfContactEmailConfirm.text = userBooking.contactInfo.emailAddressConfirm
    highlightErrorField()
  }
  
  func updateTableViewHeight() {
    var height = CGFloat(tableView(tableView, numberOfRowsInSection: 0)) * FDPassengerVC.HeightTableViewCellHeader
    if currentExpandedCell > -1 {
      height += FDPassengerVC.HeightTableViewCellDetail
      if shouldHideNasmiles(currentExpandedCell) {
        height -= FDPassengerTableCell.HeightNaSmiles
      }
    }
    heightPassengerTable.constant = height
  }
  
  func highlightErrorField() {
    tfCountryCode.superview!.markError(userBooking.contactInfo.mobilePhone.code.isEmpty)
    tfMobileNumber.superview!.markError(userBooking.contactInfo.mobilePhone.number.isEmpty)
    tfEmail1.superview!.markError(userBooking.contactInfo.emailAddress.isEmpty)
    tfPassword1.superview!.markError(imvJoinNow.isHighlighted && unwrap(str: tfPassword1.text).isEmpty)
    tfPassword2.superview!.markError(imvJoinNow.isHighlighted && unwrap(str: tfPassword1.text) != unwrap(str: tfPassword2.text))
  }
  
  func scrollToErrorField(_ errCode: ValidateErrorCode) {
    switch (errCode) {
    case .mobilePhoneNumberMissing:
      //TODO            btnMobileNumber.highlighted = true
      //TODO            scrollTo(btnMobileNumber)
      break
    case .mobilePhoneCodeMissing:
      //TODO            btnMobileCountryCode.highlighted = true
      //TODO            scrollTo(btnMobileCountryCode)
      break;
      //        case .EmailNotMuch:
      //            btnContactEmailConfirm.highlighted = true
      //            scrollTo(btnContactEmailConfirm)
    //            break;
    case .emailNotValidate:
      //TODO            btnContactEmail.highlighted = true
      //TODO            scrollTo(btnContactEmail)
      break;
    default:
      break;
    }
  }
  
  func updateJoinNow() {
    if FDLoginManager.sharedInstance.hasSavedUserCredential {
      vwNasmilesJoin.isHidden = true
    } else {
      vwNasmilesJoin.isHidden = false
      vwNasmilesPasswords.isHidden = !imvJoinNow.isHighlighted
    }
    view.layoutIfNeeded()
    highlightErrorField()
  }
  
  @IBAction func onBtnNaSmilesJoinNow(_ sender: UIButton) {
    imvJoinNow.isHighlighted = !imvJoinNow.isHighlighted
    updateJoinNow()
  }
  
  @IBAction func onBtnMobileNumber(_ sender: UIButton) {
    tfMobileNumber.becomeFirstResponder()
  }
  
  @IBAction func onBtnEmailAddress(_ sender: UIButton) {
    tfEmail1.becomeFirstResponder()
  }
  
  @IBAction func onBtnPassword1(_ sender: UIButton) {
    tfPassword1.becomeFirstResponder()
  }
  
  @IBAction func onBtnPassword2(_ sender: UIButton) {
    tfPassword2.becomeFirstResponder()
  }
  
}

extension FDPassengerVC: UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    switch (textField) {
    case tfMobileNumber:
      userBooking.contactInfo.mobilePhone.number = unwrap(str: textField.text)
      break
      //        case tfPhoneNumber:
      //            userBooking.contactInfo.workPhone.number = sender.text != nil ? sender.text! : ""
    //            break
    case tfEmail1:
      userBooking.contactInfo.emailAddress = textField.text != nil ? textField.text! : ""
      userBooking.contactInfo.emailAddressConfirm = userBooking.contactInfo.emailAddress
      break
      //        case tfContactEmailConfirm:
      //            userBooking.contactInfo.emailAddressConfirm = sender.text != nil ? sender.text! : ""
    //            break
    default:
      break
    }
    updateUI()
  }
  
  func textFieldDidChange(_ textField: UITextField) {
    textField.text = FDConvertArNumberToEn(textField.text)
  }
  
}

extension FDPassengerVC: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return userBooking.passengers.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    var height = FDPassengerVC.HeightTableViewCellHeader
    if currentExpandedCell == indexPath.row {
      height += FDPassengerVC.HeightTableViewCellDetail
      if shouldHideNasmiles(indexPath.row) {
        height -= FDPassengerTableCell.HeightNaSmiles
      }
    }
    return height
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "FDPassengerTableCell") as! FDPassengerTableCell
    cell.isDomestic = isDomestic
    if let d = userBooking.documentTypes {
      cell.documentTypes = d
    }
    cell.passenger = userBooking.passengers[indexPath.row]
    cell.delegate = self
    cell.expanded = currentExpandedCell == indexPath.row
    return cell
  }
  
}
