//
//  FDMyProfileTableCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 20/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

protocol FDMyProfileTableCellDelegate : class {
    func btnExpandClicked(_ cell:FDMyProfileTableCell)
    func btnUpdateClicked(_ cell:FDMyProfileTableCell)
    func btnDeleteClicked(_ cell:FDMyProfileTableCell)
    func pushHijiri(_ cell:FDMyProfileTableCell)
    func validateError(_ cell:FDMyProfileTableCell,errCode:ValidateErrorCode)
    func updateTableCellHeight(_ cell: FDMyProfileTableCell)
}

class FDMyProfileTableCell: UITableViewCell {

    var passenger = FDPassenger(paxType: .Adult, passengerNumber: 0) {
        didSet {
            updatePassengersDetail()
        }
    }
    var expand = false {
        didSet {
            updateBtns()
        }
    }

    var newMember = false {
        didSet {
            updateBtns()
        }
    }

    var canDelete = false {
        didSet {
            updateBtns()
        }
    }
    



    weak var delegate: FDMyProfileTableCellDelegate?

    var documentTypes = FDDocumentTypes()

    @IBOutlet var heightPassengerDetail: NSLayoutConstraint!
    @IBOutlet var viewPassengerDetail: UIView!

    @IBOutlet var labelPassengeTitle: UILabel!

    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var tfFirstName: UITextField!
    @IBOutlet var tfLastName: UITextField!
    @IBOutlet var btnDob: UIButton!
    @IBOutlet var btnDobHijri: UIButton!
    @IBOutlet var btnIssuingCountry: UIButton!

//    @IBOutlet var btnBirthCountry: UIButton!
    @IBOutlet var btnDocTypeCode: UIButton!
    @IBOutlet var btnNationality: UIButton!
    @IBOutlet var tfDocNumber: UITextField!
    @IBOutlet var btnExpirationDate: UIButton!
//    @IBOutlet var btnExpirationDateHijri: UIButton!

    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnEdit2: UIButton!
    @IBOutlet var btnAddNew: UIButton!
    @IBOutlet var btnUpadte: UIButton!
    @IBOutlet var btnDelete: UIButton!

    // for error highlight
    @IBOutlet var btnFirstName: UIButton!
    @IBOutlet var btnLastName: UIButton!
    @IBOutlet var btnIDNumber: UIButton!
    @IBOutlet var btnRelationShip: UIButton!

//    @IBOutlet var widthExpireDateHijiri: NSLayoutConstraint!
    @IBOutlet var heightExpireDate: NSLayoutConstraint!
    @IBOutlet var viewRelationship: UIView!

    @IBOutlet var tfnaSmiles: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        tfDocNumber.addTarget(self, action: #selector(FDMyProfileTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfDocNumber.addTarget(self, action: #selector(FDMyProfileTableCell.textFieldDidChange(_:)), for: .editingChanged)

        tfFirstName.addTarget(self, action: #selector(FDMyProfileTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfLastName.addTarget(self, action: #selector(FDMyProfileTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfnaSmiles.addTarget(self, action: #selector(FDMyProfileTableCell.textFieldEditingDidEnd(_:)), for: .editingDidEnd)


        if isRTL() {
            FDReplaceBtnAttributedTitle(btnDelete, title: FDLocalized("Delete"), state: UIControlState())
        }

        let imageHighlight = UIImage(named: "bg-btn-highlight")
        btnTitle.setBackgroundImage(imageHighlight, for: .highlighted)
        btnTitle.setBackgroundImage(imageHighlight, for: .highlighted)
        btnFirstName.setBackgroundImage(imageHighlight, for: .highlighted)
        btnLastName.setBackgroundImage(imageHighlight, for: .highlighted)

        btnDob.setBackgroundImage(imageHighlight, for: .highlighted)

        btnNationality.setBackgroundImage(imageHighlight, for: .highlighted)
//        btnBirthCountry.setBackgroundImage(imageHighlight, forState: .Highlighted)
        btnIssuingCountry.setBackgroundImage(imageHighlight, for: .highlighted)
        btnDocTypeCode.setBackgroundImage(imageHighlight, for: .highlighted)

        btnExpirationDate.setBackgroundImage(imageHighlight, for: .highlighted)
        btnIDNumber.setBackgroundImage(imageHighlight, for: .highlighted)
        btnRelationShip.setBackgroundImage(imageHighlight, for: .highlighted)
        
        updatePassengersDetail()
    }

    func textFieldDidChange(_ sender:UITextField) {
        sender.text = FDConvertArNumberToEn(sender.text)
    }

    func textFieldEditingDidEnd(_ sender:UITextField) {

        switch (sender) {
        case tfFirstName:
            passenger.firstName = sender.text != nil ? sender.text! : ""
            break
        case tfLastName:
            passenger.lastName = sender.text != nil ? sender.text! : ""
            break
        case tfDocNumber:
            passenger.travelDocument.docNumber = sender.text != nil ? sender.text! : ""
            break
        case tfnaSmiles:
            passenger.nasmiles = sender.text != nil ? sender.text! : ""
            break
        default:break
        }

        updatePassengersDetail()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateBtns() {
        if expand {
            btnAddNew.isHidden = true
            heightPassengerDetail.priority = 100
        } else {
            if newMember {
                btnAddNew.isHidden = false
            } else {
                btnAddNew.isHidden = true
            }
            heightPassengerDetail.priority = 900
        }

        btnEdit.setImage(UIImage(named: expand ? "icn-minus" : "icn-add"), for: UIControlState())

        btnDelete.isHidden = !canDelete

        if newMember {
            btnUpadte.setTitle(FDLocalized("SAVE"), for: UIControlState())
        } else {
            btnUpadte.setTitle(FDLocalized("UPDATE"), for: UIControlState())
        }
    }

    func updatePassengersDetail () {

        let p = passenger

        labelPassengeTitle.text = p.fullName
        btnTitle.setTitle(FDResourceManager.sharedInstance.titleByKey(p.title), for: UIControlState())
        tfFirstName.text = p.firstName
        tfLastName.text = p.lastName
        btnDob.setTitle(p.dateOfBirth?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar:passenger.dateOfBirthHijri), for: UIControlState())
        btnDobHijri.isSelected = p.dateOfBirthHijri
        btnIssuingCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(p.travelDocument.docIssuingCountry) , for: UIControlState())

//        btnBirthCountry.setTitle( FDResourceManager.sharedInstance.countryNameByCode(p.travelDocument.birthCountry), forState: .Normal)
        btnDocTypeCode.setTitle(documentTypes.getDocumentTypeName(p.travelDocument.docTypeCode), for: UIControlState())
        btnNationality.setTitle(FDResourceManager.sharedInstance.countryNameByCode(p.nationality), for: UIControlState())
        tfDocNumber.text = p.travelDocument.docNumber
        btnExpirationDate.setTitle(p.travelDocument.expirationDate?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar:p.travelDocument.expirationDateHijri), for: UIControlState())
//            btnExpirationDateHijri.selected = p.travelDocument.expirationDateHijri

        btnRelationShip.setTitle(FDRM.sharedInstance.relationshipNameByCode(p.relationship), for: UIControlState())

//        widthExpireDateHijiri.priority = passenger.travelDocument.hideHijiri ? 900 : 100
        heightExpireDate.priority = passenger.travelDocument.hideExpire ? 900 : 100

        tfnaSmiles.text = passenger.nasmiles

        highlightErrorField(true)
    }

    @IBAction func onBtnEditClicked(_ sender: UIButton) {
        self.endEditing(true)
        delegate?.btnExpandClicked(self)
    }

    @IBAction func onBtnUpdateClicked(_ sender: UIButton) {
        self.endEditing(true)

        let errCode = passenger.validateAll
        if errCode == .none {
            highlightErrorField(false)
            delegate?.btnUpdateClicked(self)
        } else {
            highlightErrorField(true)
            delegate?.validateError(self,errCode: errCode)
        }
    }

    func highlightErrorField(_ markErr: Bool) {

        if markErr {
            btnTitle.isHighlighted = passenger.title.isEmpty

            btnFirstName.isHighlighted = passenger.firstName.isEmpty || !passenger.firstName.isValidateName
            btnLastName.isHighlighted = passenger.lastName.isEmpty || !passenger.lastName.isValidateName

            btnDob.isHighlighted = passenger.dateOfBirth == nil

            btnNationality.isHighlighted = passenger.nationality.isEmpty
//            btnBirthCountry.highlighted = passenger.travelDocument.birthCountry.isEmpty
            btnIssuingCountry.isHighlighted = passenger.travelDocument.docIssuingCountry.isEmpty

            btnDocTypeCode.isHighlighted = passenger.travelDocument.docTypeCode.isEmpty

            btnIDNumber.isHighlighted = passenger.travelDocument.validateDocumentNumber != .none
            btnExpirationDate.isHighlighted = passenger.travelDocument.expirationDate == nil

            btnRelationShip.isHighlighted = passenger.relationship.isEmpty
        } else {
            btnTitle.isHighlighted = false
            btnFirstName.isHighlighted = false
            btnLastName.isHighlighted = false

            btnDob.isHighlighted = false

            btnNationality.isHighlighted = false
//            btnBirthCountry.highlighted = false
            btnIssuingCountry.isHighlighted = false
            btnDocTypeCode.isHighlighted = false

            btnExpirationDate.isHighlighted = false
            btnIDNumber.isHighlighted = false
            btnRelationShip.isHighlighted = false
        }
    }

    @IBAction func onBtnDeleteClicked(_ sender: UIButton) {
        self.endEditing(true)

        let errorMsg = FDLocalized("ARE YOU SURE?")
        UIAlertView.showWithTitle("", message: errorMsg, cancelButtonTitle: FDLocalized("NO"), otherButtonTitle: FDLocalized("YES")) { (alertView, buttonIndex) -> Void in
            if buttonIndex == 1 {
                self.delegate?.btnDeleteClicked(self)
            }
        }
    }

    @IBAction func onNationalityClicked(_ sender: UIView) {
        self.endEditing(true)
        FDUtility.selectCountry(FDLocalized("Nationality"), initValue: passenger.nationality, empty: true, origin:sender) { (selectedValue) -> Void in
            self.passenger.nationality = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onCountryofBirthClicked(_ sender: UIView) {
        self.endEditing(true)
//        FDUtility.selectCountry(FDLocalized("Country of Birth"), initValue: passenger.travelDocument.birthCountry, origin:sender) { (selectedValue) -> Void in
//            self.passenger.travelDocument.birthCountry = selectedValue
//            self.updatePassengersDetail()
//        }
    }

    @IBAction func onIssuingCountryClicked(_ sender: UIView) {
        self.endEditing(true)
        FDUtility.selectCountry(FDLocalized("Issuing Country"), initValue: passenger.travelDocument.docIssuingCountry, empty: false, origin:sender) { (selectedValue) -> Void in
            self.passenger.travelDocument.docIssuingCountry = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onDobClicked(_ sender: UIView) {

        self.endEditing(true)

        let maximumDate = Date().dateByAddingDays(-1)

        let selectedDate = passenger.dateOfBirth != nil ? passenger.dateOfBirth : maximumDate
        FDUtility.showPickerWithTitle(FDLocalized("Date of birth"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar:passenger.dateOfBirthHijri,
                                      minimumDate: nil,
                                      maximumDate: maximumDate,
                                      doneBlock: { (picker, value, index) -> Void in
            self.passenger.dateOfBirth = value as? Date
            self.updatePassengersDetail()
            }, cancelBlock: { (_) -> Void in
            }, origin: sender)
    }

    @IBAction func onDobHijriClicked(_ sender: AnyObject) {
        self.endEditing(true)
        passenger.dateOfBirthHijri = !passenger.dateOfBirthHijri
        updatePassengersDetail()
        delegate?.pushHijiri(self)
    }

    @IBAction func onIDExpirationDateClicked(_ sender: UIView) {
        self.endEditing(true)
        let selectedDate = passenger.travelDocument.expirationDate != nil ? passenger.travelDocument.expirationDate : Date()
        FDUtility.showPickerWithTitle(FDLocalized("Expiry Date"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar:passenger.travelDocument.expirationDateHijri, minimumDate:Date(),maximumDate:nil,  doneBlock: { (picker, value, index) -> Void in
            self.passenger.travelDocument.expirationDate = value as? Date
            self.updatePassengersDetail()
            }, cancelBlock: { (_) -> Void in
            }, origin: sender)
    }

    @IBAction func onIDExpirationDateHijriClicked(_ sender: AnyObject) {
        endEditing(true)
        passenger.travelDocument.expirationDateHijri = !passenger.travelDocument.expirationDateHijri
        updatePassengersDetail()
        delegate?.pushHijiri(self)
    }

  @IBAction func onTitleClicked(_ sender: UIView) {
    self.endEditing(true)
    let allTitleDes = FDResourceManager.sharedInstance.titlesDes + FDResourceManager.sharedInstance.titlesDesChildInfant
    let allTitleKeys = FDResourceManager.sharedInstance.titlesKey + FDResourceManager.sharedInstance.titlesKeyChildInfant
    let index = allTitleKeys.index(of: passenger.title)
    ActionSheetStringPicker.show(withTitle: FDLocalized("Title"), rows: allTitleDes, initialSelection: index==nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      self.passenger.title = allTitleKeys[selectedIndex]
      self.updatePassengersDetail()
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
  }

    @IBAction func onTravelDocTypeClicked(_ sender: UIView) {

        self.endEditing(true)

        let items = documentTypes.documentTypes.map({$0.name})
        let index = documentTypes.documentTypes.index(where: {$0.code == passenger.travelDocument.docTypeCode})

        ActionSheetStringPicker.show(withTitle: FDLocalized("Document Type"), rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
            picker, selectedIndex, selectedValue in

            self.passenger.travelDocument.docTypeCode = self.documentTypes.documentTypes[selectedIndex].code
            self.updatePassengersDetail()
            self.delegate?.updateTableCellHeight(self)

            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)

    }

    @IBAction func onRelationshipClicked(_ sender: UIView) {
        self.endEditing(true)

        if let rs = FDRM.sharedInstance.relationships {
            let items = rs.relationships.map({$0.name == nil ? "" : $0.name!})
            let index = rs.relationships.index(where: {$0.value == passenger.relationship})

            ActionSheetStringPicker.show(withTitle: FDLocalized("Relationship"), rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
                picker, selectedIndex, selectedValue in

                if let v = rs.relationships[selectedIndex].value {
                    self.passenger.relationship = v
                }

                self.updatePassengersDetail()

                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        } else {

        }

    }
}
