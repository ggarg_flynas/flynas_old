//
//  FDSeatMapVC.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 13/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDSeatMapVC: FDBookingCommonVC,UITableViewDelegate, UITableViewDataSource {
  
  let MoveToNextSeatSelection = "MoveToNextSeatSelection"
  enum SeatMapCellControlTag : Int {
    case headerLabel1 = 40
    case headerLabel2 = 41
    case seatmapSeat1 = 51
    case seatmapSeat2 = 52
    case seatmapSeat3 = 53
    case seatmapSeat4 = 54
    case seatmapSeat5 = 55
    case seatmapSeat6 = 56
  }
  
  enum SeatMapSeatState {
    case avaiable
    case taken
    case user
  }
  
  var seatMaps = FDFlightSeatMap()
  var seatMapApi = FDSelSeatApi.booking
  var legIndex = 0
  var passengerIndex = 0
  var seatSelectionChanged = false
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var labelFlightDirection: UILabel!
  @IBOutlet var labelLeg: UILabel!
  @IBOutlet var labelPassengerName: UILabel!
  @IBOutlet var labelSeatNumber: UILabel!
  @IBOutlet var btnPreviousPassenger: UIButton!
  @IBOutlet var btnNextPassenger: UIButton!
  //    var seatMap = [(seatPriceType : SeatPriceType,  firstRowNumber : Int, rowAmount : Int)]()
  static let clrBusiness = UIColor(hex: "f1852f")
  static let clrSpacious = UIColor(hex: "fee268")
  static let clrSuperPractical = UIColor(hex: "e63e8c")
  static let clrPractical = UIColor(hex: "23b6b7")
  static let clrSimple = UIColor(hex: "23b6b7")
  static let clrLady = UIColor(hex: "973689")
  
  var seatMap : FDSeatMap?
  var fareType : String?
  let cellButtonBlock = KKCellButtonBlock()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("seat_map_page")
    if self.seatMapApi == .webCheckin {
      log("web_checkin_seatmap_page")
    }
    reloadSeatMap(false)
    tableView.delegate = self
    tableView.dataSource = self
    updateUI()
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(FDSeatMapVC.MoveNext(_:)),
      name: NSNotification.Name(rawValue: MoveToNextSeatSelection),
      object: nil)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    moveToUserSelection()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func MoveNext(_ notification: Notification){
    SVProgressHUD.show()
    let time = DispatchTime(uptimeNanoseconds: 0) + Double(Int64(NSEC_PER_SEC) / 5) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: time) {
      if self.legIndex < self.seatMaps.seatMaps.count - 1 || self.passengerIndex < self.seatMaps.seatMaps[self.legIndex].passengerSeatInfos.count - 1 {
        self.onBtnNextClicked(nil)
        SVProgressHUD.dismiss()
      } else {
        self.onBtnBackClicked(nil)
      }
    }
  }
  
  func updateUI () {
    if seatMap != nil && passengerIndex < seatMap!.passengerSeatInfos.count {
      labelFlightDirection.text = ""
      if let direction = seatMap?.diretion {
        labelFlightDirection.text = direction == .arrive ? FDLocalized("Return Flight:") : FDLocalized("Departing Flight:")
        labelLeg.text = seatMap!.origin + " - " + seatMap!.destination
        //                flightDirectionIcn.setImageAutoFlip(direction == .Arrive ? UIImage(named: "icn-flight-destination-m") : UIImage(named: "icn-flight-origin-m"))
      }
      if let passenger = seatMap?.passengerSeatInfos[passengerIndex] {
        labelPassengerName.text = passenger.fullName
        //                labelSeatPrice.text = FDUtility.formatCurrencyWithCurrencySymbol(passenger.selectedSeatPrice)
        labelSeatNumber.text = passenger.selectedSeatNumber
        btnPreviousPassenger.isEnabled = (legIndex > 0 && seatMaps.seatMaps[legIndex-1].changeAllowed) || passengerIndex > 0
        btnNextPassenger.isEnabled = (legIndex < seatMaps.seatMaps.count - 1 && seatMaps.seatMaps[legIndex+1].changeAllowed) || passengerIndex < seatMap!.passengerSeatInfos.count - 1
        tableView.reloadData()
      }
    }
  }
  
  func reloadSeatMap(_ animated:Bool = true,goNext:Bool = true) {
    // reload seat map due to flight change
    let oldSectionNumber = numberOfSections(in: tableView)
    seatMap = seatMaps.seatMaps[legIndex]
    fareType = seatMap!.fareType
    if animated {
      tableView.beginUpdates()
      
      tableView.deleteSections(IndexSet(integersIn: NSRange(location:0, length:oldSectionNumber).toRange() ?? 0..<0), with: (goNext && !isRTL() ? .left: .right))
      
      tableView.insertSections(IndexSet(integersIn: NSRange(location:0, length:numberOfSections(in: tableView)).toRange() ?? 0..<0), with: (goNext && !isRTL() ? .right: .left))
      tableView.endUpdates()
    } else {
      tableView.reloadData()
    }
  }
  
  func moveToUserSelection (_ moveToTopWhenNoSelection:Bool = true,animated:Bool = true) {
    if seatMap != nil {
      if let (seatGroupIndex,seatRowIndex) = seatMap!.getUserSelectedSeatPosition(passengerIndex) {
        tableView.scrollToRow(at: IndexPath(row: seatRowIndex, section: seatGroupIndex), at: .none, animated: animated)
      } else {
        if moveToTopWhenNoSelection  {
          tableView.scrollsToTop = true
          //                    tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .None, animated: animated)
        }
      }
    }
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton?) {
    if self.seatSelectionChanged {
      if !SVProgressHUD.isVisible() {
        SVProgressHUD.show()
      }
      if seatMapApi == FDSelSeatApi.booking {
        let p = ["flightSeatMaps": seatMaps.passengerSeatSelectionObject]
        FDBookingManager.sharedInstance.seatMap(p, success: { (flightSeatMap,errorMsg) -> Void in
          // vertify seat response
          self.seatMaps.updateUserSeatSelection(flightSeatMap)
          FDErrorHandle.apiCallErrorWithAler(errorMsg)
          self.navigationController?.popViewController(animated: true)
          SVProgressHUD.dismiss()
        }) { (reason) -> Void in
          SVProgressHUD.dismiss()
          if !self.errorHandleAndSwitchToSearchIfExpired(reason) {
            self.navigationController?.popViewController(animated: true)
          }
        }
      } else {
        let p = ["flightSeatMaps": seatMaps.passengerSeatSelectionObject]
        FDWebCheckinManager.sharedInstance.passengerSeats(p, success: { (flightSeatMap) -> Void in
          //                self.userBooking.flightSeatMap = flightSeatMap
          // vertify seat response
          self.seatMaps.updateUserSeatSelection(flightSeatMap)
          //FDErrorHandle.apiCallErrorWithAler(errorMsg)
          
          self.navigationController?.popViewController(animated: true)
          SVProgressHUD.dismiss()
        }) { (reason) -> Void in
          SVProgressHUD.dismiss()
          if !self.errorHandleAndSwitchToSearchIfExpired(reason) {
            self.navigationController?.popViewController(animated: true)
          }
        }
      }
    } else {
      SVProgressHUD.dismiss()
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  @IBAction func onBtnPreviousClicked(_ sender: UIButton) {
    // previous passenger
    if seatMap != nil {
      if passengerIndex > 0 {
        passengerIndex -= 1
        moveToUserSelection(false)
      } else {
        assert(legIndex > 0)
        legIndex -= 1
        passengerIndex = seatMap!.passengerSeatInfos.count - 1
        reloadSeatMap(true, goNext: false)
        moveToUserSelection()
      }
      updateUI()
    }
  }
  
  @IBAction func onBtnNextClicked(_ sender: UIButton?) {
    // next passenger
    if seatMap != nil {
      if passengerIndex < seatMap!.passengerSeatInfos.count - 1 {
        passengerIndex += 1
        moveToUserSelection(false)
      } else {
        legIndex += 1
        passengerIndex = 0
        reloadSeatMap(true, goNext: true)
        moveToUserSelection()
      }
      updateUI()
    }
  }
  
  // MARK: - seat map table
  func numberOfSections(in tableView: UITableView) -> Int {
    return seatMap != nil ? seatMap!.seatGroupNumber : 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let mySeatMap = seatMap else { return 0 }
    return mySeatMap.getSeatGroupRowNumber(section)
  }
  
  func tableViewLayoutAircraft330Seats(_ cell:UITableViewCell?, rowNumber:Int) {
    if let cell = cell {
      var seatViews = [FDFlightSeatView?](repeating: nil,count: 9)
      let w = CGFloat(Int((cell.bounds.width - 30) / 9))
      let scaleFill = w < 44
      let seatH = cell.bounds.height
      for seatIndex in 0..<9 {
        var seatView : FDFlightSeatView? = cell.viewWithTag(SeatMapCellControlTag.seatmapSeat1.rawValue + seatIndex) as? FDFlightSeatView
        if seatView == nil {
          // creat and add
          seatView = FDFlightSeatView()
          seatView!.tag = SeatMapCellControlTag.seatmapSeat1.rawValue + seatIndex
          seatView!.isHidden = true
          cell.addSubview(seatView!)
        }
        
        seatViews[seatIndex] = seatView
      }
      
      var seatViewPositions = [CGFloat](repeating: 0,count: 9)
      switch rowNumber {
      case 1,29,44:
        seatViewPositions = [15+3*w,15+4*w,15+5*w,0,0,0,0,0,0]
        break
      case 2...7:
        seatViewPositions = [ 5+0*w, 5+1*w,
                              15+0.5*w+2*w, 15+0.5*w+3*w,15+0.5*w+4*w,15+0.5*w+5*w,
                              25+1*w+6*w,25+1*w+7*w,
                              0]
        break
        
      case 37...43:
        seatViewPositions = [ 5+0*w, 5+1*w,
                              15+3*w,15+4*w,15+5*w,
                              25+7*w,25+8*w,
                              0,0]
        break
      default:
        seatViewPositions = [ 5+0*w, 5+1*w, 5+2*w,
                              15+3*w,15+4*w,15+5*w,
                              25+6*w,25+7*w,25+8*w,]
      }
      
      for seatIndex in 0..<9 {
        if let seatView = seatViews[seatIndex] {
          seatView.frame = CGRect(x: seatViewPositions[seatIndex], y: 0, width: w-1, height: seatH)
          seatView.imgSeat.contentMode = scaleFill ? UIViewContentMode.scaleToFill : UIViewContentMode.center
        }
      }
      
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let mySeatMap = seatMap else { return UITableViewCell() }
    let seatType = mySeatMap.getGroupType(indexPath.section)
    var cell : UITableViewCell?
    if mySeatMap.equipmentType == EquipmentTypeAircraftType.Aircraft330.rawValue {
      cell = tableView.dequeueReusableCell(withIdentifier: "SeatMapSeatRow")
      if let cell = cell {
        cell.frame.size = tableView.rectForRow(at: indexPath).size
      }
      tableViewLayoutAircraft330Seats(cell, rowNumber: mySeatMap.getSeatRowNumber(indexPath.section, rowIndex: indexPath.row))
    } else if mySeatMap.equipmentType == EquipmentTypeAircraftType.Aircraft767.rawValue {
      let cell767 = tableView.dequeueReusableCell(withIdentifier: "Aircraft767Cell") as! Aircraft767Cell
      cell767.delegate = self
      cell767.indexPath = indexPath
      cell767.seatMap = mySeatMap
      cell767.userBooking = userBooking
      cell767.passengerInfo = seatMap!.getPassengerSeatInfo(self.passengerIndex)
      _ = cell767.prepareSeats()
      return cell767
    } else if mySeatMap.equipmentType == EquipmentTypeAircraftType.Aircraft739.rawValue {
      let cell767 = tableView.dequeueReusableCell(withIdentifier: "Aircraft739Cell") as! Aircraft767Cell
      cell767.delegate = self
      cell767.indexPath = indexPath
      cell767.seatMap = mySeatMap
      cell767.userBooking = userBooking
      cell767.passengerInfo = seatMap!.getPassengerSeatInfo(self.passengerIndex)
      _ = cell767.prepareSeats()
      return cell767
    } else { // Aircraft320 & Aircraft738
      cell = seatType == .Business ? tableView.dequeueReusableCell(withIdentifier: "SeatMapBusinessSeats") : tableView.dequeueReusableCell(withIdentifier: "SeatMapEconomySeats")
    }
    guard let myCell = cell else { return UITableViewCell() }
    let passengerInfo = self.seatMap!.getPassengerSeatInfo(self.passengerIndex)
    for seatIndex in 0..<9 {
      if let seatView = myCell.viewWithTag(SeatMapCellControlTag.seatmapSeat1.rawValue + seatIndex) as? FDFlightSeatView {
        if let seatInfo = mySeatMap.getSeatInfo(indexPath.section, rowIndex: indexPath.row, colIndex: seatIndex) {
          if seatType != nil {
            seatView.setSeatPriceType(seatType!) // free - default
          }
          seatView.setSeatNumber(seatInfo.number)
          seatView.setUserSelectAction({[weak self] () -> Void in
            if self != nil {
              if let passengerInfo = self!.seatMap!.getPassengerSeatInfo(self!.passengerIndex) {
                if let oldSeat = passengerInfo.userSelectedSeat {
                  if oldSeat.number == seatInfo.number {
                    passengerInfo.userSelectedSeat = nil
                    return
                  }
                }
                func selectSeat() {
                  passengerInfo.userSelectedSeat = seatInfo
                  self?.updateUI()
                  self?.seatSelectionChanged = true
                  // move left automatically
                  NotificationCenter.default.post(name: Notification.Name(rawValue: self!.MoveToNextSeatSelection), object: nil)
                }
                // warning user if has exit row tag
                if seatInfo.exitRow {
                  let exitRowWarning = FDLocalized("Passenger with special needs, traveling with infant, children under 15 years, elderly, frail or any other who are not able to assist our crew in the event of an emergency. You will be relocated to other seats in the aircraft and no refund to be given.")
                  UIAlertView.showWithTitle("", message: exitRowWarning , cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitles: [FDLocalized("OK")], completionHandler: { (alertView, buttonIndex) -> Void in
                    if buttonIndex == 1 {
                      selectSeat ()
                    }
                  })
                } else {
                  selectSeat()
                }
              }
            }
          })
          // user selected
          let pn = mySeatMap.getSeatUserIndex(seatInfo.number)
          if pn > -1 {
            if let p = mySeatMap.getPassengerSeatInfo(pn) {
              seatView.setUserInitial(p.nameInitial)
            }
          } else {
            // infantAllowed
            if let p = passengerInfo {
              if userBooking.passengers.count == 0 {
                userBooking.passengers = SharedModel.shared.wciPassengers
              }
              let pax = userBooking.getPassengerInfo(p.passengerNumber)
              if p.hasInfantAttached && !seatInfo.infantAllowed {
                seatView.setNotAvailToInfant()
              } else if p.paxType == PaxType.Child.rawValue && !seatInfo.childAllowed {
                seatView.setNotAvailToChild()
              } else if !seatInfo.available {
                seatView.setSeatNotAvailable()
              } else if seatInfo.isFemaleOnly && (unwrap(str: pax?.title) == "MR" || unwrap(str: pax?.title) == "MSTR") {
                seatView.setSeatNotAvailable()
              } else {
                // seat is enable by seatView.setSeatPriceType
              }
            } else {
              seatView.setSeatNotAvailable()
            }
          }
          seatView.isHidden = false
        } else {
          seatView.isHidden = true
        }
      }
    }
    return myCell
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "SeatMapSectionHeader")
    let label1 = cell?.viewWithTag(SeatMapCellControlTag.headerLabel1.rawValue) as? UILabel
    let label2 = cell?.viewWithTag(SeatMapCellControlTag.headerLabel2.rawValue) as? UILabel
    let seatType = seatMap!.getGroupType(section)
    var seatPrice = ""
    if FDResourceManager.sharedInstance.getPayWithNaSmiles() {
      let points = seatMap!.getGroupPoints(section, passengerNumber: passengerIndex)?.1
      let sar = seatMap!.getGroupPoints(section, passengerNumber: passengerIndex)?.0
      seatPrice = "\(unwrap(str: points)) \(FDLocalized("points")) + \(unwrap(str: FDUtility.formatCurrencyWithCurrencySymbol(sar as NSNumber?)))"
    } else { // show in cash
      seatPrice = unwrap(str: FDUtility.formatCurrencyWithCurrencySymbol(seatMap!.getGroupPrice(section, passengerNumber: passengerIndex)))
    }
    var headerColor = FDSeatMapVC.clrBusiness
    var seatTypeTitle = FDLocalized("Practical - ")
    switch seatType! {
    case .Business:
      seatTypeTitle = FDLocalized("Business - ")
      seatPrice = ""
      break
    case .UpFront:
      headerColor = FDSeatMapVC.clrPractical
      seatTypeTitle = FDLocalized("Practical - ")
      break
    case .ExtraLeg:
      headerColor = FDSeatMapVC.clrSpacious
      seatTypeTitle = FDLocalized("Spacious - ")
      break
    case .Premium:
      headerColor = FDSeatMapVC.clrSuperPractical
      seatTypeTitle = FDLocalized("Super practical - ")
      break
    case .Simple:
      headerColor = FDSeatMapVC.clrSimple
      seatTypeTitle = FDLocalized("Simple - ")
      break
    case .Standard:
      headerColor = FDSeatMapVC.clrSimple
      seatTypeTitle = FDLocalized("Simple - ")
      break
    case .FemaleOnly:
      headerColor = FDSeatMapVC.clrLady
      seatTypeTitle = FDLocalized("Ladies only - ")
      break
    }
    if label1 != nil {
      label1!.text = seatTypeTitle
      label1!.textColor = headerColor
    }
    if label2 != nil {
      label2!.text = seatPrice
      label2!.textColor = headerColor
    }
    return cell?.contentView
  }
  
}

extension FDSeatMapVC: Aircraft767CellDelegate {

  func seatDidSelect(seatNumber: String) {
    guard let seatInfo = seatMap?.getSeatInfo(seatNumber) else { return }
    if let passengerInfo = self.seatMap!.getPassengerSeatInfo(self.passengerIndex) {
      if let oldSeat = passengerInfo.userSelectedSeat {
        if oldSeat.number == seatInfo.number {
          passengerInfo.userSelectedSeat = nil
          return
        }
      }
      func selectSeat() {
        passengerInfo.userSelectedSeat = seatInfo
        self.updateUI()
        self.seatSelectionChanged = true
        // move left automatically
        NotificationCenter.default.post(name: Notification.Name(rawValue: self.MoveToNextSeatSelection), object: nil)
      }
      // warning user if has exit row tag
      if seatInfo.exitRow {
        let exitRowWarning = FDLocalized("Passenger with special needs, traveling with infant, children under 15 years, elderly, frail or any other who are not able to assist our crew in the event of an emergency. You will be relocated to other seats in the aircraft and no refund to be given.")
        UIAlertView.showWithTitle("", message: exitRowWarning , cancelButtonTitle: FDLocalized("CANCEL"), otherButtonTitles: [FDLocalized("OK")], completionHandler: { (alertView, buttonIndex) -> Void in
          if buttonIndex == 1 {
            selectSeat ()
          }
        })
      } else {
        selectSeat()
      }
    }
  }
  
}
