//
//  FDRegisterVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 21/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDRegisterVC: BaseVC {

//    var userID : String = ""
    var userEmail : String = ""
    var password : String = ""

    var address : String = ""
//    var city : String = ""
    var mobileCountryCode = "SA"
    var moible = ""
    var phone = ""
    var phoneCountryCode = "SA"
    var fax = ""
  var referralId = ""

    var documentTypes = FDDocumentTypes()

    var passenger = FDPassenger(paxType: .Adult, passengerNumber: 0)


  @IBOutlet weak var tfReferalId: UITextField!
    @IBOutlet var tfUserID: UITextField!
    @IBOutlet var tfPass: UITextField!

    // contact detail
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var tfFirstName: UITextField!
    @IBOutlet var tfLastName: UITextField!
//    @IBOutlet var tfAddress: UITextField!
//    @IBOutlet var tfCity: UITextField!
//    @IBOutlet var btnContactCountry: UIButton!
    @IBOutlet var btnNationality: UIButton!
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var tfPhone: UITextField!
//    @IBOutlet var tfFax: UITextField!
    @IBOutlet var btnDob: UIButton!
    @IBOutlet var btnDobHijri: UIButton!

    @IBOutlet var btnDocTypeCode: UIButton!
    @IBOutlet var btnIssuingCountry: UIButton!
    @IBOutlet var btnExpirationDate: UIButton!
    @IBOutlet var btnExpirationDateHijri: UIButton!
//    @IBOutlet var btnBirthCountry: UIButton!
    @IBOutlet var tfDocNumber: UITextField!

    @IBOutlet var scrollView: UIScrollView!

    @IBOutlet var btnNext: UIButton!
    @IBOutlet var labelTermsEn: UILabel!
    @IBOutlet var labelTermsAr: UILabel!

    // for error highlight
    @IBOutlet var btnFirstName: UIButton!
    @IBOutlet var btnLastName: UIButton!
    @IBOutlet var btnIDNumber: UIButton!

    @IBOutlet var btnPhoneNumber: UIButton!
    @IBOutlet var btnMobileNumber: UIButton!
    @IBOutlet var btnEmail: UIButton!
    @IBOutlet var btnPass: UIButton!
//    @IBOutlet var btnCity: UIButton!
//    @IBOutlet var btnAddress: UIButton!

//    @IBOutlet var widthExpireDateHijiri: NSLayoutConstraint!
    @IBOutlet var heightExpireDate: NSLayoutConstraint!

    @IBOutlet var btnMobileCountryCode: UIButton!
    @IBOutlet var btnPhoneCountryCode: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("register_page")
        labelTermsEn.isHidden = isAr()
        labelTermsAr.isHidden = isEn()
        SVProgressHUD.show()
        FDBookingManager.sharedInstance.getDocumentTypes({ (documentTypes) -> Void in
            SVProgressHUD.dismiss()
            self.documentTypes = documentTypes
            self.updatePassengersDetail()
            }, fail: { (reason) -> Void in
                SVProgressHUD.dismiss()
                self.errorHandleAndSwitchToSearchIfExpired(reason)
        })

        tfUserID.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfPass.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
//        tfAddress.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), forControlEvents: .EditingDidEnd)
//        tfCity.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), forControlEvents: .EditingDidEnd)

//        tfAddress.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), forControlEvents: .EditingChanged)
//        tfCity.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), forControlEvents: .EditingChanged)

        tfMobile.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfPhone.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)

        tfMobile.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), for: .editingChanged)
//        tfPhone.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), forControlEvents: .EditingChanged)
//        
//        tfFax.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), forControlEvents: .EditingDidEnd)
//        tfFax.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), forControlEvents: .EditingChanged)

//        tfEmail.addTarget(self, action: "textFieldEditingDidEnd:", forControlEvents: .EditingDidEnd)

      tfReferalId.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfDocNumber.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfDocNumber.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), for: .editingChanged)

        tfFirstName.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        tfLastName.addTarget(self, action: #selector(FDRegisterVC.textFieldEditingDidEnd(_:)), for: .editingDidEnd)

        tfFirstName.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), for: .editingChanged)
        tfLastName.addTarget(self, action: #selector(FDRegisterVC.textFieldDidChange(_:)), for: .editingChanged)

        let imageHighlight = UIImage(named: "bg-btn-highlight")
        btnTitle.setBackgroundImage(imageHighlight, for: .highlighted)
        btnFirstName.setBackgroundImage(imageHighlight, for: .highlighted)
        btnLastName.setBackgroundImage(imageHighlight, for: .highlighted)

        btnDob.setBackgroundImage(imageHighlight, for: .highlighted)

        btnNationality.setBackgroundImage(imageHighlight, for: .highlighted)
//        btnBirthCountry.setBackgroundImage(imageHighlight, forState: .Highlighted)
        btnIssuingCountry.setBackgroundImage(imageHighlight, for: .highlighted)
        btnDocTypeCode.setBackgroundImage(imageHighlight, for: .highlighted)

        btnExpirationDate.setBackgroundImage(imageHighlight, for: .highlighted)
        btnIDNumber.setBackgroundImage(imageHighlight, for: .highlighted)

        btnPhoneNumber.setBackgroundImage(imageHighlight, for: .highlighted)
        btnMobileNumber.setBackgroundImage(imageHighlight, for: .highlighted)

        btnEmail.setBackgroundImage(imageHighlight, for: .highlighted)
        btnPass.setBackgroundImage(imageHighlight, for: .highlighted)
//        btnCity.setBackgroundImage(imageHighlight, forState: .Highlighted)
    }

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
      dismiss(animated: true, completion: nil)
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
      dismiss(animated: true, completion: nil)
    }

    func validateForRegister () -> ValidateErrorCode {
        if userEmail.isEmpty {
            return .emailMissing
        }

        if password.isEmpty {
            return .passwordMissing
        }
      if referralId.characters.count > 0 && referralId.characters.count != 10 {
        return .referralIdMissing
      }

//        if city.isEmpty {
//            return .AddressMissing
//        }

        if moible.isEmpty {
            return .mobilePhoneNumberMissing
        }

        return passenger.validateAll
    }

    @IBAction func onBtnNextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        // validate first
        let validateError = validateForRegister()
        if validateError == .none {

            SVProgressHUD.show()
            FDLoginManager.sharedInstance.registerUser(userEmail, password: password, parameters: getRegisterObject(), success: { (memberLogin) -> Void in
                SVProgressHUD.dismiss()
              AdjustSDKMan.registration()
              self.dismiss(animated: true, completion: nil)
                }, fail: { (r) -> Void in
                    SVProgressHUD.dismiss()
                    self.errorHandle(r)
            })
        } else {
            highlightErrorField(true)
            FDErrorHandle.validateErrorHandle(validateError)
        }
    }

    func getRegisterObject() -> [AnyHashable: Any] {

        var mobileCountryCodeNumber = ""
        if let c = FDResourceManager.sharedInstance.countryPhoneNumberByCode(mobileCountryCode) {
            mobileCountryCodeNumber = c
        }

      var phones = [
        [
          "number": "+\(mobileCountryCodeNumber) \(moible)",
          "typeCode":FDPhoneTypeCode.Mobile.rawValue
        ]
      ]

        if !phone.isEmpty {
            var phoneCountryCodeNumber = ""
            if let c = FDResourceManager.sharedInstance.countryPhoneNumberByCode(phoneCountryCode) {
                phoneCountryCodeNumber = c
            }

            phones.append(
                [
                    "number":"+\(phoneCountryCodeNumber) \(phone)",
                    "typeCode":FDPhoneTypeCode.Home.rawValue
                ])
        }

        if !fax.isEmpty {
            phones.append([
                "number":fax,
                "typeCode":"F"
                ]
            )
        }

        let register : [AnyHashable: Any] = [
          "profileAddresses":[
            "addresses":[
              [
                "addressLine1":"",
                "city":"",
                "countryCode":"SA"
              ]
            ]
          ],
            "profileDetails":[
                "details":[
                    "dateOfBirth": passenger.dateOfBirth != nil ? passenger.dateOfBirth!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false) : "",
                    "email":userEmail,
                    "first":passenger.firstName,
                    "last":passenger.lastName,
                    "nationality":passenger.travelDocument.nationality,
                    "title":passenger.title
                ]
            ],
            "profilePhones":[
                "phones": phones
            ],
            "profileRegister":[
                "password":password,
              "referralId": referralId
            ],
            "profileTravelDocument":[
                "documents":[
                [
                "birthCountry":passenger.travelDocument.birthCountry,
                  "expirationDate": passenger.travelDocument.expirationDate != nil ? passenger.travelDocument.expirationDate!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false) : "",
                "first":passenger.firstName,
                "last":passenger.lastName,
                "nationality":passenger.travelDocument.nationality,
                "number":passenger.travelDocument.docNumber,
                "title":passenger.title,
                "typeCode":passenger.travelDocument.docTypeCode
                ]
                ]
            ]
        ]

        return register
    }

    func textFieldDidChange(_ sender:UITextField) {
//        switch (sender) {
//        case tfMobile,tfDocNumber :
//            sender.text = FDRemoveSpecialCharsFromString(FDConvertArNumberToEn(sender.text))
//            break
//        default:
//            break
//        }
        sender.text = FDRemoveSpecialCharsFromString(FDConvertArNumberToEn(sender.text))
    }

    func textFieldEditingDidEnd(_ sender:UITextField) {
        self.view.endEditing(true)

        switch (sender) {
        case tfUserID:
            userEmail = sender.text != nil ? sender.text! : ""
            break

        case tfPass:
            password = sender.text != nil ? sender.text! : ""
            break
        case tfReferalId:
          referralId = unwrap(str: sender.text)

        case tfFirstName:
            passenger.firstName = sender.text != nil ? sender.text! : ""
            break

        case tfLastName:
            passenger.lastName = sender.text != nil ? sender.text! : ""
            break

        case tfDocNumber:
            passenger.travelDocument.docNumber = sender.text != nil ? sender.text! : ""
            break

//        case tfAddress:
//            address = sender.text != nil ? sender.text! : ""
//            break

//        case tfEmail:
//            userEmail = sender.text != nil ? sender.text! : ""
//            break

//        case tfCity:
//            city = sender.text != nil ? sender.text! : ""
//            break

        case tfMobile:
            moible = sender.text != nil ? sender.text! : ""
            break

        case tfPhone:
            phone = sender.text != nil ? sender.text! : ""
            break

//        case tfFax:
//            fax = sender.text != nil ? sender.text! : ""
//            break
        default:break
        }

        updatePassengersDetail()
    }

    func updatePassengersDetail() {
        btnTitle.setTitle(FDResourceManager.sharedInstance.titleByKey(passenger.title), for: UIControlState())
        tfFirstName.text = passenger.firstName
        tfLastName.text = passenger.lastName
//        btnContactCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(passenger.residentCountry), forState: .Normal)
        btnNationality.setTitle(FDResourceManager.sharedInstance.countryNameByCode(passenger.travelDocument.nationality), for: UIControlState())

        btnDob.setTitle(passenger.dateOfBirth?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar: passenger.dateOfBirthHijri), for: UIControlState())
        btnDobHijri.isSelected = passenger.dateOfBirthHijri
      

        btnIssuingCountry.setTitle(FDResourceManager.sharedInstance.countryNameByCode(passenger.travelDocument.docIssuingCountry) , for: UIControlState())
//        btnBirthCountry.setTitle( FDResourceManager.sharedInstance.countryNameByCode(passenger.travelDocument.birthCountry), forState: .Normal)
        btnDocTypeCode.setTitle(documentTypes.getDocumentTypeName(passenger.travelDocument.docTypeCode), for: UIControlState())

        tfDocNumber.text = passenger.travelDocument.docNumber
        btnExpirationDate.setTitle(passenger.travelDocument.expirationDate?.toString(format: .custom("dd MMM yyyy"), showHijiriCalendar: passenger.travelDocument.expirationDateHijri), for: UIControlState())
        btnExpirationDateHijri.isSelected = passenger.travelDocument.expirationDateHijri

//        widthExpireDateHijiri.priority = passenger.travelDocument.hideHijiri ? 900 : 100
        heightExpireDate.priority = passenger.travelDocument.hideExpire ? 900 : 100

        btnMobileCountryCode.setTitle(FDResourceManager.sharedInstance.countryPhoneCodeByCode(mobileCountryCode), for: UIControlState())
        btnPhoneCountryCode.setTitle(FDResourceManager.sharedInstance.countryPhoneCodeByCode(phoneCountryCode), for: UIControlState())
    }

    @IBAction func onCountryClicked(_ sender: UIView) {
        self.view.endEditing(true)
      FDUtility.selectCountry(FDLocalized("Country of Birth"), initValue: passenger.residentCountry, empty: false, origin:sender) { (selectedValue) -> Void in
            self.passenger.residentCountry = selectedValue
            self.updatePassengersDetail()
        }
    }


    @IBAction func onNationalityClicked(_ sender: UIView) {
        self.view.endEditing(true)
        FDUtility.selectCountry(FDLocalized("Nationality"), initValue: passenger.travelDocument.nationality, empty: true, origin:sender) { (selectedValue) -> Void in
            self.passenger.travelDocument.nationality = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onCountryofBirthClicked(_ sender: UIView) {
        self.view.endEditing(true)
//        FDUtility.selectCountry(FDLocalized("Country of Birth"), initValue: passenger.travelDocument.birthCountry, origin:sender) { (selectedValue) -> Void in
//            self.passenger.travelDocument.birthCountry = selectedValue
//            self.updatePassengersDetail()
//        }
    }

    @IBAction func onIssuingCountryClicked(_ sender: UIView) {
        self.view.endEditing(true)
        FDUtility.selectCountry(FDLocalized("Issuing Country"), initValue: passenger.travelDocument.docIssuingCountry, empty: false, origin:sender) { (selectedValue) -> Void in
            self.passenger.travelDocument.docIssuingCountry = selectedValue
            self.updatePassengersDetail()
        }
    }

  @IBAction func onDobClicked(_ sender: UIView) {
    view.endEditing(true)
    let maximumDate = Date().dateByAddingDays(-1)
    let selectedDate = passenger.dateOfBirth != nil ? passenger.dateOfBirth : maximumDate
    FDUtility.showPickerWithTitle(FDLocalized("Date of birth"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: passenger.dateOfBirthHijri,
                                  minimumDate: nil,
                                  maximumDate: maximumDate,
                                  doneBlock: { (picker, value, index) -> Void in
                                    //            self.passenger.dateOfBirth = value as? NSDate
                                    if let value = value {
                                      if value is Date {
                                        self.passenger.dateOfBirth = value as? Date
                                        self.updatePassengersDetail()
                                      }
                                    }
    }, cancelBlock: { (_) -> Void in
    }, origin: sender)
  }

    @IBAction func onDobHijriClicked(_ sender: AnyObject) {
        self.view.endEditing(true)
        passenger.dateOfBirthHijri = !passenger.dateOfBirthHijri
        updatePassengersDetail()
    }

    @IBAction func onIDExpirationDateClicked(_ sender: UIView) {
        self.view.endEditing(true)
        let selectedDate = passenger.travelDocument.expirationDate != nil ? passenger.travelDocument.expirationDate : Date()
        FDUtility.showPickerWithTitle(FDLocalized("Expiry Date"), datePickerMode: .date, selectedDate: selectedDate, showHijiriCalendar: passenger.travelDocument.expirationDateHijri, minimumDate:Date(),maximumDate:nil,  doneBlock: { (picker, value, index) -> Void in
            self.passenger.travelDocument.expirationDate = value as? Date
            self.updatePassengersDetail()
            }, cancelBlock: { (_) -> Void in
            }, origin: sender)
    }

    @IBAction func onIDExpirationDateHijriClicked(_ sender: AnyObject) {
        self.view.endEditing(true)
        passenger.travelDocument.expirationDateHijri = !passenger.travelDocument.expirationDateHijri
        updatePassengersDetail()
    }

  @IBAction func onTitleClicked(_ sender: UIView) {
    self.view.endEditing(true)
    let allTitleDes = FDResourceManager.sharedInstance.titlesDes + FDResourceManager.sharedInstance.titlesDesChildInfant
    let allTitleKeys = FDResourceManager.sharedInstance.titlesKey + FDResourceManager.sharedInstance.titlesKeyChildInfant
    let index = allTitleKeys.index(of: passenger.title)
    ActionSheetStringPicker.show(withTitle: FDLocalized("Title"), rows: allTitleDes, initialSelection: index==nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      self.passenger.title = allTitleKeys[selectedIndex]
      self.updatePassengersDetail()
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
  }

    @IBAction func onTravelDocTypeClicked(_ sender: UIView) {

        self.view.endEditing(true)

        if documentTypes.documentTypes.count > 0 {
            let items = documentTypes.documentTypes.map({$0.name})
            let index = documentTypes.documentTypes.index(where: {$0.code == passenger.travelDocument.docTypeCode})

            ActionSheetStringPicker.show(withTitle: FDLocalized("Document Type"), rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
                picker, selectedIndex, selectedValue in

                self.passenger.travelDocument.docTypeCode = self.documentTypes.documentTypes[selectedIndex].code
                self.updatePassengersDetail()

                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
        
    }

    func highlightErrorField(_ markErr: Bool) {

        if markErr {
            btnTitle.isHighlighted = passenger.title.isEmpty

            btnFirstName.isHighlighted = passenger.firstName.isEmpty || !passenger.firstName.isValidateName
            btnLastName.isHighlighted = passenger.lastName.isEmpty || !passenger.lastName.isValidateName

            btnDob.isHighlighted = passenger.dateOfBirth == nil

            btnNationality.isHighlighted = passenger.nationality.isEmpty
//            btnBirthCountry.highlighted = passenger.travelDocument.birthCountry.isEmpty
            btnIssuingCountry.isHighlighted = passenger.travelDocument.docIssuingCountry.isEmpty

            btnDocTypeCode.isHighlighted = passenger.travelDocument.docTypeCode.isEmpty

            btnIDNumber.isHighlighted = passenger.travelDocument.validateDocumentNumber != .none
            btnExpirationDate.isHighlighted = passenger.travelDocument.expirationDate == nil

//            btnMobileNumber.highlighted = moible.isEmpty
            btnPhoneNumber.isHighlighted = moible.isEmpty
            btnEmail.isHighlighted = userEmail.isEmpty
            btnPass.isHighlighted = password.isEmpty
//            btnCity.highlighted = city.isEmpty
        } else {
            btnTitle.isHighlighted = false
            btnFirstName.isHighlighted = false
            btnLastName.isHighlighted = false

            btnDob.isHighlighted = false

            btnNationality.isHighlighted = false
//            btnBirthCountry.highlighted = false
            btnIssuingCountry.isHighlighted = false
            btnDocTypeCode.isHighlighted = false

            btnExpirationDate.isHighlighted = false
            btnIDNumber.isHighlighted = false
            btnMobileNumber.isHighlighted = false
            btnPhoneNumber.isHighlighted = false
            btnEmail.isHighlighted = false
            btnPass.isHighlighted = false
//            btnCity.highlighted = false
        }
    }


    @IBAction func onBtnTermClicked(_ sender: UIButton) {
        FDUtility.onBtnTermClicked(self)
    }
  
  @IBAction func onBtnReferalIdField(_ sender: UIButton) {
    tfReferalId.becomeFirstResponder()
  }
  
    @IBAction func onMobileCountryCodeClicked(_ sender: UIView) {
        self.view.endEditing(true)

        FDUtility.selectCountryPhoneCode(FDLocalized("Mobile Country Code"), initValue: mobileCountryCode, origin:sender) { (selectedValue) -> Void in
            self.mobileCountryCode = selectedValue
            self.updatePassengersDetail()
        }
    }

    @IBAction func onPhoneCountryCodeClicked(_ sender: UIView) {
        self.view.endEditing(true)

        FDUtility.selectCountryPhoneCode(FDLocalized("Phone Country Code"), initValue: phoneCountryCode, origin:sender) { (selectedValue) -> Void in
            self.phoneCountryCode = selectedValue
            self.updatePassengersDetail()
        }
    }

}

