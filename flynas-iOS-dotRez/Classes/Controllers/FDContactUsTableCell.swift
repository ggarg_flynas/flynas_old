//
//  FDContactUsTableCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import CoreLocation
import AddressBook
import MapKit

typealias FDOfficeDetail = (officeName:String,officeNumber:String,officeFax:String,officeAdd1:String,officeAdd2:String,officeAdd3:String)

class FDContactUsTableCell: UITableViewCell {

    var officeDetail = FDOfficeDetail("","","","","","") {
        didSet {
            labelOfficeName.text = officeDetail.officeName
            labelOfficeName.text = officeDetail.officeNumber
            labelOfficeName.text = officeDetail.officeFax
            labelOfficeName.text = officeDetail.officeAdd1
            labelOfficeName.text = officeDetail.officeAdd2
            labelOfficeName.text = officeDetail.officeAdd3

        }
    }

    @IBOutlet var labelOfficeName: UILabel!
    @IBOutlet var labelOfficeNumber: UILabel!
    @IBOutlet var labelOfficeFax: UILabel!
    @IBOutlet var labelOfficeAdd1: UILabel!
    @IBOutlet var labelOfficeAdd2: UILabel!
    @IBOutlet var labelOfficeAdd3: UILabel!

    @IBOutlet var btnCall: FDButton!
    @IBOutlet var btnMap: FDButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onBtnCallClicked(_ sender: AnyObject) {
        if let phoneNumber = URL(string: "tel://" + officeDetail.officeNumber) {
            UIApplication.shared.openURL(phoneNumber)
        } else {
            UIAlertView.showWithTitle("", message: FDLocalized("Invalid Phone Number"), cancelButtonTitle: FDLocalized("BACK")) { (alertView, buttonIndex) -> Void in
            }
        }
    }

    @IBAction func onBtnMapClicked(_ sender: AnyObject) {

        let geoCoder = CLGeocoder()

        geoCoder.geocodeAddressString("\(officeDetail.officeAdd1) \(officeDetail.officeAdd2) \(officeDetail.officeAdd3)",
            completionHandler:
            {(placemarks: [CLPlacemark]?, error: NSError?) in

                if error != nil {
                    print("Can't find address")
                }

                if placemarks != nil && placemarks!.count > 0 {
                    let placemark = placemarks![0]
                    if let location = placemark.location {
                        let coords = location.coordinate
                        let placemark = MKPlacemark(coordinate: coords, addressDictionary: nil)
                        let mapItem = MKMapItem(placemark: placemark)
                        mapItem.name = self.officeDetail.officeName
                        mapItem.phoneNumber = self.officeDetail.officeNumber
                        mapItem.openInMaps(launchOptions: nil)
                    }
                }
        } as! CLGeocodeCompletionHandler)
    }
}
