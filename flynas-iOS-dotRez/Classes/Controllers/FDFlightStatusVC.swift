//
//  FDFlightStatusVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightStatusVC: BaseVC {

    @IBOutlet var tfFlightNumber: UITextField!

    @IBOutlet var labelFlightInfo1: UILabel!
    @IBOutlet var labelFlightInfo2: UILabel!
    @IBOutlet var labelFlightInfo3: UILabel!
    @IBOutlet var labelFlightInfo4: UILabel!
    @IBOutlet var labelFlightInfo5: UILabel!

    @IBOutlet var labelFlightDate: UILabel!
    @IBOutlet var labelUpdatedAsOfUtc: UILabel!

    @IBOutlet var labelDepartureActualTime: UILabel!
    @IBOutlet var labelDepartureScheduledTime: UILabel!
    @IBOutlet var labelDepartureStatus: UILabel!
    @IBOutlet var labelArrivalActualTime: UILabel!
    @IBOutlet var labelArrivalScheduledTime: UILabel!
    @IBOutlet var labelArrivalStatus: UILabel!
    @IBOutlet var btnView: UIButton!



    @IBOutlet var heightFlightStatus: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("flight_status_page")
        heightFlightStatus.priority = 900
        tfFlightNumber.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onBtnViewStatusClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        let flightNumber = tfFlightNumber.text

        if flightNumber != nil && !flightNumber!.isEmpty {
            SVProgressHUD.show()
            FDBookingManager.sharedInstance.getFlightStatus(flightNumber!, success: { (flightStatus) -> Void in
                SVProgressHUD.dismiss()
                self.updateFlightStatus(flightStatus)
                }) { (reason) -> Void in
                    SVProgressHUD.dismiss()
                    self.errorHandle(reason)
            }

        } else {
            FDErrorHandle.apiCallErrorWithAler(FDLocalized("Please enter flight number."))
        }
    }

    func updateFlightStatus(_ f:FDFlightStatus) {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.heightFlightStatus.priority = 100
            self.view.layoutIfNeeded()
        }) 

        var flightInfo1 = "XY---"
        let flightInfo2 = "-"
        var flightInfo3 = "---"
        let flightInfo4 = FDLocalized("TO")
        var flightInfo5 = "---"
        var flightDate = "-- -- ----"
        var updateAs = "-------"

        var departureScheduledTime = "--:--"
        var departureActualTime = "--:--"
        var departureStatus = "------"
        var arrivalScheduledTime = "--:--"
        var arrivalActualTime = "--:--"
        var arrivalStatus = "------"


        if !f.flightStatus.origin.isEmpty {
            if let originCity = FDResourceManager.sharedInstance.getStation(f.flightStatus.origin)?.name.uppercased() {
                if let destinationCity = FDResourceManager.sharedInstance.getStation(f.flightStatus.destination)?.name.uppercased() {


                    flightInfo1 = "XY\(f.flightStatus.flightDesignator)"

                    flightInfo3 = "\(originCity)(\(f.flightStatus.origin.uppercased()))"

                    flightInfo5 = "\(destinationCity)(\(f.flightStatus.destination.uppercased()))"

//                        flightCity = "XY\(f.flightStatus.flightDesignator) - \(originCity)(\(f.flightStatus.origin.uppercaseString)) \(FDLocalized("TO")) \(destinationCity)(\(f.flightStatus.destination.uppercaseString))"
                }
            }


            if let t = f.flightStatus.flightDate {
                flightDate = t.toStringUTC(format: .custom("dd MMM yyyy"))
            }

            departureStatus = f.flightStatus.departureStatus

            if let t = f.flightStatus.updatedAsOfUtc {
                let updateAsDate = t.toString(format: .custom("dd MMM yyyy - HH:mm"))
                updateAs = "\(FDLocalized("Status update as of")) \(updateAsDate)"
            }

            if let t = f.flightStatus.departureScheduledTime {
                departureScheduledTime = t.toStringUTC(format: .custom("HH:mm"))
            }

            if let t = f.flightStatus.departureActualTime {
                departureActualTime = t.toStringUTC(format: .custom("HH:mm"))
            }

            if let t = f.flightStatus.arrivalScheduledTime {
                arrivalScheduledTime = t.toStringUTC(format: .custom("HH:mm"))
            }
            if let t = f.flightStatus.arrivalActualTime {
                arrivalActualTime = t.toStringUTC(format: .custom("HH:mm"))
            }
            arrivalStatus = f.flightStatus.arrivalStatus

        }


        FDCubeTransition(label: labelFlightInfo1, text: flightInfo1, direction: .negative, delay: 0)
        FDCubeTransition(label: labelFlightInfo2, text: flightInfo2, direction: .negative, delay: 0)
        FDCubeTransition(label: labelFlightInfo3, text: flightInfo3, direction: .negative, delay: 0)
        FDCubeTransition(label: labelFlightInfo4, text: flightInfo4, direction: .negative, delay: 0)
        FDCubeTransition(label: labelFlightInfo5, text: flightInfo5, direction: .negative, delay: 0)
        FDCubeTransition(label: labelFlightDate, text: flightDate, direction: .negative, delay: 0)
        FDCubeTransition(label: labelUpdatedAsOfUtc, text: updateAs, direction: .negative, delay: 0)

        FDCubeTransition(label: labelDepartureScheduledTime, text: departureScheduledTime, direction: .negative, delay: 0)
        FDCubeTransition(label: labelDepartureActualTime, text: departureActualTime, direction: .negative, delay: 0)
        FDCubeTransition(label: labelDepartureStatus, text: departureStatus, direction: .negative, delay: 0)

        FDCubeTransition(label: labelArrivalScheduledTime, text: arrivalScheduledTime, direction: .negative, delay: 0)
        FDCubeTransition(label: labelArrivalActualTime, text: arrivalActualTime, direction: .negative, delay: 0)
        FDCubeTransition(label: labelArrivalStatus, text: arrivalStatus, direction: .negative, delay: 0)
        
    }
}
