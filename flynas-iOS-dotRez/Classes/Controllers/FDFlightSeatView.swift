//
//  FDFlightSeatView.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 13/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightSeatView: UIView {
  
  @IBOutlet var view: UIView!
  @IBOutlet var imgSeat: UIImageView!
  @IBOutlet var btnSeat: UIButton!
  
  typealias UserSelectHandle = ()->Void
  var seatPriceType  = SeatPriceType.Standard
  var userSelectHandle : UserSelectHandle?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initView()
  }
  
  func initView() {
    Bundle.main.loadNibNamed("FDFlightSeatView", owner: self, options: nil)
    view.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(view)
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0))
  }
  
  func setSeatNumber(_ seatNumber:String) {
    btnSeat.setTitle(seatNumber, for: UIControlState())
    btnSeat.setTitleColor(UIColor.white, for: UIControlState())
    btnSeat.titleLabel?.font = fontRegular(15.0)
  }
  
  func setSeatNumber(_ rowIndex:Int, seatIndex:Int) {
    let seatNumber = "\(rowIndex)\(Character(UnicodeScalar(65 + seatIndex)!))"
    btnSeat.setTitle(seatNumber, for: UIControlState())
    btnSeat.setTitleColor(UIColor.white, for: UIControlState())
    btnSeat.titleLabel?.font = fontRegular(15.0)
  }
  
  func setSeatPriceType(_ seatPriceType: SeatPriceType) {
    self.seatPriceType = seatPriceType
    switch seatPriceType {
    case .Business:
      imgSeat.image = UIImage(named: "icn-seat-business")
      break
    case .UpFront:
      imgSeat.image = UIImage(named: "icn-seat-practical")
      break
    case .ExtraLeg:
      imgSeat.image = UIImage(named: "icn-seat-spacious")
      break
    case .Premium:
      imgSeat.image = UIImage(named: "icn-seat-superpractical")
      break
    case .Simple:
      imgSeat.image = UIImage(named: "icn-seat-simple")
      break
    case .Standard:
      imgSeat.image = UIImage(named: "icn-seat-simple")
      break
    case .FemaleOnly:
      imgSeat.image = UIImage(named: "icn-seat-lady")
      break
    }
    btnSeat.isHidden = false
    btnSeat.isEnabled = true
  }
  
  func setNotAvailToInfant() {
    setSeatNotAvailable()
  }
  
  func setNotAvailToChild() {
    setSeatNotAvailable()
  }
  
  func setSeatNotAvailable() {
    imgSeat.image = UIImage(named: "icn-seat-unavailable")
    btnSeat.isHidden = true
    btnSeat.isEnabled = false
  }
  
  func setUserInitial(_ userInitial: String?) {
    if userInitial != nil {
      switch seatPriceType {
      case .Business:
        imgSeat.image = UIImage(named: "icn-seat-business-selected")
        break
      case .UpFront:
        imgSeat.image = UIImage(named: "icn-seat-practical-selected")
        break
      case .ExtraLeg:
        imgSeat.image = UIImage(named: "icn-seat-spacious-selected")
        break
      case .Premium:
        imgSeat.image = UIImage(named: "icn-seat-superpractical-selected")
        break
      case .Simple:
        imgSeat.image = UIImage(named: "icn-seat-simple-selected")
        break
      case .Standard:
        imgSeat.image = UIImage(named: "icn-seat-simple-selected")
        break
      case .FemaleOnly:
        imgSeat.image = UIImage(named: "icn-seat-lady-selected")
        break
      }
      //      imgSeat.image = UIImage(named: "icn-seat-selected")
      btnSeat.setTitle(userInitial, for: UIControlState())
      btnSeat.setTitleColor(UIColor.black, for: UIControlState())
      btnSeat.titleLabel?.font = fontBold(15.0)
      btnSeat.isHidden = false
      btnSeat.isEnabled = false
    }
  }
  
  @IBAction func onBtnSeatClicked(_ sender: UIButton) {
    if seatPriceType == .FemaleOnly {
      UIAlertView.showWithTitle("", message: alertText(), cancelButtonTitle: FDLocalized("OK")) { (alert, index) in
        self.userSelectHandle?()
      }
    } else {
      userSelectHandle?()
    }
  }
  
  func alertText() -> String {
    if isEn() {
      return "We would like to inform you that the seats are not completely dedicated for women, but they have the priority upon booking. flynas reserve the right to place males in the females section, and that is according to the flight’s crew evaluation and decision. We promise that we will do our best to make these seats for women only."
    } else {
      return "نود إفادتكم بأن المقاعد المخصصة للسيدات ليست مؤكدة، وإنما للسيدات أولوية في حجزها ولطيران ناس الحق في التغيير وإركاب الرجال في هذه المقاعد حسب الضرورة التي يقررها طاقم الرحلة. نعدكم بأن نبذل جهدنا في أن تكون هذه المقاعد للسيدات فقط."
    }
  }
  
  func setUserSelectAction(_ selSeat:@escaping UserSelectHandle) {
    userSelectHandle = selSeat
  }
}
