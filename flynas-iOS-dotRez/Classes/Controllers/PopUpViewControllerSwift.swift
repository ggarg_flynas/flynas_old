//
//  PopUpViewControllerSwift.swift
//  NMPopUpView
//
//  Created by Nikos Maounis on 13/9/14.
//  Copyright (c) 2014 Nikos Maounis. All rights reserved.
//

import UIKit
import QuartzCore

@objc class PopUpViewControllerSwift : UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet var messageHeight: NSLayoutConstraint!
    @IBOutlet var viewHeader: UIView!

    var headerBgColor : UIColor?
    var defaultHeaderBgColor : UIColor?
    //= UIColor(red: 35/255.0, green: 188/255.0, blue: 185/255.0, alpha: 1.0)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)

        defaultHeaderBgColor = viewHeader.backgroundColor
//        if headerBgColor != nil {
//            viewHeader.backgroundColor = headerBgColor
//        } else {
//            viewHeader.backgroundColor = defaultHeaderColor
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if headerBgColor != nil {
            viewHeader.backgroundColor = headerBgColor
        } else {
            viewHeader.backgroundColor = defaultHeaderBgColor
        }
    }

    func setupUILable(_ label:UILabel, htmlStr: String) -> CGSize {

        let aux = String(format:"<span style=\"color: #4D4D4D; font-size: 14; font-weight:100\">%@</span>", htmlStr)
        let attrStr = try! NSAttributedString(data: aux.data(using: String.Encoding.unicode)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType] , documentAttributes: nil)
        let contentSize = attrStr.boundingRect(with: CGSize(width: label.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil).size

        messageHeight.constant = contentSize.height

        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.attributedText = attrStr

        return contentSize;
    }
    
    func showInView(_ aView: UIView, withTitle title : String, withMessage message: String, animated: Bool)
    {
        aView.addSubview(view)
        view.frame = aView.frame

//        aView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: aView, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
//        aView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: aView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
//        aView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: aView, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
//        aView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: aView, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))

        titleLabel.text = title

        setupUILable(messageLabel,htmlStr: message)
//        messageLabel.text = message

        view.layoutIfNeeded()

        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func closePopup(_ sender: AnyObject) {
        self.removeAnimate()
    }
}
