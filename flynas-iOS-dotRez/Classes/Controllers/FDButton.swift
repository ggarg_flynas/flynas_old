//
//  FDButton.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 27/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDButton: UIButton {


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        flipButton()
    }

    var action: ((_ sender:FDButton) -> ())? {
        didSet {
            addTarget(self, action: #selector(FDButton.callClosure), for: .touchUpInside)
        }
    }

    func callClosure() {
        if let action = action {
            action(self)
        }
    }
}
