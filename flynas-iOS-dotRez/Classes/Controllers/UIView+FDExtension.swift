//
//  UIView+FDExtension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import Foundation



extension UIView {
  
  func roundView() {
    layer.cornerRadius = frame.size.width / 2
    clipsToBounds = true
  }
  
  func rotate(_ angle: CGFloat) {
    UIView.beginAnimations("rotate", context: nil)
    UIView.setAnimationDuration(0.5)
    transform = CGAffineTransform(rotationAngle: ((angle) * CGFloat(M_PI) / 180.0))
    UIView.commitAnimations()
  }
  
}
