//
//  FDFlightDateView.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightDateView: UIView {
  
  enum DataViewStyle {
    case current
    case other
  }
  
  @IBOutlet var view: UIView!
  
  @IBOutlet var lbWeekday: UILabel!
  @IBOutlet var lbDay: UILabel!
  @IBOutlet var lbLowFare: UILabel!
  @IBOutlet weak var lbMonth: UILabel!
  @IBOutlet var loadingIndicator: UIActivityIndicatorView!
  
  @IBOutlet var imgBgFrame: UIImageView!
  var clicked : ((_ sender: UIButton?)->Void)?
  
  var date : Date?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initView()
  }
  
  func initView() {
    Bundle.main.loadNibNamed("FDFlightDateView", owner: self, options: nil)
    
    loadingIndicator.isHidden = true
    loadingIndicator.color = FDColorFlynasGreen
    
    //        view.translatesAutoresizingMaskIntoConstraints = false
    //        view.backgroundColor = FDColorFlynasGreen
    
    let margin:CGFloat = 0
    view.frame = CGRect(x: margin,y: 0, width: self.frame.size.width - 2*margin ,height: self.frame.size.height)
    
    self.addSubview(view)
    self.backgroundColor = UIColor.white
    
    //        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
    //        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
    //        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 20))
    //        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 20))
    
    self.layoutIfNeeded()
    
  }
  
  func setFare(_ fare:Float?) {
    if let fare = fare, fare == FDLowFare.LoadingPrice {
      lbLowFare.text = ""
      loadingIndicator.isHidden = false
      loadingIndicator.startAnimating()
    } else {
      lbLowFare.text = ""//FDUtility.formatCurrency(fare)
      loadingIndicator.isHidden = true
      loadingIndicator.stopAnimating()
    }
  }
  
  func setStyle (_ style:DataViewStyle, forward:Bool = true, duration:Double = 0) {
    let oldImageView = UIImageView(image: FDGetViewSnapshotImage(self))
    oldImageView.contentMode = forward ? UIViewContentMode.topRight : UIViewContentMode.topLeft
    oldImageView.clipsToBounds = true
    self.addSubview(oldImageView)
    if style == .current {
      imgBgFrame.isHidden = true
      lbWeekday.textColor = UIColor.white
      lbDay.textColor = UIColor.white
      lbMonth.textColor = UIColor.white
      lbLowFare.textColor = UIColor.white
      view.backgroundColor = FDColorFlynasGreen
    } else {
      imgBgFrame.isHidden = false
      lbWeekday.textColor = FDColorFlynasBlack
      lbDay.textColor = FDColorFlynasBlack
      lbMonth.textColor = FDColorFlynasBlack
      lbLowFare.textColor = FDColorFlynasBlack
      view.backgroundColor = UIColor.white
    }
    UIView.animate(withDuration: duration, animations: {
      oldImageView.frame.origin.x = forward ? self.frame.size.width : 0
      oldImageView.frame.size.width = 0
    }, completion: { (_) in
      oldImageView.removeFromSuperview()
    }) 
  }
  
  @IBAction func onBtnClicked(_ sender: UIButton?) {
    clicked? (sender)
  }
  
  func setDateWithAnimation(_ newDate: Date?, animationDirection: AnimationDirection) {
    var weekdayText = ""
    var dayText = ""
    var monthText = ""
    date = newDate
    if let newDate = newDate {
      weekdayText = newDate.toStringUTC(format: .custom("EEEE"))
      dayText = newDate.toStringUTC(format: .custom("dd"))
      monthText = newDate.toStringUTC(format: .custom("MMM"))
    }
    
    //        if animationDirection != .None {
    //            FDCubeTransition(label: lbWeekday, text: weekDayText, direction: animationDirection)
    //            FDCubeTransition(label: lbDayMonth, text: dayMonthText, direction: animationDirection)
    //        } else {
    lbWeekday.text = weekdayText
    lbDay.text = dayText
    lbMonth.text = monthText
    //        }
  }
  
}
