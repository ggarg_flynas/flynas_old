//
//  FDFlightScheduleVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 10/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightScheduleVC: BaseVC, UITableViewDelegate, UITableViewDataSource,FDSelectCityVCDelegate {
  
  @IBOutlet var labelOriginCity: UILabel!
  @IBOutlet var labelDestinationCity: UILabel!
  @IBOutlet var labelTo: UILabel!
  @IBOutlet var viewDetails: UIView!
  @IBOutlet var tableView: UITableView!
  @IBOutlet var btnView: UIButton!
  @IBOutlet var btnOriginCity: UIButton!
  @IBOutlet var btnSelDestCity: UIButton!
  @IBOutlet var viewNoFlight: UIView!
  
  var stationDepart : FDResourceStation?
  var stationArrive : FDResourceStation?
  var currentBtn : UIButton?
  var flightSchedule: FDFlightSchedule?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("flight_schedule_page")
    tableView.dataSource = self
    tableView.delegate = self
    viewDetails.alpha = 0
    
    updateCity()
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func onBtnViewClicked(_ sender: UIButton) {
    
    SVProgressHUD.show()
    FDBookingManager.sharedInstance.getFlightSchedule(stationDepart!.code, arrivalStation: stationArrive?.code, success: { (flightSchedule) -> Void in
      SVProgressHUD.dismiss()
      self.flightSchedule = flightSchedule
      self.updateDetails()
    }) { (reason) -> Void in
      SVProgressHUD.dismiss()
      self.errorHandle(reason)
    }
  }
  
  @IBAction func onBtnSelCityClicked(_ sender: UIButton) {
    
    self.currentBtn = sender
    pushViewController("Main", storyboardID: "FDSelectCityVC") { (controller) -> Void in
      let selCityVC = controller as! FDSelectCityVC
      selCityVC.delegate = self
      selCityVC.selectCityVCOption = .departureAndArrive
      
      switch sender {
      case self.btnOriginCity:
        selCityVC.destinationOption =  DestinationOption.depart
        selCityVC.stationDepart = self.stationDepart
        selCityVC.stationArrive = self.stationArrive
        break
      case self.btnSelDestCity:
        selCityVC.destinationOption =  DestinationOption.arrive
        selCityVC.stationDepart = self.stationDepart
        selCityVC.stationArrive = self.stationArrive
        break
      default:break
      }
    }
  }
  // MARK: - FDSelectCityVCDelegate
  func didSelect(_ stationDepart: FDResourceStation, stationArrive: FDResourceStation?) {
    self.stationArrive = stationArrive
    self.stationDepart = stationDepart
    updateCity()
  }
  
  func updateCity() {
    if let std = stationDepart {
      btnOriginCity.setTitle("\(std.name)(\(std.code))", for: UIControlState())
    } else {
      btnOriginCity.setTitle("", for: UIControlState())
    }
    
    if let sta = stationArrive {
      btnSelDestCity.setTitle("\(sta.name)(\(sta.code))", for: UIControlState())
    } else {
      btnSelDestCity.setTitle("", for: UIControlState())
    }
    
    btnView.isEnabled = stationDepart != nil
  }
  
  // MARK: - UITableViewDelegate/DataSource
  func updateDetails() {
    tableView.reloadData()
    
    if let f = self.flightSchedule {
      if let sta = FDResourceManager.sharedInstance.getStation(f.departureStation) {
        labelOriginCity.text = "\(sta.name)(\(sta.code))"
      }
      
      if let sta = FDResourceManager.sharedInstance.getStation(f.arrivalStation) {
        labelDestinationCity.text = "\(sta.name)(\(sta.code))"
        
        labelDestinationCity.isHidden = false
        labelTo.isHidden = false
      } else {
        labelDestinationCity.isHidden = true
        labelTo.isHidden = true
      }
      
      UIView.animate(withDuration: 0.5, animations: { () -> Void in
        self.viewDetails.alpha = 1.0
      }) 
      
      if f.flightSchedules.count > 0 {
        viewNoFlight.isHidden = true
        tableView.isHidden = false
      } else {
        viewNoFlight.isHidden = false
        tableView.isHidden = true
      }
      
    } else {
      UIView.animate(withDuration: 0.5, animations: { () -> Void in
        self.viewDetails.alpha = 0.0
      }) 
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return flightSchedule == nil ? 0 : flightSchedule!.flightSchedules.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell:FDFlightScheduleTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "FDFlightScheduleTableViewCell") as! FDFlightScheduleTableViewCell
    
    cell.setFlightScheduleDetail(flightSchedule!.flightSchedules[indexPath.row])
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
  }
  
}
