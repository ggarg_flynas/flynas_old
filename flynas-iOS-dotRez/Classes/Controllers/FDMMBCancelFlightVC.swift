//
//  FDMMBCancelFlightVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 12/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMMBCancelFlightVC: FDBookingCommonVC,UITableViewDelegate, UITableViewDataSource {

    var cancelFlightIndexes = [Int]()

    @IBOutlet var tableView: UITableView!
    @IBOutlet var labelMMBPrice: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("flight_cancellation_page")
        tableView.delegate = self
        tableView.dataSource = self
        self.viewTripSummary?.isHidden = true
    }

    // MARK: - UITableViewDelegate/DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return cancelFlightIndexes.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let (flight,_) = userBooking.getFlightInfo( cancelFlightIndexes[section])
        if let flight = flight {
            return flight.legs.count
        }

        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "FDMMBFlightInfoCell") as! FDMMBFlightInfoCell

        let (flight,_) = userBooking.getFlightInfo(cancelFlightIndexes[indexPath.section])
        if let flight = flight {
            cell.leg = flight.legs[indexPath.row]
        }

        return cell
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.actionDelegate?.onBtnBackClicked()
    }

    @IBAction func onBtnNextClicked(_ sender: UIButton) {

        // submit flight to cancel
        FDBookingManager.sharedInstance.cancelFlights(cancelFlightIndexes, success: {() -> Void in

            self.navigationController?.popViewController(animated: true)
            self.actionDelegate?.updatePage()
            self.actionDelegate?.onBtnContinueClicked()

            },fail: { (reason) -> Void in
        })
    }
}
