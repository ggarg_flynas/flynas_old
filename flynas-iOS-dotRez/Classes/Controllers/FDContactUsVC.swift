//
//  FDContactUsVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 1/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import MapKit


class FDContactUsVC: BaseVC
{

    let TableCellHeight = 117
    @IBOutlet var tableView: UITableView!
    @IBOutlet var heightTable: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var labelOfficeName: UILabel!
    @IBOutlet var labelOfficePhoneNumber: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
      log("contact_us_page")
    }

    @IBAction func onBtnHomeClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onBtnBackClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
      dismiss(animated: true, completion: nil)
    }

    @IBAction func onBtnCallClicked(_ sender: AnyObject) {
        if let phoneNumber = URL(string: "tel://+966920001234") {
            UIApplication.shared.openURL(phoneNumber)
        }
    }

    @IBAction func onBtnCall1Clicked(_ sender: UIButton) {
        let phoneNumber = sender.title(for: UIControlState())
        if let phoneNumber = URL(string: "tel://\(phoneNumber)") {
            UIApplication.shared.openURL(phoneNumber)
        }
    }

    @IBAction func onBtnCall2Clicked(_ sender: UIButton) {
        let phoneNumber = sender.title(for: UIControlState())
        if let phoneNumber = URL(string: "tel://\(phoneNumber)") {
            UIApplication.shared.openURL(phoneNumber)
        }
    }

    @IBAction func onBtnCall3Clicked(_ sender: UIButton) {
        let phoneNumber = sender.title(for: UIControlState())
        if let phoneNumber = URL(string: "tel://\(phoneNumber)") {
            UIApplication.shared.openURL(phoneNumber)
        }
    }

    @IBAction func onBtnMapClicked(_ sender: AnyObject) {
        //24.804559, 46.672341
        let coords = CLLocationCoordinate2D(latitude: 24.804559, longitude: 46.672341)
        let placemark = MKPlacemark(coordinate: coords, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = labelOfficeName.text
        mapItem.phoneNumber = labelOfficePhoneNumber.text
        mapItem.openInMaps(launchOptions: nil)
    }
}
