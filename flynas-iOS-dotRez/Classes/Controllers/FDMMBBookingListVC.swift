//
//  FDMMBBookingListVC.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 4/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDMMBBookingListVC: BaseVC {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var viewNoItem: UIView!
  
  var memberProfile : FDMemberProfile?
  var userEmail = ""
  var showPastBooking = true
  
  override func viewDidLoad() {
    super.viewDidLoad()
    log("booking_list_page")
    getMemberProfile()
    SharedModel.shared.isMMB = true
    SharedModel.shared.isWCI = false
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableView.reloadWithAnimation()
  }
  
  func getMemberProfile () {
    SVProgressHUD.show()
    FDLoginManager.sharedInstance.forceLoginAgain({ (memberLogin) -> Void in
      FDLoginManager.sharedInstance.retrieveMemberProfile(nil, success: { (memberProfile) -> Void in
        SVProgressHUD.dismiss()
        if let e = FDLoginManager.sharedInstance.userEmail {
          self.userEmail = e
        }
        self.memberProfile = memberProfile
        self.tableView.reloadData()
      }) { (r) -> Void in
        SVProgressHUD.dismiss()
//        self.errorHandle(r)
      }
    }) { (r) -> Void in
      SVProgressHUD.dismiss()
//      self.errorHandle(r)
    }
  }
  
  @IBAction func onBtnAddFlight(_ sender: UIButton) {
    performSegue(withIdentifier: "FDMMBBookingListVC_FDMMBSearchVC", sender: nil)
  }
  
  @IBAction func onBtnHomeClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
  @IBAction func onBtnBackClicked(_ sender: UIButton) {
    postNotification(ContainerVC.kBtnHome, userInfo: [:])
  }
  
}

// MARK: - UITableViewDelegate / DataSource
extension FDMMBBookingListVC: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if FDResourceManager.sharedInstance.isSessionLogined {
      return showPastBooking ? 2 : 1
    }
    return SearchItem.getPastFlights().isEmpty ? 1 : 2
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "FDMyBookingTableViewHeader") {
      if let seatPassengerName = cell.viewWithTag(30) as? UILabel {
        seatPassengerName.text = section == 0 ? FDLocalized("Upcoming flights") : FDLocalized("Past flights")
      }
      return cell.contentView
    }
    return UIView()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      if let m = memberProfile {
        tableView.tableFooterView?.isHidden = true
        return m.customerBookings.currentBookings.count
      }
      let searchItems = SearchItem.getUpcomingFlights()
      tableView.tableFooterView?.isHidden = false
      return searchItems.count
    case 1:
      if let m = memberProfile {
        return m.customerBookings.pastBookings.count
      }
      let searchItems = SearchItem.getPastFlights()
      tableView.tableFooterView?.isHidden = false
      return searchItems.count
    default:
      break
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "FDMyBookingTableViewCell") as! FDMyBookingTableViewCell
    cell.delegate = self
    if FDResourceManager.sharedInstance.isSessionLogined {
      let booking = indexPath.section == 0 ? memberProfile?.customerBookings.currentBookings[indexPath.row] : memberProfile?.customerBookings.pastBookings[indexPath.row]
      cell.labelPNR.text = booking?.recordLocator
      let originStation = FDResourceManager.sharedInstance.getStation(booking?.origin)
      let destinationStation = FDResourceManager.sharedInstance.getStation(booking?.destination)
      cell.labelOrigin.text = originStation?.name
      cell.labelDestination.text = destinationStation?.name
      cell.labelFlightTime.text = booking?.std?.toStringUTC(format:.custom("HH:mm EEE, dd MMM yyyy"))
      cell.showManageBtn(indexPath.section == 0)
      cell.btnCheckin.isHidden = true
    } else {
      let searchItems = indexPath.section == 0 ? SearchItem.getUpcomingFlights() : SearchItem.getPastFlights()
      let item = searchItems[indexPath.row]
      cell.labelPNR.text = item.pnr
      cell.labelOrigin.text = item.origin
      cell.labelDestination.text = item.destination
      cell.labelFlightTime.text = item.departDate.toStringUTC(format:.custom("HH:mm EEE, dd MMM"))
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return !FDResourceManager.sharedInstance.isSessionLogined
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    var searchItems = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
    searchItems.remove(at: indexPath.row)
    ArchiveManager.saveArray(searchItems, key: SearchItem.kMMBSearchDefaults)
    tableView.reloadData()
  }
  
}

// MARK: - FDMyBookingTableViewCellDelegate
extension FDMMBBookingListVC: FDMyBookingTableViewCellDelegate {
  
  func btnViewClicked(_ cell: FDMyBookingTableViewCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      if indexPath.section == 0 {
        if FDResourceManager.sharedInstance.isSessionLogined {
          if let booking = indexPath.section == 0 ? memberProfile?.customerBookings.currentBookings[indexPath.row] : memberProfile?.customerBookings.pastBookings[indexPath.row] {
            let pnr = booking.recordLocator
            SVProgressHUD.show()
            FDBookingManager.sharedInstance.getBooking(pnr, departureStation: nil, email: userEmail, lastName: nil, success: { () -> Void in
              SVProgressHUD.dismiss()
              self.pushViewController("BookingFlow2", storyboardID: "FDConfirmationVC") { (vc) -> Void in
                let c = vc as! FDConfirmationVC
                c.showBackBtn = true
                c.userBooking.pnr = pnr
                c.userBooking.userEmail = self.userEmail
                if indexPath.section == 0 {
                  c.manageBooking = true
                } else {
                  c.manageBooking = false
                }
              }
              }, fail:{ (reason) -> Void in
                SVProgressHUD.dismiss()
                if reason == FDErrorHandle.API_ERROR_5XX {
                  self.errorHandle(FDLocalized("Unable to find the booking"))
                } else {
                  self.errorHandle(reason)
                }
            })
          }
        } else {
          let searchItems = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
          let item = searchItems[indexPath.row]
          SVProgressHUD.show()
          FDBookingManager.sharedInstance.getBooking(item.pnr, departureStation: nil, email: nil, lastName: item.lastname, success: { () -> Void in
            SVProgressHUD.dismiss()
            self.pushViewController("BookingFlow2", storyboardID: "FDConfirmationVC") { (vc) -> Void in
              let c = vc as! FDConfirmationVC
              c.showBackBtn = true
              c.userBooking.pnr = item.pnr
              //              c.userBooking.userEmail = self.userEmail
              if indexPath.section == 0 {
                c.manageBooking = true
              } else {
                c.manageBooking = false
              }
            }
            }, fail:{ (reason) -> Void in
              SVProgressHUD.dismiss()
              if reason == FDErrorHandle.API_ERROR_5XX {
                self.errorHandle(FDLocalized("Unable to find the booking"))
              } else {
                self.errorHandle(reason)
              }
          })
        }
      }
    }
  }
  
  func btnCheckinClicked(_ cell: FDMyBookingTableViewCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      if indexPath.section == 0 {
        if let booking = indexPath.section == 0 ? memberProfile?.customerBookings.currentBookings[indexPath.row] : memberProfile?.customerBookings.pastBookings[indexPath.row] {
          let pnr = booking.recordLocator
          SVProgressHUD.show()
          FDWebCheckinManager.sharedInstance.getBooking(pnr, departureStation: nil, email: userEmail, lastName: nil, success: { (wciBooking) -> Void in
            SVProgressHUD.dismiss()
            if wciBooking.journeys.count > 0 {
              self.pushViewController("WebCheckin", storyboardID: "FDWCiFlightListVC") { (vc) -> Void in
                //vc
                let c = vc as! FDWCiFlightListVC
                c.wciUserCheckin.wciBooking = wciBooking
                c.pnr = pnr
              }
            } else {
              self.errorHandle(FDLocalized("You can check-in online 48 hours before your departure. Online check-in closes 4 hours before departure."))
            }
            }, fail:{ (reason) -> Void in
              SVProgressHUD.dismiss()
              if reason == FDErrorHandle.API_ERROR_5XX {
                self.errorHandle(FDLocalized("You can check-in online 48 hours before your departure. Online check-in closes 4 hours before departure."))
              } else {
                self.errorHandle(reason)
              }
          })
        }
      }
    }
  }
  
}
