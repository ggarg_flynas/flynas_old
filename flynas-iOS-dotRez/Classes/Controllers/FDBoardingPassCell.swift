//
//  FDBoardingPassCell.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

public protocol FDBoardingPassCellDelegate : class {
    func addPassToPassbook(_ passBookParameters:[AnyHashable: Any])
}

class FDBoardingPassCell: UICollectionViewCell {

    @IBOutlet var labelPassengerFullName: UILabel!
//    @IBOutlet var labelBaggage: UILabel!
//    @IBOutlet var labelMeal: UILabel!
    @IBOutlet var labelSeat: UILabel!

    @IBOutlet var imgQRCode: UIImageView!

    @IBOutlet var labelDepartDate: UILabel!

    @IBOutlet var labelDepartTime: UILabel!
    @IBOutlet var labelArriveTime: UILabel!
    @IBOutlet var labelOriginCityName: UILabel!
    @IBOutlet var labelDestinationCityName: UILabel!
    @IBOutlet var labelOriginAirportName: UILabel!
    @IBOutlet var labelDestinationAirprotName: UILabel!

    @IBOutlet var labelFlightCode: UILabel!

    @IBOutlet var labelSeqNumber: UILabel!

    weak var delegate: FDBoardingPassCellDelegate?

    @IBOutlet var labelDuration: UILabel!
    @IBOutlet var labelStops: UILabel!
    @IBOutlet var viewQrFrame: UIView!

    @IBOutlet var heightQrImage: NSLayoutConstraint!
    @IBOutlet var btnAddToWallet: UIButton!

    var boardingPass = FDBoardingPass() {
        didSet {
            updateUI()
        }
    }
    var serialNumber = ""

    func displayQRCodeImage(_ qrcodeImage:CIImage) {

        self.layoutIfNeeded()
        let w = min (viewQrFrame.frame.size.height, viewQrFrame.frame.size.width) - 12
        heightQrImage.constant = w

        let scale = w / qrcodeImage.extent.size.width
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scale, y: scale))

        imgQRCode.image = UIImage(ciImage: transformedImage)
        self.layoutIfNeeded()
    }

    func updateUI() {

        labelPassengerFullName.text = boardingPass.name?.fullName

        if boardingPass.segments.count > 0 {
        let flight = boardingPass.segments[0]

            // boarding pass
            if flight.barcodes.count > 0 {
                let barcode = flight.barcodes[0]
                let code = barcode.barcodeData
                let data = code.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
//                let filter = CIFilter(name: "CIQRCodeGenerator")
//                filter!.setValue(data, forKey: "inputMessage")
//                filter!.setValue("H", forKey: "inputCorrectionLevel")

                let filter = CIFilter(name: "CIAztecCodeGenerator")
                filter!.setValue(data, forKey: "inputMessage")
//                filter!.setValue(NSNumber(float: 10.0), forKey: "inputCorrectionLevel")
//                filter!.setValue(NSNumber(float: 10.0), forKey: "inputCompactStyle")

                if let qrcodeImage = filter!.outputImage {
                    displayQRCodeImage(qrcodeImage)
                }
            }

            if flight.legs.count > 0 {
                let l = flight.legs[0]

                if !l.seatColumn.isEmpty && l.seatRow > 0 {
                    labelSeat.text = "\(l.seatRow)\(l.seatColumn)"
                } else {
                    labelSeat.text = "--"
                }

                labelDepartDate.text = flight.departureDate?.toStringUTC(format:.custom("EEEE, dd MMM yyyy"))

                labelDepartTime.text = flight.departureDate?.toStringUTC(format:.custom("HH:mm"))
                labelArriveTime.text = FDUtility.arrivalDateHHMMString(flight.departureDate!, arrivalDate: flight.arrivalDate!)

                let originStation = FDResourceManager.sharedInstance.getStation(l.inventoryLegKey?.departureStation)
                let destinationStation = FDResourceManager.sharedInstance.getStation(l.inventoryLegKey?.arrivalStation)

                labelOriginCityName.text = originStation?.name
                labelDestinationCityName.text = destinationStation?.name
                labelOriginAirportName.text = FDRM.sharedInstance.countryNameByCode(originStation?.country)
                labelDestinationAirprotName.text = FDRM.sharedInstance.countryNameByCode(destinationStation?.country)

                if let inventoryLegKey =  l.inventoryLegKey {
                    labelFlightCode.text = "\(FDLocalized("Flight")) \(inventoryLegKey.carrierCode)\(inventoryLegKey.flightNumber)"
                } else {
                    flight.flightNumber = "-"
                }
    //            labelStops.text = FDUtility.formatStops(flight.numberOfStops)
                labelStops.text = ""

                if flight.utcDepartureDate != nil && flight.utcArrivalDate != nil {
                    labelDuration.text = FDUtility.formatDurationShort(flight.utcDepartureDate!, end: flight.utcArrivalDate!)
                } else {
                    labelDuration.text = ""
                }

                labelSeqNumber.text = FDLocalizeInt(l.boardingSequence)

            }

        }

        btnAddToWallet.setBackgroundImage(UIImage(named: isAr() ? "Add_to_Apple_Wallet_AR" :"Add_to_Apple_Wallet"), for: UIControlState())

    }

    @IBAction func onBtnAddToPassbookClicked(_ sender: UIButton) {

        var departLabel = "--"
        var departValue = "-"

        var arriveLabel = "--"
        var arriveValue = "-"

        let passengerLabel = "PASSENGER"
        let passengerValue = boardingPass.name != nil ? boardingPass.name!.fullName : ""

        let seatLabel = "SEAT"
        var seatValue = "--"

        let departTimeLabel = "DEPARTS"
        var departTimeValue = "--:--"

        let flghtLabel = "FLIGHT"
        var flghtValue = "XY 842"

        let seqLabel = "SEQ"
        var seqValue = "1"

        var barcodeMessage = ""

        // "MBU7DU5ZXY40801"
        var sn = "MB\(boardingPass.recordLocator)"

        if boardingPass.segments.count > 0 {
            let flight = boardingPass.segments[0]

            // boarding pass
            if flight.barcodes.count > 0 {
                let barcode = flight.barcodes[0]
                barcodeMessage = barcode.barcodeData
            }

            if flight.legs.count > 0 {
                let l = flight.legs[0]

                if !l.seatColumn.isEmpty && l.seatRow > 0 {
                    seatValue = "\(l.seatRow)\(l.seatColumn)"
                } else {
                    seatValue = "--"
                }

                if let d = flight.departureDate {
                    departTimeValue = d.toStringUTC(format:.custom("dd MMM, HH:mm"))
                } else {
                    departTimeValue = "--:--"
                }


                if let originStation = FDResourceManager.sharedInstance.getStation(l.inventoryLegKey?.departureStation) {
                    departLabel = originStation.name
                    departValue = originStation.code.uppercased()
                }

                if let destinationStation = FDResourceManager.sharedInstance.getStation(l.inventoryLegKey?.arrivalStation) {
                    arriveLabel = destinationStation.name
                    arriveValue = destinationStation.code.uppercased()
                }

                if let inventoryLegKey = l.inventoryLegKey {
                    flghtValue = "\(inventoryLegKey.carrierCode)\(inventoryLegKey.flightNumber)"
                } else {
                    flghtValue = ""
                }
                seqValue = FDLocalizeInt(l.boardingSequence)

                let trimmedString = flghtValue.replacingOccurrences(of: " ", with: "")
                sn = "\(sn)\(trimmedString)"
                sn = String(format: "\(sn)%02d", l.boardingSequence)
            }
        }

        if serialNumber.isEmpty {
            // "MBU7DU5ZXY40801"
            serialNumber = sn
        }

        let passBookParameters:[AnyHashable: Any] = [
            "BarcodeMessage":barcodeMessage,
            "SerialNumber":serialNumber,
            "Fields":[
                [
                    "Key":"depart",
                    "Label":departLabel,
                    "Value":departValue,
                    "Section":"primary"
                ],
                [
                    "Key":"arrive",
                    "Label":arriveLabel,
                    "Value":arriveValue,
                    "Section":"primary"
                ],

                [
                    "Key":"passenger",
                    "Label":passengerLabel,
                    "Value":passengerValue,
                    "Section":"secondary"
                ],

                [
                    "Key":"seat",
                    "Label":seatLabel,
                    "Value":seatValue,
                    "Section":"secondary"
                ],

                [
                    "Key":"boardingTime",
                    "Label":departTimeLabel,
                    "Value":departTimeValue,
                    "Section":"auxiliary"
                ],
                [
                    "Key":"flightNumber",
                    "Label":flghtLabel,
                    "Value":flghtValue,
                    "Section":"auxiliary"
                ],
                [
                    "Key":"seqNum",
                    "Label":seqLabel,
                    "Value":seqValue,
                    "Section":"auxiliary"
                ]
            ]
        ]

        let passBookRequest = [
        "Passbook":[
            "PassbookRequest": passBookParameters
            ]
        ]

        delegate?.addPassToPassbook(passBookRequest)
    }
}
