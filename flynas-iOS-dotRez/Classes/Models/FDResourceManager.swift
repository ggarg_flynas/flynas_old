//
//  FDResourceManager.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 24/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


let SERVER_TOKEN_HEADER_FIELD_NAME = "X-Session-Token"
let SERVER_VERSION_HEADER_FIELD_NAME = "X-Version"
let SERVER_LANGUAGE_HEADER_FIELD_NAME = "X-Culture"
let SERVER_VERSION_NEWVERSIONAVAILABLE = "SERVER_VERSION_NEWVERSIONAVAILABLE"

let CULTURECODE_EN = "en-US"
let CULTURECODE_AR = "en-GB"
let CULTURECODE_TR = "tr-TR"

let RESOURCE_UPDATE_FAIL = "RESOURCE_UPDATE_FAIL"

let DEFAULT_COUNTY_CODE = "SA"
let DEFAULT_COUNTY_NUMBER = "966"


let HOMEPAGE_MAIN_IMAGE_EN = "http://www.flynas.com/images/app/en/2_375x593_en.jpg"
let HOMEPAGE_AD_IMAGE_EN = "http://www.flynas.com/images/app/en/1_677x147_En.jpg"

let HOMEPAGE_MAIN_IMAGE_AR = "http://www.flynas.com/images/app/ar/2_375x593_ar.jpg"
let HOMEPAGE_AD_IMAGE_AR = "http://www.flynas.com/images/app/ar/1_677x147_Ar.jpg"


typealias FDRM = FDResourceManager
class FDResourceManager: FDCommonApiManager {
//    private static var __once: () = {
//            Static.instance = FDResourceManager()
//        }()
    static let SationResourceReady = "SationResourceReady"


    var severVersionNumber : [Int] = [SERVER_VERSION_0,SERVER_VERSION_1,SERVER_VERSION_2,SERVER_VERSION_3]

//    var fetching = false
    //
    var savedPassenger = [FDPassenger]()
    var savedContactInfo = FDContactItemInfo()

    // cache
    let FirstLaunchDefaultsName = "FirstLaunch001"
//    let LanguageUserDefaultsName = "Language"
    let CurrencyCodeUserDefaultsName = "CurrencyCode"
    let SessionTokenUserDefaultsName = "sessionToken"
    let LastDepartStationCodeDefaultsName = "LastDepartStationCode"
    let LastArriveStationCodeDefaultsName = "LastArriveStationCode"
    let LastDepartStationCode2DefaultsName = "LastDepartStationCode2"
    let LastArriveStationCode2DefaultsName = "LastArriveStationCode2"
    let LastChangeFlightCityDateDefaultsName = "LastChangeFlightCityDateDefaultsName"

    let LastSearchDepartureDateDefaultsName = "LastSearchDepartureDateDefaultsName"
    let LastSearchArriveDateDefaultsName = "LastSearchArriveDateDefaultsName"
    let LastSearchPromoCodeDefaultsName = "LastSearchPromoCodeDefaultsName"
    let LastSearchPassengerNumberDefaultsName = "LastSearchPassengerNumberDefaultsName"

    let LastWciPnrDefaultsName = "LastWciPnrDefaultsName"
    let LastSearchFlisghtOptionDefaultsName = "LastSearchFlisghtOptionDefaultsName"
    let PayWithNaSmilesDefaultsName = "PayWithNaSmilesDefaultsName"
//  static let kShowInSmilePoints = "kShowInSmilePoints"

    let Language_ALL = "All"

    var sessionToken : String?
    var dotRezCookie : String?

    var isSessionLogined = false

    var stations = [FDResourceStation]()
    var isStationReady = false
//    var isLoadingStation = false
    var currencies = [
        FDResourceString(name: "US Dollars", value: "USD"),
        FDResourceString(name: "Euro", value: "EUR"),
        FDResourceString(name: "Saudi Riyal", value: "SAR")
    ]

    var countries : [NSMutableDictionary] = [NSMutableDictionary]() // "name", "code", "phoneCode"
        {
        didSet {
            countriesName = self.countries.map({$0.object(forKey: "name") as! String})
            countriesCode = self.countries.map({$0.object(forKey: "code") as! String})
            countriesPhoneCode = self.countries.map({
                if $0.object(forKey: "phoneCode") != nil {
                    return "\($0.object(forKey: "name") as! String)(+\($0.object(forKey: "phoneCode") as! String))"
                } else {
                    return "\($0.object(forKey: "name") as! String)(+???)"
                }

            })
            countriesPhoneCodeNumber = self.countries.map({
                if $0.object(forKey: "phoneCode") != nil {
                    return "\($0.object(forKey: "phoneCode") as! String)"
                } else {
                    return "???"
                }

            })
        }
    }

    //[DocumentType]()
    var documentTypes = [FDDocumentType]()
    var specialServiceRequests = [FDSpecialServiceRequestItem]() {
        didSet {
            for item in specialServiceRequests {
                if let index = ssrRes.index(where: {$0.itemKey==item.code})
                {
                    ssrRes[index].itemName = item.name
                } else {
                    let exItem = FDExtraItem()
                    exItem.itemKey = item.code
                    exItem.itemName = item.name
                    ssrRes.append(exItem)
                }
            }
        }
    }
    var fees = [FDFeesItem]()
    func getFeeName(_ code:String) -> String {
        if let index = fees.index(where: {$0.code == code}) {
            return fees[index].name
        }

        return FDLocalized("Other Fees")
    }

    // buffer
    var countriesName = [String]()
    var countriesCode = [String]()
    var countriesPhoneCode = [String]()
    var countriesPhoneCodeNumber = [String]()

    var ssrRes = [FDExtraItem]()

    var languages : [FDResourceString] = [
        FDResourceString(name: "English", value: CULTURECODE_EN),
        FDResourceString(name: "العربية", value: CULTURECODE_AR),
        FDResourceString(name: "Türkçe", value: CULTURECODE_TR)]

    var enTitle : String {
        for item in languages {
            if item.value != nil && item.value == CULTURECODE_EN {
                if let n = item.name {
                    return n
                }
            }
        }

        return "English"
    }

    var arTitle : String {
        for item in languages {
            if item.value != nil && item.value == CULTURECODE_AR {
                if let n = item.name {
                    return n
                }
            }
        }

        return "العربية"
    }
    var currentCulture = CULTURECODE_EN
//        {
//        didSet {
//            NSUserDefaults.standardUserDefaults().setValue(currentCulture, forKey: LanguageUserDefaultsName)
//            NSUserDefaults.standardUserDefaults().synchronize()
//
//        }
//    }
    var selectedCurrencyCode = "SAR" {
        didSet {
            selectedCurrencySymbol = selectedCurrencyCode
        }
    }

    var selectedCurrencySymbol = "SAR"

    var titles = [NSMutableDictionary]() // "description", "key"
        {
        didSet {
            let titlesAdult = self.titles.filter({ (item) -> Bool in
                guard let value = item.object(forKey: "isChildOrInfant") as? Bool else {return true}
                return !value
            })

            titlesDes = titlesAdult.map({$0.object(forKey: "description") as! String})
            titlesKey = titlesAdult.map({$0.object(forKey: "key") as! String})

            let titlesChildInfant = self.titles.filter({ (item) -> Bool in
                guard let value = item.object(forKey: "isChildOrInfant") as? Bool else {return false}
                return value
            })

            titlesDesChildInfant = titlesChildInfant.map({$0.object(forKey: "description") as! String})
            titlesKeyChildInfant = titlesChildInfant.map({$0.object(forKey: "key") as! String})
        }
    }
    var titlesDes = [String]()
    var titlesKey = [String]()

    var titlesDesChildInfant = [String]()
    var titlesKeyChildInfant = [String]()

//    var titlesIsChildOrInfant = [Bool]()

    // meals
    var mealsInfo : FDMealsInfo? {
        didSet {
            if mealsInfo != nil {
                for item in mealsInfo!.meals {
//                    if nil == ssrRes.indexOf({$0.itemKey==item.itemKey}) {
//                        ssrRes.append(item)
//                    }

                    if let index = ssrRes.index(where: {$0.itemKey==item.itemKey})
                    {
                        ssrRes[index].itemName = item.itemName
                    } else {
                        ssrRes.append(item)
                    }
                }
            }
        }
    }

    var relationships : FDRelationships?

  static let sharedInstance = FDResourceManager()

    // PNR
    var latestWciPNR = "" {
        didSet {
            UserDefaults.standard.setValue(latestWciPNR, forKey: LastWciPnrDefaultsName)
            UserDefaults.standard.synchronize()
        }
    }

    func firstLaunchClean() {
        UserDefaults.standard.setValue(false, forKey: self.FirstLaunchDefaultsName)

        UserDefaults.standard.removeObject(forKey: UserEmailDefaultsName)
        UserDefaults.standard.removeObject(forKey: UserPassDefaultsName)
        UserDefaults.standard.removeObject(forKey: UserNameDefaultsName)
    }

    override init() {
        super.init()

        // detect current language
        if isEn() {
            self.currentCulture = CULTURECODE_EN
        } else {
            self.currentCulture = CULTURECODE_AR
        }

        RKObjectManager.shared().httpClient.setDefaultHeader(SERVER_LANGUAGE_HEADER_FIELD_NAME, value: self.currentCulture)

        if let firstLaunch = UserDefaults.standard.object(forKey: FirstLaunchDefaultsName) as? Bool {
            if !firstLaunch {
                sessionToken = UserDefaults.standard.object(forKey: SessionTokenUserDefaultsName) as? String
            } else {
                firstLaunchClean()
            }
        } else {
            firstLaunchClean()
        }

        #if DEBUG

            // 1
//        sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogImV0NDRoZ3UyMTVmeXd2NG9nM201a3R5ayIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.gx5N5u_g3d0-Ugt9FbQoo843YlV3fG73i-bIOcD1ee4"

            // 2
//        sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogImRldGR6aWxrZnluam12cXJpZ3R4cGswMiIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.MWe7zTZ8DlNa8K1T0-v9FFAAUL1o0vyloOhKFc-QXbQ"

            // 3
//        sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogIjRlZXhmcXB1eGt4NDBxbm1waXJzb25qeCIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.cScCKs-K7bruyHaB2sqvpSPFhsDZSOeXXj_xQB74bpk"

            // 4
//        sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogIjBpbm1wY2FmaGl6a2xkMmJrYnNzeXV1YyIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.dqeZg_Lirpbo_AgbUrXA7isQFkJPoRD0cOYqBGyDMYE"

            // 5
//            sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogInl0YzAzYWN5enExNWN2YzN2eWVzdGoyeiIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.bNqqxxc1lwjZs2TTlkYZxsSHzmi-tsOIBM8MRUleurs"

            // 6
//            sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogInFxeGdmaDN0dGplajVsa2pwcDFxaGZrdyIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.vWulJtAVFxFZ88JdCjqwFgYyRwllOKdlS5q2_jxUDEg"


            // 7
//            sessionToken = "ew0KICAidHlwIjogIkpXVCIsDQogICJhbGciOiAiSFMyNTYiDQp9.ew0KICAic2Vzc2lvbklkIjogIjEwZHk1bGw1b3BpZWNkNG9hdDFnZjRuayIsDQogICJhZGRpdGlvbmFsRGF0YSI6IHt9DQp9.keDhPH0cMkqTY7aX4M5906V8qKhNhgCtmM_74l5WVOw"

//            sessionToken = nil

            setupDefaultHttpHeader()

        #endif

        if let currencyCode = UserDefaults.standard.object(forKey: CurrencyCodeUserDefaultsName) as? String {
            selectedCurrencyCode = currencyCode
        }

//        if let language = NSUserDefaults.standardUserDefaults().objectForKey(LanguageUserDefaultsName) as? String {
//            currentCulture = language
//        } else {
//            currentCulture = "en-US"
//        }

        if let pnr = UserDefaults.standard.object(forKey: LastWciPnrDefaultsName) as? String {
            latestWciPNR = pnr
        } else {
            latestWciPNR = ""
        }

        self.loadSavedResource(currentCulture)
        loadRecentPassengersContact()

        #if DEBUG
//            latestWciPNR = "IB15QA"
        #endif
    }

    func getServerVersion(_ version:String) {
        // 1.0.0.0
        let versionNumbers = version.characters.split{$0 == "."}.map(String.init)
        for i in 0..<4 {
            if i < versionNumbers.count {
                let n : Int? = Int(versionNumbers[i])
                severVersionNumber[i] = n != nil ? n! : 0
            } else {
                severVersionNumber[i] = 0
            }
        }
    }

    func checkServerVersion() -> Bool {
        #if PRO
        var updateNeeded = false
        if severVersionNumber[0] > SERVER_VERSION_0 {
            updateNeeded = true
        } else if severVersionNumber[1] > SERVER_VERSION_1 {
            updateNeeded = true
        } else  if severVersionNumber[2] > SERVER_VERSION_2 {
            updateNeeded = true
        }
        if updateNeeded {
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: SERVER_VERSION_NEWVERSIONAVAILABLE), object: nil)
        }
        return updateNeeded
        #else
        return false
        #endif
    }

    func setupDefaultHttpHeader() {
        if sessionToken != nil {
            RKObjectManager.shared().httpClient.setDefaultHeader(SERVER_TOKEN_HEADER_FIELD_NAME, value: self.sessionToken)

//            FDPrintCookies()
            if let c = FDGetCookie("dotrez") {
                dotRezCookie = c
            }

            if dotRezCookie != nil {
                RKObjectManager.shared().httpClient.setDefaultHeader("Cookie", value: "dotrez=" + dotRezCookie!)
            }
        }

    }

  func reactiveToken(_ autoLoginin:Bool, success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {
    
    func handleAutoLogin(_ operation:RKObjectRequestOperation, results:RKMappingResult?) {
      let memberLogin:FDMemberLogin? = self.getResultObject(results?.array())
      if let memberLogin = memberLogin {
        
        self.isSessionLogined = !memberLogin.customerId.isEmpty && memberLogin.username == FDLoginManager.sharedInstance.userEmail
        
        //                if autoLoginin && !self.isSessionLogined {
        //                    // login
        //                    FDLoginManager.sharedInstance.autoLoginUser({ (memberLogin) -> Void in
        //                        success(memberLogin:memberLogin)
        //                        }, fail: { (r) -> Void in
        //                            fail (r)
        //                    })
        //                } else {
        //                    success(memberLogin:memberLogin)
        //                }
        
        //                if self.isSessionLogined {
        //                    FDLoginManager.sharedInstance.memberProfile(nil, success: { (memberProfile) in
        //
        //                        }, fail: { (_) in
        //
        //                    })
        //                }
      } else {
        self.isSessionLogined = false
        //                success(memberLogin:FDMemberLogin())
      }
      
      if autoLoginin && !self.isSessionLogined {
        // login
        FDLoginManager.sharedInstance.autoLoginUser({ (memberLogin) -> Void in
          success(memberLogin)
        }, fail: { (r) -> Void in
          success(FDMemberLogin())
        })
      } else {
        success(memberLogin != nil ? memberLogin! : FDMemberLogin())
      }
      
    }

        #if DEMO_DOTREZ
            success()
        #else
            if sessionToken == nil {

                self.isSessionLogined = false

                let operation = FDdotRezAPIManager.operationCreate
                operation.setCompletionBlockWithSuccess({ (operation, results) in
                    let token : String? = operation?.httpRequestOperation.response.allHeaderFields[SERVER_TOKEN_HEADER_FIELD_NAME] as! String?
                    if token != nil {
                        self.sessionToken = token
                        self.setupDefaultHttpHeader()
                        UserDefaults.standard.setValue(token, forKey: self.SessionTokenUserDefaultsName)

                        if let version = operation?.httpRequestOperation.response.allHeaderFields[SERVER_VERSION_HEADER_FIELD_NAME] as! String? {
                            self.getServerVersion(version)
                        }

                        if !self.checkServerVersion () {
                          let time = DispatchTime(uptimeNanoseconds: 0) + Double(Int64(NSEC_PER_SEC) / 5) / Double(NSEC_PER_SEC)
                          DispatchQueue.main.asyncAfter(deadline: time) {
//                            if autoLoginin {
//                                // login again when expire
//                                FDLoginManager.sharedInstance.autoLoginUser({ (memberLogin) -> Void in
//                                    self.isSessionLogined = true
//                                    success(memberLogin:memberLogin)
//                                    }, fail: { (r) -> Void in
//
//                                        fail (r)
//                                })
//                            } else {
//                                success(memberLogin: FDMemberLogin())
//                            }
                            handleAutoLogin (operation!, results: results)
                        }
                        } else {
                            fail(FDErrorHandle.API_SESSION_UPDATE_NEEDED)
                        }
                    }


                    }, failure: { (operation, error) in
//                        self.fetching = false
                        print("Error: " + (error?.localizedDescription)!)
                        fail (error?.localizedDescription)
                })
                operation.start()
            } else {
                self.setupDefaultHttpHeader()
                let operationCheckSession = FDdotRezAPIManager.operationCheckSession
                operationCheckSession.setCompletionBlockWithSuccess({ (operation, results) -> Void in
                  SharedModel.shared.memberlogin = self.getResultObject(results?.array())
//                    if let memberLogin:FDMemberLogin = self.getResultObject(results.array()) {
//
//                        self.isSessionLogined = !memberLogin.customerId.isEmpty
//
//                        if autoLoginin && memberLogin.customerId.isEmpty {
//                            // login
//                            FDLoginManager.sharedInstance.autoLoginUser({ (memberLogin) -> Void in
//                                success(memberLogin:memberLogin)
//                                }, fail: { (r) -> Void in
//                                    fail (r)
//                            })
//                        } else {
//                            success(memberLogin:memberLogin)
//                        }
//                    } else {
//                        self.isSessionLogined = false
//                        success(memberLogin:FDMemberLogin())
//                    }

                    handleAutoLogin (operation!, results: results)
                    }, failure: { (operation, error) -> Void in

                        self.isSessionLogined = false

                        if operation?.httpRequestOperation.response?.statusCode == 403 || operation?.httpRequestOperation.response?.statusCode == 404 || operation?.httpRequestOperation.response?.statusCode == 500 {
                            // expire or missing, need reactive or get version to check if update needed

                            self.activateSession({ () -> Void in
//                                // activate success
//                                if autoLoginin {
//                                    // login again when expire
//                                    FDLoginManager.sharedInstance.autoLoginUser({ (memberLogin) ->
//                                        Void in
//                                        success(memberLogin:memberLogin)
//                                        }, fail: { (r) -> Void in
//                                            fail(r)
//                                    })
//                                } else {
//                                    success(memberLogin:FDMemberLogin())
//                                }

                                handleAutoLogin (operation!, results: nil)

                                }, fail: fail)
                        } else {
                            print("Error: " + (error?.localizedDescription)!)
                            fail (error?.localizedDescription)
                        }
                })
                operationCheckSession.start()
            }
        #endif

    }

  func activateSession (_ success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationActivateSession
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      
      if let version = operation?.httpRequestOperation.response.allHeaderFields[SERVER_VERSION_HEADER_FIELD_NAME] as? String {
        self.getServerVersion(version)
      }
      
      if !self.checkServerVersion () {
        success()
      } else {
        fail(FDErrorHandle.API_SESSION_UPDATE_NEEDED)
      }
      
    }, failure: { (operation : RKObjectRequestOperation!, error: NSError!) in
      print("Error: " + error.localizedDescription)
      //                fail (error.localizedDescription)
      self.handleError(operation!, error: error! as NSError, fail: fail)
      
      } as? (RKObjectRequestOperation?, Error?) -> Void)
    operation.start()
  }

    // reactiveToken and auto login
    func checkSessionToken(_ success:@escaping (_ memberLogin:FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {
        if checkServerVersion() {
            fail(FDErrorHandle.API_SESSION_UPDATE_NEEDED)
        } else {
            reactiveToken(true,success: success, fail: fail)
        }
    }

  func deletSession(_ success:@escaping () -> Void, fail: @escaping (_ operation:RKObjectRequestOperation, _ error:NSError) -> Void) {
    self.isSessionLogined = false
    let operation = FDdotRezAPIManager.operationApiSessionDelete()
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      success()
    }, failure: { (operation, error) in
      fail(operation!, error! as NSError)
    })
    operation.start()
  }

    var lastUpdateTime : Date?
    func updateResourcesWhenExpire() {
        if lastUpdateTime == nil || !lastUpdateTime!.isToday() {
            updateResources({ () -> Void in
                }, fail: { (_) -> Void in

            })
        }
    }

    func resourceUpdateError(_ operation:RKObjectRequestOperation, error:NSError) {
        print("Error: " + error.localizedDescription)

        self.lastUpdateTime = nil
        NotificationCenter.default.post(name: Notification.Name(rawValue: RESOURCE_UPDATE_FAIL), object: nil)
    }

    func updateResources(_ success:() -> Void, fail: @escaping (String?) -> Void) {
        checkSessionToken({ (_) -> Void in
            self.lastUpdateTime = Date()

            // culture code parameter
            let parameters = ["cultureCode":self.currentCulture]

            // get all resource
            let operationAllResources = FDdotRezAPIManager.operationAllResources(parameters)
            operationAllResources.setCompletionBlockWithSuccess({ (operation, results) in

              let stations : FDStations? = self.getResultObject(results?.array())
              let markets : FDMarkets? = self.getResultObject(results?.array())
              let titles : FDTitles? = self.getResultObject(results?.array())
              let countries : FDCountries? = self.getResultObject(results?.array())
              let documentTypes : FDDocumentTypes? = self.getResultObject(results?.array())
              let countryPhoneCodes : FDCountryPhoneCodes? = self.getResultObject(results?.array())
              let mealsInfo : FDMealsInfo? = self.getResultObject(results?.array())
              let relationships : FDRelationships? = self.getResultObject(results?.array())
              let cultures : FDCultures? = self.getResultObject(results?.array())
              let specialServiceRequests : FDSpecialServiceRequests? = self.getResultObject(results?.array())
              let fees : FDFees? = self.getResultObject(results?.array())
              let currencies : FDCurrencies? = self.getResultObject(results?.array())

                //stations,
                if let stations = stations {
                    stations.stations.sort(by: { (s1, s2) -> Bool in
                        return s1.name < s2.name
                    })
                    self.stations = stations.stations
                    self.saveResource(self.stations, resourceType: .Stations, language: self.currentCulture)
                }
                //markets,
                if let markets = markets {
                    let marketRes = markets.markets
                  self.updateMarkets(marketRes as NSArray)
                    self.saveResource(marketRes, resourceType: .Markets, language: self.currentCulture)
                }

                //titles,
                if let titles = titles {
                    self.titles = titles.titles
                    self.saveResource(self.titles, resourceType: .Titles, language: self.currentCulture)
                }

                //countries,
                if let countries = countries {
                    self.countries = countries.countries
                    self.saveResource(self.countries, resourceType: .Countries, language:self.currentCulture)
                }

                //documentTypes
                if let documentTypes = documentTypes {
                    self.documentTypes = documentTypes.documentTypes
                    self.saveResource(self.documentTypes, resourceType: .DocTypes, language:self.currentCulture)
                }

                //countryPhoneCodes,
                if let countryPhoneCodes = countryPhoneCodes {

                    for phoneCode in countryPhoneCodes.countryPhoneCodes {
                        if let index = self.countriesCode.index(
                            where: {$0 == phoneCode["countryCode"] as! String}) {

                                self.countries[index]["phoneCode"] = phoneCode["phoneCode"]
                        }
                    }

                    // update phone code string
                    self.countriesPhoneCode = self.countries.map({ "\($0.object(forKey: "name") as! String)(+\($0.object(forKey: "phoneCode") == nil ? "???" : $0.object(forKey: "phoneCode") as! String))" })

                    self.countriesPhoneCodeNumber = self.countries.map({
                        if $0.object(forKey: "phoneCode") != nil {
                            return "\($0.object(forKey: "phoneCode") as! String)"
                        } else {
                            return "???"
                        }
                        
                    })

                    self.saveResource(self.countries, resourceType: .Countries, language:self.currentCulture)
                }

                //mealsInfo,
                if let mealsInfo = mealsInfo {
                    self.mealsInfo = mealsInfo
                }

                //relationships,
                if let relationships = relationships {
                    self.relationships = relationships
                }

                //cultures,
                if let cultures = cultures {
                    self.languages = cultures.cultures
                    self.saveResource(self.languages, resourceType: .Languages, language:nil)
                }

                //specialServiceRequests,
                if let specialServiceRequests = specialServiceRequests {
                    self.specialServiceRequests = specialServiceRequests.items
                    self.saveResource(self.specialServiceRequests, resourceType: .SpecialServiceRequests, language:self.currentCulture)
                }

                //fees,
                if let fees = fees {
                    self.fees = fees.items
                    self.saveResource(self.fees, resourceType: .Fees, language:self.currentCulture)
                }

                //currencies
                if let currencies = currencies {
                    self.currencies = currencies.currencies
                    self.saveResource(self.currencies, resourceType: .Currencies, language:self.currentCulture)
                }

                }, failure: { (operation, error) in
                  self.resourceUpdateError(operation!, error: error! as NSError)
                  fail (unwrap(str: error?.localizedDescription))

            })
            operationAllResources.start()
            }) { (reason) -> Void in
                fail (reason)
        }
    }

    // cache resource to local
    func getStation(_ stationCode:String?) -> FDResourceStation? {
        if stationCode != nil {
            if let found = stations.map({ $0.code }).index(of: stationCode!) {
                return stations[found]
            }
        }

        return nil
    }

  func updateMarkets(_ res: NSArray?) {
    if res != nil {
      for station in stations {
        for i in 0..<res!.count {
          let marketsDict = res?.object(at: i) as! NSDictionary
          
          if (marketsDict.object(forKey: "key") as! String == station.code ) {
            let marketsCode : NSArray = marketsDict.object(forKey: "value") as! NSArray
            
            var markets = [FDResourceStation]()
            for i in 0 ..< marketsCode.count {
              let stationCode = marketsCode.object(at: i) as! String
              if let found = stations.map({ $0.code }).index(of: stationCode) {
                markets.append(stations[found])
              } else {
                //                                NSLog("Can't find station (code:%@) in %@(code=%@)'s market list", stationCode, station.name!, station.code)
              }
            }
            
            markets.sort(by: { (s1, s2) -> Bool in
              return s1.name < s2.name
            })
            
            station.markets = markets;
            break;
          }
        }
        //                for var i = 0; i < res?.count; i += 1 {
        //                    let marketsDict = res?.object(at: i) as! NSDictionary
        //
        //                    if (marketsDict.object(forKey: "key") as! String == station.code ) {
        //                        let marketsCode : NSArray = marketsDict.object(forKey: "value") as! NSArray
        //
        //                        var markets = [FDResourceStation]()
        //                        for i in 0 ..< marketsCode.count {
        //                            let stationCode = marketsCode.object(at: i) as! String
        //                            if let found = stations.map({ $0.code }).index(of: stationCode) {
        //                                markets.append(stations[found])
        //                            } else {
        ////                                NSLog("Can't find station (code:%@) in %@(code=%@)'s market list", stationCode, station.name!, station.code)
        //                            }
        //                        }
        //
        //                        markets.sort(by: { (s1, s2) -> Bool in
        //                            return s1.name < s2.name
        //                        })
        //
        //                        station.markets = markets;
        //                        break;
        //                    }
        //                }
      }

            self.stations = self.stations.filter( {$0.markets.count > 0} )

            isStationReady = true
            NotificationCenter.default.post(name: Notification.Name(rawValue: FDResourceManager.SationResourceReady), object: nil)
        }
    }

    func getLatestDepartCity() -> FDResourceStation? {

        let stationCodeDefaultsName = LastDepartStationCodeDefaultsName

        if let lastChangeFlightCityDate = UserDefaults.standard.object(forKey: LastChangeFlightCityDateDefaultsName) as? Date, lastChangeFlightCityDate.isLaterThanDate(Date().dateBySubtractingDays(30)) {

            return getLatestStation(stationCodeDefaultsName)
        }

        return nil
    }

    func getLatestDepartCity2() -> FDResourceStation? {
        return getLatestStation(LastDepartStationCode2DefaultsName)
    }

    fileprivate func getLatestStation(_ stationCodeDefaultsName:String) -> FDResourceStation? {
        if let stationCode = UserDefaults.standard.object(forKey: stationCodeDefaultsName) as? String {
            if let found = stations.map({ $0.code }).index(of: stationCode) {
                return stations[found]
            }
        }

        return nil
    }

    func getLatestArriveCity() -> FDResourceStation? {
        return getLatestStation(LastArriveStationCodeDefaultsName)
    }

    func getLatestArriveCity2() -> FDResourceStation? {
        return getLatestStation(LastArriveStationCode2DefaultsName)
    }



    func getNearestStation(_ lat: Double, lon: Double) -> FDResourceStation? {

        var nearestStation : FDResourceStation?
        var nearestDistance : Double = 50000 // 50 KM

        for station in stations {
            let distance = station.distanceFrom(lat, lon:lon)
            if distance < nearestDistance {
                nearestStation = station
                nearestDistance = distance
            }
        }

        return nearestStation

    }

    func setLatestDepartCity(_ code: String?) {
        if code != nil {
            UserDefaults.standard.setValue(code, forKey: self.LastDepartStationCodeDefaultsName)
            UserDefaults.standard.setValue(Date(), forKey: self.LastChangeFlightCityDateDefaultsName)
        }
    }

    func setLatestDepartCity2(_ code: String?) {
        if code != nil {
            UserDefaults.standard.setValue(code, forKey: self.LastDepartStationCode2DefaultsName)
        }
    }

    func setLatestArriveCity(_ code: String?) {
        if code != nil {
            UserDefaults.standard.setValue(code, forKey: self.LastArriveStationCodeDefaultsName)
        }
    }

    func setLatestArriveCity2(_ code: String?) {
        if code != nil {
            UserDefaults.standard.setValue(code, forKey: self.LastArriveStationCode2DefaultsName)
        }
    }

    func getLatestSearcDepartureDate() -> Date {
        if let d = UserDefaults.standard.object(forKey: LastSearchDepartureDateDefaultsName) as? Date {
            if d.compare(Date()) == ComparisonResult.orderedDescending {
                return d
            }
        }

        return Date().dateAtStartOfDay(true)
    }

    func setLatestSearcPassengers(_ adult:Int, child:Int, infant:Int) {
        let passengerNumber = adult*100 + child*10 + infant
        UserDefaults.standard.setValue(passengerNumber, forKey: self.LastSearchPassengerNumberDefaultsName)
    }

    func getLatestSearcPassengers() -> (Int, Int, Int) {
        if let d = UserDefaults.standard.object(forKey: LastSearchPassengerNumberDefaultsName) as? Int {

            return ((d/100)%10, (d/10)%10, d%10)
        }

        return (1,0,0)
    }

    func setLatestSearcPromoCode(_ promoCode : String?) {
        if let promoCode = promoCode { UserDefaults.standard.setValue(promoCode, forKey: self.LastSearchPromoCodeDefaultsName)
        } else {
            UserDefaults.standard.setValue("", forKey: self.LastSearchPromoCodeDefaultsName)
        }
    }

    func getLatestSearcPromoCode() -> String {
        if let d = UserDefaults.standard.object(forKey: LastSearchPromoCodeDefaultsName) as? String {

            return d
        }

        return ""
    }

    func getLatestSearcArriveDate() -> Date {
        if let d = UserDefaults.standard.object(forKey: LastSearchArriveDateDefaultsName) as? Date {
            if d.compare(Date()) == ComparisonResult.orderedDescending {
                return d
            }
        }
        return Date().dateAtStartOfDay(true).dateByAddingDays(1)
    }

    func setLatestSearcDepartureDate(_ date: Date) {
        UserDefaults.standard.setValue(date, forKey: self.LastSearchDepartureDateDefaultsName)
    }

    func setLatestSearcArriveDate(_ date: Date) {
        UserDefaults.standard.setValue(date, forKey: self.LastSearchArriveDateDefaultsName)
    }

    func setFlightOption(_ f: FlightOption) {
        UserDefaults.standard.setValue(f.rawValue, forKey: self.LastSearchFlisghtOptionDefaultsName)
    }
  
//  func setShowInSmilePoints(_ inPoints: Bool) {
//    UserDefaults.standard.set(inPoints, forKey: FDResourceManager.kShowInSmilePoints)
//    UserDefaults.standard.synchronize()
//  }
//
//  func setShowInSmilePoints() -> Bool {
//    return UserDefaults.standard.bool(forKey: FDResourceManager.kShowInSmilePoints)
//  }

    func getFlightOption() -> FlightOption {
        if let d = UserDefaults.standard.object(forKey: LastSearchFlisghtOptionDefaultsName) as? Int {
            if let f = FlightOption(rawValue: d) {
                return f
            }
        }
        return FlightOption.return
    }

    func setPayWithNaSmiles(_ payWithNaSmiles: Bool) {
        UserDefaults.standard.set(payWithNaSmiles, forKey: self.PayWithNaSmilesDefaultsName)
    }

    func getPayWithNaSmiles() -> Bool {
        if let d = UserDefaults.standard.object(forKey: PayWithNaSmilesDefaultsName) as? Bool {
            return d
        }

        return false
    }

    func loadSavedResource(_ language:String?) {
        if let success = unarchiveResource(.Languages, language: nil) as? [FDResourceString] {
            languages = success
        }

        if let success = unarchiveResource(.Stations, language: language) as? [FDResourceStation] {
            stations = success
        }

        if let success = unarchiveResource(.Markets, language: language) as? NSArray {
            updateMarkets(success)
        }

        if let success = unarchiveResource(.Currencies, language: language) as? [FDResourceString] {
            currencies = success
        }

        if let success = unarchiveResource(.Countries, language: language) as? [NSMutableDictionary] {
            countries = success
        }

        if let success = unarchiveResource(.Titles, language: language) as? [NSMutableDictionary] {
            titles = success
        } else {
            self.titles = [
                    ["description":"Mr.","isChildOrInfant":false,"key":"MR"],
                    ["description":"Mrs.","isChildOrInfant":false,"key":"MRS"],
                    ["description":"Ms.","isChildOrInfant":false,"key":"MS"],
                    ["description":"Miss","isChildOrInfant":true,"key":"MISS"],
                    ["description":"Master","isChildOrInfant":true,"key":"MSTR"]
                ]
        }

//        if let s = unarchiveResource(.DocTypes, language: language) as? [NSMutableDictionary] {
//            docTypes = s
//        } else {
//
//        }

//        // todo
//        docTypes = ["Passport","National ID Card"]
//        docTypesCode = ["P","I"]

        ssrRes = [
            FDExtraItem(name: FDLocalized("No Extra Bag"), code:"XB", imgUrl: nil),
            FDExtraItem(name: FDLocalized("1 Extra Bag"), code:"X20", imgUrl: nil),
//            FDExtraItem(name: "2 Extra Bag", code:"XXB", imgUrl: nil),

//            FDExtraItem(name: "Arabic Mezze", code:"VOML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Arabic-Mezze.jpg"),
//            FDExtraItem(name: "Cheese Omelette", code:"VLML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Cheese-Omelette.jpg"),
//            FDExtraItem(name: "Chicken Biryani", code:"VJML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Chicken-Biryani.jpg"),
//            FDExtraItem(name: "Chicken Ceasar Salad", code:"LSML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Chicken-Ceasar-Salad.jpg"),
//            FDExtraItem(name: "Chicken Penne Pasta", code:"LFML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Chicken-Penne-Pasta.jpg"),
//            FDExtraItem(name: "Chicks  Shawarma", code:"LCML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Chicks--Shawarma.jpg"),
//            FDExtraItem(name: "Cream Cheese  wrap", code:"FGML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Cream-Cheese--wrap.jpg"),
//            FDExtraItem(name: "Fruit Salad", code:"FPML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Fruit-Salad.jpg"),
//            FDExtraItem(name: "Hot Foul", code:"DBML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Hot-Foul.jpg"),
//            FDExtraItem(name: "Hot kabab Wrap", code:"BLML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Hot-kabab-Wrap.jpg"),
//            FDExtraItem(name: "Mixed Grill", code:"V1ML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Mixed-Grill.jpg"),
//            FDExtraItem(name: "Sandwich Snack", code:"V2ML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Sandwich-Snack.jpg"),
//            FDExtraItem(name: "Vegetable Biryani", code:"V3ML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Vegetable-Biryani.jpg"),
//            FDExtraItem(name: "Wrap Meal Boxee", code:"GFML", imgUrl: "https://dl.dropboxusercontent.com/u/3940805/MealImages/Wrap-Meal-Boxee.jpg")
        ]
    }

    func saveRecentPassengers(_ passengers:[FDPassenger]) {
        savedPassenger.removeAll()
        for passenger in passengers {
            savedPassenger.append(FDPassenger(passenger: passenger))
        }
        #if DEBUG
        self.saveResource(self.savedPassenger, resourceType: .RecentPassengers, language:nil)
        #endif
    }

    func saveRecentContactInfo(_ contactInfo:FDContactItemInfo) {
        self.savedContactInfo.copyContact(contactInfo)
        #if DEBUG
        self.saveResource(self.savedContactInfo, resourceType: .RecentContact, language:nil)
        #endif
    }

    func loadRecentPassengersContact() {
        if let success = unarchiveResource(.RecentPassengers, language: nil) as? [FDPassenger] {
            self.savedPassenger = success
        }

        if let success = unarchiveResource(.RecentContact, language: nil) as? FDContactItemInfo {
            self.savedContactInfo = success
        }
    }

    func countryNameByCode(_ code:String?) -> String? {
        if code != nil {
            if let index = countriesCode.index(of: code!) {
                return countriesName[index]
            }
        }
        return nil
    }

    func countryPhoneCodeByCode(_ code:String?) -> String? {
        if code != nil {
            if let index = countriesCode.index(of: code!) {
                return countriesPhoneCode [index]
            }
        }
        return nil
    }

    func countryPhoneNumberByCode(_ code:String?) -> String? {
        if code != nil {
            if let index = countriesCode.index(of: code!) {
                return countries[index].object(forKey: "phoneCode") as? String
            }
        }
        return nil
    }

    func countryPhoneCodeByNumber(_ codeNumber:String?) -> String? {
        if codeNumber != nil {
            if let index = countriesPhoneCodeNumber.index(of: codeNumber!) {
                return countriesCode[index]
            }
        }
        return nil
    }

    func titleByKey(_ key:String?) -> String? {
        if key != nil {

            if let index = titles.index(where: {(item) -> Bool in
                guard let value = item.object(forKey: "key") as? String else {return false}
                return value == key
            }) {
                let title = titles[index]
                return title.object(forKey: "description") as? String
            }
        }
        return nil
    }

    func getServiceItemName(_ code:String?, defaultName:String) -> String {
        if code != nil {
            if let index = ssrRes.index(where: {$0.itemKey == code}) {
                return FDLocalized(unwrap(str: ssrRes[index].itemName))
            }
            return code!.localized
        }
        return defaultName
    }

    func getServiceItemUrl(_ code:String?) -> String? {
        if code != nil {
            if let index = ssrRes.index(where: {$0.itemKey == code}) {
                return ssrRes[index].itemImgUrl
            }
            return code!
        }

        return nil
    }

    func relationshipNameByCode(_ code:String) -> String {

        if let rs = relationships {
            for r in rs.relationships {
                if code == r.value {
                    if let n = r.name {
                        return n
                    }
                }
            }
        }

        return ""
    }

    func loadImage(_ imageUrl:String, success:@escaping (_ image: UIImage) -> Void, fail: @escaping (String?) -> Void) {

        let manager = RKObjectManager.shared()
        let downloadRequest = manager?.request(with: nil, method: .GET, path: imageUrl, parameters: nil)
        downloadRequest?.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData

        let requestOperation = AFImageRequestOperation(request: downloadRequest as URLRequest!)

        requestOperation?.setCompletionBlockWithSuccess({ (operation, result) -> Void in
//            SVProgressHUD.dismiss()
            if let data = operation?.responseData {
                if let image = UIImage(data: data) {
                    success(image)
                    return
                } else {
                    fail (FDErrorHandle.API_GENERAL_ERROR)
                }
            } else {
                fail (FDErrorHandle.API_GENERAL_ERROR)
            }

            }) { (operation, error) -> Void in
                fail (error?.localizedDescription)
            }
        manager?.httpClient.enqueue(requestOperation)
    }

    fileprivate var lastUpdateTimeMainImage : Date?
    fileprivate var homePageMainImage : UIImage?
    func getHomePageMainImage(_ success:@escaping (_ image: UIImage) -> Void, fail: @escaping (String?) -> Void) {
        let imageUrl = isAr() ? HOMEPAGE_MAIN_IMAGE_AR : HOMEPAGE_MAIN_IMAGE_EN
        if lastUpdateTimeMainImage == nil || !lastUpdateTimeMainImage!.isToday() || homePageMainImage == nil  {
            self.lastUpdateTimeMainImage = Date()
            loadImage(imageUrl, success: { (image) -> Void in
                self.homePageMainImage = image
                success(image)
                }, fail: { (r) -> Void in
                    self.lastUpdateTimeMainImage = nil
                    self.homePageMainImage = nil
                    fail(r)
            })
        } else {
            success(homePageMainImage!)
        }
    }

    fileprivate var lastUpdateTimeADImage : Date?
    fileprivate var homePageADImage : UIImage?
    func getHomePageADImage(_ success:@escaping (_ image: UIImage) -> Void, fail: @escaping (String?) -> Void) {
        let imageUrl = isAr() ? HOMEPAGE_AD_IMAGE_AR : HOMEPAGE_AD_IMAGE_EN

        if lastUpdateTimeADImage == nil || !lastUpdateTimeADImage!.isToday() || homePageADImage == nil  {
            self.lastUpdateTimeADImage = Date()
            loadImage(imageUrl, success: { (image) -> Void in
                self.homePageADImage = image
                success(image)
                }, fail: { (r) -> Void in
                    self.lastUpdateTimeADImage = nil
                    self.homePageADImage = nil
                    fail(r)
            })
        } else {
            success(homePageADImage!)
        }
    }

    func refreshImage() {
        self.lastUpdateTimeMainImage = nil
        self.homePageMainImage = nil
        self.lastUpdateTimeADImage = nil
        self.homePageADImage = nil
    }
}


