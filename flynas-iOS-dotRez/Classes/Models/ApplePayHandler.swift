//
//  ApplePayHandler.swift
//  ApplePay_sample
//
//  Created by Kenan Karakecili on 13/7/17.
//  Copyright © 2017 kenankarakecili. All rights reserved.
//

import PassKit

struct APStruct {
  var apple_displayName = ""
  var apple_network = ""
  var apple_type = ""
  var paymentData = APPaymentDataStruct()
}

struct APPaymentDataStruct {
  var data = ""
  var publicKeyHash = ""
  var transactionId = ""
  var wrappedKey = ""
  var ephemeralPublicKey = ""
  var signature = ""
  var version = ""
}

typealias PaymentCompletionHandler = (_ success: Bool, _ applePayStruct: APStruct) -> Void

class ApplePayHandler: NSObject {
  
  static let mySupportedNetworks = [
    PKPaymentNetwork.amex,
    PKPaymentNetwork.masterCard,
    PKPaymentNetwork.visa
  ]
  static let ApplePayMerchantID = "merchant.flynas.com"
  static let kDefaultsApplePaySplashHasShown = "kDefaultsApplePaySplashHasShown"
  
  var applePayVC: PKPaymentAuthorizationViewController?
  var paymentSummaryItems: [PKPaymentSummaryItem] = []
  var paymentStatus: PKPaymentAuthorizationStatus = .failure
  var completionHandler: PaymentCompletionHandler?
  var amount = "0.00"
  var token = ""
  
  func itemsToSell() -> [PKPaymentSummaryItem] {
    let fare = PKPaymentSummaryItem(label: "TOTAL", amount: NSDecimalNumber(string: amount))
    let totalAmount = fare.amount.doubleValue
    let total = PKPaymentSummaryItem(label: "FLYNAS", amount: NSDecimalNumber(value: totalAmount as Double))
    return [fare, total]
  }
  
  func startPayment(_ amount: String, completion: @escaping PaymentCompletionHandler) {
    self.amount = amount
    paymentSummaryItems = itemsToSell()
    completionHandler = completion
    let paymentRequest = PKPaymentRequest()
    paymentRequest.paymentSummaryItems = paymentSummaryItems
    paymentRequest.merchantIdentifier = ApplePayHandler.ApplePayMerchantID
    paymentRequest.merchantCapabilities = .capability3DS
    paymentRequest.countryCode = DEFAULT_COUNTY_CODE
    paymentRequest.currencyCode = FDResourceManager.sharedInstance.selectedCurrencyCode
    paymentRequest.requiredShippingAddressFields = PKAddressField()
    paymentRequest.supportedNetworks = ApplePayHandler.mySupportedNetworks
    applePayVC = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
    applePayVC?.delegate = self
    let topVC = getTopmostVC()
    if let vc = applePayVC {
      topVC?.present(vc, animated: true, completion: nil)
    }
  }
  
}

extension ApplePayHandler: PKPaymentAuthorizationViewControllerDelegate {
  
  func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    // Perform some very basic validation on the provided contact information
    // Here you would send the payment token to your server or payment provider to process
    // Once processed, return an appropriate status in the completion handler (success, failure, etc)
    print("Payment token: \(payment.token.transactionIdentifier)")
    paymentStatus = .success
    let paymentDataItem = parsePayment(payment.token.paymentData)
    let item = APStruct(apple_displayName: unwrap(str: payment.token.paymentMethod.displayName),
                        apple_network: unwrap(str: payment.token.paymentMethod.network?.rawValue),
                        apple_type: card(payment.token.paymentMethod.type),
                        paymentData: paymentDataItem)
    completionHandler!(paymentStatus == .success, item)
  }
  
  func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
    completionHandler!(false, APStruct())
  }
  
}

extension ApplePayHandler {
  
  class func getPaymentButton() -> PKPaymentButton {
    var type: PKPaymentButtonType = .plain
    if ApplePayHandler.status().canMakePayments && ApplePayHandler.status().hasAddedCards {
      type = .buy
    } else if ApplePayHandler.status().canMakePayments {
      type = .setUp
    }
    let btn = PKPaymentButton(paymentButtonType: type, paymentButtonStyle: .black)
    return btn
  }
  
  class func status() -> (canMakePayments: Bool, hasAddedCards: Bool) {
    return (PKPaymentAuthorizationViewController.canMakePayments(), PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: mySupportedNetworks, capabilities: PKMerchantCapability.capability3DS))
  }
  
  class func showSplashIfNeeded() {
    let hasShownBefore = UserDefaults.standard.bool(forKey: ApplePayHandler.kDefaultsApplePaySplashHasShown)
    if hasShownBefore || !status().canMakePayments { return }
    let topVC = getTopmostVC()
    let story = UIStoryboard(name: "Main", bundle: nil)
    let splashVC = story.instantiateViewController(withIdentifier: "ApplePaySplashVC")
    topVC?.present(splashVC, animated: true, completion: nil)
    UserDefaults.standard.set(true, forKey: ApplePayHandler.kDefaultsApplePaySplashHasShown)
    UserDefaults.standard.synchronize()
  }
  
  class func setupCards() {
    let passLibrary = PKPassLibrary()
    passLibrary.openPaymentSetup()
  }
  
  internal func parsePayment(_ data: Data) -> APPaymentDataStruct {
    let json = try? JSONSerialization.jsonObject(with: data, options: [])
    guard let myJson = json as? [String: AnyObject] else { return APPaymentDataStruct() }
    let header = myJson["header"] as! [String: String]
    let item = APPaymentDataStruct(data:               unwrap(str: myJson["data"]),
                                   publicKeyHash:      unwrap(str: header["publicKeyHash"]),
                                   transactionId:      unwrap(str: header["transactionId"]),
                                   wrappedKey:         unwrap(str: header["wrappedKey"]),
                                   ephemeralPublicKey: unwrap(str: header["ephemeralPublicKey"]),
                                   signature:          unwrap(str: myJson["signature"]),
                                   version:            unwrap(str: myJson["version"]))
    return item
  }
  
  func card(_ type: PKPaymentMethodType) -> String {
    switch type {
    case .unknown:
      return "Unknown"
    case .debit:
      return "Debit"
    case .credit:
      return "Credit"
    case .prepaid:
      return "Prepaid"
    case .store:
      return "Store"
    }
  }
  
}
