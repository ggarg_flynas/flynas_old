//
//  FDPaymentResult.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPaymentResult: NSObject {

    static let StatusReceived = "Received"
    static let AuthorizationStatusPending = "Pending"

    var authorizationStatus = ""
    var balanceDue: NSNumber = 0
    var status = ""
    var tdsRedirectUrl : String?
}

