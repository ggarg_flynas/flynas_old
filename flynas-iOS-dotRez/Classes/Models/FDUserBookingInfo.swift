//
//  FDUserBookingInfo.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 5/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum BookingFlow {
  case normalBooking
  case manageMyBooking
}

class FDUserBookingInfo: NSObject {
  
  var bookingFlow = BookingFlow.normalBooking
  var managingFlights = [Int]()
  
  var flightOption = FlightOption.return
  var departureDate : Date?
  var returnDate : Date?
  
  var adultNum: Int = 1
  var childNum: Int = 0
  var infantNum: Int = 0
  var passengerNumberExcludeInfant : Int {
    return adultNum + childNum
  }
  
  var passengers = [FDPassenger]() {
    didSet {
      updatePassengerNumber()
    }
    
  }
  var contactInfo = FDContactItemInfo()
  
  var promoCode = ""
  
  var passengersContactObject : [String:Any] {
    var passengersArray = [[String:Any]]()
    //        if let p = passengers.last {
    //            p.discountCode = promoCode
    //        }
    for passenger in passengers {
      passengersArray.append(passenger.passengerInput)
    }
    
    return ["contactInput" : contactInfo.contactInput, "passengersInput": ["passengers":passengersArray]]
  }
  
  var passengersObject : [String:Any] {
    var passengersArray = [[String:Any]]()
    //        if let p = passengers.last {
    //            p.discountCode = promoCode.uppercaseString
    //        }
    
    for passenger in passengers {
      
      passengersArray.append(passenger.passengerInput)
    }
    
    return ["passengersInput": ["passengers":passengersArray]]
  }
  
  var contactObject : [String:Any] {
    
    guard let passenger = passengers.first else {return ["contactInput":"" as AnyObject]}
    
    return ["contactInput" : ["contactItemInfo":
      [
        "mobilePhone": contactInfo.mobilePhone.phoneObject,
        "workPhone": contactInfo.workPhone.phoneObject,
        "emailAddress": contactInfo.emailAddress,
        "contactName": [
          "first":passenger.firstName,
          "last":passenger.lastName,
          "title":passenger.title
        ]
      ]]
    ]
  }
  
  var isOneWay : Bool {
    return returnDate == nil
  }
  
  var isTwoWay : Bool {
    return returnDate != nil
  }
  
  var hasCodeShareFlight : Bool {
    if let departureFlight = departureFlight, departureFlight.isCodeShare {
      return true
    }
    
    if let returnFlight = returnFlight, returnFlight.isCodeShare {
      return true
    }
    
    return false
  }
  
  var hasConnectionFlight : Bool {
    if let departureFlight = departureFlight, departureFlight.numberOfStops > 1 {
      return true
    }
    
    if let returnFlight = returnFlight, returnFlight.numberOfStops > 1 {
      return true
    }
    
    return false
  }
  
  
  func updatePassengerDiscountCode (_ passengersInput:FDPassengersInput) {
    for p in passengersInput.passengers {
      for passenger in passengers {
        if p.passengerNumber == passenger.passengerNumber {
          passenger.discountCode = p.discountCode
          break;
        }
      }
    }
  }
  
  func updatePassengerDefaultTravelDocType (_ docType: FDTravelDocumentTypeCode) {
    for passenger in passengers {
      if passenger.firstName.isEmpty || passenger.lastName.isEmpty || passenger.dateOfBirth == nil {
        passenger.travelDocument.docTypeCode = docType.rawValue
      }
    }
  }
  
  var stationDepart : FDResourceStation?
  var stationArrive : FDResourceStation?
  
  var stationDepart2 : FDResourceStation?
  var stationArrive2 : FDResourceStation?
  
  var currencyCode : String?
  var payWithNaSmiles = false
  // FDResourceManager. selectedCurrencySymbol ?
  var priceSummary = FDPriceSummary()
  
  // user booking selection
  var departureFlight: FDFlight?
  var departureFare : FDFare?
  
  var returnFlight: FDFlight?
  var returnFare : FDFare?
  
  // documentTypes
  var documentTypes: FDDocumentTypes?
  
  // baggage
  var baggageSsr : FDBaggageSsr?
  func getBaggageService(_ flightIndex:Int, passengerNumber:Int) -> FDFlightPartService? {
    let (flight, _) = getFlightInfo(flightIndex)
    return baggageSsr?.getPassengerService(flight?.origin, destination: flight?.destination, passengerNumber: passengerNumber)
  }
  
  var sportsSsr: FDSportsSsr?
  func getSportsService(_ flightIndex:Int, passengerNumber:Int) -> FDFlightPartService? {
    let (flight, _) = getFlightInfo(flightIndex)
    return sportsSsr?.getPassengerService(flight?.origin, destination: flight?.destination, passengerNumber: passengerNumber)
  }
  
  // meal
  var mealSsr : FDMealSsr?
  func getMealService(_ legIndex:Int, passengerNumber:Int) -> FDFlightPartService? {
    let (leg, _) = getLegInfo(legIndex)
    return mealSsr?.getPassengerService(leg?.origin, destination: leg?.destination, passengerNumber: passengerNumber)
  }
  
  // lounge
  var loungeSsrStruct = LoungeSsrStruct()
  var loungeSsr : FDLoungeSsr?
  func getLoungeService(_ flightIndex:Int, passengerNumber:Int) -> FDFlightPartService? {
    let (flight, _) = getFlightInfo(flightIndex)
    return loungeSsr?.getPassengerService(flight?.origin, destination: flight?.destination, passengerNumber: passengerNumber)
  }
  
  // seatMaps
  var flightSeatMap = FDFlightSeatMap() {
    didSet {
      //            // re-order with leg's order
      //            var seatMaps = [FDSeatMap]()
      //            for legIndex in 0...legNumber - 1 {
      //                if let seatMap = getSeatMap(legIndex) {
      //                    seatMap.syncSeatMap()
      //                    seatMaps.append(seatMap)
      //                } else {
      //                    seatMaps.append(FDSeatMap())
      //                }
      //            }
      //            flightSeatMap.seatMaps = seatMaps
      
      // buffer passenger full name
      for sm in flightSeatMap.seatMaps {
        sm.syncSeatMap()
        for ps in sm.passengerSeatInfos {
          if let p = getPassengerInfo(ps.passengerNumber) {
            ps.fullName = p.fullName
            ps.nameInitial = p.nameInitial
            ps.paxType = p.paxType
            ps.hasInfantAttached = getInfantPassengerInfo(ps.passengerNumber) != nil
          }
        }
        
        sm.diretion = getLegDirectionBy(sm.origin, destination: sm.destination)
        
        sm.changeAllowed = getLegChangeAllowed(sm.origin, destination: sm.destination)
      }
    }
  }
  
  // payment
  var agreeToTerms = false
  var paymentDetail = FDPayment()
  var paymentMethods = FDAvailablePaymentMethods()
  var paymentResult = FDPaymentResult()
  var bookingPolling = FDBookingPolling()
  var bookingDetails = FDBookingDetails()
  var bookingPayments : FDBookingPayments?
  
  // for ui only
  var addBagage = true
  var selectMeals = false
  var addLounge = false
  var currentExpandedMealCell : IndexPath?
  
  func updatePassengerNumber () {
    self.adultNum = 0
    self.childNum = 0
    self.infantNum = 0
    
    for p in passengers {
      switch p.paxType {
      case PaxType.Child.rawValue:
        childNum += 1
        break
      case PaxType.Infant.rawValue:
        infantNum += 1
        break
      default:
        adultNum += 1
      }
    }
  }
  
  func setPassengerNumber(_ adultNum:Int, childNum:Int, infantNum:Int) {
    self.adultNum = adultNum
    self.childNum = childNum
    self.infantNum = infantNum
    
    passengers.removeAll()
    var number = 0
    for i in (0 ..< adultNum) {
      let passenger  = FDPassenger(paxType: .Adult, passengerNumber: number)
      passenger.paxTypeIndex = i
      passengers.append(passenger)
      number += 1
    }
    
    for i in (0 ..< childNum) {
      let passenger  = FDPassenger(paxType: .Child, passengerNumber: number)
      passenger.paxTypeIndex = i
      passengers.append(passenger)
      
      number += 1
    }
    
    for i in (0 ..< infantNum) {
      let passenger  = FDPassenger(paxType: .Infant, passengerNumber: i) // attached adult number
      passenger.paxTypeIndex = i
      passengers.append(passenger)
      
      number += 1
    }
    
  }
  
  func hasInfantAttachedToPassenger(_ index:Int) -> Bool {
    return index >= 0 && index < infantNum
  }
  
  func clearUserSelect () {
    departureFlight = nil
    departureFare = nil
    returnFlight = nil
    returnFare = nil
  }
  
  func clearReturnFlight () {
    returnFlight = nil
    returnFare = nil
  }
  
  var flightNumber : Int {
    var fn = 0
    if departureFlight != nil {
      fn += 1
    }
    
    if returnFlight != nil {
      fn += 1
    }
    
    return fn
  }
  
  var hasJourney : Bool {
    if departureFlight != nil || returnFlight != nil {
      return true
    }
    return false
  }
  
  func getFlights() -> [FDFlight] {
    var flights = [FDFlight]()
    
    if departureFlight != nil {
      flights.append(departureFlight!)
    }
    
    if returnFlight != nil {
      flights.append(returnFlight!)
    }
    
    return flights
  }
  
  var legNumber : Int {
    var legNumber = 0
    if departureFlight != nil {
      legNumber += departureFlight!.legs.count
    }
    
    if returnFlight != nil {
      legNumber += returnFlight!.legs.count
    }
    
    return legNumber
  }
  
  func getFlightIndexByLegIndex(_ legIndex:Int) -> Int {
    if legIndex >= departureFlight!.legs.count {
      return 1
    }
    return 0
  }
  
  func getLegFareType(_ index:Int) -> String? {
    
    if departureFlight != nil {
      let departureLegNumber = departureFlight!.legs.count
      if index >= 0 && index < departureLegNumber {
        return departureFare?.fareType
      } else if (index >= departureFlight!.legs.count && returnFlight != nil && index < departureLegNumber + returnFlight!.legs.count) {
        return returnFare?.fareType
      }
    }
    
    return nil
  }
  
  func getFlightInfo(_ index: Int) -> (FDFlight?, DestinationOption) {
    if index == 0 && departureFlight != nil {
      return (departureFlight, .depart)
    } else if index == 1 && returnFlight != nil {
      return (returnFlight, .arrive)
    }
    return (nil, DestinationOption.depart)
  }
  
  func getLegDirectionBy(_ origin:String, destination:String) -> DestinationOption {
    
    if departureFlight != nil {
      for l in departureFlight!.legs {
        if l.origin == origin && l.destination == destination {
          return DestinationOption.depart
        }
      }
    }
    
    if returnFlight != nil {
      for l in returnFlight!.legs {
        if l.origin == origin && l.destination == destination {
          return DestinationOption.arrive
        }
      }
    }
    
    return DestinationOption.none
  }
  
  func getLegChangeAllowed(_ origin:String, destination:String) -> Bool {
    
    if departureFlight != nil {
      for l in departureFlight!.legs {
        if l.origin == origin && l.destination == destination {
          return departureFlight!.changeAllowed
        }
      }
    }
    
    if returnFlight != nil {
      for l in returnFlight!.legs {
        if l.origin == origin && l.destination == destination {
          return returnFlight!.changeAllowed
        }
      }
    }
    
    return true // default : can change the flight
  }
  
  // get leg infor by flight origin and destion, and the leg index in this flight
  func getLegInfoBy(_ origin:String?, destination:String?, index:Int) -> FDLeg? {
    
    if departureFlight != nil {
      if departureFlight?.origin == origin && departureFlight?.destination == destination && index < departureFlight!.legs.count {
        return departureFlight!.legs[index]
      } else if returnFlight?.origin == origin && returnFlight?.destination == destination && index < returnFlight!.legs.count {
        return returnFlight!.legs[index]
      }
    }
    
    return nil
  }
  
  func getLegInfo(_ index:Int) -> (FDLeg?, DestinationOption) {
    
    if departureFlight != nil {
      let departureLegNumber = departureFlight!.legs.count
      if index >= 0 && index < departureLegNumber {
        return (departureFlight!.legs[index], DestinationOption.depart)
      } else if (index >= departureFlight!.legs.count && returnFlight != nil && index < departureLegNumber + returnFlight!.legs.count) {
        return (returnFlight!.legs[index - departureLegNumber], DestinationOption.arrive)
      }
    }
    
    return (nil,DestinationOption.depart)
  }
  
  func updatePassengerLegExtraItem () {
    for passenger in passengers {
      passenger.legNumber = legNumber
    }
  }
  
  func getSellKeys () -> [String] {
    var sellKeys = [String]()
    if departureFare != nil && departureFare!.fareKey != nil &&  (managingFlights.count != 1 || managingFlights[0] == 0) {
      sellKeys.append(departureFare!.fareKey! + "|" + departureFlight!.journeyKey! + SharedModel.shared.economyFareCodeDepart)
    }
    if returnFare != nil && returnFare!.fareKey != nil &&  (managingFlights.count != 1 || managingFlights[0] == 1) {
      sellKeys.append(unwrap(str: returnFare?.fareKey) + "|" + unwrap(str: returnFlight?.journeyKey) + SharedModel.shared.economyFareCodeReturn)
    }
    return sellKeys
  }
  
  func updatePriceSummary(_ withSellKey:Bool, success:@escaping (_ priceSummary: FDPriceSummary) -> Void, fail: @escaping (String?) -> Void) {
    FDBookingManager.sharedInstance.priceSummary((withSellKey ? getSellKeys() : nil), success: { (priceSummary) -> Void in
      self.currencyCode = priceSummary.currencyCode
      self.priceSummary = priceSummary
      success(priceSummary)
      }, fail: { (reason) -> Void in
        fail(reason)
    })
  }
  
  // seat map
  func getPassengerSeatInfo(_ passengerIndex:Int, legIndex:Int) -> FDPassengerSeatInfo? {
    return getSeatMap(legIndex)?.getPassengerSeatInfo(passengerIndex)
  }
  
  func hasPassengerSeatSelected(_ passengerIndex:Int, legIndex:Int) -> Bool {
    return getPassengerSeatInfo(passengerIndex, legIndex: legIndex)?.selectedSeatNumber != nil
  }
  
  func getPassengerSeatNumber(_ passengerIndex:Int, legIndex:Int) -> String? {
    return getPassengerSeatInfo(passengerIndex, legIndex: legIndex)?.selectedSeatNumber
  }
  
  func getPassengerSeatPrice(_ passengerIndex:Int, legIndex:Int) -> NSNumber? {
    return getPassengerSeatInfo(passengerIndex, legIndex: legIndex)?.selectedSeatPrice
  }
  
  func getSeatMap(_ legIndex:Int) -> FDSeatMap? {
    let (legInfo,_) = getLegInfo(legIndex)
    if legInfo != nil {
      return flightSeatMap.getSeatMap(legInfo!.destination, origin:legInfo!.origin)
    }
    return nil
  }
  
  func getPassengerInfo(_ passengerNumber:Int) -> FDPassenger? {
    for p in passengers {
      if p.passengerNumber == passengerNumber && p.paxType != PaxType.Infant.rawValue {
        return p
      }
    }
    return nil
  }
  
  func getInfantPassengerInfo(_ passengerNumber:Int) -> FDPassenger? {
    for p in passengers {
      if p.passengerNumber == passengerNumber && p.paxType == PaxType.Infant.rawValue {
        return p
      }
    }
    return nil
  }
  
  func getUserInitial(_ passengerNumber:Int) -> String {
    let passenger = passengers[passengerNumber]
    return passenger.nameInitial
  }
  
  func updateUserSeatSelection(_ userSeatSelection: FDFlightSeatMap?) -> Bool {
    var error = false
    if let u = userSeatSelection {
      for sm in u.seatMaps {
        if let seatMap = flightSeatMap.getSeatMap(sm.destination, origin: sm.origin) {
          for ps in sm.passengerSeatInfos {
            if let passengerSeatInfo = seatMap.getPassengerSeatInfo(ps.passengerNumber) {
              if passengerSeatInfo.selectedSeat != ps.selectedSeat {
                error = true
                passengerSeatInfo.selectedSeat = ps.selectedSeat
                // update
                passengerSeatInfo.userSelectedSeat = seatMap.getSeatInfo(ps.selectedSeat)
              }
            }
          }
        }
      }
    }
    
    return error
  }
  
  // mmb
  var pnr = ""
  var userEmail : String?
  var userLastName : String?
  
  func resetFlight(_ success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    FDBookingManager.sharedInstance.getBooking(pnr, departureStation: nil, email: userEmail, lastName: userLastName, success: { () -> Void in
      
      success()
      
      }, fail:{ (reason) -> Void in
        fail(reason)
    })
    
  }
  
}
