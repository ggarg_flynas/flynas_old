//
//  FDMemberLogin.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDMemberLogin: NSObject {

    var customerId = ""
    var firstName = ""
    var lastName = ""
    var organizationName = ""
    var forcePasswordReset = false
    var roleCode = ""
    var roleName = ""
    var username = "" // email
    var naSmilesNumber = ""

    var fullName : String {
        return firstName + " " + lastName
    }

    func copyFrom(_ object:FDMemberLogin) {
        customerId = object.customerId
        firstName = object.firstName
        lastName = object.lastName
        organizationName = object.organizationName
        forcePasswordReset = object.forcePasswordReset
        roleName = object.roleName
        roleCode = object.roleCode
        username = object.username
    }

}
