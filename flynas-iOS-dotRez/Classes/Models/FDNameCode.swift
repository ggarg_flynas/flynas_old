//
//  FDNameCode.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDNameCode: NSObject, NSCoding {
    var code = ""
    var name = ""
    var des = ""

    override init () {
        super.init()
    }

    init(name: String, code: String) {
        self.code = code
        self.name = name
        self.des = ""
    }

    init(name: String, code: String, des: String) {
        self.code = code
        self.name = name
        self.des = des
    }

    required convenience internal init?(coder decoder: NSCoder) {
        self.init()
        self.name = decoder.decodeObject(forKey: "name") as! String
        self.code = decoder.decodeObject(forKey: "code") as! String
        self.des = decoder.decodeObject(forKey: "des") as! String
    }

    internal func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
        coder.encode(self.code, forKey: "code")
        coder.encode(self.des, forKey: "des")
    }
}
