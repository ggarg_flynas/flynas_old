//
//  FDContactItemInfo.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDContactInput: NSObject {
    var contactItemInfo = FDContactItemInfo()
}

class FDContactItemInfo: NSObject, NSCoding {
    var emailAddress = ""
    var mobilePhone = FDPhone()
    var workPhone = FDPhone()

    var emailAddressConfirm : String = ""

    override init () {
        super.init()
        mobilePhone.code = "SA"
        workPhone.code = "SA"
    }

    required convenience init?(coder decoder: NSCoder) {
        self.init()
        self.emailAddress = decoder.decodeObject(forKey: "emailAddress") as! String
        self.mobilePhone = decoder.decodeObject(forKey: "mobilePhone") as! FDPhone
        self.workPhone = decoder.decodeObject(forKey: "workPhone") as! FDPhone
        if let e = decoder.decodeObject(forKey: "emailAddressConfirm") {
            self.emailAddressConfirm = e as! String
        } else {
            self.emailAddressConfirm = ""
        }
    }

    func encode(with coder: NSCoder) {
        coder.encode(self.emailAddress, forKey: "emailAddress")
        coder.encode(self.mobilePhone, forKey: "mobilePhone")
        coder.encode(self.workPhone, forKey: "workPhone")
        coder.encode(self.emailAddressConfirm, forKey: "emailAddressConfirm")

    }

    var validateAll : ValidateErrorCode {

        if emailAddress.isEmpty {
            return .requiredFieldMissing
        }

        let validator = SHEmailValidator()
        do {
            try validator.validateSyntax(ofEmailAddress: emailAddress)
        } catch {
            return .emailNotValidate
        }

        if emailAddressConfirm != emailAddress {
            return .emailNotMuch
        }

        if mobilePhone.number.isEmpty {
            return .mobilePhoneNumberMissing
        }

        if mobilePhone.code.isEmpty {
            return .mobilePhoneCodeMissing
        }

        return .none
    }

    var contactInput : [String:Any] {
        return ["contactItemInfo":
            [
                "mobilePhone": mobilePhone.phoneObject,
                "workPhone": workPhone.phoneObject,
                "emailAddress": emailAddress
            ]]
    }

    func copyContact(_ contact:FDContactItemInfo) {
        self.emailAddress = contact.emailAddress

        self.mobilePhone.copyFrom(contact.mobilePhone)
        self.workPhone.copyFrom(contact.workPhone)

    }
}
