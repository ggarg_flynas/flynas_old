//
//  FDLoginManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

let UserEmailDefaultsName = "UserEmailDefaultsName"
let UserPassDefaultsName = "UserPassDefaultsName"
let UserNameDefaultsName = "UserNameDefaultsName"

class FDLoginManager: FDCommonApiManager {

  static let sharedInstance = FDLoginManager()

    var userEmail : String?
    var password : String?
    var forcePasswordReset = false
//    var memberLogin = FDMemberLogin()
    var memberProfile = FDMemberProfile()
    var userName : String?
    var lastAutoLoginTime : Date?
    var naSmilesNumber = ""

    override init() {
        super.init()
        userEmail = UserDefaults.standard.object(forKey: UserEmailDefaultsName) as? String
        password = UserDefaults.standard.object(forKey: UserPassDefaultsName) as? String
        userName = UserDefaults.standard.object(forKey: UserNameDefaultsName) as? String

    }

    var hasSavedUserCredential : Bool {
        return userEmail != nil && !userEmail!.isEmpty && password != nil && !password!.isEmpty
    }

    var hasNaSmiles : Bool {
        return !naSmilesNumber.isEmpty || FDNaSmileManager.sharedInstance.isNaSmileLogin || (hasSavedUserCredential && FDNaSmileManager.sharedInstance.isMemberNaSmileLogin)
    }

    func saveUserCredential() {
        UserDefaults.standard.setValue(userEmail, forKey: UserEmailDefaultsName)
        UserDefaults.standard.setValue(password, forKey: UserPassDefaultsName)
        UserDefaults.standard.setValue(self.userName, forKey: UserNameDefaultsName)
    }


    // workaround to force update MMB history
    func forceLoginAgain (_ success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {
        if hasSavedUserCredential {
            FDResourceManager.sharedInstance.deletSession({ 
                self.autoLoginUser(success, fail: fail)
                }, fail: { (operation, error) in
                    self.autoLoginUser(success, fail: fail)
            })
        } else {
            success(FDMemberLogin())
        }
    }

    func shouldAutoLoginUserAgain () -> Bool {
        guard let lastAutoLoginTime = lastAutoLoginTime else {
            return true
        }

        return Date().minutesAfterDate(lastAutoLoginTime) > 10
    }

    func clearAutoLoginTime () {
        lastAutoLoginTime = nil
    }

    func autoLoginUser (_ success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {
        if hasSavedUserCredential && shouldAutoLoginUserAgain() {
            lastAutoLoginTime = Date()
            loginUser(userEmail, password: password, forceLogin: false, success: success, fail: fail)
        } else {
            success(FDMemberLogin())
        }
    }

    func loginUser (_ userEmail:String?, password:String?, forceLogin:Bool, success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
            let results = loadLocalJsonAsResponse(FDdotRezAPIManager.operationWciBooking(pnr, departureStation: departureStation), fileName: "MemberLogin")

            if  let wciBooking: FDWciBooking = self.getResultObject(results.array()) {
                success(wciBooking: wciBooking)
            }else {
                fail(FDdotRezAPIManager.API_EMPTY_RES)
            }
        #else
            FDResourceManager.sharedInstance.reactiveToken(false, success: { (memberLogin) -> Void in

                if memberLogin.username != self.userEmail || forceLogin {
                    var parameters:[AnyHashable: Any]? = nil
                    if userEmail != nil && password != nil {
                        parameters = [
                            "memberLogin" : [
                                "userName": userEmail!,
                                "password": password!
                            ]
                        ]
                    }

                    self.naSmilesNumber = ""

                    let operation = FDdotRezAPIManager.operationMemberLogin(parameters)

                    operation.setCompletionBlockWithSuccess({ (operation, results) in



                      if  let memberLogin: FDMemberLogin = self.getResultObject(results?.array()) {

                            self.clearAutoLoginTime()
                            FDResourceManager.sharedInstance.isSessionLogined = true
                            self.userEmail = userEmail
                            self.password = password
                            self.userName = memberLogin.fullName
                            self.forcePasswordReset = memberLogin.forcePasswordReset
                            self.naSmilesNumber = memberLogin.naSmilesNumber

                            self.saveUserCredential()

                        self.saveJson((operation?.httpRequestOperation.responseString)!, resourceType: .MemberLogin, language: nil)

    //                        self.memberLogin = memberLogin
                        success(memberLogin)

                            // update profile after login
                            self.retrieveMemberProfile(nil, success: { (memberProfile:FDMemberProfile) -> Void in
                            }) { (r) -> Void in
                            }
                        } else {
                            fail(FDErrorHandle.API_EMPTY_RES)
                        }

                        }, failure: { (operation, error) in
                            self.handleError(operation!, error: error! as NSError, fail: fail)
                    })
                    operation.start()
                } else {

                    self.naSmilesNumber = memberLogin.naSmilesNumber
                  success(memberLogin)
                }
                
                }) { (r) -> Void in
                    fail(r)
            }
            
        #endif
        
    }

    func clearUserNamePass() {
        self.userEmail = nil
        self.password = nil
        self.userName = nil
        self.forcePasswordReset = false
        self.naSmilesNumber = ""
        UserDefaults.standard.removeObject(forKey: UserEmailDefaultsName)
        UserDefaults.standard.removeObject(forKey: UserPassDefaultsName)
        UserDefaults.standard.removeObject(forKey: UserNameDefaultsName)

        clearAutoLoginTime ()

        if FDNaSmileManager.sharedInstance.password == nil {
            FDNaSmileManager.sharedInstance.clearNaSmileObject()
        }
    }

    func logoutUser (_ success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
        self.memberProfile = FDMemberProfile()
        self.clearUserNamePass()

        if FDNaSmileManager.sharedInstance.password == nil {
            FDNaSmileManager.sharedInstance.clearNaSmileObject()
        }

//        let operation = FDdotRezAPIManager.operationApiSessionDelete()
//        operation.setCompletionBlockWithSuccess({ (operation, results) in
//            success()
//            }, failure: { (operation, error) in
//                self.handleError(operation, error: error, fail: fail)
//        })
//        operation.start()

        FDResourceManager.sharedInstance.deletSession({
            success()
            }, fail: { (operation, error) in
                self.handleError(operation, error: error, fail: fail)
        })
    }

    func retrieveMemberProfile (_ parameters:[AnyHashable: Any]?, success:@escaping (_ memberProfile:FDMemberProfile) -> Void, fail: @escaping (String?) -> Void) {

        if hasSavedUserCredential {
            FDResourceManager.sharedInstance.checkSessionToken({ (memberLogin) -> Void in
//                if memberLogin.username == self.userEmail {
                if self.userEmail != nil && memberLogin.username.caseInsensitiveCompare(self.userEmail!) == ComparisonResult.orderedSame {

                    let operation = FDdotRezAPIManager.operationMemberProfile(parameters)
                    operation.setCompletionBlockWithSuccess({ (operation, results) in
                        let oldProfile = self.memberProfile
                        self.memberProfile = FDMemberProfile()
                      if  let p: FDMemberProfile.ProfileDetails = self.getResultObject(results?.array()) {
                            self.memberProfile.profileDetails = p
                            p.details.naSmilesLoyalty.firstName = p.details.firstName
                            p.details.naSmilesLoyalty.title = p.details.title
                            p.details.naSmilesLoyalty.lastName = p.details.lastName
                            p.details.nasmiles = p.details.naSmilesLoyalty.nasLoyaltyNumber
                            p.details.naSmilesLoyalty.email = memberLogin.username
                            FDNaSmileManager.sharedInstance.setNaSmileObject(p.details.naSmilesLoyalty)
                        }

                      if  let p: FDMemberProfile.ProfileTravelDocument = self.getResultObject(results?.array()) {
                            if p.documents.count > 0 {
                                self.memberProfile.profileDetails.details.travelDocument = p.documents[0]
                            }
                        }

                      if  let p: FDMemberProfile.ProfileAddresses = self.getResultObject(results?.array()) {
                            self.memberProfile.profileAddresses = p
                        }

                      if  let p: FDMemberProfile.ProfilePhones = self.getResultObject(results?.array()) {
                            for phone in p.phones {
                                phone.parsePhoneNumber()
                            }
                            self.memberProfile.profilePhones = p
                        }

                      if  let p: FDMemberProfile.MemberFamily = self.getResultObject(results?.array()) {
                            self.memberProfile.memberFamily = p
                        }

                      if  let p: FDMemberProfile.CustomerBookings = self.getResultObject(results?.array()) {
                            p.currentBookings.sort(by: { (first, second) -> Bool in
                                if let d1 = first.sta {
                                    if let d2 = second.sta {
                                        return d1.isEarlierThanDate(d2)
                                    }
                                }
                                return false
                            })

                            p.pastBookings.sort(by: { (first, second) -> Bool in
                                if let d1 = first.sta {
                                    if let d2 = second.sta {
                                        return d1.isLaterThanDate(d2)
                                    }
                                }
                                return false
                            })
                            self.memberProfile.customerBookings = p
                        }

                        self.memberProfile.copyHijiriFlagFrom(oldProfile)

                        self.syncSelectPassengers()

                      success(self.memberProfile)
                        }, failure: { (operation, error) in
                            self.handleError(operation!, error: error! as NSError, fail: fail)
                    })
                    operation.start()
                } else {
                    fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
                }
                
                }) { (r) -> Void in
                    fail(r)
            }
        } else {
            fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
        }

    }

    func retrieveMemberFamily (_ parameters:[AnyHashable: Any]?, success:@escaping (_ memberProfile:FDMemberProfile) -> Void, fail: @escaping (String?) -> Void) {

        if hasSavedUserCredential {
            FDResourceManager.sharedInstance.checkSessionToken({ (memberLogin) -> Void in
                //                if memberLogin.username == self.userEmail {
                if self.userEmail != nil && memberLogin.username.caseInsensitiveCompare(self.userEmail!) == ComparisonResult.orderedSame {

                    let operation = FDdotRezAPIManager.operationMemberFamily(parameters)
                    operation.setCompletionBlockWithSuccess({ (operation, results) in
                        let oldProfile = self.memberProfile

                      if  let p: FDMemberProfile.MemberFamily = self.getResultObject(results?.array()) {
                            self.memberProfile.memberFamily = p
                        }
                        self.memberProfile.copyHijiriFlagFrom(oldProfile)

                        self.syncSelectPassengers()

                      success(self.memberProfile)
                        }, failure: { (operation, error) in
                            self.handleError(operation!, error: error! as NSError, fail: fail)
                    })
                    operation.start()
                } else {
                    fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
                }
                
            }) { (r) -> Void in
                fail(r)
            }
        } else {
            fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
        }
        
    }

    func memberNaSmileNumberJoin(_ pass:String, success:@escaping (_ message:String) -> Void, fail: @escaping (String?) -> Void) {
        if hasSavedUserCredential {
            FDResourceManager.sharedInstance.checkSessionToken({ (memberLogin) -> Void in
                let details = self.memberProfile.getProfileDetailsObject(unwrap(str: self.userEmail))
                let profilePhones = self.memberProfile.getProfilePhonesObject()
                let memberLoyaltyJoin = [
                    "password":pass,
                    "trackerIdentifier":"JoinMyProfileIbe"
                ]
                let parameters:[AnyHashable: Any] = [
                    "profileDetails":details,
                    "profilePhones":profilePhones,
                    "memberLoyaltyJoin":memberLoyaltyJoin
                ]
                let operation = FDdotRezAPIManager.operationMemberLoyaltyJoin(parameters)
                operation.setCompletionBlockWithSuccess({ (operation, results) in
                  if  let p: FDMemberLoyalty = self.getResultObject(results?.array()) {
                    success(p.message)
                    } else {
                        fail(FDErrorHandle.API_EMPTY_RES)
                    }
                    }, failure: { (operation, error) in
                        self.handleError(operation!, error: error! as NSError, fail: fail)
                })
                operation.start()
            }) { (r) -> Void in
                fail(r)
            }
        } else {
            fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
        }
    }

    func memberNaSmileNumberRetrieve (_ success:@escaping (_ naSmileID:String) -> Void, fail: @escaping (String?) -> Void) {
        if hasSavedUserCredential {
            FDResourceManager.sharedInstance.checkSessionToken({ (memberLogin) -> Void in
                let operation = FDdotRezAPIManager.operationMemberLoyalty("RetrieveNasmileMyProfileApp")
                operation.setCompletionBlockWithSuccess({ (operation, results) in
                  if  let p: FDMemberLoyalty = self.getResultObject(results?.array()) {
                    success(p.nasLoyaltyNumber)
                    } else {
                        fail(FDErrorHandle.API_EMPTY_RES)
                    }
                    }, failure: { (operation, error) in
                        self.handleError(operation!, error: error! as NSError, fail: fail)
                })
                operation.start()
            }) { (r) -> Void in
                fail(r)
            }
        } else {
            fail(FDErrorHandle.API_UNAUTHORIZED_ERROR)
        }
    }

    func syncSelectPassengers() {
        if FDResourceManager.sharedInstance.isSessionLogined {
            var passengers = [FDPassenger]()

            passengers.append(FDPassenger(passenger: memberProfile.profileDetails.details))

            for ps in memberProfile.memberFamily.members {
                passengers.append(FDPassenger(passenger: ps))
            }
            FDResourceManager.sharedInstance.saveRecentPassengers(passengers)

            let contactInfo = FDContactItemInfo()
            contactInfo.emailAddress = self.userEmail!
            for p in memberProfile.profilePhones.phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    contactInfo.mobilePhone.copyFrom(p)
                } else if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    contactInfo.workPhone.copyFrom(p)
                }
            }
            FDResourceManager.sharedInstance.saveRecentContactInfo(contactInfo)
        }
    }

    func registerUser (_ userEmail:String, password:String, parameters:[AnyHashable: Any], success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {


            FDResourceManager.sharedInstance.reactiveToken(false, success: { (_) -> Void in
                    let operation = FDdotRezAPIManager.operationMemberRegister(parameters)

                    operation.setCompletionBlockWithSuccess({ (operation, results) in
                        self.loginUser(userEmail, password: password, forceLogin:true, success: success, fail: fail)
                        }, failure: { (operation, error) in
                            self.handleError(operation!, error: error! as NSError, fail: fail)
                    })
                    operation.start()
                }) { (r) -> Void in
                    fail(r)
                }

        
    }

    func forgetPassword (_ memberEmail:String, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {

        FDResourceManager.sharedInstance.reactiveToken(false, success: { (_) -> Void in
            let operation = FDdotRezAPIManager.operationMemberPasswordForgot(memberEmail)

            operation.setCompletionBlockWithSuccess({ (operation, results) in
                success()
                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()
        }) { (r) -> Void in
            fail(r)
        }
    }

    func memberPasswordReset (_ userName:String, oldPassword:String, password:String, success:@escaping (_ memberLogin: FDMemberLogin) -> Void, fail: @escaping (String?) -> Void) {

        FDResourceManager.sharedInstance.reactiveToken(false, success: { (_) -> Void in

            let operation = FDdotRezAPIManager.operationMemberPasswordReset(userName, oldPassword: oldPassword, password: password)

            operation.setCompletionBlockWithSuccess({ (operation, results) in

              if  let memberLogin: FDMemberLogin = self.getResultObject(results?.array()) {
                    self.userEmail = userName
                    self.password = password
                    self.userName = memberLogin.fullName

                    self.saveUserCredential()
                    self.forcePasswordReset = memberLogin.forcePasswordReset
                    FDResourceManager.sharedInstance.isSessionLogined = true

                success(memberLogin)
                }

                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()

        }) { (r) -> Void in
            fail(r)
        }
    }
}






