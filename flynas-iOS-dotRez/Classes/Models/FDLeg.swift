//
//  FDLeg.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 1/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import RealmSwift

class FDInventoryLegKey: Object {
    dynamic var arrivalStation = ""
    dynamic var carrierCode = ""
    dynamic var departureDate : Date?
    dynamic var departureStation = ""
    dynamic var flightNumber = ""
    dynamic var opSuffix = ""
}

class FDLeg: Object {
    dynamic var destination = ""
    dynamic var carrierCode = ""

    dynamic var flightNumber = ""
    dynamic var origin = ""

    dynamic var departureDate: Date?
    dynamic var arrivalDate: Date?

    dynamic var utcArrivalDate: Date?
    dynamic var utcDepartureDate: Date?

    // wci
    dynamic var seatColumn = ""
    dynamic var seatRow = 0
    dynamic var boardingSequence = 0


    dynamic var inventoryLegKey : FDInventoryLegKey?
}
