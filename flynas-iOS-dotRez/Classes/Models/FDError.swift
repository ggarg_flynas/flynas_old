//
//  FDError.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 26/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDError: NSObject {

    var errorCode = ""  // InvalidPaxType,
    var errorType = ""  // 1347
    var severity = ""   // Error
    var errorMessage = ""

    var errorString : String {
        return errorMessage
    }

}
