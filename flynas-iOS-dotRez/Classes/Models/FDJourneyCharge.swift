//
//  FDJourneyCharge.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDJourneyCharge: NSObject {

    var destination: String?
    var origin: String?
    var flightNumber: String?
    var numberOfStops: NSNumber = 0

    var sta: Date?
    var std: Date?
    var utcSta: Date?
    var utcStd: Date?

    var paxCharges = [FDPaxCharge]()
    var serviceCharges = [FDServiceCharge]()
    var seatCharges = [FDSeatCharges]()
    var ssrCharges = [FDSsrCharges]()

    func getLoungeCharge() -> Float {
        var total:Float = 0
        for s in ssrCharges {
            if s.ssrType == FDSsrChargesType.Lounge.rawValue {
                total += s.amount.floatValue * s.count.floatValue
            }
        }

        return total
    }
}

class FDSeatCharges: NSObject {
    var seatNumber: String = ""
    var amount : NSNumber = 0
    var count: NSNumber = 0
  var chargeType = ""
  var feeCode = ""
}


