//
//  FDResourceStation.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 17/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit

open class FDResourceStation : NSObject, NSCoding {

    var code : String = ""
    var country : String?
    var latitude : String? {
        didSet {
            if let latitude = latitude,
                let ds = latitude.substringToIndex(2),
                let ms = latitude.substringFromIndex(2, toIndex: 4),
                let ss = latitude.substringFromIndex(4, toIndex: 6),
                let d = Double(ds),
                let m = Double(ms),
                let s = Double(ss),
                let direction = latitude.substringFromIndex(6, toIndex: 7)
            {


                lat = d + m / 60 + s / 3600

                if direction == "S" {
                    lat = -1 * lat
                }
            } else {
                lat = 0
            }
        }
    }
    var longtitude : String? {
        didSet {
            if let longtitude = longtitude,
                let ds = longtitude.substringToIndex(3),
                let ms = longtitude.substringFromIndex(3, toIndex: 5),
                let ss = longtitude.substringFromIndex(5, toIndex: 7),
                let d = Double(ds),
                let m = Double(ms),
                let s = Double(ss),
                let direction = longtitude.substringFromIndex(7, toIndex: 8)
            {

                lon = d + m / 60 + s / 3600
                if direction == "W" {
                    lon = -1 * lon
                }
            } else {
                lon = 0
            }
        }
    }
    var name : String = ""

    var markets = [FDResourceStation]()


    // cache 
    var lat : Double = 0
    var lon : Double = 0


    override init () {
        super.init()
    }

//    init(_ dic : NSDictionary) {
//        self.code = dic["code"] as! String
//        self.country = dic["country"] as? String
//        self.latitude = dic["latitude"] as? String
//        self.longtitude = dic["longtitude"] as? String
//        self.name = dic["name"] as? String
//    }

    required convenience public init?(coder decoder: NSCoder) {
        self.init()
        self.code = decoder.decodeObject(forKey: "code") as! String
        self.country = decoder.decodeObject(forKey: "country") as? String
        self.latitude = decoder.decodeObject(forKey: "latitude") as? String
        self.longtitude = decoder.decodeObject(forKey: "longtitude") as? String
        self.name = decoder.decodeObject(forKey: "name") as! String
        let savedMarkets = decoder.decodeObject(forKey: "markets") as? [FDResourceStation]
        if savedMarkets != nil {
        }
    }

    open func encode(with coder: NSCoder) {
        coder.encode(self.code, forKey: "code")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.country, forKey: "country")
        coder.encode(self.latitude, forKey: "latitude")
        coder.encode(self.longtitude, forKey: "longtitude")
    }

    open func distanceFrom(_ lat: Double, lon: Double) -> Double {
        return FDDistance(lat, lon1: lon, lat2: self.lat, lon2: self.lon, unit: "K")
    }
}
