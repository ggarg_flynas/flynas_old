//
//  FDSettings.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//


let FDColorFlynasGreen = UIColor(red: 35/255.0, green: 188/255.0, blue: 185/255.0, alpha: 1.0)
let FDColorFlynasGreenText = UIColor(red: 0/255.0, green: 187/255.0, blue: 178/255.0, alpha: 1.0)
let FDColorFlynasBlack = UIColor(red: 25.0/255.0, green: 25.0/255.0, blue: 25.0/255.0, alpha: 1.0)
