//
//  FDTrip.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 1/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDTrip: NSObject {
    var origin : String?
    var destination : String?
    var date : Date?
    var flights : [FDFlight] = [] {
        didSet {
            if flights.count == 1 {
                expandedIndexPath = 0
            }
        }
    }

    var expandedIndexPath = -1
}
