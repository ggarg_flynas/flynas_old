//
//  SearchItem.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 23/2/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

class SearchItem: NSObject, NSCoding {
  
  static let kMMBSearchDefaults = "kMMBSearchDefaults"
  
  let pnr: String
  let lastname: String
  let origin: String
  let destination: String
  let departDate: Date
  
  init(pnr: String, lastname: String, origin: String, destination: String, departDate: Date) {
    self.pnr = pnr
    self.lastname = lastname
    self.origin = origin
    self.destination = destination
    self.departDate = departDate
  }

  required init?(coder aDecoder: NSCoder) {
    self.pnr = unwrap(str: aDecoder.decodeObject(forKey: "kSearchItemPnr"))
    self.lastname = unwrap(str: aDecoder.decodeObject(forKey: "kSearchItemLastname"))
    self.origin = unwrap(str: aDecoder.decodeObject(forKey: "kSearchItemOrigin"))
    self.destination = unwrap(str: aDecoder.decodeObject(forKey: "kSearchItemDestination"))
    self.departDate = aDecoder.decodeObject(forKey: "kSearchItemDepartDate") as! Date
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(pnr, forKey: "kSearchItemPnr")
    aCoder.encode(lastname, forKey: "kSearchItemLastname")
    aCoder.encode(origin, forKey: "kSearchItemOrigin")
    aCoder.encode(destination, forKey: "kSearchItemDestination")
    aCoder.encode(departDate, forKey: "kSearchItemDepartDate")
  }
  
}

extension SearchItem {
  
  class func getUpcomingFlights() -> [SearchItem] {
    var itemsToReturn: [SearchItem] = []
    let items = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
    for item in items {
      if item.departDate.compare(Date()) == .orderedDescending || item.departDate.compare(Date()) == .orderedSame {
        itemsToReturn.append(item)
      }
    }
    return itemsToReturn
  }
  
  class func getPastFlights() -> [SearchItem] {
    var itemsToReturn: [SearchItem] = []
    let items = ArchiveManager.retrieve(SearchItem.kMMBSearchDefaults) as! [SearchItem]
    for item in items {
      if item.departDate.compare(Date()) == .orderedAscending {
        itemsToReturn.append(item)
      }
    }
    return itemsToReturn
  }
  
}
