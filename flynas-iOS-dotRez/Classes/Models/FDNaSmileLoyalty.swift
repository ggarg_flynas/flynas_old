//
//  FDNaSmileLoyalty.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/05/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation
import RealmSwift

class FDNaSmileLoyalty: Object {

    let convertFactor = 25 // 25 Point = 1 SAR

    dynamic var balancePoints = 0
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var loyaltyExpiryDate : Date?
    dynamic var loyaltyRedeemAmount = 0
    dynamic var nasLoyaltyNumber = ""
    dynamic var originalBookingInPoints = false
    dynamic var rankLevel = ""
    dynamic var title = ""
    dynamic var email = ""
    dynamic var nasmilesBarcodePayload = ""
    dynamic var minimumLoyaltyThreshold : Int = 100  // point


    func copyFrom(_ o : FDNaSmileLoyalty) {
        self.balancePoints = o.balancePoints
        self.firstName = o.firstName
        self.lastName = o.lastName
        self.loyaltyExpiryDate = o.loyaltyExpiryDate
        self.loyaltyRedeemAmount = o.loyaltyRedeemAmount
        self.nasLoyaltyNumber = o.nasLoyaltyNumber
        self.originalBookingInPoints = o.originalBookingInPoints
        self.rankLevel = o.rankLevel
        self.title = o.title
        self.email = o.email
        self.nasmilesBarcodePayload = o.nasmilesBarcodePayload
        self.minimumLoyaltyThreshold = o.minimumLoyaltyThreshold
    }

    var fullName : String {
        return (firstName + " " + lastName).capitalized
    }

    var formatedNasLoyaltyNumber : String {
        if nasLoyaltyNumber.isEmpty {
            return ""
        }

        var formatedCardNumber = nasLoyaltyNumber
        formatedCardNumber.insert(" ", at: formatedCardNumber.characters.index(formatedCardNumber.startIndex, offsetBy: 7))
        formatedCardNumber.insert(" ", at: formatedCardNumber.characters.index(formatedCardNumber.startIndex, offsetBy: 3))
        return formatedCardNumber
    }

    var tier : String {
        switch rankLevel {
        case "Platinium":
            return FDLocalized("PLATINUM")
        case "Gold":
            return FDLocalized("GOLD")
        case "Silver":
            return FDLocalized("SILVER")
        case "Bronze":
            return FDLocalized("BRONZE")
        default:
            return FDLocalized("BRONZE")
        }
    }

    var tierImage : String {
//        let rankLevel = "SLVR"
        switch rankLevel {
        case "Platinium":
            return "img-naSmileCard-Platinum"
        case "Gold":
            return "img-naSmileCard-Gold"
        case "Silver":
            return "img-naSmileCard-Silver"
        case "Bronze":
            return "img-naSmileCard-Bronze"
        default:
            return "img-naSmileCard-Bronze"
        }
    }

    var formatedExpiryDate : String {
        if let loyaltyExpiryDate = loyaltyExpiryDate {
            return loyaltyExpiryDate.toString(format: .custom("MM/yy"))
        } else {
            return "--/--"
        }

    }

    var maxRedeemablePoint : Int {
        // round to up 100
        let redeemableAmount = Int(ceilf(Float(loyaltyRedeemAmount * convertFactor) / 100) * 100)
        return min (redeemableAmount, balancePoints)
    }

    var minRedeemablePoint : Int {
        // round to up 100
        return minimumLoyaltyThreshold
    }

    func getValue(_ point: Int) -> Float {
        return Float(point / convertFactor)
    }

    func getBarcodePayload() -> String {
        if nasmilesBarcodePayload.isEmpty {
            return firstName + "|" + lastName + "|" + nasLoyaltyNumber + "|" + email + "|" + rankLevel
        } else {
            return nasmilesBarcodePayload
        }
    }

}

class FDMemberLoyalty : NSObject {

    var updateSuccess = false
    var nasLoyaltyNumber = ""
    var message = ""

}
