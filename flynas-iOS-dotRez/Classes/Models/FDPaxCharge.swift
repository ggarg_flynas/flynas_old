//
//  FDPaxCharge.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPaxCharge: NSObject {
    var fareType: String = ""
    var paxType: String = ""
    var paxCount: NSNumber = 0
    var paxFareAmount: NSNumber = 0
    var paxFareTotalDiscountAmount: NSNumber = 0
    var totalAmount: NSNumber = 0
  var bundleCode = ""
  var bundlePrice: NSNumber = 0

    func getNameValue() -> (FDTripSummaryView.Flight.FeeList) {
        let currencySymbol = FDResourceManager.sharedInstance.selectedCurrencySymbol
        var paxString = paxType
        if isRTL() {
            switch paxType {
                case "ADT":
                    paxString = FDLocalized("ADT")
                break
            case "CHD":
                paxString = FDLocalized("CHD")
                break
            case "INF":
                paxString = FDLocalized("INF")
                break
            default:
                break
            }
        }
      let feeList = FDTripSummaryView.Flight.FeeList(feeName: FDLocalizeInt(paxCount.intValue) + "x " + paxString,
                                                     feeValue: currencySymbol + " " + FDUtility.formatCurrency(totalAmount)!,
                                                     hasTax: false,
                                                     isTax: false,
                                                     expanded: false)
        return feeList
    }

    func getNameAmountValue() -> (String,Int,Float) {
        var paxString = paxType
        if isRTL() {
            switch paxType {
            case "ADT":
                paxString = FDLocalized("ADT")
                break
            case "CHD":
                paxString = FDLocalized("CHD")
                break
            case "INF":
                paxString = FDLocalized("INF")
                break
            default:
                break
            }
        }
        return (paxString, paxCount.intValue, totalAmount.floatValue)
    }

}
