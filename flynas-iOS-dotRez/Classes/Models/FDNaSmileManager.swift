//
//  FDNaSmileManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/05/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation
import RealmSwift

let NasMemberAccountIdDefaultsName = "NasMemberAccountIdDefaultsName"
let NaSmilePassDefaultsName = "NaSmilePassDefaultsName"


class FDNaSmileManager: FDCommonApiManager {
  
  static let sharedInstance = FDNaSmileManager()

    var nasMemberAccountId : String?
    var password : String?
    var memberLogin : FDNaSmileLoyalty?
    var lastLoginTime : Date?

    var isNaSmileLogin : Bool {
        guard memberLogin != nil else {return false}

        if password == nil {
            return false
        }

        guard let lastLoginTime = lastLoginTime, lastLoginTime.dateByAddingHours(24).isLaterThanDate(Date()) else {return false}

        return true
    }

    var isMemberNaSmileLogin : Bool {
        return memberLogin != nil && password == nil && !memberLogin!.nasLoyaltyNumber.isEmpty
    }

    var isSessionLogined = false

    override init() {
        super.init()
        nasMemberAccountId = UserDefaults.standard.object(forKey: NasMemberAccountIdDefaultsName) as? String
//        password = NSUserDefaults.standardUserDefaults().objectForKey(NaSmilePassDefaultsName) as? String

        if let naSmileObject = loaodNaSmileObject (){
            memberLogin = naSmileObject
        }
    }

    func manualLogin(_ userEmail:String?, password:String?, accountInfo: Bool = true, success:@escaping (_ memberLogin: FDNaSmileLoyalty) -> Void, fail: @escaping (String?) -> Void) {
        loginUser(userEmail, password: password, accountInfo: accountInfo, success: { (memberLogin) in
            self.lastLoginTime = Date()
            success(memberLogin)
            }, fail: { (error) in
                fail(error)
        })

    }

    func autoLogin(_ success:@escaping (_ memberLogin: FDNaSmileLoyalty) -> Void, fail: @escaping (String?) -> Void) -> Bool {
        if isNaSmileLogin {
            if let nasMemberAccountId = nasMemberAccountId , let password = password {
                loginUser(nasMemberAccountId, password: password, success: { (memberLogin) in
                    success(memberLogin)
                    }, fail: { (error) in
                        fail(error)
                })

                return true
            } else if FDLoginManager.sharedInstance.hasSavedUserCredential {
                FDLoginManager.sharedInstance.retrieveMemberProfile (nil, success: { (memberProfile) in
                  success(memberProfile.profileDetails.details.naSmilesLoyalty)
                    }, fail: { (error) in
                        fail(error)
                })

                return true
            }
        }

        if nil != password {
            self.logoutUser({}, fail: { (_) in })
        }

        return false
    }

    fileprivate func loginUser (_ userEmail:String?, password:String?, accountInfo: Bool = true, success:@escaping (_ memberLogin: FDNaSmileLoyalty) -> Void, fail: @escaping (String?) -> Void) {

        if let userEmail = userEmail , let password = password {
            FDResourceManager.sharedInstance.reactiveToken(false, success: { (memberLogin) -> Void in

                self.isSessionLogined = false
                let operation = FDdotRezAPIManager.operationLoyalty(userEmail, password: password, accountInfo: accountInfo)

                operation.setCompletionBlockWithSuccess({ (operation, results) in

                  if  let memberLogin: FDNaSmileLoyalty = self.getResultObject(results?.array()) {
                        self.nasMemberAccountId = userEmail
                        self.password = password
                        self.memberLogin = memberLogin

                        UserDefaults.standard.setValue(userEmail, forKey: NasMemberAccountIdDefaultsName)
//                        NSUserDefaults.standardUserDefaults().setValue(password, forKey: NaSmilePassDefaultsName)

                        self.isSessionLogined = true

                        self.saveNaSmileObject(memberLogin)

                    success(memberLogin)
                    } else {
                        fail(FDErrorHandle.API_EMPTY_RES)
                        self.logoutUser({
                            }, fail: { (_) in
                        })
                    }

                    }, failure: { (operation, error) in
                        self.logoutUser({
                            }, fail: { (_) in
                        })
                        self.handleError(operation!, error: error! as NSError, fail: fail)
                })
                operation.start()
            }) { (r) -> Void in
                fail(r)
                self.logoutUser({
                    }, fail: { (_) in
                })
            }
        }
    }

    func logoutUser (_ success:() -> Void, fail: (String?) -> Void) {
        if self.password != nil {
            self.nasMemberAccountId = nil
            self.password = nil
            self.memberLogin = nil
            self.lastLoginTime = nil
            UserDefaults.standard.removeObject(forKey: UserEmailDefaultsName)
            UserDefaults.standard.removeObject(forKey: UserPassDefaultsName)

            self.saveNaSmileObject(nil)
        } else {
            // naSmiles info from member, can't log out here
        }

        success()
    }

    func forgetPassword (_ nasMemberAccountId:String, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
        let operation = FDdotRezAPIManager.operationSendLoyaltyPassword(nasMemberAccountId)

        operation.setCompletionBlockWithSuccess({ (operation, results) in
            success()
            }, failure: { (operation, error) in
                self.handleError(operation!, error: error! as NSError, fail: fail)
        })
        operation.start()
    }

    func clearNaSmileObject () {
        saveNaSmileObject(nil)
        self.memberLogin = nil
        self.password = nil
    }

    func saveNaSmileObject(_ naSmileLoyaltyObject:FDNaSmileLoyalty?) {

      let loyaltyResult = FDRealmManager.realm.objects(FDNaSmileLoyalty.self)
        if loyaltyResult.count > 0 {
            try! FDRealmManager.realm.write {
                FDRealmManager.realm.delete(loyaltyResult)
            }
        }

        if let naSmileLoyaltyObject = naSmileLoyaltyObject {
            let newObj = FDNaSmileLoyalty()
            newObj.copyFrom(naSmileLoyaltyObject)
            try! FDRealmManager.realm.write {
                FDRealmManager.realm.add(newObj)
            }
        }
    }

    func loaodNaSmileObject () -> FDNaSmileLoyalty? {
      let loyaltyResult = FDRealmManager.realm.objects(FDNaSmileLoyalty.self)
        if let first = loyaltyResult.first {
            let newObj = FDNaSmileLoyalty()
            newObj.copyFrom(first)
            return newObj
        }

        return nil
    }

    func setNaSmileObject(_ naSmileLoyaltyObject:FDNaSmileLoyalty) {

        if !naSmileLoyaltyObject.nasLoyaltyNumber.isEmpty {
            memberLogin = naSmileLoyaltyObject
            saveNaSmileObject(naSmileLoyaltyObject)
            password = nil
            lastLoginTime = nil
        }
    }


}
