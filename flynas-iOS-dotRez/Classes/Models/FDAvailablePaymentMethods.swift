//
//  FDAvailablePaymentMethods.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDAvailablePaymentMethods: NSObject {

    class FDPaymentMethod: NSObject {
        var code = ""
        var feeAmount: NSNumber = 0
        var name = ""
    }

    var balanceDue: NSNumber = 0
    var currencyCode = "SAR"
    var loyaltyBalanceDue: NSNumber = 0
    var methods = [FDPaymentMethod]()
    var totle: NSNumber = 0
    var totalPaymentAmount: NSNumber = 0
  var smilePointsRedeemable = 0
  var remainingAmount = 0.0

  func getSeatVat(_ userBooking: FDUserBookingInfo) -> (String, Double) {
    var tax: Double = 0.0
    for jCharge in userBooking.priceSummary.journeyCharges {
      for seat in jCharge.seatCharges {
        if seat.feeCode == "K7" {
          tax += seat.count.doubleValue * seat.amount.doubleValue
        }
      }
    }
    return ("\(currencyCode) \(unwrap(str: FDUtility.formatCurrency(tax as NSNumber)))", tax)
  }
  
  func getBalnceDue(inPoints: Bool) -> String {
    if inPoints {
      let strPoints = "\(smilePointsRedeemable) \(FDLocalized("points"))"
      let strSar = FDUtility.formatCurrencyWithCurrencySymbol(remainingAmount as NSNumber)
      return "\(strPoints) + \(unwrap(str: strSar))"
    }
    return "\(currencyCode) \(unwrap(str: FDUtility.formatCurrency(balanceDue)))"
  }
  
  func getKsaVatDue(_ vat: String) -> String {
    if let doubleVat = Double(vat) {
      return "\(currencyCode) \(unwrap(str: FDUtility.formatCurrency(doubleVat as NSNumber)))"
    }
    return "\(currencyCode) \(FDUtility.formatCurrency(0)!)"
  }
  
  func getSubtotal(_ vat: String) -> String {
    if let doubleVat = Double(vat) {
      let subtotal = max((Double(balanceDue) - doubleVat), 0)
      return "\(currencyCode) \(FDUtility.formatCurrency(subtotal as NSNumber)!)"
    }
    return "\(currencyCode) \(FDUtility.formatCurrency(Double(balanceDue) as NSNumber)!)"
  }

    var canPayByCreditCard : Bool {
        for method in methods {
            if method.code == "MC" || method.code == "VI" || method.code == "AX" {
                return true
            }
        }
        return false
    }

    var canPayBySADAD : Bool {
        for method in methods {
            if method.code == "SA" {
                return true
            }
        }
        return false
    }

    var canPayByNasCredit : Bool {
        for method in methods {
            if method.code == "CF" {
                return true
            }
        }
        return false
    }
  
  var canPayBySadadOLP : Bool {
    for method in methods {
      if method.code == "OP" {
        return true
      }
    }
    return false
  }

    var canPayByNaSmileRedemption : Bool {
        for method in methods {
            if method.code == "NS" {
                return true
            }
        }
        return false
    }
}
