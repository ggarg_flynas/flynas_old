//
//  FDFlight.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 1/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import RealmSwift

class FDFlight: Object {
    dynamic var origin : String?
    dynamic var destination : String?
    dynamic var flightNumber : String?
    dynamic var journeyKey : String?
    dynamic var numberOfStops = 1
    dynamic var operatedByFlightNumber = ""
    dynamic var operatedByText = ""

    dynamic var utcArrivalDate : Date?
    dynamic var utcDepartureDate : Date?
    dynamic var departureDate : Date?
    dynamic var arrivalDate : Date?

    var fares = List<FDFare>()
    var legs = List<FDLeg>()

    // wci
    var barcodes = List<FDBarCode>()

    // mmb
    dynamic var cancellationAllowed = true
    dynamic var changeAllowed = true

    var isCodeShare : Bool {
        return !operatedByFlightNumber.isEmpty
    }

}
