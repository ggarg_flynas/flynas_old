//
//  FDPhone.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum FDPhoneTypeCode : String {
    case Mobile = "H"
    case Home = "W"
//    case Fax = "F"
}

class FDPhone: NSObject, NSCoding {
    var code = "SA" {
        didSet {
            if code.isEmpty {
                code = "SA"
            }
        }
    }
    var number = ""

    var phone = ""
    //
    var idDeafult = false
    var typeCode = "" // M,F,H

    override init () {
        super.init()
    }

    required convenience init?(coder decoder: NSCoder) {
        self.init()
        self.code = decoder.decodeObject(forKey: "code") as! String
        self.number = decoder.decodeObject(forKey: "number") as! String
        self.phone = decoder.decodeObject(forKey: "phone") as! String
    }

    func encode(with coder: NSCoder) {
        coder.encode(self.code, forKey: "code")
        coder.encode(self.number, forKey: "number")
        coder.encode(self.phone, forKey: "phone")
    }

    func updatePhoneNumber () {
        if let c = FDRM.sharedInstance.countryPhoneNumberByCode (code) {
            phone = "+\(c) \(number)"
        } else {
            phone = number
        }
    }

    func parsePhoneNumber () {
        var countryNumber = ""

        do {
//            let string = "+123 5555"
            let re = try NSRegularExpression(pattern: "\\+(.+) (.+)", options: .caseInsensitive)
            let matches = re.matches(in: phone, options: [], range: NSRange(location: 0, length: phone.characters.count))

//            print("number of matches: \(matches.count)")

            if let match = matches.first {
                // range at index 0: full match
                // range at index 1: first capture group
                countryNumber = (phone as NSString).substring(with: match.rangeAt(1))
                number = (phone as NSString).substring(with: match.rangeAt(2))


                if let c = FDRM.sharedInstance.countryPhoneCodeByNumber (countryNumber) {
                    code = c
                } else {
                    code = "SA"
                }
            } else {
                code = "SA"
                number = phone
            }
        } catch {
            // regex was bad!
            code = "SA"
            number = phone
        }
    }


    var phoneObject : [String:AnyObject] {
        updatePhoneNumber ()
//        if number.isEmpty {
        if code.isEmpty {
            code = "SA"
        }
        return ["number":number as AnyObject,"phone":phone as AnyObject,"code":code as AnyObject]
//        } else {
//            return []
//        }
    }

    func copyFrom(_ phone:FDPhone) {
        self.number = phone.number
        self.code = phone.code
        self.phone = phone.phone
        self.typeCode = phone.typeCode
        self.idDeafult = phone.idDeafult
    }
}
