//
//  FDExtraItem.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 23/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDExtraItem: NSObject {
    var itemKey : String?
    var itemPrice : NSNumber = 0
    var itemName : String?

    var itemImgUrl : String?
    var itemType : String?

    override init() {
        super.init()
    }

    convenience init(name:String, code:String, imgUrl:String?) {
        self.init()
        itemName = name
        itemKey = code
        itemImgUrl = imgUrl
    }
}
