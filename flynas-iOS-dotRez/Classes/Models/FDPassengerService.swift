//
//  FDPassengerService.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPassengerService: NSObject {
    static let noSelected = "N/A"
  class FlightPartService: NSObject {
    class Service: NSObject {
      var code = "N/A"
      var count = 0
      var price: NSNumber = 0
      var remainingAmount = 0.0
      var smilePointsRedeemable = 0
    }
    var destination = ""
    var origin = ""
    var services = [Service]()
    var bundleCode: String?
    var serviceInBundle: NSArray?
    
    func getSelectedBaggageItem() -> [Service] {
      var items: [Service] = []
      for item in services {
        if item.count > 0 {
          items.append(item)
        }
      }
      return items
    }
    
    func getSelectedMealItem() -> Service? {
      for item in services {
        if item.count > 0 {
          return item
        }
      }
      return nil
    }
    
    func getSelectedSportsItems() -> [Service] {
      var items: [Service] = []
      for item in services {
        if item.count > 0 {
          items.append(item)
        }
      }
      return items
    }
    
    func getSelectedLoungeItems() -> [Service] {
      var items: [Service] = []
      for item in services {
        if item.count > 0 {
          items.append(item)
        }
      }
      return items
    }
    
  }

    var flightPartServices = [FlightPartService]()
    var passengerNumber = -1

    func getFlightPartService(_ origin:String?, destination:String?) -> FlightPartService? {
        for fs in flightPartServices {
            if fs.origin == origin && fs.destination == destination {
                return fs
            }
        }
        return nil
    }
}

typealias FDFlightPartService = FDPassengerService.FlightPartService
typealias FDServiceItem = FDFlightPartService.Service

