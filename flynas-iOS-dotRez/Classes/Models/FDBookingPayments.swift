//
//  FDBookingPayments.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDBookingPayments : NSObject {
    class Payment : NSObject {
        static let StatusPendingCustomerAction = "PendingCustomerAction"
        static let StatusPending = "Pending"
        static let SADADBillerCode = "026"
        
        var accountName = ""
        var accountNumber = ""
        var authorizationStatus = "" //
        var code = ""
        var collectedAmount: NSNumber = 0
        var collectedCurrencyCode = ""
        var createdDate : Date? // "9999-12-31T00:00:00Z"

        var expiryDate = "" // "2016-07"

        var quotedAmount: NSNumber = 0
        var quotedCurrencyCode = ""
        var status = ""  // "Approved"
                        // SADAD payment status of “PendingCustomerAction” would indicate a successful creation of a booking using SADAD payment option.
        var xReferenceNumber = ""
        var paymentNumber: NSNumber = 0

        // cvv
        var verificationCode = ""
    }

    var balanceDue : NSNumber = 0
    var total : NSNumber = 0

    var payments = [Payment]()

    func getPendingCF() -> Payment? {
        for payment in payments {
            if payment.status == "Pending" && payment.code == "CF" {
                return payment
            }
        }

        return nil
    }

    func getPendingNS() -> Payment? {
        for payment in payments {
            // 29/12/2016, 09:13:31 - work around, backend fail to set satus to Pending. backend will fix it next sprint.
            // can only use createDate to test the pending status.
            // Todo - fix this workaround when backend update
            if /*payment.status == "Pending" *&&*/ payment.code == "NS", let createdDate = payment.createdDate, createdDate.year() > 2999 {
                return payment
            }
        }

        return nil
    }
}

typealias FDPayment = FDBookingPayments.Payment
