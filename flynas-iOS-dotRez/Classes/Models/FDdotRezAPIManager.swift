//
//  FDdotRezAPIManager.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 15/09/2015.
//  Copyright (c) 2015 Matchbyte. All rights reserved.
//

import UIKit

let SERVER_VERSION_0 = 1
let SERVER_VERSION_1 = 0
let SERVER_VERSION_2 = 17
let SERVER_VERSION_3 = 0

//let FDVersionNumber = "Designed by Harley, Product of Matchbyte [r]"
let FDVersionNumber = "UAT"

//let FDApiBaseUrl = "https://xymobileapp.flynas.com/" // PROD
let FDApiBaseUrl = "https://uat-book-flynas.matchbyte.net/develop_r41/"
//let FDApiBaseUrl = "https://uat-book-flynas.matchbyte.net/release_r41/"

//let FDApiVersion = "v\(SERVER_VERSION_0).\(SERVER_VERSION_1).\(SERVER_VERSION_2)/"
let FDApiVersion = "api/"

let isAdjustPROD = false

class FDdotRezAPIManager: NSObject {

    class func initAPI() {
        let manager = RKObjectManager(baseURL: URL(string: FDApiBaseUrl))
        manager?.requestSerializationMIMEType = RKMIMETypeJSON
        RKObjectManager.setShared(manager)
    }

    class var operationCreate : RKObjectRequestOperation {
        let routePath = "api/" + "SessionCreate"
        let routeMethod = RKRequestMethod.POST
        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)
        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: routePath,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))
      return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func getSessionInfoMapping() -> RKObjectMapping {
        let memberLogin  = RKObjectMapping(for: FDMemberLogin.self)
        memberLogin?.addAttributeMappings(from: [
            "customerId",
            "firstName",
            "lastName",
            "organizationName",
            "forcePasswordReset",
            "roleCode",
            "roleName",
            "username",
            "naSmilesNumber"
            ])

        return memberLogin!
    }

    class var operationCheckSession : RKObjectRequestOperation {
        let routePath = FDApiVersion + "SessionCheck"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        let acceptCode = RKStatusCodeIndexSetForClass(200)
        let rd = RKResponseDescriptor(
            mapping: getSessionInfoMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "sessionInfo",
            statusCodes: acceptCode)
      return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:[rd as Any])
    }

    class var operationActivateSession : RKObjectRequestOperation {
        return operationCreate
    }

    class func getFlightAvailabilityMapping() -> RKObjectMapping {
        let legsMapping = RKObjectMapping(for: FDLeg.self)
        legsMapping?.addAttributeMappings(from: ["arrivalDate", "departureDate", "destination", "flightNumber", "carrierCode",  "origin", "utcArrivalDate", "utcDepartureDate"])
      let ssrPriceList = RKObjectMapping(for: FDSsrPriceList.self)
      ssrPriceList?.addAttributeMappings(from: ["ssrCode", "bundleSSRType", "price", "taxTotal"])
      let bundleOffers = RKObjectMapping(for: FDBundleOffers.self)
      bundleOffers?.addAttributeMappings(from: ["bundleCode", "bundleType", "paxType", "price", "programCode", "programLevel", "taxTotal", "smilePointsEarnedTotal", "smilePointsRedeemableTotal", "remainingAmountTotal"])
      bundleOffers?.addRelationshipMapping(withSourceKeyPath: "ssrPriceList", mapping: ssrPriceList)
      let bundleSet = RKObjectMapping(for: FDBundleSet.self)
      bundleSet?.addAttributeMappings(from: ["bundleSetCode"])
      bundleSet?.addRelationshipMapping(withSourceKeyPath: "bundleOffers", mapping: bundleOffers)
        let fares = RKObjectMapping(for: FDFare.self)
        fares?.addAttributeMappings(from: ["fareKey", "fareType", "price", "remainingSeats", "promoFare", "smilePointsEarned", "smilePointsRedeemable", "remainingAmount"])
      fares?.addRelationshipMapping(withSourceKeyPath: "bundleSet", mapping: bundleSet)
        let flights = RKObjectMapping(for: FDFlight.self)
        flights?.addAttributeMappings(from: ["arrivalDate", "departureDate", "destination", "flightNumber", "journeyKey", "numberOfStops", "origin", "utcArrivalDate", "utcDepartureDate","operatedByFlightNumber","operatedByText"])
        flights?.addRelationshipMapping(withSourceKeyPath: "fares", mapping: fares)
        flights?.addRelationshipMapping(withSourceKeyPath: "legs", mapping: legsMapping)
        let trips = RKObjectMapping(for: FDTrip.self)
        trips?.addAttributeMappings(from: ["date", "destination", "origin"])
        trips?.addRelationshipMapping(withSourceKeyPath: "flights", mapping: flights)
        let flightAvailability  = RKObjectMapping(for: FDFlightsAvailability.self)
        flightAvailability?.addAttributeMappings(from: ["currencyCode"])
        flightAvailability?.addRelationshipMapping(withSourceKeyPath: "trips", mapping: trips)
        return flightAvailability!
    }

    class func getLowFareAvailabilityMapping() -> RKObjectMapping {

        let dateFareMapping = RKObjectMapping(for: FDLowFare.self)
        dateFareMapping?.addAttributeMappings(from: ["price", "date"])

        let tripsMapping = RKObjectMapping(for: FDTripFare.self)
        tripsMapping?.addAttributeMappings(from: ["destination", "origin"])
        tripsMapping?.addRelationshipMapping(withSourceKeyPath: "dates", mapping: dateFareMapping)

        let lowFareAvailabilityMapping  = RKObjectMapping(for: FDLowFareAvailability.self)
        lowFareAvailabilityMapping?.addAttributeMappings(from: ["currencyCode"])
        lowFareAvailabilityMapping?.addRelationshipMapping(withSourceKeyPath: "trips", mapping: tripsMapping)

        return lowFareAvailabilityMapping!
    }

    class func operationLowFareFlightSearch(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "LowFareFlightSearch"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)


        let rd2 = RKResponseDescriptor(
            mapping: getLowFareAvailabilityMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "lowFareAvailability",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd2!],routeMethod: routeMethod))
    }

  class func operationSearch(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
    let routePath = FDApiVersion + "FlightSearch"
    let routeMethod = RKRequestMethod.POST
    let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)
    let rd = RKResponseDescriptor(
      mapping: getFlightAvailabilityMapping(),
      method: routeMethod,
      pathPattern: nil,
      keyPath: "flightsAvailability",
      statusCodes: RKStatusCodeIndexSetForClass(200))
    let rd2 = RKResponseDescriptor(
      mapping: getLowFareAvailabilityMapping(),
      method: routeMethod,
      pathPattern: nil,
      keyPath: "lowFareAvailability",
      statusCodes: RKStatusCodeIndexSetForClass(200))
    return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!,rd2!],routeMethod: routeMethod))
  }

    class func operationFlightAdd(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "FlightSell"
        let routeMethod = RKRequestMethod.POST
        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)
        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!], routeMethod:routeMethod))
    }

    class var operationLanguage : RKObjectRequestOperation {
        let routePath = FDApiVersion + "Cultures"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)


        let stringMapping = RKObjectMapping(for: FDResourceString.self)
        stringMapping?.addAttributeMappings(from: ["name", "value"])

        let cultures  = RKObjectMapping(for: NSMutableDictionary.self)
        cultures?.addRelationshipMapping(withSourceKeyPath: "cultures", mapping: stringMapping)

        let rd = RKResponseDescriptor(
            mapping: cultures,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "cultures",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }
  
   class func operationLounge(_ parameters: [String: Any]?, remove: Bool) -> RKObjectRequestOperation {
      let routePath = FDApiVersion + (remove ? "RemoveLounge" : "Lounge")
      let routeMethod: RKRequestMethod = parameters == nil ? .GET : .POST
        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)
        let loungeSsr  = RKObjectMapping(for: FDLoungeSsr.self)
        loungeSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())
        let rd = RKResponseDescriptor(
          mapping: loungeSsr,
          method: routeMethod,
          pathPattern: nil,
          keyPath: "loungeSsr",
          statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
      }

    class func operationPriceSummary(_ sellKeys:[String]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "PriceSummary"
        var routeMethod = RKRequestMethod.GET

        var parameters : [AnyHashable: Any]? = nil
        if sellKeys != nil {
            parameters = ["priceSummary" : ["sellKeys" : sellKeys!]]
            routeMethod = RKRequestMethod.POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let paxCharges  = RKObjectMapping(for: FDPaxCharge.self)
        paxCharges?.addAttributeMappings(from: ["fareType", "paxType", "paxCount", "paxFareAmount", "paxFareTotalDiscountAmount", "totalAmount", "bundleCode", "bundlePrice"])

        let ssrCharges  = RKObjectMapping(for: FDSsrCharges.self)
        ssrCharges?.addAttributeMappings(from: ["ssrType", "feeCode", "amount","count"])
        let seatCharges  = RKObjectMapping(for: FDSeatCharges.self)
        seatCharges?.addAttributeMappings(from: ["seatNumber", "amount","count", "chargeType", "feeCode"])

        let serviceCharges  = RKObjectMapping(for: FDServiceCharge.self)
        serviceCharges?.addAttributeMappings(from: ["code", "amount"])

        let journeyCharges  = RKObjectMapping(for: FDJourneyCharge.self)
        journeyCharges?.addAttributeMappings(from: ["destination", "origin", "flightNumber", "numberOfStops", "sta", "std", "utcSta", "utcStd"])
        journeyCharges?.addRelationshipMapping(withSourceKeyPath: "paxCharges", mapping: paxCharges)
        journeyCharges?.addRelationshipMapping(withSourceKeyPath: "serviceCharges", mapping: serviceCharges)
        journeyCharges?.addRelationshipMapping(withSourceKeyPath: "seatCharges", mapping: seatCharges)
        journeyCharges?.addRelationshipMapping(withSourceKeyPath: "ssrCharges", mapping: ssrCharges)

        let bookingCharges  = RKObjectMapping(for: FDServiceCharge.self)
        bookingCharges?.addAttributeMappings(from: ["code", "amount"])

        let priceSummaryMapping  = RKObjectMapping(for: FDPriceSummary.self)
        priceSummaryMapping?.addAttributeMappings(from: ["balanceDue", "currencyCode", "hasUncommittedChanges","recordLocator","total", "taxAmount", "hasLoyaltyPayment","loyaltyAmount","loyaltyNumber","creditShellAmount", "remainingAmountTotal", "smilePointsRedeemableTotal", "smilePointsRedeemedTotal", "smilePointsEarnedTotal"])
        priceSummaryMapping?.addRelationshipMapping(withSourceKeyPath: "journeyCharges", mapping: journeyCharges)
        priceSummaryMapping?.addRelationshipMapping(withSourceKeyPath: "bookingCharges", mapping: bookingCharges)
        let rd = RKResponseDescriptor(
            mapping: priceSummaryMapping,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "priceSummary",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func getErrorMapping() -> RKObjectMapping {

        let errorInfo = RKObjectMapping(for: FDError.self)
        errorInfo?.addAttributeMappings(from: ["errorCode","errorMessage","errorType","severity"])

        return errorInfo!
    }

    class func getContactInputMapping() -> RKObjectMapping {
        let phoneInfo  = RKObjectMapping(for: FDPhone.self)
        phoneInfo?.addAttributeMappings(from: ["code","number","phone"])

        let contactItemInfo  = RKObjectMapping(for: FDContactItemInfo.self)
        contactItemInfo?.addAttributeMappings(from: ["emailAddress"])
        contactItemInfo?.addRelationshipMapping(withSourceKeyPath: "mobilePhone", mapping: phoneInfo)
        contactItemInfo?.addRelationshipMapping(withSourceKeyPath: "workPhone", mapping: phoneInfo)

        let contactInput = RKObjectMapping(for: FDContactInput.self)
        contactInput?.addRelationshipMapping(withSourceKeyPath: "contactItemInfo", mapping: contactItemInfo)
        return contactInput!
    }

    class func getPassengerInputMapping () -> RKObjectMapping{
        let travelDocument  = RKObjectMapping(for: FDTravelDocument.self)
        travelDocument?.addAttributeMappings(from: ["birthCountry", "docNumber","docTypeCode","expirationDate","nationality","docIssuingCountry"])

        let passenger  = RKObjectMapping(for: FDPassenger.self)
        passenger?.addAttributeMappings(from: ["dateOfBirth", "firstName","lastName","discountCode","passengerNumber","paxType","residentCountry","title"])
        passenger?.addRelationshipMapping(withSourceKeyPath: "travelDocument", mapping: travelDocument)

        let passengersInput = RKObjectMapping(for: FDPassengersInput.self)
        passengersInput?.addAttributeMappings(from: ["regionFlightType"])
        passengersInput?.addRelationshipMapping(withSourceKeyPath: "passengers", mapping: passenger)

        return passengersInput!
    }

    class func operationPassengersContact(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "PassengersContact"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = .POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: getPassengerInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "passengersInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        let rd2 = RKResponseDescriptor(
            mapping: getContactInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "contactInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!, rd2!],routeMethod: routeMethod))
    }

    class func operationBookingContacts(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "BookingContacts"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = .POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd2 = RKResponseDescriptor(
            mapping: getContactInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "contactInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd2!],routeMethod: routeMethod))
    }

    class func operationBookingPassengers(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "BookingPassengers"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = .POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: getPassengerInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "passengersInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func flightSeatMapsMapping() -> RKObjectMapping {
        let seat  = RKObjectMapping(for: FDFlightSeatMap.SeatMap.SeatGroup.Seat.self)
        seat?.addAttributeMappings(from: ["available", "compartment", "infantAllowed", "number", "status","exitRow","childAllowed", "isFemaleOnly"])

        let seatGroup  = RKObjectMapping(for: FDFlightSeatMap.SeatMap.SeatGroup.self)
        seatGroup?.addAttributeMappings(from: ["deck", "type","priceGroup"])
        seatGroup?.addRelationshipMapping(withSourceKeyPath: "seats", mapping: seat)

        let passengerPriceGroup = RKObjectMapping(for: FDFlightSeatMap.SeatMap.PassengerSeatInfo.PriceGroup.self)
        passengerPriceGroup?.addAttributeMappings(from: ["price", "type", "remainingAmount", "smilePointsRedeemable"])

        let passengerSeatInfo  = RKObjectMapping(for: FDFlightSeatMap.SeatMap.PassengerSeatInfo.self)
        passengerSeatInfo?.addAttributeMappings(from: ["passengerNumber", "selectedSeat"])
        passengerSeatInfo?.addRelationshipMapping(withSourceKeyPath: "passengerPriceGroups", mapping: passengerPriceGroup)

        let seatMap  = RKObjectMapping(for: FDFlightSeatMap.SeatMap.self)
        seatMap?.addAttributeMappings(from: ["destination", "flightKey","origin","fareType","equipmentType"])
        seatMap?.addRelationshipMapping(withSourceKeyPath: "passengerSeatInfos", mapping: passengerSeatInfo)
        seatMap?.addRelationshipMapping(withSourceKeyPath: "seatGroups", mapping: seatGroup)

        let flightSeatMaps = RKObjectMapping(for: FDFlightSeatMap.self)
        flightSeatMaps?.addRelationshipMapping(withSourceKeyPath: "seatMaps", mapping: seatMap)

        return flightSeatMaps!
    }

    class func operationSeats(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "Seats"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = RKRequestMethod.POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        // object mapping
        let rd = RKResponseDescriptor(
            mapping: flightSeatMapsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightSeatMaps",
            statusCodes: RKStatusCodeIndexSetForClass(200))

//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))
//
//        let errorArray  = RKObjectMapping(forClass: NSArray.self)
//        let errorRd2 = RKResponseDescriptor(
//            mapping: errorArray,
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "",
//            statusCodes: RKStatusCodeIndexSetForClass(400))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationBaggage(_ parameters:[AnyHashable: Any]?, delete:Bool) -> RKObjectRequestOperation {
        var routePath = FDApiVersion + "Baggage"
        var routeMethod = RKRequestMethod.GET
        if parameters != nil {
            routeMethod = RKRequestMethod.POST

            if delete {
                routePath = FDApiVersion + "RemoveBaggage"
            }

            routePath = "\(routePath)?origin=\(parameters!["origin"] as! String)&&destination=\(parameters!["destination"] as! String)&&pn=\(parameters!["pn"]!)&&code=\(parameters!["code"] as! String)"
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let baggageSsr  = RKObjectMapping(for: FDBaggageSsr.self)
        baggageSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())

        let rd = RKResponseDescriptor(
            mapping: baggageSsr,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "baggageSsr",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }
  
  class func operationSports(_ parameters: [String: AnyObject]?, delete: Bool) -> RKObjectRequestOperation {
    let routePath = FDApiVersion + (delete ? "RemoveSportEquipment" : "SportEquipment")
    let routeMethod: RKRequestMethod = parameters == nil ? .GET : .POST
    let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)
    let sportsSsr  = RKObjectMapping(for: FDSportsSsr.self)
    sportsSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())
    let rd = RKResponseDescriptor(
      mapping: sportsSsr,
      method: routeMethod,
      pathPattern: nil,
      keyPath: "sportEquipmentSsr",
      statusCodes: RKStatusCodeIndexSetForClass(200))
    return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
  }

    class func getPassengerServiceMapping() -> RKObjectMapping{
        let service  = RKObjectMapping(for: FDPassengerService.FlightPartService.Service.self)
        service?.addAttributeMappings(from: ["code","count","price", "smilePointsRedeemable", "remainingAmount"])
        let flightPartService  = RKObjectMapping(for: FDPassengerService.FlightPartService.self)
        flightPartService?.addAttributeMappings(from: ["destination","origin", "bundleCode", "serviceInBundle"])
        flightPartService?.addRelationshipMapping(withSourceKeyPath: "services", mapping: service)
        let passengerService  = RKObjectMapping(for: FDPassengerService.self)
        passengerService?.addAttributeMappings(from: ["passengerNumber"])
        passengerService?.addRelationshipMapping(withSourceKeyPath: "flightPartServices", mapping: flightPartService)

        return passengerService!
    }

    class func operationMeal(_ parameters:[AnyHashable: Any]?, delete:Bool) -> RKObjectRequestOperation {

        var routePath = FDApiVersion + "Meal"
        var routeMethod = RKRequestMethod.GET
        if parameters != nil {
            routeMethod = RKRequestMethod.POST

            if delete {
                routePath = FDApiVersion + "RemoveMeal"
            }

            routePath = "\(routePath)?origin=\(parameters!["origin"] as! String)&&destination=\(parameters!["destination"] as! String)&&pn=\(parameters!["pn"]!)&&code=\(parameters!["code"] as! String)"
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping begin
        let mealSsr  = RKObjectMapping(for: FDMealSsr.self)
        mealSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())

        let rd = RKResponseDescriptor(
            mapping: mealSsr,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "mealSsr",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // object mapping end

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationDocumentTypes(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "DocumentTypes"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

//        let documentType  = RKObjectMapping(forClass: FDDocumentType.self)
//        documentType.addAttributeMappingsFromArray(["code","name"])
//
//        let documentTypes  = RKObjectMapping(forClass: FDDocumentTypes.self)
//        documentTypes.addRelationshipMappingWithSourceKeyPath("documentTypes", mapping: documentType)

        let documentTypes = getDocumentTypesMapping()

        let rd = RKResponseDescriptor(
            mapping: documentTypes,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "documentTypes",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationMeals(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "Meals"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

//        let meal  = RKObjectMapping(forClass: FDExtraItem.self)
//        meal.addAttributeMappingsFromDictionary(["code":"itemKey","name":"itemName","url":"itemImgUrl"])
//
//        let mealsInfo  = RKObjectMapping(forClass: FDMealsInfo.self)
//        mealsInfo.addRelationshipMappingWithSourceKeyPath("meals", mapping: meal)

        let mealsInfo = getMealsInfoMapping()

        let rd = RKResponseDescriptor(
            mapping: mealsInfo,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "mealsInfo",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    // payment
    class func operationPaymentMethods() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "PaymentMethods"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let method  = RKObjectMapping(for: FDAvailablePaymentMethods.FDPaymentMethod.self)
        method?.addAttributeMappings(from: ["code","feeAmount","name"])

        let availablePaymentMethods  = RKObjectMapping(for: FDAvailablePaymentMethods.self)
        availablePaymentMethods?.addAttributeMappings(from: ["balanceDue","currencyCode","loyaltyBalanceDue","totle","totalPaymentAmount", "remainingAmount", "smilePointsRedeemable"])
        availablePaymentMethods?.addRelationshipMapping(withSourceKeyPath: "methods", mapping: method)
        let rd = RKResponseDescriptor(
            mapping: availablePaymentMethods,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "availablePaymentMethods",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        // object mapping end
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationPayments(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "Payments"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        // object mapping
        let result  = RKObjectMapping(for: FDPaymentResult.self)
        result?.addAttributeMappings(from: ["authorizationStatus","balanceDue","status","tdsRedirectUrl"])

        let paymentResult  = RKObjectMapping(for: NSMutableDictionary.self)
        paymentResult?.addRelationshipMapping(withSourceKeyPath: "result", mapping: result)

        let rd = RKResponseDescriptor(
            mapping: paymentResult,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "paymentResult",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        // object mapping end

        // error mapping
        let errorRd = RKResponseDescriptor(
            mapping: getErrorMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "errors",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        //

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle( [rd!,errorRd!],routeMethod: routeMethod))
    }
  
  class func operationSadadOLP(olpId: String, returnPage: String) -> RKObjectRequestOperation {
    let routePath = FDApiVersion + "SadadOnlinePaymentInfo?olpId=" + olpId + "&returnPage=" + returnPage
    let routeMethod = RKRequestMethod.GET
    let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)
    let sadadOnlinePaymentData = RKObjectMapping(for: FDSadadOnlinePaymentData.FDSadadOnlinePaymentData.self)
    sadadOnlinePaymentData?.addAttributeMappings(from: ["accessCode", "amount", "command", "currency", "customerEmail", "language", "merchantIdentifier", "merchantReference", "payFortLink", "paymentOption", "returnUrl", "sadadOlp", "signature"])
    let sadadOnlinePaymentDataResponse = RKObjectMapping(for: FDSadadOnlinePaymentData.self)
    sadadOnlinePaymentDataResponse?.addRelationshipMapping(withSourceKeyPath: "sadadOnlinePaymentData", mapping: sadadOnlinePaymentData)
    let rd = RKResponseDescriptor(
      mapping: sadadOnlinePaymentDataResponse,
      method: routeMethod,
      pathPattern: nil,
      keyPath: "sadadOnlinePayment",
      statusCodes: RKStatusCodeIndexSetForClass(200))
    return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
  }

    class func operationBookingPolling() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "BookingPolling"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let bookingPolling  = RKObjectMapping(for: FDBookingPolling.self)
        bookingPolling?.addAttributeMappings(from: ["maxQueryCount",
            "pollingWaitTime",
            "repeatQueryInterval",
            "sessionPaymentQueryCount",
            "shouldContinuePolling",
            "startQueryAfter"])


        let rd = RKResponseDescriptor(
            mapping: bookingPolling,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "bookingPolling",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        // object mapping end

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func getBookingFlightMapping() -> RKObjectMapping {
        let leg = RKObjectMapping(for: FDLeg.self)
        leg?.addAttributeMappings(from: ["arrivalDate", "departureDate", "destination", "flightNumber", "carrierCode",  "origin", "utcArrivalDate", "utcDepartureDate"])

        let segment = RKObjectMapping(for: FDFlight.self)
        segment?.addAttributeMappings(from: [
            "origin" : "origin",
            "destination":"destination",
            "departureDate":"departureDate",
            "arrivalDate":"arrivalDate",
            "flightDesignator":"flightNumber",
            ])
        segment?.addRelationshipMapping(withSourceKeyPath: "legs", mapping: leg)


        let journey = RKObjectMapping(for: FDBookingFlights.Journey.self)
        journey?.addAttributeMappings(from: [
            "arrivalDate","departureDate","destination","origin","cancellationAllowed","changeAllowed", "isCodeShare", "bundleCode", "serviceInBundle"])
        journey?.addRelationshipMapping(withSourceKeyPath: "segments", mapping: segment)

        let bookingFlights = RKObjectMapping(for: FDBookingFlights.self)
        bookingFlights?.addRelationshipMapping(withSourceKeyPath: "journeys", mapping: journey)

        return bookingFlights!
    }

    class func operationBooking(_ submitPayment:Bool = false) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "Booking"
        let routeMethod = submitPayment ? RKRequestMethod.POST : RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        // 1 bookingDetails
        let bookingDetails  = RKObjectMapping(for: FDBookingDetails.self)
        bookingDetails?.addAttributeMappings(from: [
            "bookingDate",
            "createdDate",
            "modifiedDate",
            "recordLocator",
            "status",
            "hasUncommittedChanges",
            "isGdsBooking",
            "currencyCode",
            "hasLoyaltyPayment",
            "loyaltyAmount",
            "loyaltyNumber"
            ])
        let rd = RKResponseDescriptor(
            mapping: bookingDetails,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "bookingDetails",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // 2 - bookingFlights
        let rd2 = RKResponseDescriptor(
            mapping: getBookingFlightMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "bookingFlights",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // 3 - passengersInput
        let rd3 = RKResponseDescriptor(
            mapping: getPassengerInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "passengersInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // 4 - contactInput
        let rd4 = RKResponseDescriptor(
            mapping: getContactInputMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "contactInput",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // 5 - bookingPayments
        let payment  = RKObjectMapping(for: FDBookingPayments.Payment.self)
        payment?.addAttributeMappings(from: [
            "accountName",
            "accountNumber",
            "authorizationStatus",
            "code",
            "collectedAmount",
            "collectedCurrencyCode",
            "createdDate",
            "expiryDate",
            "quotedAmount",
            "quotedCurrencyCode",
            "status",
            "paymentNumber",
            "xReferenceNumber"])

        let bookingPayments  = RKObjectMapping(for: FDBookingPayments.self)
        bookingPayments?.addAttributeMappings(from: [
            "balanceDue",
            "total"])
        bookingPayments?.addRelationshipMapping(withSourceKeyPath: "payments", mapping: payment)

        let rd5 = RKResponseDescriptor(
            mapping: bookingPayments,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "bookingPayments",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // 6 - baggageSsr
        let baggageSsr  = RKObjectMapping(for: FDBaggageSsr.self)
        baggageSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())

        let rd6 = RKResponseDescriptor(
            mapping: baggageSsr,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "baggageSsr",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        // 7 - mealSsr
        let mealSsr  = RKObjectMapping(for: FDMealSsr.self)
        mealSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())

        let rd7 = RKResponseDescriptor(
            mapping: mealSsr,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "mealSsr",
            statusCodes: RKStatusCodeIndexSetForClass(200))

      // 9 - sportsSsr
      let sportSsr = RKObjectMapping(for: FDSportsSsr.self)
      sportSsr?.addRelationshipMapping(withSourceKeyPath: "passengerServices", mapping: getPassengerServiceMapping())
      let rd9 = RKResponseDescriptor(mapping: sportSsr,
                                     method: routeMethod,
                                     pathPattern: nil,
                                     keyPath: "sportEquipmentSsr",
                                     statusCodes: RKStatusCodeIndexSetForClass(200))
      
        // 8 - flightSeatMaps
        let rd8 = RKResponseDescriptor(
            mapping: flightSeatMapsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightSeatMaps",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        // object mapping end

      return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!,rd2!, rd3!,rd4!,rd5!,rd6!, rd7!, rd8!, rd9!], routeMethod:routeMethod))
    }

    class func operationFlightSchedule(_ departureStation:String, arrivalStation:String?) -> RKObjectRequestOperation {
        var routePath = FDApiVersion + "FlightSchedule?departureStation=\(departureStation)"

        if arrivalStation != nil {
            routePath.append("&arrivalStation=\(arrivalStation!)")
        }

        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let flightSchedule  = RKObjectMapping(for: FDFlightSchedule.FlightSchedule.self)
        flightSchedule?.addAttributeMappings(from: ["arrivalTime",
            "beginDate",
            "departureTime",
            "endDate",
            "flightDesignator",
            "flightFrequency"])

        let flightScheduleResponse  = RKObjectMapping(for: FDFlightSchedule.self)
        flightScheduleResponse?.addAttributeMappings(from: ["arrivalStation",
            "departureStation"])
        flightScheduleResponse?.addRelationshipMapping(withSourceKeyPath: "flightSchedules", mapping: flightSchedule)


        let rd = RKResponseDescriptor(
            mapping: flightScheduleResponse,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightSchedule",
            statusCodes: RKStatusCodeIndexSetForClass(200))

//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationFlightStatus(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "FlightStatus?flightNumber=\(parameters["flightNumber"] as! String)" // flightDate=\(parameters["flightDate"] as! String)&
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let flightStatus  = RKObjectMapping(for: FDFlightStatus.FlightStatus.self)
        flightStatus?.addAttributeMappings(from: ["arrivalActualTime",
            "arrivalScheduledTime",
            "arrivalStatus",
            "departureActualTime",
            "departureScheduledTime",
            "departureStatus",
            "destination",
            "flightDate",
            "flightDesignator",
            "origin",
            "updatedAsOfUtc"])

        let flightStatusResponse  = RKObjectMapping(for: FDFlightStatus.self)
        flightStatusResponse?.addAttributeMappings(from: ["flightDate",
            "flightNumber"])
        flightStatusResponse?.addRelationshipMapping(withSourceKeyPath: "flightStatus", mapping: flightStatus)

        let rd = RKResponseDescriptor(
            mapping: flightStatusResponse,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightStatus",
            statusCodes: RKStatusCodeIndexSetForClass(200))
//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    //RetrieveBookingWci
    class func operationRetrieveBookingWci(_ pnr:String, departureStation:String?, email:String?, lastName:String?) -> RKObjectRequestOperation {
        var routePath = FDApiVersion + "RetrieveBookingWci?pnr=\(pnr)"

        if email != nil && !email!.isEmpty {
            let e = email!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let e2 = e.replacingOccurrences(of: "@",with: "%40")
            routePath.append("&emailAddress=\(e2)")
        }

        if lastName != nil && !lastName!.isEmpty {
            let e = lastName!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let e2 = e.replacingOccurrences(of: "@",with: "%40")
            routePath.append("&lastName=\(e2)")
        }

        if departureStation != nil && !departureStation!.isEmpty {
            routePath.append("&departureStation=\(departureStation!)")
        }

        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "wciBooking",
            statusCodes: RKStatusCodeIndexSetForClass(200))

      return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: [rd as Any])
    }

    // WciBooking/?pnr=ME2B7E&departureStation=RUH
    class func operationWciBooking() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciBooking"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let passenger  = RKObjectMapping(for: FDWciBooking.Journey.Segment.Passenger.self)
        passenger?.addAttributeMappings(from: ["firstName",
            "isCheckedIn",
            "lastName",
            "passengerNumber","unitDesignator"])

        let segment = RKObjectMapping(for: FDWciBooking.Journey.Segment.self)
        segment?.addAttributeMappings(from: ["arrivalDate",
            "arrivalStation",
            "departureDate",
            "departureStation",
            "duration",
            "flightDesignator",
            "segmentNumber"])
        segment?.addRelationshipMapping(withSourceKeyPath: "passengers", mapping: passenger)

        let journey  = RKObjectMapping(for: FDWciBooking.Journey.self)
        journey?.addAttributeMappings(from: ["arrivalDate",
            "arrivalStation",
            "departureDate",
            "departureStation",
            "journeyNumber"])
        journey?.addRelationshipMapping(withSourceKeyPath: "segments", mapping: segment)

        let wciBooking  = RKObjectMapping(for: FDWciBooking.self)
        wciBooking?.addRelationshipMapping(withSourceKeyPath: "journeys", mapping: journey)

        let rd = RKResponseDescriptor(
            mapping: wciBooking,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "wciBooking",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:  rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationWciCheckinPassengers(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciCheckInPassengers"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: routePath,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))


//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationWciPassengerDetails(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciPassengerDetails"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = RKRequestMethod.POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let travelDocument  = RKObjectMapping(for: FDTravelDocument.self)
        travelDocument?.addAttributeMappings(from: [
            "documentNumber" : "docNumber",
            "documentTypeCode":"docTypeCode",
            "expirationDate":"expirationDate",
            "issuingCountry":"docIssuingCountry",
            "nationality":"nationality"])

        let passengerDetail  = RKObjectMapping(for: FDWciPassenger.self)
        passengerDetail?.addAttributeMappings(from: ["firstName",
            "passengerNumber",
            "lastName","dob",
            "paxType"])
        passengerDetail?.addRelationshipMapping(withSourceKeyPath: "travelDocuments", mapping: travelDocument)

        let wciPassengerDetails  = RKObjectMapping(for: FDWciPassengerDetails.self)
        wciPassengerDetails?.addRelationshipMapping(withSourceKeyPath: "passengerDetails", mapping: passengerDetail)

        let rd = RKResponseDescriptor(
            mapping: wciPassengerDetails,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "wciPassengerDetails",
            statusCodes: RKStatusCodeIndexSetForClass(200))


//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationWciSeats(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciSeats"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = RKRequestMethod.POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        // object mapping

        let rd = RKResponseDescriptor(
            mapping: flightSeatMapsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightSeatMaps",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationWciAutoSeats() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciAutoSeats"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        // object mapping
        let rd = RKResponseDescriptor(
            mapping: flightSeatMapsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "flightAutoSeatMaps",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func operationWciCheckIn(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "WciCheckIn"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "wciCheckIn",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod: routeMethod))
    }

    class func rdByAppendErrorHandle(_ rds:[RKResponseDescriptor], routeMethod:RKRequestMethod) -> [RKResponseDescriptor] {

//        let errorRd = RKResponseDescriptor(
//            mapping: getErrorMapping(),
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "errors",
//            statusCodes: RKStatusCodeIndexSetForClass(500))

        let errorRd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "",
            statusCodes: RKStatusCodeIndexSetForClass(500))

        let errorRd2 = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSArray.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "",
            statusCodes: RKStatusCodeIndexSetForClass(400))

        var r = rds
        r.append(errorRd!)
        r.append(errorRd2!)
        return r
    }

    //
    class func operationMemberLogin(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberLogin"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = RKRequestMethod.POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: getSessionInfoMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberLogin",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationMemberProfile(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberProfile"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = .POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)


        let booking = RKObjectMapping(for: FDBookings.self)
        booking?.addAttributeMappings(from: [
            "destination",
            "origin",
            "recordLocator",
            "sta",
            "std",
            "isCheckInAllowed"])
        let customerBookings  = RKObjectMapping(for: FDMemberProfile.CustomerBookings.self)
        customerBookings?.addRelationshipMapping(withSourceKeyPath: "currentBookings", mapping: booking)
        customerBookings?.addRelationshipMapping(withSourceKeyPath: "pastBookings", mapping: booking)
        let rd6 = RKResponseDescriptor(
            mapping: customerBookings,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "customerBookings",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        let rd5 = RKResponseDescriptor(
            mapping: getFamilyMapping (),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberFamily",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        let phone  = RKObjectMapping(for: FDPhone.self)
        phone?.addAttributeMappings(from: [
            "number": "phone",
            "deafult":"idDeafult",
            "typeCode":"typeCode"])
        phone?.addRelationshipMapping(withSourceKeyPath: "document", mapping: phone)
        let profilePhones  = RKObjectMapping(for: FDMemberProfile.ProfilePhones.self)
        profilePhones?.addRelationshipMapping(withSourceKeyPath: "phones", mapping: phone)
        let rd4 = RKResponseDescriptor(
            mapping: profilePhones,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "profilePhones",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        let addresses  = RKObjectMapping(for: FDMemberProfile.ProfileAddresses.Addresses.self)
        addresses?.addAttributeMappings(from: [
            "addressLine1",
            "addressLine2",
            "addressLine3",
            "city",
            "countryCode",
            "postalCode",
            "provinceState"])
        let profileAddresses  = RKObjectMapping(for: FDMemberProfile.ProfileAddresses.self)
        profileAddresses?.addRelationshipMapping(withSourceKeyPath: "addresses", mapping: addresses)
        let rd3 = RKResponseDescriptor(
            mapping: profileAddresses,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "profileAddresses",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        let nasmiles  = RKObjectMapping(for: FDNaSmileLoyalty.self)
        nasmiles?.addAttributeMappings(from: [
            "nasmilesPoints" : "balancePoints",
            "nasmilesLevel" : "rankLevel",
            "nasmilesNumber" : "nasLoyaltyNumber",
            "nasmilesExpiryDate" : "loyaltyExpiryDate",
            "nasmilesBarcodePayload" : "nasmilesBarcodePayload"])
        let details  = RKObjectMapping(for: FDPassenger.self)
        details?.addAttributeMappings(from: [
            "dateOfBirth" : "dateOfBirth",
            "first":"firstName",
            "last":"lastName",
            "gender":"gender",
            "nationality":"nationality",
            "title":"title",
            "middle":"middleName"])

        details?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "nasmiles", toKeyPath: "naSmilesLoyalty", with: nasmiles))
//        details.addRelationshipMappingWithSourceKeyPath("nasmiles", mapping: nasmiles)


        let profileDetails  = RKObjectMapping(for: FDMemberProfile.ProfileDetails.self)
        profileDetails?.addRelationshipMapping(withSourceKeyPath: "details", mapping: details)
        let rd2 = RKResponseDescriptor(
            mapping: profileDetails,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "profileDetails",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        let document  = RKObjectMapping(for: FDTravelDocument.self)
        document?.addAttributeMappings(from: [
            "birthCountry" : "birthCountry",
            "number":"docNumber",
            "expirationDate":"expirationDate",
            "issueDate":"issueDate",
            "nationality":"nationality",
            "first":"first",
            "last":"last",
            "title":"title",
            "typeCode":"docTypeCode",
            "default":"isDefault"])
        let profileTravelDocument = RKObjectMapping(for: FDMemberProfile.ProfileTravelDocument.self)
        profileTravelDocument?.addRelationshipMapping(withSourceKeyPath: "documents", mapping: document)
        let rd = RKResponseDescriptor(
            mapping: profileTravelDocument,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "profileTravelDocument",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!, rd2!,rd3!,rd4!,rd5!,rd6!],routeMethod:routeMethod))
    }

    class func getFamilyMapping () -> RKObjectMapping {

        let document2  = RKObjectMapping(for: FDTravelDocument.self)
        document2?.addAttributeMappings(from: [
            "birthCountry" : "birthCountry",
            "expirationDate":"expirationDate",
            "issuingCountry":"docIssuingCountry",
            "number":"docNumber",
            "type":"docTypeCode"])
        let member  = RKObjectMapping(for: FDPassenger.self)
        member?.addAttributeMappings(from: [
            "dateOfBirth" : "dateOfBirth",
            "firstName":"firstName",
            "lastName":"lastName",
            "gender":"gender",
            "nationality":"nationality",
            "title":"title",
            "middleName":"middleName",
            "relationship":"relationship",
            "nasmiles":"nasmiles"])
        member?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "document", toKeyPath: "travelDocument", with: document2))
        let memberFamily  = RKObjectMapping(for: FDMemberProfile.MemberFamily.self)
        memberFamily?.addRelationshipMapping(withSourceKeyPath: "members", mapping: member)

        return memberFamily!

    }

    class func operationMemberFamily(_ parameters:[AnyHashable: Any]?) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "Family"
        var routeMethod = RKRequestMethod.GET

        if parameters != nil {
            routeMethod = .POST
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd5 = RKResponseDescriptor(
            mapping: getFamilyMapping (),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberFamily",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd5!],routeMethod:routeMethod))
    }

    class func operationMemberRegister(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberRegister"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationApiSessionDelete() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "SessionDelete"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }
//
//    class func operationRelationships(parameters:[NSObject : AnyObject]) -> RKObjectRequestOperation {
//        let routePath = FDApiVersion + "Relationships"
//        let routeMethod = RKRequestMethod.GET
//
//        let request = RKObjectManager.sharedManager().requestWithObject(nil, method: routeMethod, path: routePath, parameters: parameters)
//
//        let item = RKObjectMapping(forClass: FDResourceString.self)
//        item.addAttributeMappingsFromDictionary(["code":"value","name":"name"])
//
//        let relationships  = RKObjectMapping(forClass: FDRelationships.self)
//        relationships.addRelationshipMappingWithSourceKeyPath("relationships", mapping: item)
//
//        let rd = RKResponseDescriptor(
//            mapping: relationships,
//            method: routeMethod,
//            pathPattern: nil,
//            keyPath: "relationships",
//            statusCodes: RKStatusCodeIndexSetForClass(200))
//        return RKObjectRequestOperation(request:request, responseDescriptors: rdByAppendErrorHandle([rd], routeMethod: routeMethod))
//    }

    class func getBoardingPassesMapping() -> RKObjectMapping {

        let barCode = RKObjectMapping(for: FDBarCode.self)

        barCode?.addAttributeMappings(from: [
            "barcodeData",
            "barcodeType",
            "hmac",
            "payload",
            "salt",
            "sequenceNumber"])

        let inventoryLegKey = RKObjectMapping(for: FDInventoryLegKey.self)
        inventoryLegKey?.addAttributeMappings(from: ["arrivalStation", "carrierCode", "departureDate", "departureStation", "flightNumber",  "opSuffix"])

        let leg = RKObjectMapping(for: FDLeg.self)

        leg?.addAttributeMappings(from: [
            "sta" : "arrivalDate",
            "std":"departureDate",
            "boardingSequence":"boardingSequence",
            "seatRow":"seatRow",
            "seatColumn":"seatColumn",

//            "arriveStation":"origin",
//            "utcArrivalDate":"utcArrivalDate",
//            "departureDate":"utcDepartureDate"
            ])
        leg?.addRelationshipMapping(withSourceKeyPath: "inventoryLegKey", mapping: inventoryLegKey)

        let segment = RKObjectMapping(for: FDFlight.self)
        segment?.addAttributeMappings(from: [
            "departureStation" : "origin",
            "arrivalStation":"destination",
            "departureTime":"departureDate",
            "arrivalTime":"arrivalDate",
//            "carrierCode":"nationality",
            "flightNumber":"flightNumber",
//            "classOfService":"last",
            ])
        segment?.addRelationshipMapping(withSourceKeyPath: "legs", mapping: leg)
        segment?.addRelationshipMapping(withSourceKeyPath: "barcodes", mapping: barCode)

        let name = RKObjectMapping(for: FDName.self)
        name?.addAttributeMappings(from: [
            "first",
            "last",
            "middle",
            "suffix",
            "title"])



        let item = RKObjectMapping(for: FDBoardingPass.self)
        item?.addAttributeMappings(from: [
            "agentId",
            "recordLocator"
            ])
        item?.addRelationshipMapping(withSourceKeyPath: "barCode", mapping: barCode)
        item?.addRelationshipMapping(withSourceKeyPath: "name", mapping: name)
        item?.addRelationshipMapping(withSourceKeyPath: "segments", mapping: segment)

        let boardingPasses  = RKObjectMapping(for: FDBoardingPasses.self)
        boardingPasses?.addAttributeMappings(from: [
            "barCodeType",
            "passengerNumber",
            "pnr",
            "sellKey",
            "serialNumbers"])
        boardingPasses?.addRelationshipMapping(withSourceKeyPath: "items", mapping: item)
//        boardingPasses.addRelationshipMappingWithSourceKeyPath("serialNumbers", mapping: strItem)

        return boardingPasses!
    }

    class func operationBoardingPasses(_ pnr:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "BoardingPasses?Pnr=" + pnr
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)


        let rd = RKResponseDescriptor(
            mapping: getBoardingPassesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "boardingPasses",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!], routeMethod: routeMethod))

    }

    class func operationWciBoardingPasses(_ pnr:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "wciBoardingPass"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        let rd = RKResponseDescriptor(
            mapping: getBoardingPassesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "wciBoardingPasses",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!], routeMethod: routeMethod))
    }

    // /api/RetrieveBooking?pnr=E7JSMJ&email=&lastName=H&phone=
    class func operationRetrieveBooking(_ pnr:String, phone:String?, email:String?, lastName:String?) -> RKObjectRequestOperation {
        var routePath = FDApiVersion + "RetrieveBooking?pnr=\(pnr)"
        let routeMethod = RKRequestMethod.GET

        if email != nil && !email!.isEmpty {
            let e = email!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let e2 = e.replacingOccurrences(of: "@",with: "%40")
            routePath.append("&email=\(e2)")
        }

        if lastName != nil && !lastName!.isEmpty {
            let e = lastName!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let e2 = e.replacingOccurrences(of: "@",with: "%40")
            routePath.append("&lastName=\(e2)")
        }

        if phone != nil && !phone!.isEmpty {
            routePath.append("&phone=\(phone!)")
        }

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:rdByAppendErrorHandle([rd!], routeMethod: routeMethod))
    }

    class func operationCancelFlights(_ flightIndexs:[Int]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "cancelFlights"
        let routeMethod = RKRequestMethod.POST

        let parameters = ["cancelFlights" :
            ["flightIndexes": flightIndexs]
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:rdByAppendErrorHandle([rd!], routeMethod: routeMethod))
    }

    class func operationBookingPassengers() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "BookingPassengers"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:rdByAppendErrorHandle([rd!], routeMethod: routeMethod))
    }

    class func getStationsMapping() -> RKObjectMapping {
        let mappingStation  = RKObjectMapping(for: FDResourceStation.self)
        mappingStation?.addAttributeMappings(from: ["code": "code",
            "country": "country",
            "latitude": "latitude",
            "longtitude": "longtitude",
            "name": "name"])

        let mapping  = RKObjectMapping(for: FDStations.self)
        mapping?.addRelationshipMapping(withSourceKeyPath: "stations", mapping: mappingStation)

        return mapping!
    }

    class func getMarketsMapping() -> RKObjectMapping {
        let mappingItems = RKObjectMapping.request()
        mappingItems?.addAttributeMappings(from: ["key", "value"])

        let mapping  = RKObjectMapping(for: FDMarkets.self)
        mapping?.addRelationshipMapping(withSourceKeyPath: "markets", mapping: mappingItems)

        return mapping!
    }

    class func getTitlesMapping() -> RKObjectMapping {
        let titles2Mapping  = RKObjectMapping(for: NSMutableDictionary.self)
        titles2Mapping?.addAttributeMappings(from: ["description":"description", "key":"key", "isChildOrInfant":"isChildOrInfant"])

        let titlesMapping  = RKObjectMapping(for: FDTitles.self)
        titlesMapping?.addRelationshipMapping(withSourceKeyPath: "titles", mapping: titles2Mapping)
        return titlesMapping!
    }

    class func getCountriesMapping() -> RKObjectMapping {
        let countries2Mapping  = RKObjectMapping(for: NSMutableDictionary.self)
        countries2Mapping?.addAttributeMappings(from: ["name":"name", "code":"code"])

        let countriesMapping  = RKObjectMapping(for: FDCountries.self)
        countriesMapping?.addRelationshipMapping(withSourceKeyPath: "countries", mapping: countries2Mapping)

        return countriesMapping!
    }

    class func getDocumentTypesMapping() -> RKObjectMapping {
        let documentType  = RKObjectMapping(for: FDDocumentType.self)
        documentType?.addAttributeMappings(from: ["code","name"])

        let documentTypes  = RKObjectMapping(for: FDDocumentTypes.self)
        documentTypes?.addRelationshipMapping(withSourceKeyPath: "documentTypes", mapping: documentType)
        return documentTypes!
    }

    class func getCountryPhoneCodesMapping() -> RKObjectMapping {
        let countries2Mapping  = RKObjectMapping(for: NSMutableDictionary.self)
        countries2Mapping?.addAttributeMappings(from: ["countryCode":"countryCode", "phoneCode":"phoneCode"])

        let countriesMapping  = RKObjectMapping(for: FDCountryPhoneCodes.self)
        countriesMapping?.addRelationshipMapping(withSourceKeyPath: "countryPhoneCodes", mapping: countries2Mapping)
        return countriesMapping!
    }

    class func getMealsInfoMapping() -> RKObjectMapping {
        let meal  = RKObjectMapping(for: FDExtraItem.self)
        meal?.addAttributeMappings(from: ["code":"itemKey","name":"itemName","url":"itemImgUrl"])

        let mealsInfo  = RKObjectMapping(for: FDMealsInfo.self)
        mealsInfo?.addRelationshipMapping(withSourceKeyPath: "meals", mapping: meal)
        return mealsInfo!
    }

    class func getRelationshipsMapping() -> RKObjectMapping {
        let item = RKObjectMapping(for: FDResourceString.self)
        item?.addAttributeMappings(from: ["code":"value","name":"name"])

        let relationships  = RKObjectMapping(for: FDRelationships.self)
        relationships?.addRelationshipMapping(withSourceKeyPath: "relationships", mapping: item)
        return relationships!
    }

    class func getCulturesMapping() -> RKObjectMapping {
        let stringMapping = RKObjectMapping(for: FDResourceString.self)
        stringMapping?.addAttributeMappings(from: ["name", "value"])

        let cultures  = RKObjectMapping(for: FDCultures.self)
        cultures?.addRelationshipMapping(withSourceKeyPath: "cultures", mapping: stringMapping)
        return cultures!
    }

    class func getSpecialServiceRequestsMapping() -> RKObjectMapping {
        let item = RKObjectMapping(for: FDSpecialServiceRequestItem.self)
        item?.addAttributeMappings(from: ["name", "code"])

        let specialServiceRequests  = RKObjectMapping(for: FDSpecialServiceRequests.self)
        specialServiceRequests?.addRelationshipMapping(withSourceKeyPath: "items", mapping: item)
        return specialServiceRequests!
    }

    class func getFeesMapping() -> RKObjectMapping {
        let item = RKObjectMapping(for: FDFeesItem.self)
        item?.addAttributeMappings(from: ["name", "code"])
        item?.addAttributeMappings(from: ["description":"des"])

        let fees  = RKObjectMapping(for: FDFees.self)
        fees?.addRelationshipMapping(withSourceKeyPath: "items", mapping: item)
        return fees!
    }

    class func getCurrenciesMapping() -> RKObjectMapping {
        let stringMapping = RKObjectMapping(for: FDResourceString.self)
        stringMapping?.addAttributeMappings(from: ["name", "value"])

        let currencies  = RKObjectMapping(for: FDCurrencies.self)
        currencies?.addRelationshipMapping(withSourceKeyPath: "currencies", mapping: stringMapping)
        return currencies!
    }

    class func operationAllResources (_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "AllResources"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        //stations
        let stations = RKResponseDescriptor(
            mapping: getStationsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "stations",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        //markets,
        let markets = RKResponseDescriptor(
            mapping: getMarketsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "markets",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //titles,
        let titles = RKResponseDescriptor(
            mapping: getTitlesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "titles",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //Countries
        let countries = RKResponseDescriptor(
            mapping: getCountriesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "countries",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //DocumentTypes
        let documentTypes = RKResponseDescriptor(
            mapping: getDocumentTypesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "documentTypes",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //CountryPhoneCodes
        let countryPhoneCodes = RKResponseDescriptor(
            mapping: getCountryPhoneCodesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "countryPhoneCodes",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //MealsInfo
        let mealsInfo = RKResponseDescriptor(
            mapping: getMealsInfoMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "mealsInfo",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //Relationships
        let relationships = RKResponseDescriptor(
            mapping: getRelationshipsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "relationships",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //Cultures
        let cultures = RKResponseDescriptor(
            mapping: getCulturesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "cultures",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //SpecialServiceRequests
        let specialServiceRequests = RKResponseDescriptor(
            mapping: getSpecialServiceRequestsMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "specialServiceRequests",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //Fees
        let fees = RKResponseDescriptor(
            mapping: getFeesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "fees",
            statusCodes: RKStatusCodeIndexSetForClass(200))


        //Currencies
        let currencies = RKResponseDescriptor(
            mapping: getCurrenciesMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "currencies",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([
            stations!,
            markets!,
            titles!,
            countries!,
            documentTypes!,
            countryPhoneCodes!,
            mealsInfo!,
            relationships!,
            cultures!,
            specialServiceRequests!,
            fees!,
            currencies!
            ], routeMethod: routeMethod))
    }

    class func operationPassbook(_ passBookParameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = "api/passbook"
        let routeMethod = RKRequestMethod.POST

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: passBookParameters)

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors:rdByAppendErrorHandle([rd!], routeMethod: routeMethod))
    }
}

// MARK: - naSmile
extension FDdotRezAPIManager {

    class func getLoyaltyMapping() -> RKObjectMapping {
        // object mapping
        let loyaltyInfo  = RKObjectMapping(for: FDNaSmileLoyalty.self)
        loyaltyInfo?.addAttributeMappings(from: [
            "balancePoints",
            "firstName",
            "lastName",
            "loyaltyExpiryDate",
            "loyaltyRedeemAmount",
            "nasLoyaltyNumber",
            "originalBookingInPoints",
            "rankLevel",
            "title",
            "nasmilesBarCodePayload",
            "minimumLoyaltyThreshold"
            ])

        return loyaltyInfo!
    }


    class func operationLoyalty(_ nasMemberAccountId:String, password:String, accountInfo:Bool) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "loyalty"
        let routeMethod = RKRequestMethod.GET

        let parameters = [
            "nasMemberAccountId":nasMemberAccountId,
            "password":password,
            "AccountInfo":accountInfo ? "true" : "false"
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters as [AnyHashable: Any])

        let rd = RKResponseDescriptor(
            mapping: getLoyaltyMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "loyaltyInfo",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func getMemberLoyaltyMapping() -> RKObjectMapping {
        // object mapping
        let loyaltyInfo  = RKObjectMapping(for: FDMemberLoyalty.self)
        loyaltyInfo?.addAttributeMappings(from: [
            "nasLoyaltyNumber",
            "updateSuccess"
            ])

        return loyaltyInfo!
    }


    class func operationMemberLoyalty(_ trackerIdentifier:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberLoyalty"
        let routeMethod = RKRequestMethod.GET

        let parameters = [
//            "nasLoyaltyNumber":nasMemberAccountId,
            "trackerIdentifier":trackerIdentifier //"UpdateNasmileMyProfileIBE"
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters as [AnyHashable: Any])

        let rd = RKResponseDescriptor(
            mapping: getMemberLoyaltyMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberLoyalty",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func getMemberLoyaltyJoinMapping() -> RKObjectMapping {
        // object mapping
        let loyaltyInfo  = RKObjectMapping(for: FDMemberLoyalty.self)
        loyaltyInfo?.addAttributeMappings(from: [
            "joinMessage":"message"
            ])

        return loyaltyInfo!
    }


    class func operationMemberLoyaltyJoin(_ parameters:[AnyHashable: Any]) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberLoyaltyJoin"
        let routeMethod = RKRequestMethod.POST


        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        let rd = RKResponseDescriptor(
            mapping: getMemberLoyaltyJoinMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberLoyaltyJoin",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationSendLoyaltyPassword(_ nasMemberAccountId:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "SendLoyaltyPassword"
        let routeMethod = RKRequestMethod.POST

        let parameters = [
            "nasMemberAccountId":nasMemberAccountId
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters as [AnyHashable: Any])

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationRefundMixedPayment() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "RefundMixedPayment"
        let routeMethod = RKRequestMethod.POST

        let parameters = ["refundMixedPayment":["":""]]
        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)

        // object mapping
        //{"refundMixedPayment":{"balanceDue":0}}
        let result  = RKObjectMapping(for: FDPaymentResult.self)
        result?.addAttributeMappings(from: ["balanceDue"])

        let rd = RKResponseDescriptor(
            mapping: result,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "refundMixedPayment",
            statusCodes: RKStatusCodeIndexSetForClass(200))
        // object mapping end

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle( [rd!],routeMethod: routeMethod))
    }
}

extension FDdotRezAPIManager {

    class func operationMemberPasswordForgot(_ memberEmail:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberPasswordForgot"
        let routeMethod = RKRequestMethod.POST

        let parameters = [
            "forgotMemberPassword": ["username":memberEmail]
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters as [AnyHashable: Any])

        let rd = RKResponseDescriptor(
            mapping: RKObjectMapping(for: NSNull.self),
            method: routeMethod,
            pathPattern: nil,
            keyPath: nil,
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationMemberPasswordReset(_ userName:String, oldPassword:String, password:String) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "MemberPasswordReset"
        let routeMethod = RKRequestMethod.POST

        let parameters = [
            "passwordReset":
                [
                    "username":userName,
                    "oldPassword":oldPassword,
                    "password":password,
                ]
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters as [AnyHashable: Any])

        let rd = RKResponseDescriptor(
            mapping: getSessionInfoMapping(),
            method: routeMethod,
            pathPattern: nil,
            keyPath: "memberLogin",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }

    class func operationCreditFile() -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "CreditFile"
        let routeMethod = RKRequestMethod.GET

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: nil)


        let mapping  = RKObjectMapping(for: FDCreditFileInfo.self)
        mapping?.addAttributeMappings(from: ["amountAlreadyUsed", "availableAmount", "creditFileAvailable"])

        let rd = RKResponseDescriptor(
            mapping: mapping,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "creditFileInfo",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }
    
    class func operationRemovePayment(_ paymentNumber: Int) -> RKObjectRequestOperation {
        let routePath = FDApiVersion + "RemovePayment"
        let routeMethod = RKRequestMethod.POST

        let parameters = [
            "removePayment":
                [
                    "paymentNumber":paymentNumber
            ]
        ]

        let request = RKObjectManager.shared().request(with: nil, method: routeMethod, path: routePath, parameters: parameters)


        let mapping  = RKObjectMapping(for: NSNull.self)

        let rd = RKResponseDescriptor(
            mapping: mapping,
            method: routeMethod,
            pathPattern: nil,
            keyPath: "",
            statusCodes: RKStatusCodeIndexSetForClass(200))

        return RKObjectRequestOperation(request:request! as URLRequest, responseDescriptors: rdByAppendErrorHandle([rd!],routeMethod:routeMethod))
    }
  
}
