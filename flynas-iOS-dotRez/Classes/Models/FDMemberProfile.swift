//
//  FDMemberProfile.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 20/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDMemberProfile: NSObject {
    class ProfileTravelDocument: NSObject {
        var documents = [FDTravelDocument]()

        func copyFrom(_ object:ProfileTravelDocument) {
            documents.removeAll()
            for doc in object.documents {
                let a = FDTravelDocument()
                a.copyFrom(doc)
                documents.append(a)
            }
        }
    }

    class ProfileDetails: NSObject {
        var details = FDPassenger()

        func copyFrom(_ object:ProfileDetails) {
            details.copyFrom(object.details)
        }
    }

    class ProfileAddresses: NSObject {
        class Addresses: NSObject {
            var addressLine1 = ""
            var addressLine2 = ""
            var addressLine3 = ""
            var city = ""
            var countryCode = ""
            var postalCode = ""
            var provinceState = ""

            func copyFrom(_ object:Addresses) {
                addressLine1 = object.addressLine1
                addressLine2 = object.addressLine2
                addressLine3 = object.addressLine3
                city = object.city
                countryCode = object.countryCode
                postalCode = object.postalCode
                provinceState = object.provinceState
            }
        }

        var addresses = [Addresses]()

        // make sure at least one
        override init() {
            addresses.append(Addresses())
        }

        func copyFrom(_ object:ProfileAddresses) {
            addresses.removeAll()
            for addr in object.addresses {
                let a = Addresses()
                a.copyFrom(addr)
                addresses.append(a)
            }

            if addresses.count < 1{
                addresses.append(Addresses())
            }
        }


    }

    class ProfilePhones: NSObject {
        var phones = [FDPhone]()

//        var faxPhone : String {
//            for p in phones {
//                if p.typeCode == FDPhoneTypeCode.Fax.rawValue {
//                    return p.number
//                }
//            }
//
//            return ""
//        }
//
//        func setWorkPhone(number:String) {
//            for p in phones {
//                if p.typeCode == FDPhoneTypeCode.Fax.rawValue {
//                    p.number = number
//                    return
//                }
//            }
//
//        }
        var mobileWithCountryCode : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    p.updatePhoneNumber()
                    return p.phone
                }
            }

            return ""
        }

        var mobile : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    return p.number
                }
            }

            return ""
        }

        var mobileCode : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    return p.code
                }
            }

            return "SA"
        }

        func setMobilePhone(_ number:String) {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    p.number = number
                    return
                }
            }

        }

        func setMobilePhoneCode(_ code:String) {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Mobile.rawValue {
                    p.code = code
                    return
                }
            }

        }

        var homePhoneWithCountryCode : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    p.updatePhoneNumber()
                    return p.phone
                }
            }

            return ""
        }

        var homePhone : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    return p.number
                }
            }

            return ""
        }

        var homePhoneCode : String {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    return p.code
                }
            }

            return "SA"
        }

        func setHomePhone(_ number:String) {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    p.number = number
                    return
                }
            }

        }

        func setHomePhoneCode(_ code:String) {
            for p in phones {
                if p.typeCode == FDPhoneTypeCode.Home.rawValue {
                    p.code = code
                    return
                }
            }
        }

        func copyFrom(_ object:ProfilePhones) {
            phones.removeAll()
            for p in object.phones {
                let a = FDPhone()
                a.copyFrom(p)
                phones.append(p)
            }
        }
    }

    class MemberFamily: NSObject {
        var members = [FDPassenger]()

        func copyFrom(_ object:MemberFamily) {
            members.removeAll()
            for p in object.members {
                let a = FDPassenger()
                a.copyFrom(p)
                members.append(a)
            }
        }
    }

    class CustomerBookings: NSObject {
        class Bookings: NSObject {
            var destination = ""
            var origin = ""
            var recordLocator = ""
            var sta : Date?
            var std : Date?
            var isCheckInAllowed = false

            func copyFrom(_ object:Bookings) {
                destination = object.destination
                origin = object.origin
                recordLocator = object.recordLocator
                sta = object.sta
                std = object.std
            }
        }

        var currentBookings = [Bookings]()
        var pastBookings = [Bookings]()

        func copyFrom(_ object:CustomerBookings) {
            pastBookings.removeAll()
            for p in object.pastBookings {
                let a = Bookings()
                a.copyFrom(p)
                pastBookings.append(a)
            }

            currentBookings.removeAll()
            for p in object.currentBookings {
                let a = Bookings()
                a.copyFrom(p)
                currentBookings.append(a)
            }
        }
    }

//    var profileTravelDocument = ProfileTravelDocument()
    var profileDetails = ProfileDetails()
    var profileAddresses = ProfileAddresses()
    var profilePhones = ProfilePhones()
    var memberFamily = MemberFamily()
    var customerBookings = CustomerBookings()


    func copyFrom(_ object:FDMemberProfile) {
//        profileTravelDocument.copyFrom(object.profileTravelDocument)
        profileDetails.copyFrom(object.profileDetails)
        profileAddresses.copyFrom(object.profileAddresses)
        profilePhones.copyFrom(object.profilePhones)
        memberFamily.copyFrom(object.memberFamily)
        customerBookings.copyFrom(object.customerBookings)
    }

    func copyHijiriFlagFrom(_ object:FDMemberProfile) {

        profileDetails.details.dateOfBirthHijri = object.profileDetails.details.dateOfBirthHijri
        profileDetails.details.travelDocument.expirationDateHijri = object.profileDetails.details.travelDocument.expirationDateHijri

        let n = min(memberFamily.members.count, object.memberFamily.members.count)
        for i in 0..<n {
            memberFamily.members[i].dateOfBirthHijri = object.memberFamily.members[i].dateOfBirthHijri
            memberFamily.members[i].travelDocument.expirationDateHijri = object.memberFamily.members[i].travelDocument.expirationDateHijri
        }
    }

  func getProfileDetailsObject (_ email: String) -> [AnyHashable: Any] {

        let parameter = [

                "details": [
                    "dateOfBirth": profileDetails.details.dateOfBirth != nil ? profileDetails.details.dateOfBirth!.toString(format: .custom("yyyy-MM-dd")) : "",
                    "first": profileDetails.details.firstName,
                    "gender": profileDetails.details.gender,
                    "last": profileDetails.details.lastName,
                    "middle": profileDetails.details.middleName,
                    "nationality": profileDetails.details.nationality,
                    "title": profileDetails.details.title,
                    "password": "",
                    "email" : email
                ]
        ]

        return parameter
    }

    func getProfilePhonesObject () -> [AnyHashable: Any] {
        var phones =  [[
            "number":profilePhones.mobileWithCountryCode,
            "typeCode":FDPhoneTypeCode.Mobile.rawValue,
          "default": false
            ]
        ]

        if !profilePhones.homePhone.isEmpty {
            phones.append(
                [
                    "number":profilePhones.homePhoneWithCountryCode,
                    "typeCode":FDPhoneTypeCode.Home.rawValue
                ])
        }

        let parameter = [
            "phones":phones
        ]

        return parameter
    }

}

typealias FDBookings = FDMemberProfile.CustomerBookings.Bookings

