//
//  FDBookingFlights.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 11/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDBookingFlights: NSObject {

    class Journey: NSObject {

        var arrivalDate : Date?
        var departureDate : Date?

        var destination = ""
        var origin = ""

        var cancellationAllowed = true
        var changeAllowed = true

        var isCodeShare = false
      var bundleCode: String?
      var serviceInBundle: NSArray?
        var segments = [FDFlight]()

        func getFlight() -> FDFlight {
            // mapping there: Journey -> FDFlight
            //                segments : FDFlight -> Legs
            //  legs -> no use
            let f = FDFlight()
            f.arrivalDate = arrivalDate
            f.departureDate = departureDate
            f.destination = destination
            f.origin = origin
            f.cancellationAllowed = cancellationAllowed
            f.changeAllowed = changeAllowed
            for s in segments {
                for leg in s.legs {
                    f.legs.append(leg)
                }
            }

            return f
        }
    }

    var journeys = [Journey]()
}
