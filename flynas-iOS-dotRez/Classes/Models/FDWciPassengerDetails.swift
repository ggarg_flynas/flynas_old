//
//  FDWciPassengerDetails.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWciPassengerDetails: NSObject {

    var passengerDetails = [FDWciPassenger]()

    func getPassengerInfo(_ passengerNumber:Int) -> FDWciPassenger? {
        for p in passengerDetails {
            if p.passengerNumber == passengerNumber && p.paxType != PaxType.Infant.rawValue {
                return p
            }
        }

        return nil
    }

    func getInfantPassengerInfo(_ passengerNumber:Int) -> FDWciPassenger? {
        for p in passengerDetails {
            if p.passengerNumber == passengerNumber && p.paxType == PaxType.Infant.rawValue {
                return p
            }
        }
        return nil
    }
}
