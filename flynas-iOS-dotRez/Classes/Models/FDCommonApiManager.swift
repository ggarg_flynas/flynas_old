//
//  FDCommonApiManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


enum PersistedResourceType : String {

    // save object
    case Languages = "Languages" // no multi-lang
    case Currencies = "Currencies"
    case Stations = "Stations"
    case Markets = "Markets"
    case Countries = "Countries"
    case RecentPassengers = "RecentPassengers" // no multi-lang
    case RecentContact = "RecentContact" // no multi-lang
    case Titles = "Titles"
    case DocTypes = "DocTypes"
    case SpecialServiceRequests = "SpecialServiceRequests"
    case Fees = "Fees"
    //        case Baggages
    //        case Meals

    //  --- new solution: save json object ---
    // member
    case MemberLogin = "MemberLogin"
    case naSmileLogin = "naSmileLogin"

}

class FDCommonApiManager: NSObject {
  func getResultObject<T> (_ results: [Any]?) -> T? {

        if let resultArray = results {
            for i in 0..<resultArray.count {
                if resultArray[i] is T {
                    return resultArray[i] as? T
                }
            }
        }

        return nil
    }

    func handleError (_ operation:RKObjectRequestOperation, error:NSError, fail: (String?) -> Void) {
        handleErrorWithHttpRequestOperation(operation.httpRequestOperation, error: error, fail:fail)
    }

    func handleErrorWithHttpRequestOperation (_ operation:AFHTTPRequestOperation, error:NSError, fail: (String?) -> Void) {

//        #if DEBUG
//        if let response = operation.response {
//            print("GET \(response.statusCode) response: \(operation.responseString)")
//            print(NSString(data: operation.responseData, encoding: NSUTF8StringEncoding))
//        }
//        #endif

        if operation.response?.statusCode == 400 {
            fail (operation.responseString)
            return
        } else if operation.response?.statusCode == 401 {
            fail (FDErrorHandle.API_UNAUTHORIZED_ERROR)
            return
        }  else if operation.response?.statusCode == 403 {
            fail (FDErrorHandle.API_SESSION_ERROR_EXPIRE)
            return
        }  else if operation.response?.statusCode == 404 {
            fail (FDErrorHandle.API_GENERAL_ERROR)
            return
        } else if operation.response?.statusCode >= 500 {
            let errorMessage = getIfErrorMessage(operation.responseString)
            fail (errorMessage)
            return
        } else {
            if let errors = error.userInfo[RKObjectMapperErrorObjectsKey] as? [FDError] {

                var errorMessage : String?
                for error in errors {
                    if errorMessage == nil {
                        errorMessage = error.errorMessage
                    } else {
                        errorMessage! += error.errorMessage + "\n"
                    }
                }
                fail (errorMessage)
                return
            }

            fail (error.localizedDescription)
        }
    }
  
  func getIfErrorMessage(_ responseStr: String?) -> String {
    guard let myResponseStr = responseStr else {
      return FDErrorHandle.API_GENERAL_ERROR
    }
    guard let errorDic = convertStringToDictionary(myResponseStr) else {
      return FDErrorHandle.API_GENERAL_ERROR
    }
    let errors = errorDic["errors"] as? [[String : AnyObject]]
    guard let errorMessage = errors?.first!["errorMessage"] as? String else {
      return FDErrorHandle.API_GENERAL_ERROR
    }
    return replaceErrorMessage(errorMessage)
  }
  
  func replaceErrorMessage(_ msg: String) -> String {
    if msg.contains("Unable to find Agent") {
      return FDLocalized("Invalid username/password combination.")
    }
    return msg
  }

    func loadLocalJsonAsResponse(_ operation:RKObjectRequestOperation, fileName:String) -> RKMappingResult {

        let path = Bundle.main.path(forResource: fileName, ofType: "json")

        let content = try! String(contentsOfFile:path!, encoding: String.Encoding.utf8)

        let MIMEType = "application/json"
        let data = content.data(using: String.Encoding.utf8)
        let parseData = try! RKMIMETypeSerialization.object(from: data, mimeType: MIMEType)

        let resDes = operation.responseDescriptors[0] as! RKResponseDescriptor

        let mappingsDictionary = [resDes.keyPath: resDes.mapping]
        let mapper = RKMapperOperation(representation: parseData, mappingsDictionary: mappingsDictionary)
        try! mapper?.execute()

        return mapper!.mappingResult
    }

    func persistedFilePath(_ resourceType: PersistedResourceType, language:String?) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

        let fileName = language == nil ? resourceType.rawValue : resourceType.rawValue + "_" + language!
        let fileURL = documentsURL.appendingPathComponent(fileName)

        return fileURL.path
    }

    func saveResource(_ res: Any, resourceType: PersistedResourceType, language:String?) {
        let filePath = persistedFilePath(resourceType, language: language)
        if NSKeyedArchiver.archiveRootObject(res, toFile: filePath) {
        } else {
            print("[saveResource] Failed to save: \(filePath)")
        }
    }

    func unarchiveResource(_ resourceType: PersistedResourceType, language: String?) -> AnyObject? {
        let filePath = persistedFilePath(resourceType, language: language)

        if FileManager.default.fileExists(atPath: filePath) {
            return NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as AnyObject?
        }

        return nil
    }

    func saveJson(_ jsonString: String, resourceType: PersistedResourceType, language:String?) {
        saveResource(jsonString as AnyObject, resourceType: resourceType, language: language)
    }

    func loadJson(_ resourceType: PersistedResourceType, language:String?) -> String {
        if let s = unarchiveResource(resourceType,language: language) as? String {
            return s
        }
        return ""
    }
}
