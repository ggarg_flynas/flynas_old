//
//  FDFlightStatus.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightStatus: NSObject {
    class FlightStatus: NSObject {
        var arrivalActualTime : Date?
        var arrivalScheduledTime : Date?
        var arrivalStatus = ""
        var departureActualTime : Date?
        var departureScheduledTime : Date?
        var departureStatus = ""
        var destination = ""
        var flightDate : Date?
        var flightDesignator = ""
        var origin = ""
        var updatedAsOfUtc : Date?

    }

    var flightDate : Date?
    var flightNumber = ""

    var flightStatus = FlightStatus()
}
