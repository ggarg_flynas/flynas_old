//
//  FDDocumentTypes.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 7/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDDocumentTypes: NSObject {

    var documentTypes = [FDDocumentType]()

    func getDocumentTypeName(_ code:String) -> String? {
        for d in documentTypes {
            if d.code == code {
                return d.name
            }
        }

        return nil
    }
}

typealias FDDocumentType = FDNameCode
