//
//  FDResourceString.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 8/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

open class FDResourceString: NSObject, NSCoding {
    var name : String?
    var value : String?

    override init () {
        super.init()
    }

    init(name: String, value: String) {
        self.value = value
        self.name = name
    }

    required convenience public init?(coder decoder: NSCoder) {
        self.init()
        self.name = decoder.decodeObject(forKey: "name") as? String
        self.value = decoder.decodeObject(forKey: "value") as? String
    }

    open func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
        coder.encode(self.value, forKey: "value")
    }
}
