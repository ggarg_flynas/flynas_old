//
//  FDWciBooking.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDWciBooking: NSObject {
    class Journey: NSObject {
        class Segment: NSObject {
            class Passenger: NSObject {
                var firstName = ""
                var isCheckedIn = false
                var lastName = ""
                var paxType = ""
                var passengerNumber = 0
                var unitDesignator : String?
                var dob: Date?
                var dateOfBirthHijri = false

                // buffer
                var fullName : String {
                    return firstName + " " + lastName
                }

              var nameInitial : String {
                return (firstName.substringToIndex(1)! + lastName.substringToIndex(1)!).uppercased()
              }

                var isEdiable = false
                var prepareToCheckin = false
                var travelDocuments = [FDTravelDocument]()

                var validateAll : ValidateErrorCode {
                    if dob == nil {
                        return .dobMissing
                    }

                    if travelDocuments.count > 0 {
                        return travelDocuments[0].validateDocument
                    } else {
                        return .docNumberMissing
                    }
                }
            }

            var arrivalDate : Date?
            var arrivalStation = ""
            var departureDate : Date?
            var departureStation = ""
            var duration = ""
            var flightDesignator = ""
            var passengers = [Passenger]()
            var segmentNumber = 0

            // buffer
            var journeyNumber = 0
            var pnr = ""

        }
        var arrivalDate: Date?
        var arrivalStation = ""
        var departureDate: Date?
        var departureStation = ""
        var journeyNumber = 0

        var segments = [Segment]()

        // buffer
        var pnr = ""

        var allPassengerHasSeat : Bool {
            for sm in segments {
                for ps in sm.passengers {
                    if ps.paxType != PaxType.Infant.rawValue  && ps.prepareToCheckin && ps.unitDesignator == nil {
                        return false
                    }
                }
            }
            
            return true
        }
    }

    var journeys = [Journey]()
}

typealias FDWciJourney = FDWciBooking.Journey
typealias FDWciSegment = FDWciBooking.Journey.Segment
typealias FDWciPassenger = FDWciBooking.Journey.Segment.Passenger
