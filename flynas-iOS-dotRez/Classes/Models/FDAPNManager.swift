//
//  FDAPNManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 29/06/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation
import RealmSwift

let FD_RECEIVE_NOTIFICATION = "FD_RECEIVE_NOTIFICATION"
let FD_AppTokenUserDefaultsName = "FD_AppTokenUserDefaultsName"

class FDAPNManager: FDCommonApiManager {
  
  static let sharedInstance = FDAPNManager()

    #if DEBUG
    let pushWizard_url = "https://pushwizard.com/api/10a885e541843c7b1a280e3f2f9df44fca5f381f54e5f66adad4780ccef82169/JSON"
    #endif

  //    let url = "http://awsmatchbyte-dev.us-west-2.elasticbeanstalk.com/Api/ApiRegistrations/Registration"
  //    let url = "http://awsmatchbyte-uat.us-west-2.elasticbeanstalk.com/Api/ApiRegistrations/Registration"
//      let url = "https://cviip8nh35.execute-api.eu-central-1.amazonaws.com/prod/registration" // PROD
//  let url = "https://cviip8nh35.execute-api.eu-central-1.amazonaws.com/uat/registration" // UAT
    let url = "https://cviip8nh35.execute-api.eu-central-1.amazonaws.com/dev/registration" // DEV

    var notifications = [FDNotificationItem]()
    var deviceToken : String?
    var appToken : String?

    let realm : Realm

    override init() {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let filePath = paths[0].appendingPathComponent("MyNotifications")

        realm = try! Realm(configuration: Realm.Configuration(
            fileURL: filePath,
            readOnly: false,
            schemaVersion: 4,
            migrationBlock: { migration, oldSchemaVersion in
            } )
        )

      let ns = realm.objects(FDNotificationItem).sorted(byKeyPath: "receiveDate",ascending:false)
        for n in ns {
            notifications.append(n)
        }

        appToken = UserDefaults.standard.object(forKey: FD_AppTokenUserDefaultsName) as? String

    }

    func registerDeviceToken(_ tokenString: String, success:() -> Void, fail: @escaping (String?) -> Void) {
        print("Device Token:", tokenString)
        deviceToken = tokenString
        updateDeviceToken(success, fail: fail)
    }

    let LastDeviceTokenLocaleDefaultsName = "LastDeviceTokenLocale"

    func notSameAsLastToken(_ deviceToken:String, locale:String) -> Bool {
        if let deviceTokenLocale = UserDefaults.standard.object(forKey: LastDeviceTokenLocaleDefaultsName) as? String {
            return deviceTokenLocale != "\(deviceToken)\(locale)"
        }
        return true
    }

    func saveCurrentToken (_ deviceToken:String, locale:String) {
        UserDefaults.standard.setValue("\(deviceToken)\(locale)", forKey: LastDeviceTokenLocaleDefaultsName)
    }

    func clearCurrentToken () {
        UserDefaults.standard.removeObject(forKey: LastDeviceTokenLocaleDefaultsName)
    }

    func updateDeviceToken(_ success:() -> Void, fail: @escaping (String?) -> Void) {

        if appToken == nil {
            appToken = UIDevice.current.identifierForVendor!.uuidString
            UserDefaults.standard.setValue(appToken, forKey:FD_AppTokenUserDefaultsName)
        }

        let locale = isAr() ? CULTURECODE_AR : CULTURECODE_EN

        var parameters : [String:Any] =
            ["AppToken":appToken! as AnyObject,
             "PushToken":unwrap(str: deviceToken) as AnyObject,
             "OS":1,
             "Locale": locale]

        if FDLoginManager.sharedInstance.hasSavedUserCredential {
            parameters["User"] = FDLoginManager.sharedInstance.userEmail as AnyObject?
        }


        let manager = RKObjectManager.shared()
        let downloadRequest = manager?.request(with: nil, method: .POST, path: url, parameters: parameters as [AnyHashable: Any])

        let requestOperation = AFHTTPRequestOperation(request: downloadRequest as URLRequest!)

        requestOperation?.setCompletionBlockWithSuccess({ (operation, result) -> Void in
            SVProgressHUD.dismiss()
            if let data = operation?.responseData {
                print (data)

                self.saveCurrentToken (unwrap(str: self.deviceToken), locale:locale)
            } else {
                fail (FDErrorHandle.API_GENERAL_ERROR)
            }

        }) { (operation, error) -> Void in
            fail (error?.localizedDescription)
        }
        manager?.httpClient.enqueue(requestOperation)

//        #if DEBUG
//
//        updateDeviceTokenToPushWizard({
//
//            }) { (_) in
//
//        }
//
//        #endif

    }
#if DEBUG
  func updateDeviceTokenToPushWizard(success:() -> Void, fail: @escaping (String?) -> Void) {

        guard let deviceToken = deviceToken else {
            fail(nil)
            return
        }

        let parameters = ["command":"Addtoken",
                          "token":deviceToken,
                          "v0":"Addtoken"]


      let manager = RKObjectManager.shared()
      let downloadRequest = manager?.request(with: nil, method: .POST, path: pushWizard_url, parameters: parameters as [NSObject : AnyObject])

    let requestOperation = AFHTTPRequestOperation(request: downloadRequest! as URLRequest)

      requestOperation?.setCompletionBlockWithSuccess({ (operation, result) -> Void in
            SVProgressHUD.dismiss()
        if let data = operation?.responseData {
                print (data)
            } else {
                fail (FDErrorHandle.API_GENERAL_ERROR)
            }

        }) { (operation, error) -> Void in
          fail (unwrap(str: error?.localizedDescription))
        }
      manager?.httpClient.enqueue(requestOperation)
        
    }
#endif
    
    func getAPNInfo(_ success:() -> Void, fail: @escaping (String?) -> Void) {

        let parameters = ["command":"Appdetail"]

        let manager = RKObjectManager.shared()
        let downloadRequest = manager?.request(with: nil, method: .POST, path: url, parameters: parameters)

        let requestOperation = AFHTTPRequestOperation(request: downloadRequest as URLRequest!)

        requestOperation?.setCompletionBlockWithSuccess({ (operation, result) -> Void in
            SVProgressHUD.dismiss()
            if let data = operation?.responseData {
                print (data)
            } else {
                fail (FDErrorHandle.API_GENERAL_ERROR)
            }

        }) { (operation, error) -> Void in
            fail (error?.localizedDescription)
        }
        manager?.httpClient.enqueue(requestOperation)
    }

    func printDict(_ jsonPayload: [AnyHashable: Any]) {

            do {
                let jsonData = try JSONSerialization.data(withJSONObject: jsonPayload, options: JSONSerialization.WritingOptions.prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data

                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
                // here "decoded" is of type `AnyObject`, decoded from JSON data

                print(decoded)

            } catch let error as NSError {
                print(error)
            }
    }

    func handlePush(_ remoteNotificationPayload: [AnyHashable: Any], live:Bool) {
        #if DEBUG
            printDict(remoteNotificationPayload)
        #endif

        let msg1 = getPushMsg(remoteNotificationPayload)

        if msg1.type == FDNotificationType.textMsg.rawValue {
//            notifications.append(msg1)
            notifications.insert(msg1, at: 0)

            try! self.realm.write {
                self.realm.add(msg1)
            }

            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: FD_RECEIVE_NOTIFICATION), object: msg1)
        }

    }

    func getPushMsg(_ remoteNotificationPayload: [AnyHashable: Any]) -> FDNotificationItem {

        let msg1 = FDNotificationItem()

        if let type = remoteNotificationPayload["type"] as? Int {
            msg1.type = type
        } else {
            msg1.type = FDNotificationType.textMsg.rawValue
        }

        if let title = remoteNotificationPayload["t"] as? String {
            msg1.title = title
        } else {
            msg1.title = FDLocalized("Notification")
        }

        if let aps = remoteNotificationPayload["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["body"] as? NSString {
                    //Do stuff
                    msg1.message = message as String
                }
            } else if let alert = aps["alert"] as? NSString {
                msg1.message = alert as String
            }
        }

        if let timeStamp = remoteNotificationPayload["ts"] as? String {
            msg1.receiveDate = RKDateFromStringWithFormatters(timeStamp, RKObjectMapping.defaultDateFormatters())
        } else {
            msg1.receiveDate = Date()
        }

        return msg1
    }
//
//    func getPushMsg(remoteNotificationPayload: [NSObject : AnyObject]) -> FDNotificationItem {
//
//        let msg1 = FDNotificationItem()
//
//        if let type = remoteNotificationPayload["type"] as? Int {
//            msg1.type = type
//        }
//
//        msg1.receiveDate = NSDate()
//
//        msg1.title = FDLocalized("Notification")
//
//        if let message = remoteNotificationPayload["message"] as? String {
//            msg1.message = message
//        }
//
//        return msg1
//    }

}
