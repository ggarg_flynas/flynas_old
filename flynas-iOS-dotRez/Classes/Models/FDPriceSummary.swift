//  FDPriceSummary.swift
//
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDPriceSummary: NSObject {
  
  var balanceDue : NSNumber = 0
  var hasUncommittedChanges = false
  var currencyCode = ""
  var recordLocator = ""
  var total : NSNumber = 0
  var journeyCharges = [FDJourneyCharge]()
  var bookingCharges = [FDServiceCharge]()
  var hasLoyaltyPayment = false
  var loyaltyAmount : NSNumber = 0
  var loyaltyNumber = ""
  var creditShellAmount : NSNumber = 0
  var taxAmount = ""
  var smilePointsRedeemableTotal = 0
  var remainingAmountTotal = 0.0
  var smilePointsRedeemedTotal = 0
  var smilePointsEarnedTotal = 0
  
}
