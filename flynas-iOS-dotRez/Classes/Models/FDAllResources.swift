//
//  FDAllResources.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 18/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

class FDAllResources: NSObject {
    var stations = FDStations()
    var markets = FDMarkets()
    var titles = FDTitles()
    var countries = FDCountries()
    var documentTypes = FDDocumentTypes()
    var countryPhoneCodes = FDCountryPhoneCodes()
    var mealsInfo = FDMealsInfo()
    var relationships = FDRelationships()
    var cultures = FDCultures()
    var specialServiceRequests = FDSpecialServiceRequests()
    var fees = FDFees()
    var currencies = FDCurrencies()
}

class FDStations: NSObject {
    var stations = [FDResourceStation]()
}

class FDMarkets: NSObject {
    var markets = [NSMutableDictionary]()
}

class FDTitles: NSObject {
    var titles = [NSMutableDictionary]()
}

class FDCountries: NSObject {
    var countries = [NSMutableDictionary]()

}

//class FDDocumentTypes: NSObject {
//
//}

class FDCountryPhoneCodes: NSObject {
    var countryPhoneCodes = [NSMutableDictionary]()
}
//
//class FDMealsInfo: NSObject {
//
//}
//
//class FDRelationships: NSObject {
//
//}

class FDCultures: NSObject {
    var cultures = [FDResourceString]()

}

typealias FDSpecialServiceRequestItem = FDNameCode
class FDSpecialServiceRequests: NSObject {
    var items = [FDSpecialServiceRequestItem]()
}

typealias FDFeesItem = FDNameCode
class FDFees: NSObject {
    var items = [FDFeesItem]()
}

class FDCurrencies: NSObject {
    var currencies = [FDResourceString]()
}


