//
//  FDFlightSeatMap.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 26/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum EquipmentTypeAircraftType : String {
    case Aircraft320 = "320" // default
    case Aircraft330 = "330"
    case Aircraft738 = "738"
  case Aircraft767 = "767"
  case Aircraft739 = "739"
}

enum SeatPriceType : String {
    case Business = "Business" // 0
    case ExtraLeg = "ExtraLeg" // 1
    case Premium = "Premium"   // 2
    case Standard = "Standard" // 3
  case FemaleOnly = "FemaleOnly" // 4
    case UpFront = "UpFront"   // 5
    case Simple = "Simple"   // 6
}

enum SeatStatus : String {
    case Open = "Open"
    case HeldForAnotherSession = "HeldForAnotherSession"
    case HeldForThisSession = "HeldForThisSession"
}

class FDFlightSeatMap: NSObject {

    class SeatMap: NSObject {
        class PassengerSeatInfo: NSObject {
            class PriceGroup: NSObject {
                var price : NSNumber = -1
                var type = 0
              var smilePointsRedeemable = 0
              var remainingAmount = 0.0
            }

            var passengerNumber = 0
            var passengerPriceGroups = [PriceGroup]()
            var selectedSeat : String? {
                didSet {
                    if selectedSeat == nil && userSelectedSeat != nil {
                        userSelectedSeat = nil
                    } else if selectedSeat != nil && userSelectedSeat != nil && selectedSeat != userSelectedSeat!.number {
                        // update userSelectedSeat
                        // not here, can only be updated from seatMap
                    }
                }
            }

            // buffer
            var fullName = ""
            var nameInitial = ""
            var paxType = ""
            var flightKey = ""
            var hasInfantAttached = false
            var userSelectedSeat : SeatGroup.Seat? // local cache
                {
                didSet {
                    if userSelectedSeat != nil  {
                        if selectedSeat != userSelectedSeat!.number {
                            selectedSeat = userSelectedSeat!.number
                        }
                    } else {
                        selectedSeat = nil
                    }
                }
            }

            func getPrice(_ priceGroup:Int) -> NSNumber? {
                for passengerPriceGroup in passengerPriceGroups {
                    if passengerPriceGroup.type == priceGroup {
                        return passengerPriceGroup.price
                    }
                }
                return nil
            }
          
          func getPoints(_ priceGroup:Int) -> (Double, Int)? {
            for passengerPriceGroup in passengerPriceGroups {
              if passengerPriceGroup.type == priceGroup {
                return (passengerPriceGroup.remainingAmount, passengerPriceGroup.smilePointsRedeemable)
              }
            }
            return nil
          }

            var selectedSeatNumber : String? {
                return selectedSeat
//                return userSelectedSeat?.number
            }

            var selectedSeatPrice : NSNumber? {
                if userSelectedSeat != nil {
                    return getPrice(userSelectedSeat!.priceGroup)
                } else {
                    return nil
                }
            }

            func removePassengerSeat() -> [String : Any] {
                var passengerSeats = [[String:AnyObject]]()
                if userSelectedSeat != nil {
                    let passengerSeat : [String:AnyObject] = [
                        "passengerNumber": passengerNumber as AnyObject,
                        "flightKey":flightKey as AnyObject,
                        "deck":userSelectedSeat!.deck as AnyObject,
                        "compartment":userSelectedSeat!.compartment as AnyObject,
                        "unitDesignator": "XX" as AnyObject]
                    passengerSeats.append(passengerSeat)
                    userSelectedSeat = nil
                }

                return ["flightSeatMaps": ["passengerSeatKeys":passengerSeats]]
            }

        }

        class SeatGroup: NSObject {
            class Seat: NSObject {
                var available = true
                var compartment = ""
                var infantAllowed = false
                var childAllowed = false
                var number = ""
                var status = ""
                var exitRow = false
              var isFemaleOnly = false

                // buffer
//                var type = ""
                var priceGroup = 0
                var deck = 1
                var rowNumber : Int {
                    if let n = Int(number.substring(to: number.index(number.endIndex, offsetBy: -1))) {
                        return n
                    }

                    return 0
                }

                // buffer to quick search the seat section/row in tableView
                var seatGroupIndex = 0
                var seatRowIndex = 0
            }
            var deck = 1
            var type = ""
            var seats = [Seat]()
            var priceGroup = 0

            // buffer
            var seatRows = [FDSeatRow]()
        }

        class FDSeatRow: SeatGroup {
            var rowNumber = 0
            init(deck:Int, type:String, rowNumber:Int) {
                super.init()
                self.deck = deck
                self.type = type
                self.rowNumber = rowNumber
            }
        }

        var destination = ""
        var flightKey = ""
        var origin = ""
        var fareType = ""
        var equipmentType = EquipmentTypeAircraftType.Aircraft320.rawValue  // default 320, new type 330

        var passengerSeatInfos = [PassengerSeatInfo]()
        var seatGroups = [SeatGroup]()

        // buffer
        var diretion = DestinationOption.none
        // mmb
        var changeAllowed = true

        func isAcceptFareType(_ seatGroupType:String) -> Bool {

            if seatGroupType.lowercased() == "unknown" {
                return false
            }

            // business fare only accept business seat group.
            return (fareType == PriceOption.Business.rawValue && seatGroupType == SeatPriceType.Business.rawValue) || (fareType != PriceOption.Business.rawValue && seatGroupType != SeatPriceType.Business.rawValue)
        }

        func syncSeatMap() {
            var filterSeatGroup = [SeatGroup]()
            for seatGroupIndex in 0..<seatGroups.count {
                let sg  = seatGroups[seatGroupIndex]
                if isAcceptFareType(sg.type) {
                    var rows = [FDSeatRow]()
                    for seat in sg.seats {
                        seat.deck = sg.deck
                        seat.priceGroup = sg.priceGroup
                        seat.seatGroupIndex = filterSeatGroup.count

                        let rn = seat.rowNumber
                        if let index = rows.index(where: {$0.rowNumber==rn}) {
                            seat.seatRowIndex = index
                            rows[index].seats.append(seat)
                        } else {
                            let newRow = FDSeatRow(deck: sg.deck, type: sg.type, rowNumber: rn)

                            seat.seatRowIndex = rows.count

                            newRow.seats.append(seat)
                            rows.append(newRow)
                        }

                        // update user seat
                        for passengerSeatInfo in passengerSeatInfos {
                            passengerSeatInfo.flightKey = self.flightKey
                            if passengerSeatInfo.selectedSeat != nil && passengerSeatInfo.userSelectedSeat == nil {
                                if passengerSeatInfo.selectedSeat! == seat.number {
                                    passengerSeatInfo.userSelectedSeat = seat
                                    if seat.available == false {
                                        if seat.status == SeatStatus.HeldForThisSession.rawValue {
                                            seat.available = true // re-open for edit
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if rows.count > 0 {
                        // sort seats in row
                        for row in rows {
                            row.seats.sort(by: { (first, second) -> Bool in
                                return first.number < second.number
                            })
                        }

                        sg.seatRows = rows
                        filterSeatGroup.append(sg)
                    }
                }
                // todo sort seats and rows
                //- not necessary if server return right order


            }

            seatGroups = filterSeatGroup
        }

        var seatGroupNumber : Int {
            return seatGroups.count
        }

        func getSeatGroupRowNumber(_ groupIndex:Int) -> Int {
            if groupIndex < seatGroups.count {
                return seatGroups[groupIndex].seatRows.count
            }
            return 0
        }

        func getPassengerSeatInfo(_ passengerNumber: Int) -> PassengerSeatInfo? {
            for passengerSeatInfo in passengerSeatInfos {
                if passengerSeatInfo.passengerNumber == passengerNumber || passengerSeatInfos.count == 1 {
                    return passengerSeatInfo
                }
            }
            return nil
        }

        func getGroupType(_ groupIndex:Int) -> SeatPriceType? {
            var seatType = seatGroups[groupIndex].type

            if seatType == "Standard1" {
                seatType = SeatPriceType.Standard.rawValue
            }

            if let type = SeatPriceType(rawValue:seatType) {
                return type
            } else {
                print("SeatMap - unknow group type(\(seatType)), default as Standard")
                return SeatPriceType.Standard
            }
        }

        func getGroupPrice(_ groupIndex:Int, passengerNumber:Int) -> NSNumber? {
            let passengerSeatInfo = getPassengerSeatInfo(passengerNumber)
            let p = passengerSeatInfo?.getPrice(seatGroups[groupIndex].priceGroup)
            return p
        }
      
      func getGroupPoints(_ groupIndex:Int, passengerNumber:Int) -> (Double, Int)? {
        let passengerSeatInfo = getPassengerSeatInfo(passengerNumber)
        let p = passengerSeatInfo?.getPoints(seatGroups[groupIndex].priceGroup)
        return p
      }

        func getSeatNumber(_ groupIndex:Int, rowIndex:Int, colIndex:Int) -> String {
            let seatRow = seatGroups[groupIndex].seatRows[rowIndex]
            return seatRow.seats[colIndex].number
        }

        func getSeatRowNumber(_ groupIndex:Int, rowIndex:Int) -> Int {
            if groupIndex >= 0 && groupIndex < seatGroups.count {
                if rowIndex >= 0 && rowIndex < seatGroups[groupIndex].seatRows.count {
                    let seatRow = seatGroups[groupIndex].seatRows[rowIndex]
                    if seatRow.seats.count > 0 {
                        return seatRow.seats[0].rowNumber
                    }
                }
            }

            return 0
        }

        func getSeatInfo(_ groupIndex:Int, rowIndex:Int, colIndex:Int) -> FDSeat? {

            if groupIndex >= 0 && groupIndex < seatGroups.count {
                if rowIndex >= 0 && rowIndex < seatGroups[groupIndex].seatRows.count {
                    let seatRow = seatGroups[groupIndex].seatRows[rowIndex]
                    if colIndex >= 0 && colIndex < seatRow.seats.count {
                        return seatRow.seats[colIndex]
                    }
                }
            }

//            if seatGroups[groupIndex].type == SeatPriceType.Business.rawValue {
//                if colIndex < seatRow.seats.count {
//                    return seatRow.seats[colIndex]
//                }
//            } else {
//                let seatColNumber = "\(Character(UnicodeScalar(65 + colIndex)))"
//                for seatInfo in seatRow.seats {
//                    let cn = seatInfo.number.substringFromIndex(seatInfo.number.endIndex.advancedBy(-1))
//                    if cn == seatColNumber {
//                        return seatInfo
//                    }
//                }
//            }

            return nil
        }

        func getSeatInfo(_ seatNumber: String?) -> FDSeat? {
            if let seatNumber = seatNumber {
                for sg in seatGroups {
                    for seat in sg.seats {
                        if seat.number == seatNumber {
                            return seat
                        }
                    }
                }
            }

            return nil
        }

        func getSeatUserIndex(_ seatNumber:String) -> Int {
            for passengerSeatInfo in passengerSeatInfos {
                if passengerSeatInfo.userSelectedSeat != nil && passengerSeatInfo.userSelectedSeat!.number == seatNumber {
                    return passengerSeatInfo.passengerNumber
                }
            }

            return -1
        }

        func getUserSelectedSeatPosition(_ passengerNumber:Int) -> (Int, Int)? {
            if let passengerSeatInfo = getPassengerSeatInfo(passengerNumber) {
                if passengerSeatInfo.userSelectedSeat != nil {
                    return (passengerSeatInfo.userSelectedSeat!.seatGroupIndex,passengerSeatInfo.userSelectedSeat!.seatRowIndex)
                }
            }

            return nil
        }

    }

    func getSeatMap(_ destination:String, origin:String) -> FDSeatMap? {
        for seatMap in seatMaps {
            if seatMap.destination == destination && seatMap.origin == origin {
                return seatMap
            }
        }
        return nil
    }
  
  func getSeatNumberAndPrice(_ org: String?, des: String?, priceSum: FDPriceSummary) -> (Int, Float) {
    var serviceNumber = 0
    var servicePrice : Float = 0
    for journeyCharge in priceSum.journeyCharges {
      if journeyCharge.origin == unwrap(str: org) && journeyCharge.destination == unwrap(str: des) {
        for seatCharge in journeyCharge.seatCharges {
          serviceNumber = seatCharge.chargeType == "ServiceCharge" ? serviceNumber + 1 : serviceNumber
          servicePrice += Float(seatCharge.amount)
        }
      }
    }
    return (serviceNumber, servicePrice)
  }

    func getServiceNumberAndPrice(_ origin:String?, destination:String?) -> (Int, Float) {
        var serviceNumber = 0
        var servicePrice : Float = 0
        for sm in seatMaps {
            if sm.destination == destination && sm.origin == origin {
                for ps in sm.passengerSeatInfos {
                    if let us = ps.userSelectedSeat {
                        serviceNumber += 1
                        if let pg = ps.getPrice(us.priceGroup) {
                            servicePrice += pg.floatValue
                        }
                    }
                }
            }
        }
        return (serviceNumber, servicePrice)
    }

    var allPassengerHasSeat : Bool {
        for sm in seatMaps {
            for ps in sm.passengerSeatInfos {
                if ps.selectedSeat == nil {
                    return false
                }
            }
        }

        return true
    }

    func updateUserSeatSelection(_ userSeatSelection: FDFlightSeatMap?) {
        if let u = userSeatSelection {
            for sm in u.seatMaps {
                if let seatMap = getSeatMap(sm.destination, origin: sm.origin) {
                    for ps in sm.passengerSeatInfos {
                        if let passengerSeatInfo = seatMap.getPassengerSeatInfo(ps.passengerNumber) {
                            passengerSeatInfo.selectedSeat = ps.selectedSeat
                        }
                    }
                }
            }
        }
    }

    var seatMaps = [SeatMap]()

    var passengerSeatSelectionObject : [String : AnyObject] {

        var passengerSeats = [[String:AnyObject]]()
        for seatMap in seatMaps {
            for passengerSeatInfo in seatMap.passengerSeatInfos {
                if passengerSeatInfo.userSelectedSeat != nil {
                    let passengerSeat : [String:AnyObject] = [
                        "passengerNumber": passengerSeatInfo.passengerNumber as AnyObject,
                        "flightKey":seatMap.flightKey as AnyObject,
                        "deck":passengerSeatInfo.userSelectedSeat!.deck as AnyObject,
                        "compartment":passengerSeatInfo.userSelectedSeat!.compartment as AnyObject,
                        "unitDesignator":passengerSeatInfo.userSelectedSeat!.number as AnyObject]
                    passengerSeats.append(passengerSeat)
                }
            }
        }

        return ["passengerSeatKeys":passengerSeats as AnyObject]
    }

    var isAllPassengersHavaSeat : Bool {
        for seatMap in seatMaps {
            for passengerSeatInfo in seatMap.passengerSeatInfos {
                if passengerSeatInfo.userSelectedSeat == nil {
                    return false
                }
            }
        }
        return true
    }

}

typealias FDSeat = FDFlightSeatMap.SeatMap.SeatGroup.Seat
typealias FDPassengerSeatInfo = FDFlightSeatMap.SeatMap.PassengerSeatInfo
typealias FDSeatMap = FDFlightSeatMap.SeatMap
