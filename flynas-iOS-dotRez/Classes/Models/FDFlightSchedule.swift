//
//  FDFlightSchedule.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 9/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightSchedule: NSObject {

    class FlightSchedule: NSObject {
        var arrivalTime : Date?
        var beginDate : Date?
        var departureTime : Date?
        var endDate : Date?
        var flightDesignator : String?
        var flightFrequency : String?

    }

    var arrivalStation = ""
    var departureStation = ""

    var flightSchedules = [FlightSchedule]()

}

typealias FDFlightScheduleDetail = FDFlightSchedule.FlightSchedule
