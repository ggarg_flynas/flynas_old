//
//  FDTravelDocument.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum FDTravelDocumentTypeCode : String {
    case Passport = "P"
    case NationalIDCard = "I"
    case Iqama = "IQAM"
    case OtherAirlineFrequentFlyer = "OAFF"
}

class FDTravelDocument: NSObject, NSCoding {

    var birthCountry = "SA"
    var docNumber = ""
    var docTypeCode : String = FDTravelDocumentTypeCode.Passport.rawValue {
        didSet {
            updateDocType ()
        }
    }

    var expirationDate: Date?
    var nationality = "SA"{
        didSet {
            if oldValue == birthCountry || birthCountry.isEmpty {
                birthCountry = nationality
            }

            if oldValue == docIssuingCountry || docIssuingCountry.isEmpty {
                docIssuingCountry = nationality
            }
        }
    }

    var docIssuingCountry = "SA"
    var expirationDateHijri = false

    var isDefault = false // for member profile
    var title = ""
    var first = ""
    var middle = ""
    var last = ""
    var issueDate : Date?

    override init () {
        super.init()
    }

    func copyFrom(_ travelDocument:FDTravelDocument) {
        self.birthCountry = travelDocument.birthCountry
        self.docNumber = travelDocument.docNumber
        self.docTypeCode = travelDocument.docTypeCode
        self.expirationDate = travelDocument.expirationDate
        self.nationality = travelDocument.nationality
        self.expirationDateHijri = travelDocument.expirationDateHijri
        self.isDefault = travelDocument.isDefault
        self.title = travelDocument.title
        self.first = travelDocument.first
        self.middle = travelDocument.middle
        self.last = travelDocument.last
        self.issueDate = travelDocument.issueDate
    }

    required convenience init?(coder decoder: NSCoder) {
        self.init()
        self.birthCountry = unwrap(str: decoder.decodeObject(forKey: "birthCountry"))
        self.docNumber = unwrap(str: decoder.decodeObject(forKey: "docNumber"))
        self.docTypeCode = unwrap(str: decoder.decodeObject(forKey: "docTypeCode"))
        self.expirationDate = decoder.decodeObject(forKey: "expirationDate") as? Date
        self.nationality = unwrap(str: decoder.decodeObject(forKey: "nationality"))
        self.expirationDateHijri = unwrap(bool: decoder.decodeObject(forKey: "expirationDateHijri"))
    }

    func encode(with coder: NSCoder) {
        coder.encode(self.birthCountry, forKey: "birthCountry")
        coder.encode(self.docNumber, forKey: "docNumber")
        coder.encode(self.docTypeCode, forKey: "docTypeCode")
        coder.encode(self.expirationDate, forKey: "expirationDate")
        coder.encode(self.nationality, forKey: "nationality")
        coder.encode(self.expirationDateHijri, forKey: "expirationDateHijri")
    }


//    var docTypeString : String {
//        let index = FDResourceManager.sharedInstance.docTypesCode.indexOf(docTypeCode)
//
//        return index != nil ? FDResourceManager.sharedInstance.docTypes[index!] : docTypeCode
//    }

    var travelDocumentInput : [String:AnyObject] {
        var travelDocumentInput = [
                "docNumber": docNumber,
                "docTypeCode": docTypeCode,
                "birthCountry": birthCountry,
                "nationality": nationality,
                "docIssuingCountry": docIssuingCountry
            ]

        if hideExpire {
            travelDocumentInput["expirationDate"] = "2026-01-01"
        } else {
            travelDocumentInput["expirationDate"] = expirationDate != nil ? expirationDate!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false, utcTime: false) : ""
        }

        return travelDocumentInput as [String : AnyObject]
    }

    var validateDocumentNumber : ValidateErrorCode {
        if docNumber.isEmpty {
            return .docNumberMissing
        } else if docTypeCode == FDTravelDocumentTypeCode.Passport.rawValue {
            if !docNumber.isAlphanumeric {
                return .notValidatePassportNumber
            }
        }
        return .none
    }

    var validateDocument : ValidateErrorCode {
        if docTypeCode.isEmpty {
            return .docTypeCodeMissing
        } else if docNumber.isEmpty {
            return .docNumberMissing
//        } else if nationality.isEmpty {
//            return .NationalityMissing
        } else if docIssuingCountry.isEmpty && docTypeCode == FDTravelDocumentTypeCode.Passport.rawValue {
                return .issuingCountryMissing
        } else if birthCountry.isEmpty {
//            return .BirthCountryMissing
        } else if docTypeCode == FDTravelDocumentTypeCode.Passport.rawValue {
            if !docNumber.isAlphanumeric {
                return .notValidatePassportNumber
            } else if expirationDate == nil {
                return .expirationDateMissing
            }
        } else if docTypeCode == FDTravelDocumentTypeCode.NationalIDCard.rawValue {
//            if !docNumber.is10Alphanumeric {
//                return .NotValidateIqamaNumber
//            }
        } else if docTypeCode == FDTravelDocumentTypeCode.Iqama.rawValue {
//            if !docNumber.is10Alphanumeric {
//                return .NotValidateNationalIDCard
//            }
        } else if docTypeCode == FDTravelDocumentTypeCode.OtherAirlineFrequentFlyer.rawValue {
        }

        return .none
    }

    func updateDocType () {
        if docTypeCode.isEmpty {

        } else if docTypeCode == FDTravelDocumentTypeCode.Passport.rawValue {
            expirationDateHijri = false
        }
    }

    var hideIssuingCountry : Bool {
        return docTypeCode != FDTravelDocumentTypeCode.Passport.rawValue
    }

    var hideExpire : Bool {
        return docTypeCode != FDTravelDocumentTypeCode.Passport.rawValue
    }

    var hideHijiri : Bool {
        return docTypeCode == FDTravelDocumentTypeCode.Passport.rawValue || hideExpire
    }

}
