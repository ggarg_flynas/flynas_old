//
//  FDWebCheckinManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 16/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import RealmSwift

class FDWebCheckinManager: FDCommonApiManager {

  static let sharedInstance = FDWebCheckinManager()

    let realm = try! Realm()

    func getBooking (_ pnr:String, departureStation:String?, email:String?, lastName:String?, success:@escaping (_ wciBooking: FDWciBooking) -> Void, fail: @escaping (String?) -> Void) {

        FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
            let operation = FDdotRezAPIManager.operationRetrieveBookingWci(pnr, departureStation: departureStation, email:email, lastName:lastName)

            operation.setCompletionBlockWithSuccess({ (operation, results) in

                let operationBooking = FDdotRezAPIManager.operationWciBooking()
                operationBooking.setCompletionBlockWithSuccess({ (operation, results) in
                  if  let wciBooking: FDWciBooking = self.getResultObject(results?.array()) {
                    success(wciBooking)
                    } else {
                        fail(FDErrorHandle.API_EMPTY_RES)
                    }

                    }, failure: { (operation, error) in
                        self.handleError(operation!, error: error! as NSError, fail: fail)
                })
                operationBooking.start()

                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()

            }) { (r) -> Void in
                fail(r)
        }

    }

    func checkinPassenger (_ checkinPassengers:[AnyHashable: Any], success:@escaping () -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
                success()
        #else

                let operation = FDdotRezAPIManager.operationWciCheckinPassengers(checkinPassengers)

                operation.setCompletionBlockWithSuccess({ (operation, results) in
                    success()
                    }, failure: { (operation, error) in
                        self.handleError(operation!, error: error! as NSError, fail: fail)
                })
                operation.start()

        #endif
    }

    func passengerDetails (_ checkinPassengers:[AnyHashable: Any]?, success:@escaping (_ wciPassengerDetails:FDWciPassengerDetails) -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
            success()
        #else

            let operation = FDdotRezAPIManager.operationWciPassengerDetails(checkinPassengers)

            operation.setCompletionBlockWithSuccess({ (operation, results) in
              if  let wciPassengerDetails:FDWciPassengerDetails = self.getResultObject(results?.array()) {
                success(wciPassengerDetails)
                } else {
                    fail(FDErrorHandle.API_EMPTY_RES)
                }
                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()

        #endif
    }

    func passengerSeats (_ checkinPassengers:[AnyHashable: Any]?, success:@escaping (_ wsiFlightSeatMap:FDFlightSeatMap) -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
            success()
        #else

            let operation = FDdotRezAPIManager.operationWciSeats(checkinPassengers)

            operation.setCompletionBlockWithSuccess({ (operation, results) in
              if  let wsiFlightSeatMap:FDFlightSeatMap = self.getResultObject(results?.array()) {
                success(wsiFlightSeatMap)
                } else {
                    fail(FDErrorHandle.API_EMPTY_RES)
                }
                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()
            
        #endif
    }

    func passengerAutoSeats (_ success:@escaping (_ wsiFlightSeatMap:FDFlightSeatMap) -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
            success()
        #else

            let operation = FDdotRezAPIManager.operationWciAutoSeats()

            operation.setCompletionBlockWithSuccess({ (operation, results) in
              if  let wsiFlightSeatMap:FDFlightSeatMap = self.getResultObject(results?.array()) {
                success(wsiFlightSeatMap)
                } else {
                    fail(FDErrorHandle.API_EMPTY_RES)
                }
                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()
        #endif
    }

    func checkIn (_ checkinPassengers:[AnyHashable: Any]?, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {

        #if DEMO_DOTREZ
            success()
        #else

            let operation = FDdotRezAPIManager.operationWciCheckIn(checkinPassengers)

            operation.setCompletionBlockWithSuccess({ (operation, results) in
//                if  let wsiFlightSeatMap:FDFlightSeatMap = self.getResultObject(results.array()) {
                    success()
//                } else {
//                    fail(FDdotRezAPIManager.API_EMPTY_RES)
//                }
                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()

        #endif
    }

    func removeCache () {
        let bps = self.realm.objects(FDBoardingPasses)
        if bps.count > 0 {
            try! self.realm.write {
                self.realm.delete(bps)
            }
        }
    }

    func getBoardingPasses (_ pnr:String, departureStation:String?, success:@escaping (_ boardingPasses: FDBoardingPasses) -> Void, fail: @escaping (String?) -> Void) {

        FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
            let operation = FDdotRezAPIManager.operationBoardingPasses(pnr)

            operation.setCompletionBlockWithSuccess({ (operation, results) in

              if  let boardingPasses: FDBoardingPasses = self.getResultObject(results?.array()) {

                    boardingPasses.syncMemToRealm()
//                    self.removeCache()
                    try! self.realm.write {
                        self.realm.add(boardingPasses)
                    }
                success(boardingPasses)
                } else {
                    fail(FDErrorHandle.API_EMPTY_RES)
                }

                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()
            
            }) { (r) -> Void in
                fail(r)
        }
    }

    func getWciBoardingPasses (_ pnr:String, departureStation:String?, success:@escaping (_ boardingPasses: FDBoardingPasses) -> Void, fail: @escaping (String?) -> Void) {


        #if DEMO_DOTREZ
            let results = loadLocalJsonAsResponse(FDdotRezAPIManager.operationWciBooking(pnr, departureStation: departureStation), fileName: "WciBooking")

            if  let wciBooking: FDWciBooking = self.getResultObject(results.array()) {
                success(wciBooking: wciBooking)
            }else {
                fail(FDdotRezAPIManager.API_EMPTY_RES)
            }
        #else

        FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
            let operation = FDdotRezAPIManager.operationWciBoardingPasses(pnr)

            operation.setCompletionBlockWithSuccess({ (operation, results) in

              if  let boardingPasses: FDBoardingPasses = self.getResultObject(results?.array()) {

                    boardingPasses.syncMemToRealm()
//                    self.removeCache()
                    try! self.realm.write {
                        self.realm.add(boardingPasses)
                    }
                success(boardingPasses)
                } else {
                    fail(FDErrorHandle.API_EMPTY_RES)
                }

                }, failure: { (operation, error) in
                    self.handleError(operation!, error: error! as NSError, fail: fail)
            })
            operation.start()

            }) { (r) -> Void in
                fail(r)
        }

        #endif
    }

  func getWciPassbookPassData (_ passBookParameters:[AnyHashable: Any], success:@escaping (_ boardingPassData: Data) -> Void, fail: @escaping (String?) -> Void) {
    
    //        let requestOperation = FDdotRezAPIManager.operationPassbook(passBookParameters)
    let routePath = "api/passbook"
    let routeMethod = RKRequestMethod.POST
    
    let manager = RKObjectManager.shared()
    let request = manager?.request(with: nil, method: routeMethod, path: routePath, parameters: passBookParameters)
    
    let requestOperation = AFHTTPRequestOperation(request: request as URLRequest!)
    requestOperation?.setCompletionBlockWithSuccess({ (operation, result) -> Void in
      if let data = operation?.responseData {
        success(data)
      } else {
        fail(FDErrorHandle.API_GENERAL_ERROR)
      }
    }) { (operation, error) -> Void in
      self.handleErrorWithHttpRequestOperation(operation!, error: error! as NSError, fail: fail)
    }
    manager?.httpClient.enqueue(requestOperation)
  }

    func getCachedBoardingPasses() -> FDBoardingPasses? {
        let boardingPasses = realm.objects(FDBoardingPasses)
        if boardingPasses.count > 0 {
            if let bp = boardingPasses.first , let lastFlightTime = bp.lastFlightTime, Date().isEarlierThanDate(lastFlightTime.dateByAddingHours(6))
            {
                bp.syncRealmToMem()
                return bp
            } else {
//                self.removeCache()
                try! self.realm.write {
                    self.realm.delete(boardingPasses)
                }
            }
        }
        return nil
    }
}
