//
//  FDBookingManager.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum DestinationOption : Int {
  case none = 0
  case depart = 3
  case arrive
}

class FDBookingManager: FDCommonApiManager {
  
  static let sharedInstance = FDBookingManager()
  
  override init() {
    super.init()
  }
  
  func searchFlight( _ originCode: String, destinationCode:String, departureDate:Date, returnDate:Date?, adultCount:Int, childCount:Int, infantCount:Int, currencyCode:String, isChangeFlow:Bool, promoCode:String, success:@escaping (_ flightsAvailability: FDFlightsAvailability?, _ lowFareAvailability: FDLowFareAvailability?) -> Void, fail: @escaping (String?) -> Void) {
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      var flights = [
        ["origin":originCode,"destination":destinationCode,"date":departureDate.toString(format: .custom("yyyy-MM-dd"), applyLocele: false, utcTime: true)]
      ]
      if (returnDate != nil) {
        flights.append(["origin":destinationCode,"destination":originCode,"date":returnDate!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false, utcTime: true)])
      }
      var parameters = ["flightSearch":
        [ "flights":flights,
          "adultCount":adultCount,
          "childCount":childCount,
          "infantCount":infantCount,
          "promoCode": promoCode,
          "selectedCurrencyCode":currencyCode]
      ]
      if isChangeFlow {
        parameters = ["flightSearch":
          [   "flights":flights,
            "isChangeFlow":true,
            "selectedCurrencyCode":currencyCode]
        ]
      }
      let operation = FDdotRezAPIManager.operationSearch(parameters)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        let flightsAvailability: FDFlightsAvailability? = self.getResultObject(results?.array())
        let lowFareAvailability: FDLowFareAvailability? = self.getResultObject(results?.array())
        success(flightsAvailability, lowFareAvailability)
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
    }) { (errorString) -> Void in
      fail(errorString)
    }
  }
  
  func flightAdd (_ flightFareKeys:[String], success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    
    #if DEMO_DOTREZ
      success()
    #else
      
      let parameters = ["marketSell":
        ["keys": flightFareKeys]
      ]
      
      let operation = FDdotRezAPIManager.operationFlightAdd(parameters)
      
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        print("FlightAdd - OK")
        
        success()
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func passengerAdd (_ passengers:[AnyHashable : Any]?, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    
    #if DEMO_DOTREZ
      
      success()
      
    #else
      
      let operation = FDdotRezAPIManager.operationPassengersContact(passengers)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        success()
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func bookingContacts (_ passengers:[AnyHashable: Any]?, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    
    #if DEMO_DOTREZ
      
      success()
      
    #else
      
      let operation = FDdotRezAPIManager.operationBookingContacts(passengers)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        success()
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func bookingPassengers(_ passengers: [AnyHashable: Any]?, success:@escaping (_ passengersInput: FDPassengersInput?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationBookingPassengers(passengers)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let result : FDPassengersInput? = self.getResultObject(results?.array())
      success(result)
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func priceSummary (_ flightFareKeys:[String]?, success:@escaping (_ priceSummary: FDPriceSummary) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationPriceSummary(flightFareKeys)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        success(results?.firstObject() as! FDPriceSummary)
        }, failure: { (operation, error) in
          print("Error: " + unwrap(str: error?.localizedDescription))
          fail (error?.localizedDescription)
      })
      operation.start()
  }
  
  func getLounge(_ params: [String: Any]?, remove: Bool, success: @escaping (_ lounge: FDLoungeSsr?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationLounge(params, remove: remove)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      success(results?.firstObject() as? FDLoungeSsr)
    }) { (operation, error) in
      self.handleError(operation!, error: error! as NSError, fail: fail)
    }
    operation.start()
  }
  
  func seatMap (_ seatSelected:[AnyHashable: Any]?, success:@escaping (_ flightSeatMap:FDFlightSeatMap?, _ error:String?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationSeats(seatSelected)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        let resultArray = results?.array()
        let result : FDFlightSeatMap? = self.getResultObject(resultArray)
        let errorMsg = self.getErrorMessage(resultArray)
        success(result, errorMsg)
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
  }
  
  func getBaggage (_ baggageSelected:[AnyHashable: Any]?, delete:Bool,  success:@escaping (_ baggage: FDBaggageSsr?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationBaggage(baggageSelected, delete: delete)
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        success(results?.firstObject() as? FDBaggageSsr)
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
  }
  
  func getSportsEquipment(_ params: [String: AnyObject]?, delete: Bool, success: @escaping (_ sports: FDSportsSsr?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationSports(params, delete: delete)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      success(results?.firstObject() as? FDSportsSsr)
    }) { (operation, error) in
      self.handleError(operation!, error: error! as NSError, fail: fail)
    }
    operation.start()
  }
  
  func getMeal (_ mealSelected:[AnyHashable: Any]?, delete:Bool = false,  success:@escaping (_ meal: FDMealSsr?) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationMeal(mealSelected, delete: delete)
    
    #if DEMO_DOTREZ
      let result = loadLocalJsonAsResponse("Meal")
      success(priceSummary: results.firstObject() as! FDMealSsr)
    #else
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        success(results?.firstObject() as? FDMealSsr)
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func getPaymentMethods (_ success:@escaping (_ paymentMethods: FDAvailablePaymentMethods) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationPaymentMethods()
    
    #if DEMO_DOTREZ
      let result = loadLocalJsonAsResponse("PaymentMethods")
      success(priceSummary: results.firstObject() as! NSMutableDictionary)
    #else
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        success(results!.firstObject() as! FDAvailablePaymentMethods)
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func getErrorMessage(_ results:[Any]?) -> String? {
    
    var errorMessage : String?
    
    if let resultArray = results {
      for i in 0..<resultArray.count {
        if (resultArray[i] as AnyObject).isKind(of: FDError.self) {
          let error = resultArray[i] as! FDError
          if errorMessage == nil {
            errorMessage = error.errorMessage
          } else {
            errorMessage! += error.errorMessage + "\n"
          }
        }
      }
    }
    
    return errorMessage
  }
  
  func makePayments(_ paymentDetail:FDPayment, success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    
    #if DEMO_DOTREZ
      success()
    #else
      
      let parameters = ["creditCardPayment":
        ["payment":[
          "code" : paymentDetail.code,
          "cardHolderName" : paymentDetail.accountName,
          "cardNumber" : paymentDetail.accountNumber,
          "expiryDate" : paymentDetail.expiryDate,
          "verificationCode" : paymentDetail.verificationCode
          ]
        ]
      ]
      
      let operation = FDdotRezAPIManager.operationPayments(parameters)
      
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        let resultArray = results?.array()
        let result : NSMutableDictionary?  = self.getResultObject(resultArray)
        
        if result != nil {
          if let paymentResult = result!["result"] {
            success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
          }
        } else {
          fail(FDErrorHandle.API_EMPTY_RES)
        }
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func makeSADADPayments(_ success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    let parameters = ["sadadPayment":
      ["code":""]
    ]
    let operation = FDdotRezAPIManager.operationPayments(parameters)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let result : NSMutableDictionary?  = self.getResultObject(resultArray)
      
      if result != nil {
        if let paymentResult = result!["result"] {
          success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func makeSadadOLPPayments(olpId: String, returnPage: String, _ success:@escaping (_ paymentResult: FDSadadOnlinePaymentData?) -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationSadadOLP(olpId: olpId, returnPage: returnPage)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      success(results?.firstObject() as? FDSadadOnlinePaymentData)
    }) { (operation, error) in
      self.handleError(operation!, error: error! as NSError, fail: fail)
    }
    operation.start()
  }
  
  func makeApplePayPayments(_ item: APStruct, success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    let myValue = item.paymentData.ephemeralPublicKey.isEmpty ? item.paymentData.wrappedKey : item.paymentData.ephemeralPublicKey
    let parameters = ["applePay": [
      "request": [
        "digitalWallet":           "APPLE_PAY",
        "appleData":               item.paymentData.data,
        "appleSignature":          item.paymentData.signature,
        "appleTransactionId":      item.paymentData.transactionId,
        "appleEphemeralPublicKey": myValue,
        "applePublicKeyHash":      item.paymentData.publicKeyHash,
        "appleDisplayName":        item.apple_displayName,
        "appleNetwork":            item.apple_network,
        "appleType":               item.apple_type,
        "appleVersion":            item.paymentData.version,
        "orderDescription":        "",
        "customerIp":              IPAddressHelper.getWebIP(),
        "customerName":            ""
      ]
      ]
    ]
    // DONTFORGET
    //    let asd = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
    //    let str = String(data: asd, encoding: NSUTF8StringEncoding)
    //    UIAlertView.showWithTitle("Pay", message: str, cancelButtonTitle: "copy") { (alertView, buttonIndex) in
    //      UIPasteboard.generalPasteboard().string = str
    //    }
    let operation = FDdotRezAPIManager.operationPayments(parameters)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let result : NSMutableDictionary?  = self.getResultObject(resultArray)
      if result != nil {
        if let paymentResult = result!["result"] {
          success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func getBookingPolling (_ success:@escaping (_ bookingPolling: FDBookingPolling) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationBookingPolling()
    
    #if DEMO_DOTREZ
      let result = loadLocalJsonAsResponse("BookingPolling")
      success(bookingPolling: results.firstObject() as! FDBookingPolling)
    #else
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        success(results!.firstObject() as! FDBookingPolling)
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  
  
  func getBookingDetail (_ submitPayment: Bool = false, success:@escaping (_ bookingDetails: FDBookingDetails, _ bookingPayments: FDBookingPayments?, _ bookingFlights : FDBookingFlights?, _ contactInput : FDContactInput?, _ passengersInput : FDPassengersInput?, _ baggageSsr : FDBaggageSsr?,_ mealSsr : FDMealSsr?, _ sportSsr: FDSportsSsr?, _ flightSeatMap : FDFlightSeatMap? ) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationBooking(submitPayment)
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      
      if  let bookingDetails : FDBookingDetails = self.getResultObject(results?.array()) {
        let bookingPayments : FDBookingPayments? = self.getResultObject(results?.array())
        
        let bookingFlights : FDBookingFlights? = self.getResultObject(results?.array())
        
        let contactInput : FDContactInput? = self.getResultObject(results?.array())
        let passengersInput : FDPassengersInput? = self.getResultObject(results?.array())
        
        let baggageSsr : FDBaggageSsr? = self.getResultObject(results?.array())
        let mealSsr : FDMealSsr? = self.getResultObject(results?.array())
        let sportSsr: FDSportsSsr? = self.getResultObject(results?.array())
        let flightSeatMap : FDFlightSeatMap? = self.getResultObject(results?.array())
        
        // workaround for GDC and codeshare
        if let bookingFlights = bookingFlights {
          
          if bookingDetails.isGdsBooking {
            for j in bookingFlights.journeys {
              j.cancellationAllowed = false
              j.changeAllowed = false
            }
          } else {
            for j in bookingFlights.journeys {
              if j.isCodeShare {
                j.cancellationAllowed = false
                j.changeAllowed = false
              }
            }
          }
        }
        
        success(bookingDetails, bookingPayments, bookingFlights, contactInput, passengersInput, baggageSsr, mealSsr, sportSsr, flightSeatMap)
        
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func getDocumentTypes (_ success:@escaping (_ documentTypes: FDDocumentTypes) -> Void, fail: @escaping (String?) -> Void) {
    
    FDRM.sharedInstance.checkSessionToken({ (_) -> Void in
      // culture code parameter
      let parameters = ["cultureCode":FDResourceManager.sharedInstance.currentCulture]
      let operation = FDdotRezAPIManager.operationDocumentTypes(parameters)
      
      #if DEMO_DOTREZ
        let result = loadLocalJsonAsResponse("DocumentTypes")
        success(bookingPolling: results.firstObject() as! FDBookingPolling)
      #else
        operation.setCompletionBlockWithSuccess({ (operation, results) in
          
          if  let documentTypes : FDDocumentTypes = self.getResultObject(results?.array()) {
            success(documentTypes)
          } else {
            fail(FDErrorHandle.API_EMPTY_RES)
          }
          
          }, failure: { (operation, error) in
            self.handleError(operation!, error: error! as NSError, fail: fail)
        })
        operation.start()
        
      #endif
    }) { (r) -> Void in
      fail(r)
    }
    
  }
  
  func getMeals (_ success:@escaping (_ mealsInfo: FDMealsInfo) -> Void, fail: @escaping (String?) -> Void) {
    
    // culture code parameter
    let parameters = ["cultureCode":FDResourceManager.sharedInstance.currentCulture]
    let operation = FDdotRezAPIManager.operationMeals(parameters)
    
    #if DEMO_DOTREZ
      let result = loadLocalJsonAsResponse("Meals")
      success(bookingPolling: results.firstObject() as! FDBookingPolling)
    #else
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        if  let mealsInfo : FDMealsInfo = self.getResultObject(results?.array()) {
          success(mealsInfo)
        } else {
          fail(FDErrorHandle.API_EMPTY_RES)
        }
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    #endif
  }
  
  func getFlightSchedule (_ departureStation:String, arrivalStation:String?, success:@escaping (_ flightSchedule: FDFlightSchedule) -> Void, fail: @escaping (String?) -> Void) {
    
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      let operation = FDdotRezAPIManager.operationFlightSchedule(departureStation, arrivalStation: arrivalStation)
      
      #if DEMO_DOTREZ
        let result = loadLocalJsonAsResponse("FlightSchedule")
        success(bookingPolling: results.firstObject() as! FDBookingPolling)
      #else
        operation.setCompletionBlockWithSuccess({ (operation, results) in
          
          if  let flightSchedule : FDFlightSchedule = self.getResultObject(results?.array()) {
            success(flightSchedule)
          } else {
            fail(FDErrorHandle.API_EMPTY_RES)
          }
          
          }, failure: { (operation, error) in
            self.handleError(operation!, error: error! as NSError, fail: fail)
        })
        operation.start()
        
      #endif
      
    }) { (r) -> Void in
      fail(r)
    }
    
  }
  
  func getFlightStatus (_ flightNumber:String, success:@escaping (_ flightStatus: FDFlightStatus) -> Void, fail: @escaping (String?) -> Void) {
    
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      
      let date = Date().dateByAddingDays(-2)
      let formatter = DateFormatter()
      formatter.dateFormat = "MM/dd/yyyy"
      formatter.timeZone = TimeZone(abbreviation: "GST")
      print(formatter.string(from: date))
      
      //"11/07/2015"
      let parameters = ["flightDate":formatter.string(from: date), "flightNumber" : flightNumber]
      
      let operation = FDdotRezAPIManager.operationFlightStatus(parameters)
      
      #if DEMO_DOTREZ
        let result = loadLocalJsonAsResponse("Flightstatus")
        success(bookingPolling: results.firstObject() as! FDBookingPolling)
      #else
        operation.setCompletionBlockWithSuccess({ (operation, results) in
          
          if  let flightStatus : FDFlightStatus = self.getResultObject(results?.array()) {
            success(flightStatus)
          } else {
            fail(FDErrorHandle.API_EMPTY_RES)
          }
          
          }, failure: { (operation, error) in
            self.handleError(operation!, error: error! as NSError, fail: fail)
        })
        operation.start()
        
      #endif
      
    }) { (r) -> Void in
      fail(r)
    }
  }
  
  
  func getBooking (_ pnr:String, departureStation:String?, email:String?, lastName:String?, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      let operation = FDdotRezAPIManager.operationRetrieveBooking(pnr, phone: nil, email:email, lastName:lastName)
      
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        success()
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    }) { (r) -> Void in
      fail(r)
    }
  }
  
  func cancelFlights (_ flightIndexs:[Int], success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      let operation = FDdotRezAPIManager.operationCancelFlights(flightIndexs)
      
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        success()
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    }) { (r) -> Void in
      fail(r)
    }
  }
  
  func makeCreditShellPayments(_ success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    
    let parameters = ["creditShellPayment":
      ["canCreateShell":true
      ]
    ]
    
    let operation = FDdotRezAPIManager.operationPayments(parameters)
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let result : NSMutableDictionary?  = self.getResultObject(resultArray)
      
      if result != nil {
        if let paymentResult = result!["result"] {
          success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func retriveBookingPassengers(_ success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationBookingPassengers()
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      success()
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func makeRefundMixedPayment(_ success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationRefundMixedPayment()
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let paymentResult : FDPaymentResult?  = self.getResultObject(resultArray)
      
      if let paymentResult = paymentResult {
        if paymentResult.balanceDue == 0 {
          success(paymentResult, "")
        } else {
          fail(FDErrorHandle.API_REFUND_FAIL)
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func getCreditFileInfo (_ success:@escaping (_ creditFileInfo: FDCreditFileInfo) -> Void, fail: @escaping (String?) -> Void) {
    
    let operation = FDdotRezAPIManager.operationCreditFile()
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      
      if let creditFileInfo = self.getResultObject(results?.array()) as FDCreditFileInfo? {
        success(creditFileInfo)
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
    
  }
  
  func removePayment (_ paymentNumber: Int, success:@escaping () -> Void, fail: @escaping (String?) -> Void) {
    let operation = FDdotRezAPIManager.operationRemovePayment(paymentNumber)
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      
      success()
      
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func makeCFPayments(_ amount: Double, success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    let parameters = ["creditFilePayment":
      ["paymentAmount": String(format: "%.2f", amount)]
    ]
    
    let operation = FDdotRezAPIManager.operationPayments(parameters)
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let result : NSMutableDictionary?  = self.getResultObject(resultArray)
      
      if result != nil {
        if let paymentResult = result!["result"] {
          success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func makeNSPayments(_ amount: Float, nasMemberAccountId: String, success:@escaping (_ paymentResult: FDPaymentResult, _ errorMessage:String?) -> Void, fail: @escaping (String?) -> Void) {
    
    let parameters = ["loyaltyPayment":
      ["redeemAmount": amount,
        "NasEmailAddress": nasMemberAccountId]
    ]
    
    let operation = FDdotRezAPIManager.operationPayments(parameters)
    
    operation.setCompletionBlockWithSuccess({ (operation, results) in
      let resultArray = results?.array()
      let result : NSMutableDictionary?  = self.getResultObject(resultArray)
      
      if result != nil {
        if let paymentResult = result!["result"] {
          success(paymentResult as! FDPaymentResult, self.getErrorMessage(resultArray))
        }
      } else {
        fail(FDErrorHandle.API_EMPTY_RES)
      }
      }, failure: { (operation, error) in
        self.handleError(operation!, error: error! as NSError, fail: fail)
    })
    operation.start()
  }
  
  func cacheLowFare (_ dates:[Date], success:@escaping (_ lowFareAvailability: FDLowFareAvailability?) -> Void, fail: @escaping (String?) -> Void) {
    
    FDResourceManager.sharedInstance.checkSessionToken({ (_) -> Void in
      
      var ds = [String]()
      
      for date in dates {
        ds.append(date.toString(format: .custom("yyyy-MM-dd"), applyLocele: false, utcTime: true))
      }
      
      let parameters = ["changedDatesFlightSearch":
        ["dates":ds]
      ]
      
      let operation = FDdotRezAPIManager.operationLowFareFlightSearch(parameters)
      
      operation.setCompletionBlockWithSuccess({ (operation, results) in
        
        let lowFareAvailability : FDLowFareAvailability? = self.getResultObject(results?.array())
        
        success(lowFareAvailability)
        
        
        }, failure: { (operation, error) in
          self.handleError(operation!, error: error! as NSError, fail: fail)
      })
      operation.start()
      
    }) { (errorString) -> Void in
      fail(errorString)
    }
  }
  
}
