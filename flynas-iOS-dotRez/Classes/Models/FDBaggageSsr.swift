//
//  FDBaggageSsr.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDBaggageSsr: NSObject {
    var passengerServices = [FDPassengerService]()

    func getPassengerService(_ origin:String?, destination:String?, passengerNumber:Int) -> FDFlightPartService? {
        for ps in passengerServices {
            if ps.passengerNumber == passengerNumber {
                if let fs = ps.getFlightPartService(origin, destination: destination) {
                    return fs
                }
            }
        }
        return nil
    }

  func getBaggageServiceNumberAndPrice(_ origin:String?, destination:String?) -> (Int, Float) {
    var serviceNumber = 0
    var servicePrice : Float = 0
    for ps in passengerServices {
      for fs in ps.flightPartServices {
        if fs.origin == origin && fs.destination == destination {
          for item in fs.getSelectedBaggageItem() {
            serviceNumber += item.count
            servicePrice += item.price.floatValue * Float(item.count)
          }
        }
      }
    }
    return (serviceNumber, servicePrice)
  }
  
  func getMealServiceNumberAndPrice(_ origin:String?, destination:String?) -> (Int, Float) {
    var serviceNumber = 0
    var servicePrice : Float = 0
    for ps in passengerServices {
      for fs in ps.flightPartServices {
        if fs.origin == origin && fs.destination == destination {
          for item in fs.getSelectedBaggageItem() {
            let bundleMealCode = unwrap(str: fs.serviceInBundle?.firstObject)
            let isBundleMeal = item.code == bundleMealCode
            if !isBundleMeal {
              serviceNumber += item.count
              servicePrice += item.price.floatValue * Float(item.count)
            }
          }
        }
      }
    }
    return (serviceNumber, servicePrice)
  }
  
  func getSportsServiceNumberAndPrice(_ origin:String?, destination:String?) -> (Int, Float) {
    var serviceNumber = 0
    var servicePrice : Float = 0
    for ps in passengerServices {
      for fs in ps.flightPartServices {
        if fs.origin == origin && fs.destination == destination {
          for item in fs.getSelectedSportsItems() {
            serviceNumber += item.count
            servicePrice += item.price.floatValue * Float(item.count)
          }
        }
      }
    }
    return (serviceNumber, servicePrice)
  }
  
  func getLoungeServiceNumberAndPrice(_ origin:String?, destination:String?) -> (Int, Float) {
    var serviceNumber = 0
    var servicePrice : Float = 0
    for ps in passengerServices {
      for fs in ps.flightPartServices {
        if fs.origin == origin && fs.destination == destination {
          for item in fs.getSelectedLoungeItems() {
            serviceNumber += item.count
            servicePrice += item.price.floatValue * Float(item.count)
          }
        }
      }
    }
    return (serviceNumber, servicePrice)
  }
  
  func needUserAction() -> Bool {
        for ps in passengerServices {
            for fp in ps.flightPartServices {
                if fp.services.count > 0 {
                    return true
                }
            }
        }
        return false
    }

    func hasServiceSelected() -> Bool {
        for ps in passengerServices {
            for fp in ps.flightPartServices {
              return fp.getSelectedBaggageItem().count > 0
            }
        }
        return false
    }
}
