//
//  FDFare.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 1/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import RealmSwift

enum PriceOption : String {
  case none = "none"
  case Economy = "Economy"
  case Business = "Business"
  case StaffStandby = "StaffStandby"
  case StaffConfirm = "StaffConfirm"

  static func GetPriceOption(_ fareType:String) -> PriceOption {
    switch fareType {
    case PriceOption.Economy.rawValue:
      return .Economy
    case PriceOption.Business.rawValue:
      return .Business
    case PriceOption.StaffStandby.rawValue:
      return .StaffStandby
    case PriceOption.StaffConfirm.rawValue:
      return .StaffConfirm
    default:
      return .none
    }
  }
}

class FDFare: Object {
    dynamic var fareKey : String?
    dynamic var fareType : String = "none"
    dynamic var price : Float = 0
    dynamic var remainingSeats : Int = 0
    dynamic var promoFare = false
  var bundleSet: FDBundleSet?
  dynamic var smilePointsEarned: Int = 0
  dynamic var smilePointsRedeemable: Int = 0
  dynamic var remainingAmount: Float = 0.0
}

class FDBundleSet: Object {
  dynamic var bundleSetCode: String?
  var bundleOffers = List<FDBundleOffers>()
}

class FDBundleOffers: Object {
  dynamic var bundleCode: String?
  dynamic var bundleType: String?
  dynamic var paxType: String?
  dynamic var price: Float = 0
  dynamic var programCode: String?
  dynamic var programLevel: String?
  var ssrPriceList = List<FDSsrPriceList>()
  dynamic var taxTotal: Float = 0
  dynamic var smilePointsEarnedTotal: Int = 0
  dynamic var smilePointsRedeemableTotal: Int = 0
  dynamic var remainingAmountTotal: Float = 0.0
}

class FDSsrPriceList: Object {
  dynamic var bundleSSRType: String?
  dynamic var price: Float = 0
  dynamic var ssrCode: String?
  dynamic var taxTotal: Float = 0
}
