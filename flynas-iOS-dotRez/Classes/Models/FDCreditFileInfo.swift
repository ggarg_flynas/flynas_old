//
//  FDCreditFileInfo.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 6/09/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import Foundation

class FDCreditFileInfo: NSObject {

    var amountAlreadyUsed:NSNumber = 0
    var availableAmount:NSNumber = 0
    var creditFileAvailable = false
}