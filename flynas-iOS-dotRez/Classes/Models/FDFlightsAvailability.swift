//
//  FDFlightsAvailability.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 1/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDFlightsAvailability: NSObject {
    var currencyCode : String?
    var trips : [FDTrip] = []
}

class FDLowFareAvailability: NSObject {

    class TripFare: NSObject {

        class LowFare: NSObject {
            static let LoadingPrice : Float = -1
            var price : Float = 0
            var date : Date?
        }


        var origin : String?
        var destination : String?
        var dates : [LowFare] = []

        func updateLowFare(_ lowFare: LowFare) {
            if let dateNew = lowFare.date {
                for i in 0..<dates.count {
                    let fareOld = dates[i]
                    if let dateOld = fareOld.date {
                        if dateOld.isEqualToDateIgnoringTime(dateNew) {
                            // update
                            fareOld.price = lowFare.price
                            return
                        } else if dateNew.isEarlierThanDate(dateOld) {
                            // inert to first
                            dates.insert(lowFare, at: i)
                            return
                        }
                    }
                }

                // append to last
                dates.append(lowFare)
            }
        }

        func getNextCacheDate() -> Date {
            if let date = dates.last?.date {
                return date.dateByAddingDays(1)
            }
            return Date()
        }
    }


    var currencyCode : String?
    var trips : [TripFare] = []

    func getTrip(_ origin:String?, destination:String?) -> TripFare? {
        for trip in trips {
            if trip.origin == origin && trip.destination == destination {
                return trip
            }
        }

        return nil
    }

    func addLowFares(_ lowFareAvailability: FDLowFareAvailability?) {
        if let lowFareAvailability = lowFareAvailability {
            for tripNew in lowFareAvailability.trips {
                if let tripOld = getTrip(tripNew.origin, destination: tripNew.destination) {
                    for lowFare in tripNew.dates {
                        tripOld.updateLowFare(lowFare)
                    }
                } else {
                    trips.append(tripNew)
                }
            }
        }
    }

    func getLowFee(_ date:Date?, tripIndex:Int) -> Float? {
        if let date = date {
            if tripIndex >= 0 && tripIndex < trips.count {
                let trip = trips[tripIndex]

                for dateFare in trip.dates {
                    if let fareDate = dateFare.date, fareDate.isEqualToDateIgnoringTime(date) {
                        return dateFare.price
                    }
                }
            }
        }

        return nil
    }

    func getFirstDate(_ tripIndex:Int) -> Date? {
        if tripIndex >= 0 && tripIndex < trips.count {
            return trips[tripIndex].dates.last?.date
        }

        return nil
    }

    func getLastDate(_ tripIndex:Int) -> Date? {
        if tripIndex >= 0 && tripIndex < trips.count {
            return trips[tripIndex].dates.last?.date
        }

        return nil
    }

    func getNextDates (_ forward: Bool) -> [Date] {
        var dates = [Date]()
        for trip in trips {
            let fareLoading = FDLowFare()
            fareLoading.date = Date()
            fareLoading.price = FDLowFare.LoadingPrice

            if let fareDate = forward ? trip.dates.last : trip.dates.first, var date = fareDate.date  {
                if date.isToday() {
                    if let lastDate = trip.dates.last?.date {
                        date = lastDate.dateByAddingDays(1)
                    } else {
                        date = Date()
                    }
                } else {
                    date = date.dateByAddingDays(forward ? 1 : -1)
                }
                fareLoading.date = date
            }

            dates.append(fareLoading.date!)
            trip.updateLowFare(fareLoading)
        }
        return dates
    }

    func getCacheLowFeeDates(_ date:Date?, tripIndex:Int) -> [Date] {

        var dates = [Date]()
        for i in 0..<trips.count {

            let trip = trips[i]

            let fareLoading = FDLowFare()
            fareLoading.price = FDLowFare.LoadingPrice

            if i == tripIndex {
                fareLoading.date = date
            } else {
                fareLoading.date = trip.getNextCacheDate()
            }
//
//
//            if let fareDate = forward ? trip.dates.last : trip.dates.first, var date = fareDate.date  {
//                if date.isToday() {
//                    if let lastDate = trip.dates.last?.date {
//                        date = lastDate.dateByAddingDays(1)
//                    } else {
//                        date = NSDate()
//                    }
//                } else {
//                    date = date.dateByAddingDays(forward ? 1 : -1)
//                }
//                fareLoading.date = date
//            }

            dates.append(fareLoading.date!)
            trip.updateLowFare(fareLoading)
        }
        return dates
//        
//        if let date = date {
//            if tripIndex >= 0 && tripIndex < trips.count {
//                let trip = trips[tripIndex]
//
//                for dateFare in trip.dates {
//                    if let fareDate = dateFare.date where fareDate.isEqualToDateIgnoringTime(date) {
//                        return dateFare.price
//                    }
//                }
//            }
//        }
//
//        return nil
    }

}

typealias FDLowFare = FDLowFareAvailability.TripFare.LowFare
typealias FDTripFare = FDLowFareAvailability.TripFare
