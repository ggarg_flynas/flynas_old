//
//  SadadOLP.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 18/6/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import Foundation

class FDSadadOnlinePaymentData: NSObject {
  
  var sadadOnlinePaymentData = FDSadadOnlinePaymentData()
  
  class FDSadadOnlinePaymentData: NSObject {
    var accessCode = ""
    var amount = ""
    var command = ""
    var currency = ""
    var customerEmail = ""
    var language = ""
    var merchantIdentifier = ""
    var merchantReference = ""
    var payFortLink = ""
    var paymentOption = ""
    var returnUrl = ""
    var sadadOlp = ""
    var signature = ""
  }
  
}
