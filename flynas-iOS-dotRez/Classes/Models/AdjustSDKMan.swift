//
//  AdjustSDK.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 7/6/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import Foundation

class AdjustSDKMan {
  
  class func register() {
    let environment = isAdjustPROD ? ADJEnvironmentProduction : ADJEnvironmentSandbox
    let adjConfig = ADJConfig(appToken: "wotjvmh5ccn4", environment: environment)
    Adjust.appDidLaunch(adjConfig)
  }
  
  class func login(naSmilesNum: String) {
    let event = ADJEvent(eventToken: "k2lday")
    event?.addCallbackParameter("Login", value: naSmilesNum)
    Adjust.trackEvent(event)
  }
  
  class func registration() {
    let event = ADJEvent(eventToken: "1wgzyj")
    event?.addCallbackParameter("Registration (become a member)", value: "")
    Adjust.trackEvent(event)
  }
  
  class func searchTicket() {
    let event = ADJEvent(eventToken: "jf6nvo")
    event?.addCallbackParameter("Search ticket", value: "")
    Adjust.trackEvent(event)
  }
  
  class func confirmTicket(isBusiness: Bool) {
    let event = ADJEvent(eventToken: "7ubod9")
    let ticketclass = isBusiness ? "business class" : "economy class"
    event?.addCallbackParameter("Confirm ticket (add-to-card)", value: ticketclass)
    Adjust.trackEvent(event)
  }
  
  class func cancelFlight() {
    let event = ADJEvent(eventToken: "jhb41a")
    event?.addCallbackParameter("Cancel booking", value: "")
    Adjust.trackEvent(event)
  }
  
  class func ticketPurchase(revenue: Double, currency: String, isBusiness: Bool, adults: Int, kids: Int, bag: Bool, meal: Bool) {
    let event = ADJEvent(eventToken: "oarevu")
    let classStr = isBusiness ? "Business" : "Economy"
    let bagStr = bag ? "Yes" : "No"
    let mealStr = meal ? "Yes" : "No"
    let str = "Class: \(classStr), Number of adults: \(adults) kids: \(kids), Extra bag: \(bagStr), Extra meal: \(mealStr)"
    event?.addCallbackParameter("Ticket purchase", value: str)
    event?.setRevenue(revenue, currency: currency)
    Adjust.trackEvent(event)
  }
  
  class func mealPurchase() {
    let event = ADJEvent(eventToken: "yqlq8m")
    event?.addCallbackParameter("Meal purchase", value: "no revenue appended as already part of the ticket price")
    Adjust.trackEvent(event)
  }
  
  class func seatPurchase() {
    let event = ADJEvent(eventToken: "c3cny1")
    event?.addCallbackParameter("Seat purchase", value: "")
    Adjust.trackEvent(event)
  }
  
  class func onlineCheckin(naSmilesNumber: String) {
    let event = ADJEvent(eventToken: "z59k6a")
    event?.addCallbackParameter("Online check-in", value: unwrap(str: naSmilesNumber))
    Adjust.trackEvent(event)
  }
  
}
