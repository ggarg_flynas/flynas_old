//
//  FDPassenger.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum PaxType : String {
  case Adult = "ADT"
  case Child = "CHD"
  case Infant = "INFT"
  
  static func isChildOrInfant(_ paxType:String) -> Bool {
    return paxType != Adult.rawValue
  }
}


class FDPassengersInput: NSObject {
  var passengers = [FDPassenger]()
  
  // Domestic, GulfCooperationCouncil, International
  // International and GulfCooperationCouncil consider as International
  var regionFlightType = ""
}

class FDPassenger: NSObject, NSCoding {
  
  class FrequentFlyer : NSObject {
    var carrierCode = "XY"
    var docNumber = ""
    var docTypeCode = "OAFF"
    
    var jsonObject : [String:AnyObject]? {
      
      
      let trimmedString = docNumber.trimmingCharacters(in: CharacterSet.whitespaces)
      
      //            docNumber.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet)
      
      if !trimmedString.isEmpty {
        return ["carrierCode": carrierCode as AnyObject,
                "docNumber": trimmedString as AnyObject,
                "docTypeCode": docTypeCode as AnyObject]
      } else {
        return ["carrierCode": "" as AnyObject,
                "docNumber": "" as AnyObject,
                "docTypeCode": "" as AnyObject]
      }
    }
  }
  
  
  
  var dateOfBirth: Date?
  var dateOfBirthHijri = false
  var discountCode = ""
  var firstName = ""
  var lastName = ""
  var residentCountry = "SA"
  var title = ""
  var travelDocument = FDTravelDocument()
  
  var naSmilesLoyalty = FDNaSmileLoyalty()
  
  // booking flow
  var passengerNumber = 0
  var paxType = PaxType.Adult.rawValue
  var isChildOrInfant : Bool {
    return PaxType.isChildOrInfant(paxType)
  }
  
  
  var frequentFlyer = FrequentFlyer()
  
  // index in the same paxType
  var paxTypeIndex = 0
  
  // member profile
  var gender = "" // Male, Female
  var nationality = "SA" {
    didSet {
      travelDocument.nationality = nationality
    }
  }
  var middleName = ""
  var nasmiles = ""
  var relationship = ""
  
  var genderValue : Int {
    return 1
  }
  
  // extra
  var legNumber : Int = 0 // { // leg totle
  //        didSet {
  //            meal.removeAll()
  //            baggage.removeAll()
  //            seat.removeAll()
  //            for _ in 1...legNumber {
  //                meal.append(FDResourceManager.sharedInstance.meals[0])
  //                baggage.append(FDResourceManager.sharedInstance.baggages[0])
  ////                seat.append(nil)
  //            }
  //        }
  //    }
  //    var meal = [FDExtraItem?]()
  //    var baggage = [FDExtraItem?]()
  
  // seats
  //    var seat = [FDExtraItem?]()
  
  override init () {
    super.init()
  }
  
  convenience init(paxType:PaxType, passengerNumber:Int) {
    self.init()
    self.paxType = paxType.rawValue
    self.passengerNumber = passengerNumber
  }
  
  convenience init(passenger:FDPassenger) {
    self.init()
    self.copyFrom(passenger)
  }
  
  
  func copyFrom(_ pssenger:FDPassenger) {
    self.dateOfBirth = pssenger.dateOfBirth
    self.discountCode = pssenger.discountCode
    self.firstName = pssenger.firstName
    self.lastName = pssenger.lastName
    //        self.passengerNumber = pssenger.passengerNumber
    //        self.paxType = pssenger.paxType
    self.residentCountry = pssenger.residentCountry
    self.title = pssenger.title
    self.travelDocument.copyFrom(pssenger.travelDocument)
    self.dateOfBirthHijri = pssenger.dateOfBirthHijri
    
    self.naSmilesLoyalty.copyFrom(pssenger.naSmilesLoyalty)
    
    gender = pssenger.gender
    nationality = pssenger.nationality
    middleName = pssenger.middleName
    nasmiles = pssenger.nasmiles
    relationship = pssenger.relationship
    
    //        paxTypeIndex = pssenger.paxTypeIndex
    frequentFlyer.docNumber = pssenger.frequentFlyer.docNumber
    
    legNumber = pssenger.legNumber
  }
  
  
  required convenience init?(coder decoder: NSCoder) {
    self.init()
    self.dateOfBirth = decoder.decodeObject(forKey: "dateOfBirth") as? Date
    self.discountCode = unwrap(str: decoder.decodeObject(forKey: "discountCode"))
    self.firstName = unwrap(str: decoder.decodeObject(forKey: "firstName"))
    self.lastName = unwrap(str: decoder.decodeObject(forKey: "lastName"))
    self.passengerNumber = unwrap(int: decoder.decodeObject(forKey: "passengerNumber"))
    self.paxType = unwrap(str: decoder.decodeObject(forKey: "paxType"))
    self.residentCountry = unwrap(str: decoder.decodeObject(forKey: "residentCountry"))
    self.title = unwrap(str: decoder.decodeObject(forKey: "title"))
    self.travelDocument = decoder.decodeObject(forKey: "travelDocument") as? FDTravelDocument ?? FDTravelDocument()
    self.dateOfBirthHijri = unwrap(bool: decoder.decodeObject(forKey: "dateOfBirthHijri"))
    
    if let s = decoder.decodeObject(forKey: "frequentFlyer.docNumber") as? String {
      self.frequentFlyer.docNumber = s
    } else {
      self.frequentFlyer.docNumber = ""
    }
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(self.dateOfBirth, forKey: "dateOfBirth")
    coder.encode(self.discountCode, forKey: "discountCode")
    coder.encode(self.firstName, forKey: "firstName")
    coder.encode(self.lastName, forKey: "lastName")
    coder.encode(self.passengerNumber, forKey: "passengerNumber")
    coder.encode(self.paxType, forKey: "paxType")
    coder.encode(self.residentCountry, forKey: "residentCountry")
    coder.encode(self.title, forKey: "title")
    coder.encode(self.travelDocument, forKey: "travelDocument")
    coder.encode(self.dateOfBirthHijri, forKey: "dateOfBirthHijri")
    coder.encode(self.frequentFlyer.docNumber, forKey: "frequentFlyer.docNumber")
  }
  
  var fullName : String {
    return firstName + " " + lastName
  }
  
  var nameInitial : String {
    return "\(unwrap(str: firstName.substringToIndex(1)).uppercased())\(unwrap(str: lastName.substringToIndex(1)).uppercased())"
  }
  
  var typeString : String {
    switch (paxType) {
    case PaxType.Adult.rawValue:
      return FDLocalized("Adult")
    case PaxType.Child.rawValue:
      return FDLocalized("Child")
    case PaxType.Infant.rawValue:
      return FDLocalized("Infant")
    default:
      return FDLocalized("Passenger")
    }
  }
  
  var typeNumberString : String {
    switch (paxType) {
    case PaxType.Adult.rawValue:
      return FDLocalized("Adult") + " \(FDLocalizeInt(paxTypeIndex+1))"
    case PaxType.Child.rawValue:
      return FDLocalized("Child") + " \(FDLocalizeInt(paxTypeIndex+1))"
    case PaxType.Infant.rawValue:
      return FDLocalized("Infant") + " \(FDLocalizeInt(paxTypeIndex+1))"
    default:
      return FDLocalized("Passenger") + " \(FDLocalizeInt(paxTypeIndex+1))"
    }
  }
  
  var progressString : String {
    return validateAll == .none ? FDLocalized("Complete") : FDLocalized("Incomplete")
  }
  
  var validateAll : ValidateErrorCode {
    
    if title.isEmpty {
      return .titleMissing
    }
    
    if firstName.isEmpty {
      return .firstNameMissing
    }
    
    if !firstName.isValidateName {
      return .notValidateName
    }
    
    if lastName.isEmpty {
      return .lastNameMissing
    }
    
    if !lastName.isValidateName {
      return .notValidateName
    }
    
    if dateOfBirth == nil {
      return .dobMissing
    }
    
    if paxType == "INFT" {
      let departureDate = FDResourceManager.sharedInstance.getLatestSearcDepartureDate()
      let minimumDOB = departureDate.dateByAddingDays(-8)
      if dateOfBirth?.compare(minimumDOB) == .orderedDescending {
        return .minimumDOB
      }
    }
    
    if travelDocument.docTypeCode.isEmpty {
      return .docTypeCodeMissing
    }
    
    if travelDocument.docNumber.isEmpty {
      return .docNumberMissing
    } else {
      let error = travelDocument.validateDocument
      
      if error != .none {
        return error
      }
    }
    
    //        if travelDocument.nationality.isEmpty {
    //            return .NationalityMissing
    //        }
    
    if travelDocument.docIssuingCountry.isEmpty {
      return .issuingCountryMissing
    }
    
    //        if travelDocument.birthCountry.isEmpty {
    //            return .BirthCountryMissing
    //        }
    
    //        if travelDocument.expirationDate == nil {
    //            return .ExpirationDateMissing
    //        }
    
    return .none
  }
  
  var passengerInput : [String:Any] {
    var paramaters : [String:Any] = [
      "travelDocument": travelDocument.travelDocumentInput as AnyObject,
      "firstName": firstName as AnyObject,
      "lastName": lastName as AnyObject,
      "passengerNumber": passengerNumber as AnyObject,
      "paxType": paxType as AnyObject,
      "residentCountry": residentCountry as AnyObject,
      "title": title as AnyObject,
      "dateOfBirth": (dateOfBirth != nil) ? dateOfBirth!.toString(format: .custom("yyyy-MM-dd"), applyLocele: false, utcTime: false) : "",
      "discountCode" : discountCode as AnyObject,
      //            "nasmiles":nasmiles
    ]
    
    if let fn = frequentFlyer.jsonObject {
      paramaters["frequentFlyer"] = fn as AnyObject?
      //            paramaters.setValue(fn, forKey: "frequentFlyer")
    } else {
      
      //            paramaters["frequentFlyer"] = ["carrierCode":"XY","docNumber":nasmiles,"docTypeCode":"OAFF"]
    }
    
    return paramaters
  }
  
  
}
