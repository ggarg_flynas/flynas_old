//
//  FDSsrCharges.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 19/02/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

enum FDSsrChargesType :String {
    case Baggage = "Baggage"
    case Meals = "Meals"
    case Lounge = "Lounge"
  case Sports = "SportEquipment"
}

class FDSsrCharges: NSObject {
    var ssrType: String = ""
    var feeCode: String = ""
    var amount : NSNumber = 0
    var count: NSNumber = 0
}
