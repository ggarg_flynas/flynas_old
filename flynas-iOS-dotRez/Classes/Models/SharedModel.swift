//
//  SharedModel.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 27/7/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import Foundation

class SharedModel {
  
  static let shared = SharedModel()
  var journeys: [FDJourneyCharge] = []
  var isWCI = false
  var isMMB = false
  var isDocTypeSet = false
  var numberOfStops = 0
  var economyFareCodeDepart = ""
  var economyFareCodeReturn = ""
  var wciPassengers: [FDPassenger] = []
//  var bundleMealCode: [String] = []
  var banners = [UIImage(named: "Splash5")!, UIImage(named: "Splash5")!, UIImage(named: "Splash5")!]
  var memberlogin: FDMemberLogin?
  var pnr = ""
  var userbooking: FDUserBookingInfo?
  var checkinPassengers = [FDWciPassenger]()
  var wciPassengerDetails = FDWciPassengerDetails()
  var checkinJourney = FDWciJourney()
  
}
