//
//  FDBookingPolling.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDBookingPolling: NSObject {
    var maxQueryCount = 5
    var pollingWaitTime = 2
    var repeatQueryInterval = 3
    var sessionPaymentQueryCount = 1
    var shouldContinuePolling = false
    var startQueryAfter = 2
}
