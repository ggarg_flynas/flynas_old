//
//  FDBoardingPasses.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 2/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import RealmSwift


class FDString : Object {
    dynamic var value = ""
}

class FDBoardingPasses: Object {

    dynamic var barCodeType = ""
    var items = List<FDBoardingPass>()
    dynamic var passengerNumber : String?
    dynamic var pnr = ""
    dynamic var sellKey = ""
    var serialNumbers = [String]()
    var serialNumbersRealm = List<FDString>()

    override static func ignoredProperties() -> [String] {
        return ["serialNumbers"]
    }

    func syncMemToRealm() {
        serialNumbersRealm.removeAll()
        for s in serialNumbers {
            serialNumbersRealm.append(FDString(value: ["value":s]))
        }
    }

    func syncRealmToMem() {
        serialNumbers.removeAll()
        for s in serialNumbersRealm {
            serialNumbers.append(s.value)
        }
    }

    var lastFlightTime : Date? {
        var lastDateTime : Date? = nil
        for item in items {
            for flight in item.segments {
                if lastDateTime == nil {
                    lastDateTime = flight.utcDepartureDate
                } else {
                    if let utcDepartureDate = flight.utcDepartureDate, utcDepartureDate.isLaterThanDate(lastDateTime!) {
                        lastDateTime = flight.utcDepartureDate
                    }
                }
            }
        }

        return lastDateTime
    }
}

class FDBarCode : Object {
    dynamic var barcodeData = ""
    dynamic var barcodeType = ""
    dynamic var hmac = ""
    dynamic var payload = ""
    dynamic var salt = ""
    dynamic var sequenceNumber = 0

}

class FDName : Object {
    dynamic var first = ""
    dynamic var last = ""
    dynamic var middle = ""
    dynamic var suffix = ""
    dynamic var title = ""

    var fullName : String {
        return first + " " + last
    }
}

class FDBoardingPass : Object {

    dynamic var agentId = ""
    dynamic var recordLocator = ""
    dynamic var barCode : FDBarCode?
    dynamic var name : FDName?
    var segments = List<FDFlight>()
}
