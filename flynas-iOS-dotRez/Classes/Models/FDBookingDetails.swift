//
//  FDBookingDetails.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 3/11/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

class FDBookingDetails: NSObject {
  
  static let StatusConfirmed = "Confirmed"
  
  var bookingDate = Date()
  var createdDate = Date()
  var modifiedDate = Date()
  var recordLocator = ""
  var status = "" //"status": "Confirmed", "Hold"
  var hasUncommittedChanges = false
  var currencyCode = ""
  var isGdsBooking = false
  var hasLoyaltyPayment = false
  var loyaltyAmount = 0
  var loyaltyNumber = ""
  
}
