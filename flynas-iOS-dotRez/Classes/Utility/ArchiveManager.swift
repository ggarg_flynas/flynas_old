//
//  ArchiveManager.swift
//  CobhamCheckin-iPad
//
//  Created by kenankarakecili on 1/2/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import Foundation

class ArchiveManager {
  
  class func saveArray(_ array: [AnyObject], key: String) {
    let data = NSKeyedArchiver.archivedData(withRootObject: array)
    UserDefaults.standard.set(data, forKey: key)
    UserDefaults.standard.synchronize()
  }
  
  class func retrieve(_ key: String) -> [AnyObject] {
    var arrayToReturn: [AnyObject] = []
    guard let data = UserDefaults.standard.object(forKey: key) as? Data else {
      return arrayToReturn
    }
    if let items = NSKeyedUnarchiver.unarchiveObject(with: data) as? [AnyObject] {
      arrayToReturn = items
    }
    return arrayToReturn
  }
  
}
