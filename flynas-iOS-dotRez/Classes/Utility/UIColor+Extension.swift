//
//  UIColor+Extension.swift
//  CobhamCheckin-iPad
//
//  Created by River Huang on 22/04/2016.
//  Copyright © 2016 Matchbyte. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(rgb: UInt32, alpha:CGFloat = 1.0) {

        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }

    convenience init(hex: String, alpha:CGFloat = 1.0) {
        var rgb: UInt32 = 0
        let s: Scanner = Scanner(string: hex)
        s.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        s.scanHexInt32(&rgb)

        self.init(rgb:rgb)
    }
}
