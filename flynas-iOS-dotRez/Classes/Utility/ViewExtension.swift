//
//  CustomView.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 27/9/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

extension UIView {
  
  func markError(_ mark: Bool) {
    if mark {
      addBorder(UIColor.red, name: "error")
    } else {
      addBorder(nil, name: "error")
    }
  }
  
  func addBorder(_ color: UIColor?, name: String) {
    if let mylayers = layer.sublayers?.filter({unwrap(str: $0.name).contains(name)}) {
      for mylayer in mylayers {
        mylayer.removeFromSuperlayer()
      }
    }
    guard let mycolor = color else { return }
    let left = CGRect(x: 0, y: 0, width: 0.5, height: bounds.size.height)
    let right = CGRect(x: bounds.size.width - 0.5, y: 0, width: 0.5, height: bounds.size.height)
    let top = CGRect(x: 0, y: 0, width: bounds.size.width, height: 0.5)
    let bottom = CGRect(x: 0, y: bounds.size.height - 0.5, width: bounds.size.width, height: 0.5)
    let frames = [left, right, top, bottom]
    for frame in frames {
      let border = CALayer()
      border.frame = frame
      border.name = name
      border.borderColor = mycolor.cgColor
      border.borderWidth = 0.5
      layer.addSublayer(border)
    }
    layoutIfNeeded()
  }
  
}
