//
//  File.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 9/6/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import Foundation
import EventKit

class CalendarHelper {
  
  class func addToCalendar(_ title: String, startDate: Date, endDate: Date, completion: @escaping ((Bool) -> Void)) {
    let eventStore = EKEventStore()
    switch EKEventStore.authorizationStatus(for: .event) {
    case .authorized:
      let success = insertEvent(title, store: eventStore, start: startDate, end: endDate)
      completion(success)
    case .denied:
      print("access denied")
      completion(false)
    default:
      eventStore.requestAccess(to: .event) { (granted, error) in
        DispatchQueue.main.async(execute: { 
          if granted {
            let success = self.insertEvent(title, store: eventStore, start: startDate, end: endDate)
            completion(success)
          } else {
            print("access denied")
            completion(false)
          }
        })
      }
    }
  }
  
  class func insertEvent(_ eventTitle: String, store: EKEventStore, start: Date, end: Date) -> Bool {
    let calendars = store.calendars(for: .event)
    for calendar in calendars {
      let event = EKEvent(eventStore: store)
      event.calendar = calendar
      event.title = eventTitle
      event.startDate = start
      event.endDate = end
      do {
        try store.save(event, span: .thisEvent)
        return true
      } catch {
        print("an error occured")
        return false
      }
    }
    return false
  }
  
}
