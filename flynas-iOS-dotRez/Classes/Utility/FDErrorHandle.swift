//
//  FDErrorHandle.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

enum ValidateErrorCode {
    case none
    case titleMissing
    case firstNameMissing
    case lastNameMissing
    case addressMissing
    case dobMissing
  case minimumDOB
    case residentCountryMissing
    case docTypeCodeMissing
    case docNumberMissing
//    case NationalityMissing
    case birthCountryMissing
    case expirationDateMissing
    case issuingCountryMissing
    case relationshipMissing
    case notValidateName

    case notValidatePassportNumber
    case notValidateIqamaNumber
    case notValidateNationalIDCard

    case passwordMissing
  case referralIdMissing
    case emailMissing
    case emailNotValidate
    case emailNotMuch
    case dobNotValidate

    case mobilePhoneNumberMissing
    case mobilePhoneCodeMissing
    case requiredFieldMissing

    case currentPasswordNotMatching
    case passwordCannotEmpty
    case confirmPasswordNotMatch
    //...
}

class FDErrorHandle: NSObject {

//    private static var __once: () = {
//            Static.instance = FDErrorHandle()
//        }()

    static var API_ERROR_5XX = "API_ERROR_5XX"
    static var API_SESSION_ERROR_EXPIRE = FDLocalized("Session expire.")
    static var API_UNAUTHORIZED_ERROR = FDLocalized("Please login and try again.")
    static var ERR_SECOND_DEPARTURE_DATE_EARLIER_THAN_FIRST_DEPARTURE_DATE = FDLocalized("Second departure date earlier than first departure date.")
    static var API_EMPTY_RES = FDLocalized("Empty response")
    static var API_SESSION_UPDATE_NEEDED = FDLocalized("New version avaiable.")

//    static var API_GENERAL_ERROR = FDLocalized("An error has occurred, please try again later, if the error continues please contact flynas.")
  static var API_GENERAL_ERROR = FDLocalized("We are currently working on upgrading our systems, please try again in a few hours, thanks.")
    static var API_REFUND_FAIL = API_GENERAL_ERROR

    static var API_LOGIN_FAIL = FDLocalized("Invalid username/password combination.")
    static var API_CANNOT_PAY_BY_NASMILE = FDLocalized("This ticket cannot be redeemed by points.")
    static var API_NASMILE_BALANCE_LIMITE = FDLocalized("Your accumulated SMILE Points do not meet the minimum threshold of 10000, therefore you are not yet able to make a redemption booking. The good news is that you will earn more SMILE Points on this booking!")

    static var PASS_NOT_SAME = FDLocalized("Please enter the same password.")

    static var PASS_EMPTY = FDLocalized("Please enter your password")

  static let sharedInstance = FDErrorHandle()

    class func apiCallError(_ reason: String?) {
        print(reason)
    }

    class func convertUnicodeChar(_ s: String) -> String {

        let transform = "Any-Hex/Java"
        let input = s as NSString
        let convertedString = input.mutableCopy() as! NSMutableString

        CFStringTransform(convertedString, nil, transform as NSString, true)

        return convertedString as String
    }

    class func apiCallErrorWithAler(_ reason: String?) {
        if var errorMsg = reason {

            errorMsg = errorMsg.replacingOccurrences(of: "[\"", with: "")
            errorMsg = errorMsg.replacingOccurrences(of: "\"]", with: "")

            errorMsg = errorMsg.replacingOccurrences(of: "\\\"", with: "\"")

            errorMsg = errorMsg.replacingOccurrences(of: "flings", with: "flynas")

//            errorMsg = errorMsg.stringByReplacingOccurrencesOfString("\"]", withString: "")

          errorMsg = unwrap(str: errorMsg.removingPercentEncoding)
            errorMsg = convertUnicodeChar(errorMsg)

            UIAlertView.showWithTitle("", message: errorMsg, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
            })
        }
    }

    class func validateErrorHandle(_ errorCode:ValidateErrorCode) {

        var errorMessage = FDLocalized("There was an error processing your request. Please confirm all Passenger information is correct to continue.")

        // todo - checkc ar
        switch errorCode {
        case .requiredFieldMissing:
            errorMessage = FDLocalized("Please fill all fields in English")
            break
        case .firstNameMissing:
            errorMessage = FDLocalized("Please enter your first name")
            break
        case .titleMissing:
            errorMessage = FDLocalized("Please enter your title")
            break
        case .lastNameMissing:
            errorMessage = FDLocalized("Please enter your last name")
            break
        case .addressMissing:
            errorMessage = FDLocalized("Please enter your Address")
            break
        case .dobMissing:
            errorMessage = FDLocalized("Please enter your date of birth")
            break
        case .residentCountryMissing:
            errorMessage = FDLocalized("Please enter your resident country")
            break
        case .docNumberMissing:
            errorMessage = FDLocalized("Please enter your document number")
            break
        case .docTypeCodeMissing:
            errorMessage = FDLocalized("Please enter your document type")
            break
//        case .NationalityMissing:
//            errorMessage = FDLocalized("Please enter your nationality")
//            break
        case .birthCountryMissing:
            errorMessage = FDLocalized("Please enter your birth country")
            break
        case .expirationDateMissing:
            errorMessage = FDLocalized("Please enter document expiration date")
            break
        case .issuingCountryMissing:
            errorMessage = FDLocalized("Please enter your issuing country")
            break
        case .mobilePhoneNumberMissing:
            errorMessage = FDLocalized("Please enter your mobile phone number")
            break
        case .mobilePhoneCodeMissing:
            errorMessage = FDLocalized("Please enter your mobile phone country code")
            break
        case .notValidateName:
            errorMessage = FDLocalized("Please enter a validate name")
            break

        case .notValidateNationalIDCard, .notValidateIqamaNumber, .notValidatePassportNumber:

            errorMessage = FDLocalized("Please fill all fields in English")
            break

        case .emailMissing:
            errorMessage = FDLocalized("Please enter your email")
            break

        case .emailNotMuch:
            errorMessage = FDLocalized("Email addresses do not match")
            break
        case .emailNotValidate:
            errorMessage = FDLocalized("Please enter a valid email.")
            break

        case .currentPasswordNotMatching:
            errorMessage = FDLocalized("Current password is not correct.")
            break

        case .passwordCannotEmpty:
            errorMessage = FDErrorHandle.PASS_EMPTY
            break

        case .confirmPasswordNotMatch:
            errorMessage = FDErrorHandle.PASS_NOT_SAME
            break
          
        case .minimumDOB:
          errorMessage = FDLocalized("Infant passenger must be over 8 days old")
        case .referralIdMissing:
          errorMessage = FDLocalized("It must be a valid nasmiles account number (10 digital number)")
        default:
            errorMessage = FDLocalized("There was an error processing your request. Please confirm all Passenger information is correct to continue.")
            break
        }



        UIAlertView.showWithTitle("", message:  errorMessage, cancelButtonTitle: FDLocalized("OK") , completionHandler: { (alertView, buttonIndex) -> Void in
        })
    }
}
