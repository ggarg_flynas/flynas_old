//
//  Utilities.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 26/4/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import Foundation

func unwrap(str: Any?) -> String {
  if let value = str {
    return String(describing: value)
  } else {
    return ""
  }
}

func unwrap(int: Any?) -> Int {
  if let value = int as? NSNumber {
    return Int(value)
  }
  return -1
}

func unwrap(bool: Any?) -> Bool {
  if let value = bool as? Bool {
    return value
  }
  return false
}

func getTopmostVC() -> UIViewController? {
  if var topVC = UIApplication.shared.keyWindow?.rootViewController {
    while let presentedVC = topVC.presentedViewController {
      topVC = presentedVC
    }
    return topVC
  }
  return nil
}

func postNotification(_ name: String, userInfo: [String : Any]) {
  DispatchQueue.main.async {
    NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: userInfo)
  }
}

func showMessageOnly(message: String) {
  showCompleteMessage(title: "", message: message, cancel: false, completion: nil)
}

func showMessageWithCompletion(message: String, completion: @escaping () -> Void) {
  showCompleteMessage(title: "", message: message, cancel: false, completion: completion)
}

func showCompleteMessage(title: String, message: String, cancel: Bool, completion: (() -> Void)?) {
  if message.count == 0 { return }
  let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
  let action = UIAlertAction(title: FDLocalized("OK"), style: .default) { (alertAction) in
    if let myCompletion = completion {
      myCompletion()
    }
  }
  alert.addAction(action)
  if cancel {
    alert.addAction(UIAlertAction(title: FDLocalized("Cancel"), style: .cancel, handler: nil))
  }
  if getTopmostVC() is UIAlertController == false {
    getTopmostVC()?.present(alert, animated: true, completion: nil)
  }
}

extension UITableView {
  
  func reloadWithAnimation() {
    UIView.transition(with: self,
                              duration: 0.2,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.reloadData()
      }, completion: nil)
  }
  
}

extension UILabel {
  
  func setTextWithAnimation(_ str: String) {
    UIView.transition(with: self,
                              duration: 0.2,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.text = str
      }, completion: nil)
  }
  
}

extension Double {
  
  // Rounds the double to decimal places value
  func rounded(toPlaces places:Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return (self * divisor).rounded() / divisor
  }
  
}

extension Array {
  func splitBy(subSize: Int) -> [[Element]] {
    return stride(from: 0, to: self.count, by: subSize).map { startIndex in
      if let endIndex = self.index(startIndex, offsetBy: subSize, limitedBy: self.count) {
        return Array(self[startIndex ..< endIndex])
      }
      return Array()
    }
  }
}
