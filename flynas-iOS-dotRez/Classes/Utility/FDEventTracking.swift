//
//  FDEventTracking.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 17/12/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

#if PRO
// flynas tracing id
let GA_TRACKING_ID = "UA-8026711-5"
var fdGATracker : GAITracker?
#endif

//#if DEBUG
//// test tracing id
//let GA_TRACKING_ID_TEST = "UA-71509581-1"
//var fdGATracker_TEST : GAITracker?
//#endif

func FDEventTrackingInit() {
#if PRO
    let gai = GAI.sharedInstance()
    fdGATracker = gai.trackerWithTrackingId(GA_TRACKING_ID)
    gai.trackUncaughtExceptions = true  // report uncaught exceptions
#if TARGET_IPHONE_SIMULATOR
//            gai.logger.logLevel = GAILogLevel.Verbose  // remove before app release
#endif

#endif
}

func FDEventTrackingSendEvent(_ category:String, action:String, label:String = "", value:NSNumber = 0) {

    #if PRO

    let event = GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: label, value: value).build() as [NSObject:AnyObject]

    if fdGATracker != nil {
        fdGATracker!.send(event)
    }

    #endif
}

func FDEventTrackingBooking(_ productName:String, pnr:String, price:NSNumber) {

    #if PRO

    let product = GAIEcommerceProduct()
    product.setName(productName)
    product.setPrice(price)

    let productAction = GAIEcommerceProductAction()
    productAction.setAction(kGAIPAPurchase)
    productAction.setTransactionId(pnr)

    let builder = GAIDictionaryBuilder.createScreenView()
    // Add the transaction data to the screenview.
    builder.setProductAction(productAction)
    builder.addProduct(product)

    let event = builder.build() as [NSObject:AnyObject]

    if fdGATracker != nil {
        fdGATracker!.send(event)
    }

    #endif
}

class FDEventTracking: NSObject {

}
