//
//  KKImageView.swift
//  Motto
//
//  Created by Kenan Karakecili on 10/07/2016.
//  Copyright © 2016 Kenan Karakecili. All rights reserved.
//

import UIKit

class KKImageView: UIImageView {
  
  fileprivate static let shared = KKImageView()
  fileprivate var images: [[String: UIImage]] = []
  
  func downloadImage(_ urlString: String, placeholder: UIImage?) {
    for item in KKImageView.shared.images {
      if let img = item[urlString] {
        image = img
        return
      }
    }
    if let ph = placeholder { image = ph }
    guard let url = URL(string: urlString) else { return }
    URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
      guard let myData = data else { return }
      let anImage = UIImage(data: myData)
      guard let myImage = anImage else { return }
      DispatchQueue.main.async(execute: { 
        self.image = myImage
        KKImageView.shared.images.append([urlString: myImage])
      })
      }).resume()
  }
  
}
