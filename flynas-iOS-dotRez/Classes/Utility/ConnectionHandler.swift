//
//  ConnectionHandler.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 18/6/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import UIKit

class ConnectionHandler: NSObject {
  
    private typealias ConnectionComp = (_ responseData: Data?) -> Void
    private typealias ParsingComp = (_ parsedJson: [String: AnyObject]?) -> Void
    typealias ResultJsonComp = (_ responseJson: [String: AnyObject]?) -> Void
  
    static private let shared = ConnectionHandler()
    private override init() {}
  
    class func requestGetConnection(urlString: String, completion: @escaping ResultJsonComp) {
        let request = NSMutableURLRequest(url: URL(string: urlString)!)
        request.httpMethod = "GET"
        request.addValue("text/plain; charset=utf-8", forHTTPHeaderField: "Content-Type")
        ConnectionHandler.shared.startConnection(request: request as URLRequest) { (responseData) in
          ConnectionHandler.shared.startParsingData(data: responseData) { (parsedJson) in
            if let json = parsedJson {
              completion(json)
            } else {
              completion(nil)
            }
          }
        }
      }
  
  class func requestConnection(request: URLRequest, completion: @escaping ResultJsonComp) {
    ConnectionHandler.shared.startConnection(request: request) { (responseData) in
      ConnectionHandler.shared.startParsingData(data: responseData) { (parsedJson) in
        completion(parsedJson)
      }
    }
  }
  
    class func requestPostConnection(urlString: String, body: [String: String], completion: @escaping ResultJsonComp) {
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "POST"
    //    request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    //    request.httpBody = try! PropertyListSerialization.data(fromPropertyList: body, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
        request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: .init(rawValue: 0))
    //    request.httpBody = NSKeyedArchiver.archivedData(withRootObject: body)
    //    print("\nREQUEST:\n" + body.description)
        print("\nREQUEST:\n\(NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue)!)")
        ConnectionHandler.shared.startConnection(request: request) { (responseData) in
          ConnectionHandler.shared.startParsingData(data: responseData) { (parsedJson) in
            completion(parsedJson)
          }
        }
      }
  
    private func startConnection(request: URLRequest, completion: @escaping ConnectionComp) {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let task = session.dataTask(with: request) { (data, response, error) in
          if error != nil || data == nil {
            completion(nil)
            return
          }
          completion(data)
        }
        task.resume()
      }
  
    private func startParsingData(data: Data?, completion: @escaping ParsingComp) {
        DispatchQueue.global(qos: .background).async {
            if let myData = data {
              do {
                print("\nRESPONSE:\n\(NSString(data: myData, encoding: String.Encoding.utf8.rawValue)!)")
                let json = try JSONSerialization.jsonObject(with: myData, options: JSONSerialization.ReadingOptions())
                DispatchQueue.main.async {
                  completion(json as? [String : AnyObject])
                }
              } catch {
                DispatchQueue.main.async {
                  completion(["raw": String(data: myData, encoding: String.Encoding.utf8) as AnyObject])
                }
              }
            } else {
              DispatchQueue.main.async {
                completion(nil)
              }
            }
          }
      }
  
  }
