//
//  FDUtility.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 15/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit

// constant
public func FDConvertArNumberToEn(_ n: String?) -> String? {
  
  guard var en = n else {return n}
  
  let ar_en_number = [
    "٠":"0",
    "١":"1",
    "٢":"2",
    "٣":"3",
    "٤":"4",
    "٥":"5",
    "٦":"6",
    "٧":"7",
    "٨":"8",
    "٩":"9"
  ]
  
  for (ar_num,en_num) in ar_en_number {
    en = en.replacingOccurrences(of: ar_num, with: en_num)
  }
  
  return en
}

public func FDLocalizeInt(_ n: Int) -> String {
  
  let formatter = NumberFormatter()
  //        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
  if formatter.isRTL() {
    formatter.locale = Locale(identifier: IOS_LANGUAGE_CODE_AR)
  } else {
    formatter.locale = Locale(identifier: IOS_LANGUAGE_CODE_EN)
  }
  if let s = formatter.string(from: n as NSNumber) {
    return s
  }
  
  return ""
}

public func FDFormatRemaindingSeats(_ remainingSeats : Int) -> String {
  if remainingSeats > 1 {
    return "\(FDLocalizeInt(remainingSeats)) " + FDLocalized("seats left")
  } else if remainingSeats == 1 {
    return "\(FDLocalizeInt(remainingSeats)) " + FDLocalized("seat left")
  } else {
    return ""
  }
}

func FDReplaceBtnAttributedTitle(_ btn:UIButton, title:String, state: UIControlState) {
  if let t = btn.attributedTitle(for: state) {
    let s = NSMutableAttributedString(attributedString: t)
    s.replaceCharacters(in: NSRange(location: 0,length: s.length), with: title)
    btn.setAttributedTitle(s, for: state)
  }
}

func FDReplaceLableAttributedTitle(_ label:UILabel, title:String) {
  if let t = label.attributedText {
    let s = NSMutableAttributedString(attributedString: t)
    s.replaceCharacters(in: NSRange(location: 0,length: s.length), with: title)
    label.attributedText = s
  }
}

func FDPrintCookies () {
  let storage = HTTPCookieStorage.shared
  if storage.cookies != nil && storage.cookies!.count > 0 {
    for cookie in storage.cookies! {
      print("\ncookie=\(cookie)\n")
    }
  } else {
    print("\nno cookies\n")
  }
}

func FDClearCookies (_ cookieName:String? = nil) {
  
  let storage = HTTPCookieStorage.shared
  storage.cookieAcceptPolicy = .always
  if storage.cookies != nil {
    for cookie in storage.cookies! {
      if cookieName == nil || cookieName! == cookie.name {
        storage.deleteCookie(cookie)
      }
    }
    UserDefaults.standard.synchronize()
  }
}

func FDGetCookie(_ cookieName:String) -> String? {
  
  let storage = HTTPCookieStorage.shared
  storage.cookieAcceptPolicy = .always
  if storage.cookies != nil {
    for cookie in storage.cookies! {
      if cookieName == cookie.name {
        return cookie.value
      }
    }
  }
  
  return nil
}

func delay(_ delay:Double, closure:@escaping ()->()) {
  DispatchQueue.main.asyncAfter(
    deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func FDGetViewSnapshotImage(_ view:UIView) -> UIImage? {
  UIGraphicsBeginImageContext(view.frame.size)
  view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
  let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
  UIGraphicsGetCurrentContext()
  
  return snapshotImageFromMyView
}

func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
  if let data = text.data(using: String.Encoding.utf8) {
    do {
      return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
    } catch let error as NSError {
      print(error)
    }
  }
  return nil
}

func FDCubeTransition(label: UILabel, text: String?, direction: AnimationDirection, delay:TimeInterval = 0) {
  
  let auxLabel = UILabel(frame: label.frame)
  auxLabel.text = text
  auxLabel.font = label.font
  auxLabel.textAlignment = label.textAlignment
  auxLabel.textColor = label.textColor
  
  //        auxLabel.backgroundColor = label.backgroundColor
  
  let auxLabelOffset = CGFloat(direction.rawValue) *
    label.frame.size.height/2.0
  
  auxLabel.transform = CGAffineTransform(scaleX: 1.0, y: 0.1).concatenating(CGAffineTransform(translationX: 0.0, y: auxLabelOffset))
  
  label.superview!.addSubview(auxLabel)
  
  UIView.animate(withDuration: 0.5, delay: delay, options: .curveEaseOut, animations: {
    auxLabel.transform = CGAffineTransform.identity
    label.transform = CGAffineTransform(scaleX: 1.0, y: 0.1).concatenating(CGAffineTransform(translationX: 0.0, y: -auxLabelOffset))
    }, completion: {_ in
      label.text = auxLabel.text
      label.transform = CGAffineTransform.identity
      
      auxLabel.removeFromSuperview()
  })
  
}

class FDUtility: NSObject {
  
  class func localizeInt(_ n: Int) -> String {
    return FDLocalizeInt(n)
  }
  
  
  
  // 1,234.56
  class func formatCurrency(_ number:NSNumber?) -> String? {
    if number != nil {
      let formatter = NumberFormatter()
      formatter.numberStyle = NumberFormatter.Style.currency
      formatter.currencySymbol = ""
      //        formatter.locale = NSLocale(localeIdentifier: "en_US")
      if formatter.isRTL() {
        formatter.locale = Locale(identifier: IOS_LANGUAGE_CODE_AR)
      } else {
        formatter.locale = Locale(identifier: IOS_LANGUAGE_CODE_EN)
      }
      return formatter.string(from: number!)
    }
    
    return nil
  }
  
  // SAR 100.00
  class func formatCurrencyWithCurrencySymbol(_ number:NSNumber?) -> String? {
    if number != nil {
      let currencySymbol = FDResourceManager.sharedInstance.selectedCurrencySymbol
      if let s = formatCurrency(number) {
        return currencySymbol + " " + s
      }
    }
    
    return nil
  }
  
  
  //    class func formatDecimalStyle(number:NSNumber) -> String? {
  //        let formatter = NSNumberFormatter()
  //        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
  //        return formatter.stringFromNumber(number)
  //    }
  
  class func formatStops(_ stops:Int) -> String {
    if stops < 1 {
      return FDLocalized("Non-stop")
    } else {
      return "\(FDLocalizeInt(stops)) \(FDLocalized("stop"))"
    }
  }
  
  class func formatDuration(_ begin: Date, end:Date) -> String {
    
    let timeInterval = end.timeIntervalSince(begin)
    let h = Int(timeInterval/3600)
    let m = Int((timeInterval.truncatingRemainder(dividingBy: 3600)))/60
    
    
    return "\(FDLocalizeInt(h)) \(h > 1 ? FDLocalized("hours") : FDLocalized("hour")) \(FDLocalizeInt(m)) \(m > 1 ? FDLocalized("minutes") : FDLocalized("minute"))"
  }
  
  class func formatDurationShort(_ begin: Date, end:Date) -> String {
    
    let timeInterval = end.timeIntervalSince(begin)
    
    return "\(FDLocalizeInt(Int(timeInterval/3600)))\(FDLocalized("h"))\(FDLocalizeInt(Int((timeInterval.truncatingRemainder(dividingBy: 3600))/60)))\(FDLocalized("m"))"
  }
  
  static var popViewController : PopUpViewControllerSwift?
  
  class func popInfo(_ title : String, message: String, headerBgColor:UIColor? = nil) {
    
    let window = UIApplication.shared.keyWindow
    let rootVC = window?.rootViewController
    if popViewController == nil {
      popViewController = PopUpViewControllerSwift(nibName: "PopUpViewController", bundle: nil)
    }
    popViewController!.headerBgColor = headerBgColor
    popViewController!.showInView((rootVC?.view)!, withTitle: title, withMessage: message, animated: true)
  }
  
  class func selectCountry (_ title:String, initValue:String, empty: Bool, origin:UIView, success:@escaping (String) -> Void) {
    var items = FDResourceManager.sharedInstance.countriesName
    if empty { items.insert("", at: 0) }
    var codes = FDResourceManager.sharedInstance.countriesCode
    if empty { codes.insert("", at: 0) }
    let index = codes.index(of: initValue)
    ActionSheetStringPicker.show(withTitle: title, rows: items, initialSelection: index==nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      success(codes[selectedIndex])
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: origin)
  }
  
  class func selectCountryPhoneCode (_ title:String, initValue:String, origin:UIView, success:@escaping (String) -> Void) {
    let items = FDResourceManager.sharedInstance.countriesPhoneCode
    let index = FDResourceManager.sharedInstance.countriesCode.index(of: initValue)
    
    ActionSheetStringPicker.show(withTitle: title, rows: items, initialSelection: index == nil ? 0 : index!, doneBlock: {
      picker, selectedIndex, selectedValue in
      
      success(FDResourceManager.sharedInstance.countriesCode[selectedIndex])
      
      }, cancel: { ActionMultipleStringCancelBlock in return }, origin: origin)
  }
  
  class func arrivalDateHHMMString(_ departureDate: Date?, arrivalDate: Date?) -> String {
    if let arrivalDate = arrivalDate {
      if let departureDate = departureDate {
        let days = departureDate.daysBeforeDateIgnoringTime(arrivalDate)
        if days > 0 {
          if (arrivalDate as NSObject).isRTL() {
            return "(\(FDLocalizeInt(days))+)\(arrivalDate.toStringUTC(format:.custom("HH:mm")))"
          } else {
            return "\(arrivalDate.toStringUTC(format:.custom("HH:mm")))(+\(FDLocalizeInt(days)))"
          }
        }
      }
      return arrivalDate.toStringUTC(format:.custom("HH:mm"))
    }
    
    return "--:--"
    
  }
  
  // trasfomr 2016-07 -> 07 / 16
  class func getExpiryDateString(_ dateString: String) -> String {
    if dateString.lengthOfBytes(using: String.Encoding.ascii) >= 7 {
//      let index = dateString.startIndex
      return dateString.substringFromIndex(5, toIndex: 7)! + " / " + dateString.substringFromIndex(2, toIndex: 4)!
    }
    
    return dateString
  }
  
  // 02:00:00 -> 2 hours 0 min
  class func getDurationString(_ durationString: String?) -> String? {
    if let s = durationString {
      //      let index = s.startIndex
      return s.substringToIndex(2)! + " \(FDLocalized("hour")) " + s.substringFromIndex(3, toIndex: 5)! + " \(FDLocalized("min"))"
    }
    
    return durationString
  }
  
  
  //    func getPaymentCreditCardType(paymentCreditCardType:String) -> String {
  //
  //    NSString * _paymentCreditCardType = nil;
  //    //    NSDictionary* cardType = [NSDictionary dictionaryWithObjectsAndKeys:
  //    //                              @"Visa",@"VI",
  //    //                              @"American Express",@"AX",
  //    //                              @"BC Card",@"BC",
  //    //                              @"MasterCard",@"CA",
  //    //                              @"Discover",@"DS",
  //    //                              @"Diners Club",@"DC",
  //    //                              @"Carta Si",@"T",
  //    //                              @"Carte Bleue",@"R",
  //    //                              @"Electron",@"E",
  //    //                              @"Japan Credit Bureau",@"JC",
  //    //                              @"Maestro",@"TO",
  //    //                              nil];
  //
  //    NSDictionary* cardType = [NSDictionary dictionaryWithObjectsAndKeys:
  //    @"VI",@"Visa",
  //    @"AX",@"American Express",
  //    @"BC",@"BC Card",
  //    @"CA",@"MasterCard",
  //    @"DS",@"Discover",
  //    @"DC",@"Diners Club",
  //    @"T",@"Carta Si",
  //    @"R",@"Carte Bleue",
  //    @"E",@"Electron",
  //    @"JC",@"Japan Credit Bureau",
  //    @"TO",@"Maestro",
  //    nil];
  //
  //    _paymentCreditCardType = [cardType objectForKey:paymentCreditCardType];
  //
  //    if (_paymentCreditCardType == nil) {
  //    _paymentCreditCardType = paymentCreditCardType;
  //    }
  //
  //    return _paymentCreditCardType;
  //    }
  //
  
  class func showPickerWithTitle(_ title:String,
                                 datePickerMode:UIDatePickerMode,
                                 selectedDate:Date?,
                                 showHijiriCalendar:Bool,
                                 minimumDate:Date?,
                                 maximumDate:Date?,
                                 doneBlock:@escaping ActionDateDoneBlock2,
                                 cancelBlock:@escaping ActionDateCancelBlock2,
                                 origin:UIView) -> ActionSheetDatePicker2 {
    
    let picker = ActionSheetDatePicker2(title: title, datePickerMode: datePickerMode, selectedDate: selectedDate, doneBlock: doneBlock, cancel: cancelBlock, origin: origin)
    
    picker?.minimumDate = minimumDate
    picker?.maximumDate = maximumDate
    
    picker?.showHijiriCalendar = showHijiriCalendar
    picker?.timeZone = TimeZone(abbreviation: "UTC")
    
    if (picker?.isRTL())! {
      picker?.locale = Locale(identifier: IOS_LANGUAGE_CODE_AR)
    } else {
      picker?.locale = Locale(identifier: IOS_LANGUAGE_CODE_EN)
    }
    picker?.show()
    return picker!
  }
  
  class func showPickerWithTitle(_ title:String,
                                 datePickerMode:UIDatePickerMode,
                                 selectedDate:Date?,
                                 showHijiriCalendar:Bool,
                                 doneBlock:@escaping ActionDateDoneBlock2,
                                 cancelBlock:@escaping ActionDateCancelBlock2,
                                 origin:UIView) -> ActionSheetDatePicker2 {
    
    let picker = ActionSheetDatePicker2(title: title, datePickerMode: datePickerMode, selectedDate: selectedDate, doneBlock: doneBlock, cancel: cancelBlock, origin: origin)
    
    picker?.showHijiriCalendar = showHijiriCalendar
    picker?.timeZone = TimeZone(abbreviation: "UTC")
    
    if (picker?.isRTL())! {
      picker?.locale = Locale(identifier: IOS_LANGUAGE_CODE_AR)
    } else {
      picker?.locale = Locale(identifier: IOS_LANGUAGE_CODE_EN)
    }
    
    picker?.show()
    return picker!
  }
  
  class func onBtnTermClicked(_ vc:UIViewController) {
    let url = vc.isEn()
      ? "http://www.flynas.com/images/terms_and_conditions/Feb-7-2016-flynas-TERMS-AND-CONDITIONS-OF-CARRIAGE-2016-E.pdf"
      : "http://www.flynas.com/images/terms_and_conditions/Feb-7-2016-flynas-TERMS-AND-CONDITIONS-OF-CARRIAGE-2016-A.pdf"
    let webView = SVModalWebViewController(address: url)
    vc.present(webView!, animated: true, completion: nil)
  }
  
  class func naSmileTierBenefits(_ vc:UIViewController) {
    let url = vc.isEn() ? "http://www.flynas.com/en/nasmiles/tier-benefits" : "http://www.flynas.com/ar/nasmiles/tier-benefits"
    FDUtility.showUrl(vc, url:url)
  }
  
  class func showUrl(_ vc:UIViewController, url:String) {
    let webView = SVModalWebViewController(address: url)
    
    vc.present(webView!, animated: true, completion: { () -> Void in
    })
  }
  
  class func getVersionNumber() -> String {
    var appVerNumber = "2.5"
    var appBuildNumber = "76"
    
    if let b = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String {
      appBuildNumber = b
    }
    
    if let v = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")  as? String {
      appVerNumber = v
    }
    
    return "\(appVerNumber).\(appBuildNumber)"
  }
}


///////////////////////////////////////////////////////////////////////
///  This function converts decimal degrees to radians              ///
///////////////////////////////////////////////////////////////////////
func deg2rad(_ deg:Double) -> Double {
  return deg * M_PI / 180
}

///////////////////////////////////////////////////////////////////////
///  This function converts radians to decimal degrees              ///
///////////////////////////////////////////////////////////////////////
func rad2deg(_ rad:Double) -> Double {
  return rad * 180.0 / M_PI
}

// "M"-"Miles", "K"-"Kilometers", "N"-"Nautical Miles"
func FDDistance(_ lat1:Double, lon1:Double, lat2:Double, lon2:Double, unit:String) -> Double {
  let theta = lon1 - lon2
  var dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
  dist = acos(dist)
  dist = rad2deg(dist)
  dist = dist * 60 * 1.1515
  if (unit == "K") {
    dist = dist * 1.609344
  }
  else if (unit == "N") {
    dist = dist * 0.8684
  }
  return dist
}

public func FDRemoveSpecialCharsFromString(_ text: String?) -> String? {
  guard let text = text else {
    return nil
  }
  
  let okayChars : Set<Character> =
    Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
  return String(text.characters.filter {okayChars.contains($0) })
}
