//
//  IPAddressHelper.h
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 27/9/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

#ifndef IPAddressHelper_h
#define IPAddressHelper_h


#endif /* IPAddressHelper_h */

@interface IPAddressHelper: NSObject

+ (NSString *)getIPAddress;
+ (NSString *)getIPAddress:(BOOL)preferIPv4;
+ (NSDictionary *)getIPAddresses;
+ (NSString *)getWebIP;

@end
