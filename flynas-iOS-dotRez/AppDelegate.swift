//
//  AppDelegate.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 14/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import GoogleTagManager
import Realm
import RealmSwift

let ACTION_FOR_SHORTCUTITEM = "ACTION_FOR_SHORTCUTITEM"

enum ShortcutIdentifier: String {
  case First
  case Second
  case Third
  case Fourth
  init?(fullType: String) {
    guard let last = fullType.components(separatedBy: ".").last else { return nil }
    
    self.init(rawValue: last)
  }
  var type: String {
    return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
  }
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  let colorGreen = FDColorFlynasGreen
  var launchedShortcutItem: AnyObject?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    resetRealm()
    AdjustSDKMan.register()
    UIApplication.shared.applicationIconBadgeNumber = 0
    initPushNotification(app: application)
    FirebaseApp.configure()
    FDRealmManager.InitRealm()
    FDdotRezAPIManager.initAPI()
    UITextField.appearance().tintColor = colorGreen
    UITextView.appearance().tintColor = colorGreen
    SVProgressHUD.setDefaultMaskType(.clear)
    SVProgressHUD.setForegroundColor(colorGreen)
    #if DEBUG
      AFHTTPRequestOperationLogger.shared().startLogging()
      AFHTTPRequestOperationLogger.shared().level = AFHTTPRequestLoggerLevel.AFLoggerLevelDebug
    #endif
    FDResourceManager.sharedInstance.activateSession({ () -> Void in
      FDResourceManager.sharedInstance.updateResourcesWhenExpire()
    }) { (r) -> Void in
      print(unwrap(str: r))
    }
    FDEventTrackingInit()
    FDEventTrackingSendEvent("System", action:"Launch")
//    if let remoteNotificationPayload = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any]  {
//      FDAPNManager.sharedInstance.handlePush(remoteNotificationPayload, live:false)
//    }
    var shouldPerformAdditionalDelegateHandling = true
    if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] {
      launchedShortcutItem = shortcutItem as AnyObject?
      shouldPerformAdditionalDelegateHandling = false
    }
    return shouldPerformAdditionalDelegateHandling
  }
  
  func resetRealm() {
    let key = "kRealmDidResetDefaults\(SERVER_VERSION_2)"
    let didReset = UserDefaults.standard.bool(forKey: key)
    if !didReset {
      let defaultPath = Realm.Configuration.defaultConfiguration.fileURL?.path
      try? FileManager.default.removeItem(atPath: defaultPath!)
      UserDefaults.standard.set(true, forKey: key)
      UserDefaults.standard.synchronize()
    }
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    Messaging.messaging().shouldEstablishDirectChannel = false
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    firebaseHandler()
    DispatchQueue.main.async(execute: {
      _ = self.handleShortCutItem(self.launchedShortcutItem)
      self.launchedShortcutItem = nil
    })
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    FDResourceManager.sharedInstance.activateSession({ () -> Void in
      FDResourceManager.sharedInstance.updateResourcesWhenExpire()
    }) { (r) -> Void in
    }
    FDEventTrackingSendEvent("System", action:"Wakeup")
    DispatchQueue.main.async {
      NotificationCenter.default.post(name: Notification.Name(rawValue: kFDAppDidEnterForeground), object: nil)
    }
  }
  
  func initPushNotification(app: UIApplication) {
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
        if error == nil {
          print("requestAuthorization success")
        }
      }
    } else {
      UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
    }
    app.registerForRemoteNotifications()
    NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
  }
  
  func firebaseHandler() {
    Messaging.messaging().shouldEstablishDirectChannel = true
//    Messaging.messaging().delegate = self
    Messaging.messaging().subscribe(toTopic: "all")
    //    // DONTFORGET
//    UIAlertView.showWithTitle("", message: unwrap(str: InstanceID.instanceID().token()), cancelButtonTitle: "copy") { (alertView, buttonIndex) in
//      UIPasteboard.general.string = InstanceID.instanceID().token()
//    }
    let isFirst = UserDefaults.standard.bool(forKey: "kFNAppInstall")
    if isFirst { return }
    UserDefaults.standard.set(true, forKey: "kFNAppInstall")
    let title = "app_install"
    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
      AnalyticsParameterItemID: "id-\(title)" as NSObject,
      AnalyticsParameterItemName: title as NSObject,
      AnalyticsParameterContentType: "cont" as NSObject
      ])
  }
  
  func refreshToken(notification: Notification) {
    let token = InstanceID.instanceID().token()
    print("Firebase token: \(unwrap(str: token))")
    firebaseHandler()
  }
  
}

// MARK: - ShortCut item
extension AppDelegate {
  
  func handleShortCutItem(_ shortcutItem: AnyObject?) -> Bool {
    guard let shortcut = shortcutItem as? UIApplicationShortcutItem else { return false }
    guard ShortcutIdentifier(fullType: shortcut.type) != nil else { return false }
    guard let shortCutType = shortcut.type as String? else { return false }
    NotificationCenter.default.post(name: Notification.Name(rawValue: ACTION_FOR_SHORTCUTITEM), object: shortCutType)
    return true
  }
  
  func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
    let handledShortCutItem = handleShortCutItem(shortcutItem)
    completionHandler(handledShortCutItem)
  }
  
}

// MARK: - Push notification delegate
//extension AppDelegate: MessagingDelegate {
//
//  func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
//    print(unwrap(str: Messaging.messaging().fcmToken))
//  }
//
//  func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
//    UIApplication.shared.registerForRemoteNotifications()
//  }
//
//  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//    let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
//    var tokenString = ""
//    for i in 0..<deviceToken.count {
//      tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
//    }
//    Messaging.messaging().apnsToken = deviceToken
//    FDAPNManager.sharedInstance.registerDeviceToken(tokenString, success: {
//      }, fail:  { (reason) in
//    })
//  }
//
//  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//    FDAPNManager.sharedInstance.handlePush(userInfo, live:true)
//  }
//
//}

