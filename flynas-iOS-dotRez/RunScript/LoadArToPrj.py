#!/usr/bin/env python
# encoding: utf-8

"""
LoadArToPrj.py

0. input : the root of project, should have
Localizable_ar.string in the project folder
python ${SRCROOT}/${TARGET_NAME}/RunScript/LoadArToPrj.py ${SRCROOT}/${TARGET_NAME} 

1. use ibtool to regenerate storyboard string table to ar folder

2. use genstrings to re-generate dynamic string table to tmp file
	
3. merge current dynamic string, left thest different translation to the end of file

4. load translation ar file

5. load to string table by string

before do this, please check:

1. update all storyboard string to ar.lproj
python ${SRCROOT}/${TARGET_NAME}/RunScript/AutoGenStrings.py ${SRCROOT}/${TARGET_NAME} 

2. load all strings from swift to Base.lproj/Localizable.strings
find ${SRCROOT}/${TARGET_NAME}/Classes -name *.swift | xargs genstrings *.swift -s FDLocalized -o ${SRCROOT}/${TARGET_NAME}/Base.lproj

3. merge android translation
user this link to convert from android string table
https://localise.biz/free/converter/ios-to-android

rename to Localizable_en.strings and Localizable_ar.strings
run MergeEnAr.py
report missging or duplicate in Localizable_ar_err.txt file

4. Load Localizable_ar.strings to ar.lproj/Localizable.strings, and all other storboard.strings
report missging or duplicate in ar.lproj/Localizable.txt file


load Localizable_ar.strings to ar.lproj
by set _ar key to _en value and add missing to bottom

"""

import imp 
import sys 
import os
import glob
import string  
import re  
import time
import traceback

imp.reload(sys) 
sys.setdefaultencoding('utf-8') #,utf-8,\u4e00-\u9fa5

KGenerateStringsFile = 'TempfileOfStoryboardNew.strings'

ColonRegex = ur'["](.*?)["]'
CommnetRegex = ur'/\*(.*?)\*/'

#KeyParamRegex = ur'["](.*?)["](\s*)=(\s*)["](.*?)["];'
KeyParamRegex = re.compile(ur'/\*(.*?)\*/\n["](.*?)["](\s*)=(\s*)["](.*?)["];', re.VERBOSE | re.MULTILINE)

AnotationRegexPrefix = ur'/\*(.*?)\*/'

def getCharaset(string_txt):
	filedata = bytearray(string_txt[:4])
	if len(filedata) < 4 :
		return 0
	if  (filedata[0] == 0xEF) and (filedata[1] == 0xBB) and (filedata[2] == 0xBF):
		print 'utf-8'
		return 1
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE) and (filedata[2] == 0x00) and (filedata[3] == 0x00):
		print 'utf-32/UCS-4,little endian'
		return 3
	elif (filedata[0] == 0x00) and (filedata[1] == 0x00) and (filedata[2] == 0xFE) and (filedata[3] == 0xFF):
		print 'utf-32/UCS-4,big endian'
		return 3
	elif (filedata[0] == 0xFE) and (filedata[1] == 0xFF):
		print 'utf-16/UCS-2,little endian'
		return 2
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE):
		print 'utf-16/UCS-2,big endian'
		return 2
	else:
		print 'can not recognize!'
		return 0

def decoder(string_txt):
	var  = getCharaset(string_txt)
	if var == 1:
		return string_txt.decode("utf-8")
	elif var == 2:
		return string_txt.decode("utf-16")
	elif var == 3:
		return string_txt.decode("utf-32")
	else:
		return string_txt

def constructAnotationRegex(str):
    return AnotationRegexPrefix + '\n' + str

def getAnotationOfString(string_txt,suffix):
	anotationRegex = re.compile(constructAnotationRegex(suffix), re.VERBOSE | re.MULTILINE)
	anotationString = ''
	try:
   		anotationMatch = re.search(anotationRegex,unicode(string_txt))
		if anotationMatch:
			match = re.search(AnotationRegexPrefix,anotationMatch.group(0))
			if match:
				anotationString = match.group(0)
	except Exception as e:
   		print 'Exception:' 
   		print e

	return anotationString

def loadStringFile(stringFiltPath):
	nspf=open(stringFiltPath,"r")
	newString_txt =  decoder(str(nspf.read(5000000)))  # origin file content string
	nspf.close()

	# strings table - item: 2 lines
	# /* Comment  */       - commnet 1 line
	# "key" = "value"  - key and value
	
	item_dic = {}   # dict of key = item 
	key_dic = {}    # dict of key = value
	comment_dic = {} # dict of key = comment
	keyComment_dic = {} # dict of key + comment = item
	rKv = {}			# dict of value = key
	keyCommentK_dic = {}# dict of key + comment = key
	
	for stfmatch in re.finditer(KeyParamRegex , newString_txt):
		
		linestr = stfmatch.group(0)
		leftvalue = stfmatch.group(2)
		rightvalue = stfmatch.group(5)
		anotationString = stfmatch.group(1)

		item_dic[leftvalue] = linestr
		key_dic[leftvalue] = rightvalue
		comment_dic[leftvalue] = anotationString
		kc = 'key="' + leftvalue + '",comment="' + anotationString + '"'
		keyComment_dic[kc] = linestr
		keyCommentK_dic[kc] = leftvalue
		rKv[rightvalue] = leftvalue


	return (item_dic, key_dic, comment_dic, keyComment_dic, newString_txt, rKv, keyCommentK_dic)

def compareWithFilePath(sourceEnFilePath,sourceArFilePath):
	
	print "begin-1 \n"
	#read newStringfile 
	nspf=open(sourceEnFilePath,"r")
	
	#newString_txt =  str(nspf.read(5000000)).decode("utf-16")
	newString_txt =  decoder(str(nspf.read(5000000)))
	nspf.close()
	
	item_dic = {}
	enString_dic = {}
	anotation_dic = {}
	
	for stfmatch in re.finditer(KeyParamRegex , newString_txt):
		
		linestr = stfmatch.group(0)
		leftvalue = stfmatch.group(2)
		rightvalue = stfmatch.group(5)
		anotationString = stfmatch.group(1)
		# print "leftvalue = " + leftvalue + "\n"
		# print "rightvalue = " + rightvalue + "\n"
		# print "anotationString = " + anotationString + "\n"
		# print "linestr = " + linestr + "\n"
		item_dic[leftvalue] = linestr
		enString_dic[leftvalue] = rightvalue
		anotation_dic[leftvalue] = anotationString

	#read originalStringfile 
	ospf=open(sourceArFilePath,"r")
	originalString_txt =  decoder(str(ospf.read(5000000)))
	ospf.close()
	arString_dic = {}
	for stfmatch in re.finditer(KeyParamRegex , originalString_txt):
		linestr = stfmatch.group(0)
		leftvalue = stfmatch.group(2)
		rightvalue = stfmatch.group(5)
		anotationString = stfmatch.group(1)
		arString_dic[leftvalue] = rightvalue
		
		# linematchs = re.findall(ColonRegex, linestr)
		# if len(linematchs) == 2:
		# 	leftvalue = linematchs[0]
		# 	rightvalue = linematchs[1]
		# 	arString_dic[leftvalue] = rightvalue

	#compare and reset key in ar
	for key in arString_dic:
		if(key in enString_dic):
			keystr = '%s'%key
			replacestr = enString_dic[key]
			match = re.search(replacestr , originalString_txt)
			if match is None:
				originalString_txt = originalString_txt.replace(keystr,replacestr)
			else:
				# report duplicate
				print '!!! key "'+keystr+'" is duplicate'

	#compare and add new param to ar string
	executeOnce = 1
	for key in enString_dic:
		values = (key, enString_dic[key])
		if(key not in arString_dic):
			newline = ''
			if executeOnce == 1:
				newline += '\n'
				# timestamp = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
				# newline = '\n//##################################################################################\n'
				# newline	+='//#           AutoGenStrings            '+timestamp+'\n'
				# newline	+='//##################################################################################\n'
				executeOnce = 0
			newline += item_dic[key] + '\n\n'
			originalString_txt += newline
			# report missing
			keystr = '%s'%key
			print '!!! key "'+keystr+'" is missing'
	#write into origial file
	sbfw=open(sourceArFilePath,"w")
	sbfw.write(originalString_txt)
	sbfw.close()

def extractFileName(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	return seg[lastindex]

def extractFilePrefix(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	prefix =  seg[lastindex].split('.')[0]
	return prefix

def generateStoryboardStringsfile(storyboard_path,tempstrings_path):
	cmdstring = 'ibtool '+storyboard_path+' --generate-strings-file '+tempstrings_path
	if os.system(cmdstring) == 0:
		return 1

arErr = ""
def reportErr(err):
	global arErr
	print err
	arErr += err

def saveErr(errLogFile):
	global arErr
	#write error log
	sbfw=open(errLogFile,"w")
	sbfw.write(arErr)
	sbfw.close()

def main():

	prjPath = sys.argv[1]
	print prjPath

	# 1. use ibtool to regenerate storyboard string table to ar folder
	generateStoryboardString(prjPath)

	# 2. use genstrings to re-generate dynamic string table to tmp file
	generateDynamicString(prjPath)

	# 4. load translation ar to string table
	loadTranslation(prjPath)


def generateDynamicString(prjPath):
	sourceCodePath = prjPath + '/Classes'
	sourceCodeNames = '*.swift'

	newFile = prjPath + '/Localizable.strings'
	oldFile = prjPath + '/Base.lproj/Localizable.strings'
	cmdstring = 'find ' + sourceCodePath + ' -name \\'+ sourceCodeNames + ' | xargs genstrings ' + sourceCodeNames + ' -s FDLocalized -o ' + prjPath
	print cmdstring
	if os.system(cmdstring) == 0:
		nspf=open(newFile,"r")
		newString_txt =  decoder(str(nspf.read(5000000)))  # origin file content string
		nspf.close()

		onlValueStr	= "/* No comment provided by engineer. */"
		newValueStr = "/*  */"
		newString_txt = newString_txt.replace(onlValueStr,newValueStr)

		sbfw=open(newFile,"w")
		sbfw.write(newString_txt)
		sbfw.close()

		# 3. merge current dynamic string, left thest different translation to the end of file
		missingKeyDicAr = {}
		itemEnNew, keyEnNew, commentEnNew, keyCommentEnNew, fileStrEnNew, rKvEnNew, kckEnNew = loadStringFile(newFile)
		itemEnOld, keyEnOld, commentEnOld, keyCommentEnOld, fileStrEnOld, rKvEnOld, kckEnOld = loadStringFile(oldFile)
		for kc in keyCommentEnOld:
			if(kc in keyCommentEnNew):
				oldItem = keyCommentEnNew[kc]
				newItem = keyCommentEnOld[kc]
				fileStrEnNew = fileStrEnNew.replace(oldItem,newItem)
			else:
				if not kc in missingKeyDicAr:
					missingKeyDicAr[kc] = keyCommentEnOld[kc]

		#write back to string file
		sbfw=open(oldFile,"w")
		sbfw.write(fileStrEnNew)
		sbfw.close()

		os.remove(newFile)
	else:
		print 'genstrings strings fail'


def generateStoryboardString(prjPath):
	filePath = prjPath
	KSourceFile = 'Base.lproj/*.storyboard'
	KTargetPath = 'ar.lproj/'
	
	sourceFilePath = filePath + '/' + KSourceFile 
	sourceFile_list = glob.glob(sourceFilePath)
	if len(sourceFile_list) == 0:
		print sourceFilePath + 'error dictionary,you should choose the dic upper the Base.lproj'
		return
	
	for sourcePath in sourceFile_list:
		sourceprefix = extractFilePrefix(sourcePath)
		sourcename = extractFileName(sourcePath)
		print 'init with %s'%sourcename
		targetFile = filePath + '/' + KTargetPath + sourceprefix + ".strings"
		if generateStoryboardStringsfile(sourcePath,targetFile) == 1:
			print '- - genstrings %s successfully'%sourcename
			print 'finish with %s'%targetFile
		else:
			print '- - genstrings %s error'%sourcename


def loadTranslation(prjPath):

	sourceArFilePath = prjPath + '/RunScript/Localizable_ar.strings'
	
	targetArStoryboardFilePath = prjPath + '/ar.lproj/*.strings'
	sourceEnFilePath = prjPath + '/Base.lproj/Localizable.strings'
	targetArFilePath = prjPath + '/ar.lproj/Localizable.strings'
	
	missingKeyDicAr = {}

	#load transliation file
	itemDicAr, keyDicAr, commentDicAr, keyCommentDicAr, arDicFileStr, rKvDic, kck1 = loadStringFile(sourceArFilePath)

	if os.path.isfile(targetArFilePath):
		os.remove(targetArFilePath)
	
	# load to storyboard string files
	targeArStoryBoardFiles = glob.glob(targetArStoryboardFilePath)
	if len(targeArStoryBoardFiles) == 0:
		print targetArStoryboardFilePath + ' - no string file found'
	else:
		for tf in targeArStoryBoardFiles:
			print 'handle %s'%tf
			itemSrcAr, keySrcAr, commentSrcAr, keyCommentSrcAr, arFileStr, rKv, kck2 = loadStringFile(tf)
			for key in keySrcAr:
				keystr = keySrcAr[key]
				# print key + "=" + keystr
				if(keystr in keyDicAr):
					newValueStr = '"' + key + '" = "' + keyDicAr[keystr] + '";'
					onlValueStr = '"' + key + '" = "' + keystr + '";'

					# print newValueStr + '\n'
					# print onlValueStr + '\n'

					oldItem = itemSrcAr[key]
					newItem = oldItem.replace(onlValueStr,newValueStr)

					# print oldItem + '\n'
					# print newItem + '\n'

					arFileStr = arFileStr.replace(oldItem,newItem)
				else:
					# reportErr("missing in " + tf + "\n" + itemSrcAr[key] + "\n")
					if not keystr in missingKeyDicAr and not keystr in rKvDic:
						newline = '\n/*  */\n' + '"' + keystr + '" = "' + keystr + '";'
						missingKeyDicAr[keystr] = newline

			#write back to string file
			sbfw=open(tf,"w")
			sbfw.write(arFileStr)
			sbfw.close()
	
	# en string table to be translate
	itemSrcEn, keySrcEn, commentSrcEn, keyCommentSrcEn, enFileStr, rKv2, kck3 = loadStringFile(sourceEnFilePath)
	
	#compare and reset key in ar
	for keyComment in keyCommentSrcEn:
		if(keyComment in keyCommentDicAr):
			# keystr = '%s'%keyComment
			oldStr = keyCommentSrcEn[keyComment]
			newStr = keyCommentDicAr[keyComment]
			enFileStr = enFileStr.replace(oldStr,newStr)
		else:
			# reportErr("missing :" + keyComment + "\n")
			keystr = kck3[keyComment]
			if not keystr in missingKeyDicAr and not keystr in rKvDic:
				newline = '\n' + keyCommentSrcEn[keyComment]
				missingKeyDicAr[keystr] = newline


	#write into ar file
	sbfw=open(targetArFilePath,"w")
	sbfw.write(enFileStr)
	sbfw.close()
	
	# write missing back to ar Dict for translate
	
	for key in missingKeyDicAr:
		arDicFileStr += '\n' + missingKeyDicAr[key]

	# arDicFileStr += '\n'

	sbfw=open(sourceArFilePath,"w")
	sbfw.write(arDicFileStr)
	sbfw.close()
	# errFilePath = 'Localizable_err.txt'
	# saveErr(errFilePath)


if __name__ == '__main__':
	main()
