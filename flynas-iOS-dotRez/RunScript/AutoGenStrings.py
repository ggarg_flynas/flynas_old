#!/usr/bin/env python
# encoding: utf-8

"""
untitled.py
Created by linyu on 2015-02-13.
Copyright (c) 2015 __MyCompanyName__. All rights reserved.
"""

import imp 
import sys 
import os
import glob
import string  
import re  
import time
import traceback

imp.reload(sys) 
sys.setdefaultencoding('utf-8') #,utf-8,\u4e00-\u9fa5

KSourceFile = 'Base.lproj/*.storyboard'

KTargetFile = '*.lproj/*.strings'  

KGenerateStringsFile = 'TempfileOfStoryboardNew.strings'

ColonRegex = ur'["](.*?)["]'
CommnetRegex = ur'/\*(.*?)\*/'

# KeyParamRegex = ur'["](.*?)["](\s*)=(\s*)["](.*?)["];'
KeyParamRegex = re.compile(ur'/\*(.*?)\*/\n["](.*?)["](\s*)=(\s*)["](.*?)["];', re.VERBOSE | re.MULTILINE)

AnotationRegexPrefix = ur'/\*(.*?)\*/'

def getCharaset(string_txt):
	filedata = bytearray(string_txt[:4])
	if len(filedata) < 4 :
		return 0
	if  (filedata[0] == 0xEF) and (filedata[1] == 0xBB) and (filedata[2] == 0xBF):
		print 'utf-8'
		return 1
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE) and (filedata[2] == 0x00) and (filedata[3] == 0x00):
		print 'utf-32/UCS-4,little endian'
		return 3
	elif (filedata[0] == 0x00) and (filedata[1] == 0x00) and (filedata[2] == 0xFE) and (filedata[3] == 0xFF):
		print 'utf-32/UCS-4,big endian'
		return 3
	elif (filedata[0] == 0xFE) and (filedata[1] == 0xFF):
		print 'utf-16/UCS-2,little endian'
		return 2
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE):
		print 'utf-16/UCS-2,big endian'
		return 2
	else:
		print 'can not recognize!'
		return 0

def decoder(string_txt):
	var  = getCharaset(string_txt)
	if var == 1:
		return string_txt.decode("utf-8")
	elif var == 2:
		return string_txt.decode("utf-16")
	elif var == 3:
		return string_txt.decode("utf-32")
	else:
		return string_txt

def constructAnotationRegex(str):
    return AnotationRegexPrefix + '\n' + str

def getAnotationOfString(string_txt,suffix):
	anotationRegex = constructAnotationRegex(suffix)
	anotationString = ''
	try:
   		anotationMatch = re.search(anotationRegex,unicode(string_txt))
		if anotationMatch:
			match = re.search(AnotationRegexPrefix,anotationMatch.group(0))
			if match:
				anotationString = match.group(0)
	except Exception as e:
   		print 'Exception:' 
   		print e

	return anotationString

def compareWithFilePath(newStringPath,originalStringPath):
	#read newStringfile 
	print "compareWithFilePath - 1 \n"
	nspf=open(newStringPath,"r")
	#newString_txt =  str(nspf.read(5000000)).decode("utf-16")
	newString_txt =  decoder(str(nspf.read(5000000)))
	nspf.close()

	#write into origial file
	sbfw=open(originalStringPath,"w")
	sbfw.write(newString_txt)
	sbfw.close()

def extractFileName(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	return seg[lastindex]

def extractFilePrefix(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	prefix =  seg[lastindex].split('.')[0]
	return prefix

def generateStoryboardStringsfile(storyboard_path,tempstrings_path):
	cmdstring = 'ibtool '+storyboard_path+' --generate-strings-file '+tempstrings_path
	if os.system(cmdstring) == 0:
		return 1

def main():
	filePath = sys.argv[1]
	sourceFilePath = filePath + '/' + KSourceFile 
	sourceFile_list = glob.glob(sourceFilePath)
	if len(sourceFile_list) == 0:
		print sourceFilePath + 'error dictionary,you should choose the dic upper the Base.lproj'
		return
	targetFilePath = filePath + '/' + KTargetFile
	targetFile_list = glob.glob(targetFilePath)
	tempFile_Path = filePath + '/' + KGenerateStringsFile
	if len(targetFile_list) == 0:
		print 'error framework , no .lproj dic was found'
		return
	for sourcePath in sourceFile_list:
		sourceprefix = extractFilePrefix(sourcePath)
		sourcename = extractFileName(sourcePath)
		print 'init with %s'%sourcename
		tempFile_Path = filePath + '/fd_' + sourcename + ".strings"
		if generateStoryboardStringsfile(sourcePath,tempFile_Path) == 1:
			print '- - genstrings %s successfully'%sourcename
			for targetPath in targetFile_list:
				targetprefix = extractFilePrefix(targetPath)
				targetname = extractFileName(targetPath) 
				if cmp(sourceprefix,targetprefix) == 0:
					print '- - dealing with %s'%targetPath
					compareWithFilePath(tempFile_Path,targetPath)
			print 'finish with %s'%sourcename
			os.remove(tempFile_Path)
		else:
			print '- - genstrings %s error'%sourcename




if __name__ == '__main__':
	main()
