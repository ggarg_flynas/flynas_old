#!/usr/bin/env python
# encoding: utf-8

"""
MergeEnAr.py

user this link to convert from android string table
https://localise.biz/free/converter/ios-to-android

merge Localizable_en.strings to Localizable_ar.strings
by set _ar key to _en value and add missing to bottom

"""

import imp 
import sys 
import os
import glob
import string  
import re  
import time
import traceback

arErr = ""

imp.reload(sys) 
sys.setdefaultencoding('utf-8') #,utf-8,\u4e00-\u9fa5

KGenerateStringsFile = 'TempfileOfStoryboardNew.strings'

ColonRegex = ur'["](.*?)["]'
CommnetRegex = ur'/\*(.*?)\*/'

#KeyParamRegex = ur'["](.*?)["](\s*)=(\s*)["](.*?)["];'
KeyParamRegex = re.compile(ur'/\*(.*?)\*/\n["](.*?)["](\s*)=(\s*)["](.*?)["];', re.VERBOSE | re.MULTILINE)

AnotationRegexPrefix = ur'/\*(.*?)\*/'

def getCharaset(string_txt):
	filedata = bytearray(string_txt[:4])
	if len(filedata) < 4 :
		return 0
	if  (filedata[0] == 0xEF) and (filedata[1] == 0xBB) and (filedata[2] == 0xBF):
		print 'utf-8'
		return 1
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE) and (filedata[2] == 0x00) and (filedata[3] == 0x00):
		print 'utf-32/UCS-4,little endian'
		return 3
	elif (filedata[0] == 0x00) and (filedata[1] == 0x00) and (filedata[2] == 0xFE) and (filedata[3] == 0xFF):
		print 'utf-32/UCS-4,big endian'
		return 3
	elif (filedata[0] == 0xFE) and (filedata[1] == 0xFF):
		print 'utf-16/UCS-2,little endian'
		return 2
	elif (filedata[0] == 0xFF) and (filedata[1] == 0xFE):
		print 'utf-16/UCS-2,big endian'
		return 2
	else:
		print 'can not recognize!'
		return 0

def decoder(string_txt):
	var  = getCharaset(string_txt)
	if var == 1:
		return string_txt.decode("utf-8")
	elif var == 2:
		return string_txt.decode("utf-16")
	elif var == 3:
		return string_txt.decode("utf-32")
	else:
		return string_txt

def constructAnotationRegex(str):
    return AnotationRegexPrefix + '\n' + str

def getAnotationOfString(string_txt,suffix):
	anotationRegex = re.compile(constructAnotationRegex(suffix), re.VERBOSE | re.MULTILINE)
	anotationString = ''
	try:
   		anotationMatch = re.search(anotationRegex,unicode(string_txt))
		if anotationMatch:
			match = re.search(AnotationRegexPrefix,anotationMatch.group(0))
			if match:
				anotationString = match.group(0)
	except Exception as e:
   		print 'Exception:' 
   		print e

	return anotationString

def compareWithFilePath(sourceEnFilePath,sourceArFilePath):
	
	print "begin-1 \n"
	#read newStringfile 
	nspf=open(sourceEnFilePath,"r")
	
	#newString_txt =  str(nspf.read(5000000)).decode("utf-16")
	newString_txt =  decoder(str(nspf.read(5000000)))
	nspf.close()
	
	item_dic = {}
	enString_dic = {}
	anotation_dic = {}
	
	for stfmatch in re.finditer(KeyParamRegex , newString_txt):
		
		linestr = stfmatch.group(0)
		leftvalue = stfmatch.group(2)
		rightvalue = stfmatch.group(5)
		anotationString = stfmatch.group(1)
		# print "leftvalue = " + leftvalue + "\n"
		# print "rightvalue = " + rightvalue + "\n"
		# print "anotationString = " + anotationString + "\n"
		# print "linestr = " + linestr + "\n"
		item_dic[leftvalue] = linestr
		enString_dic[leftvalue] = rightvalue
		anotation_dic[leftvalue] = anotationString

	#read originalStringfile 
	ospf=open(sourceArFilePath,"r")
	originalString_txt =  decoder(str(ospf.read(5000000)))
	ospf.close()
	arString_dic = {}
	for stfmatch in re.finditer(KeyParamRegex , originalString_txt):
		linestr = stfmatch.group(0)
		leftvalue = stfmatch.group(2)
		rightvalue = stfmatch.group(5)
		anotationString = stfmatch.group(1)
		arString_dic[leftvalue] = rightvalue
		
		# linematchs = re.findall(ColonRegex, linestr)
		# if len(linematchs) == 2:
		# 	leftvalue = linematchs[0]
		# 	rightvalue = linematchs[1]
		# 	arString_dic[leftvalue] = rightvalue

	arErr = ""
	#compare and reset key in ar
	for key in arString_dic:
		if(key in enString_dic):
			keystr = '%s'%key
			replacestr = enString_dic[key]
			# match = re.search(replacestr , originalString_txt)
			originalString_txt = originalString_txt.replace(keystr,replacestr)
				

	#compare and add new param to ar string
	# executeOnce = 1
	# for key in enString_dic:
	# 	values = (key, enString_dic[key])
	# 	if(key not in arString_dic):
	# 		newline = ''
	# 		if executeOnce == 1:
	# 			newline += '\n'
	# 			# timestamp = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
	# 			# newline = '\n//##################################################################################\n'
	# 			# newline	+='//#           AutoGenStrings            '+timestamp+'\n'
	# 			# newline	+='//##################################################################################\n'
	# 			executeOnce = 0
	# 		newline += item_dic[key] + '\n\n'
	# 		originalString_txt += newline
	# 		# report missing
	# 		keystr = '%s'%key
	# 		print '!!! key "'+keystr+'" is missing'

	#write into origial file
	sbfw=open(sourceArFilePath,"w")
	sbfw.write(originalString_txt)
	sbfw.close()
	
	# saveErr()

def reportErr(err):
	print err
	arErr += err

def saveErr():
	arErrFilePath = 'Localizable_ar_err.txt'
	#write error log
	sbfw=open(arErrFilePath,"w")
	sbfw.write(arErr)
	sbfw.close()


def extractFileName(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	return seg[lastindex]

def extractFilePrefix(file_path):
	seg = file_path.split('/')
	lastindex = len(seg) - 1
	prefix =  seg[lastindex].split('.')[0]
	return prefix

def generateStoryboardStringsfile(storyboard_path,tempstrings_path):
	cmdstring = 'ibtool '+storyboard_path+' --generate-strings-file '+tempstrings_path
	if os.system(cmdstring) == 0:
		return 1

def main():
	print "begin \n"
	sourceEnFilePath = 'Localizable_en.strings'
	sourceArFilePath = 'Localizable_ar.strings'
	
	compareWithFilePath(sourceEnFilePath,sourceArFilePath)


if __name__ == '__main__':
	main()
