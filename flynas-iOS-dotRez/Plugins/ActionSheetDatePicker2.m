//
//Copyright (c) 2011, Tim Cinel
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:
//* Redistributions of source code must retain the above copyright
//notice, this list of conditions and the following disclaimer.
//* Redistributions in binary form must reproduce the above copyright
//notice, this list of conditions and the following disclaimer in the
//documentation and/or other materials provided with the distribution.
//* Neither the name of the <organization> nor the
//names of its contributors may be used to endorse or promote products
//derived from this software without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#import "ActionSheetDatePicker2.h"
#import <objc/message.h>
#import "flynas_iOS_dotRez-Swift.h"

#define HijiriYearBegin 1000
#define HijiriYearEnd 2000

@interface ActionSheetDatePicker2()  <UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, assign) UIDatePickerMode datePickerMode;
@property (nonatomic, strong) NSDate *selectedDate;

@property (nonatomic,strong) NSArray *data; //Array of string arrays :)
@property (nonatomic,assign) NSArray *initialSelection;

@property (nonatomic, strong) NSCalendar * hijriCalendar;
@property (nonatomic, strong) NSDateFormatter * hijriFormatter;

@end

@implementation ActionSheetDatePicker2

+ (id)showPickerWithTitle:(NSString *)title
           datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate
                   target:(id)target action:(SEL)action origin:(id)origin {
    ActionSheetDatePicker2 *picker = [[ActionSheetDatePicker2 alloc] initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:target action:action origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

+ (id)showPickerWithTitle:(NSString *)title
           datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate
                   target:(id)target action:(SEL)action origin:(id)origin cancelAction:(SEL)cancelAction {
    ActionSheetDatePicker2 *picker = [[ActionSheetDatePicker2 alloc] initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:target action:action origin:origin cancelAction:cancelAction];
    [picker showActionSheetPicker];
    return picker;
}

+ (id)showPickerWithTitle:(NSString *)title
           datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate
              minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate
                   target:(id)target action:(SEL)action origin:(id)origin {
    ActionSheetDatePicker2 *picker = [[ActionSheetDatePicker2 alloc] initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:target action:action origin:origin];
    [picker setMinimumDate:minimumDate];
    [picker setMaximumDate:maximumDate];
    [picker showActionSheetPicker];
    return picker;
}

+ (id)showPickerWithTitle:(NSString *)title
           datePickerMode:(UIDatePickerMode)datePickerMode
             selectedDate:(NSDate *)selectedDate
                doneBlock:(ActionDateDoneBlock2)doneBlock
              cancelBlock:(ActionDateCancelBlock2)cancelBlock
                   origin:(UIView*)view
{
    ActionSheetDatePicker2* picker = [[ActionSheetDatePicker2 alloc] initWithTitle:title
                                                                  datePickerMode:datePickerMode
                                                                    selectedDate:selectedDate
                                                                       doneBlock:doneBlock
                                                                     cancelBlock:cancelBlock
                                                                          origin:view];
    [picker showActionSheetPicker];
    return picker;
}

+ (id)showPickerWithTitle:(NSString *)title
           datePickerMode:(UIDatePickerMode)datePickerMode
             selectedDate:(NSDate *)selectedDate
              minimumDate:(NSDate *)minimumDate
              maximumDate:(NSDate *)maximumDate
                doneBlock:(ActionDateDoneBlock2)doneBlock
              cancelBlock:(ActionDateCancelBlock2)cancelBlock
                   origin:(UIView*)view
{
    ActionSheetDatePicker2* picker = [[ActionSheetDatePicker2 alloc] initWithTitle:title
                                                                  datePickerMode:datePickerMode
                                                                    selectedDate:selectedDate
                                                                       doneBlock:doneBlock
                                                                     cancelBlock:cancelBlock
                                                                          origin:view];
    [picker setMinimumDate:minimumDate];
    [picker setMaximumDate:maximumDate];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate target:(id)target action:(SEL)action origin:(id)origin
{
    self = [self initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:target action:action origin:origin cancelAction:nil];
    return self;
}

- (id)initWithTitle:(NSString *)title datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate target:(id)target action:(SEL)action origin:(id)origin
{
    self = [self initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:target action:action origin:origin cancelAction:nil];
    self.minimumDate = minimumDate;
    self.maximumDate = maximumDate;
    return self;
}

- (id)initWithTitle:(NSString *)title datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate target:(id)target action:(SEL)action origin:(id)origin cancelAction:(SEL)cancelAction
{
    self = [super initWithTarget:target successAction:action cancelAction:cancelAction origin:origin];
    if (self) {
        self.title = title;
        self.datePickerMode = datePickerMode;
        self.selectedDate = selectedDate;
    }
    return self;
}

- (id)initWithTitle:(NSString *)title datePickerMode:(UIDatePickerMode)datePickerMode selectedDate:(NSDate *)selectedDate minimumDate:(NSDate *)minimumDate maximumDate:(NSDate *)maximumDate target:(id)target action:(SEL)action cancelAction:(SEL)cancelAction origin:(id)origin
{
    self = [super initWithTarget:target successAction:action cancelAction:cancelAction origin:origin];
    if (self) {
        self.title = title;
        self.datePickerMode = datePickerMode;
        self.selectedDate = selectedDate;
        self.minimumDate = minimumDate;
        self.maximumDate = maximumDate;
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
               datePickerMode:(UIDatePickerMode)datePickerMode
                 selectedDate:(NSDate *)selectedDate
                    doneBlock:(ActionDateDoneBlock2)doneBlock
                  cancelBlock:(ActionDateCancelBlock2)cancelBlock
                       origin:(UIView*)origin
{
    self = [self initWithTitle:title datePickerMode:datePickerMode selectedDate:selectedDate target:nil action:nil origin:origin];
    if (self) {
        self.onActionSheetDone = doneBlock;
        self.onActionSheetCancel = cancelBlock;
    }
    return self;
}

- (UIView *)configuredPickerView {

    if (!self.showHijiriCalendar) {
        CGRect datePickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
        datePicker.datePickerMode = self.datePickerMode;
        datePicker.maximumDate = self.maximumDate;
        datePicker.minimumDate = self.minimumDate;
        datePicker.minuteInterval = self.minuteInterval;
        datePicker.calendar = self.calendar;
        datePicker.timeZone = self.timeZone;
        datePicker.locale = self.locale;

        if (self.showHijiriCalendar) {
            if (self.hijriCalendar == nil) {
                self.hijriCalendar =[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamic];
                self.hijriFormatter = [[NSDateFormatter alloc] init];
                self.hijriFormatter.locale = self.locale;
                self.hijriFormatter.calendar = self.hijriCalendar;
                self.hijriFormatter.timeZone = self.timeZone;
            }
            datePicker.calendar = self.hijriCalendar;
        }

        // if datepicker is set with a date in countDownMode then
        // 1h is added to the initial countdown
        if (self.datePickerMode == UIDatePickerModeCountDownTimer) {
            datePicker.countDownDuration = self.countDownDuration;
            // Due to a bug in UIDatePicker, countDownDuration needs to be set asynchronously
            // more info: http://stackoverflow.com/a/20204317/1161723
            dispatch_async(dispatch_get_main_queue(), ^{
                datePicker.countDownDuration = self.countDownDuration;
            });
        } else {
            [datePicker setDate:self.selectedDate animated:NO];
        }

        [datePicker addTarget:self action:@selector(eventForDatePicker:) forControlEvents:UIControlEventValueChanged];

        //need to keep a reference to the picker so we can clear the DataSource / Delegate when dismissing (not used in this picker, but just in case somebody uses this as a template for another picker)
        self.pickerView = datePicker;

        return datePicker;
    } else {


        if (self.hijriCalendar == nil) {
            self.hijriCalendar =[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamic];
            self.hijriFormatter = [[NSDateFormatter alloc] init];
            self.hijriFormatter.calendar = self.hijriCalendar;
            self.hijriFormatter.locale = self.locale;
            self.hijriFormatter.timeZone = self.timeZone;
        }

        [self setInitDate:self.selectedDate];

        self.data = [self getDateData:30];
      if (!self.data) {
        return nil;
      }
        CGRect pickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
        UIPickerView *stringPicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
        stringPicker.delegate = self;
        stringPicker.dataSource = self;

        [self performInitialSelectionInPickerView:stringPicker];

        if (self.data.count == 0) {
            stringPicker.showsSelectionIndicator = NO;
            stringPicker.userInteractionEnabled = NO;
        } else {
            stringPicker.showsSelectionIndicator = YES;
            stringPicker.userInteractionEnabled = YES;
        }

        //need to keep a reference to the picker so we can clear the DataSource / Delegate when dismissing
        self.pickerView = stringPicker;

        return stringPicker;
    }
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)action origin:(id)origin
{
    if (self.onActionSheetDone)
    {
        if (self.datePickerMode == UIDatePickerModeCountDownTimer)
            self.onActionSheetDone(self, @(((UIDatePicker *)self.pickerView).countDownDuration), origin);
        else
            self.onActionSheetDone(self, self.selectedDate, origin);

        return;
    }
    else if ([target respondsToSelector:action])
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        if (self.datePickerMode == UIDatePickerModeCountDownTimer) {
            [target performSelector:action withObject:@(((UIDatePicker *)self.pickerView).countDownDuration) withObject:origin];

        } else {
            [target performSelector:action withObject:self.selectedDate withObject:origin];
        }
#pragma clang diagnostic pop
        else
            NSAssert(NO, @"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(action));
}

- (void)notifyTarget:(id)target didCancelWithAction:(SEL)cancelAction origin:(id)origin
{
    if (self.onActionSheetCancel)
    {
        self.onActionSheetCancel(self);
        return;
    }
    else
        if ( target && cancelAction && [target respondsToSelector:cancelAction] )
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [target performSelector:cancelAction withObject:origin];
#pragma clang diagnostic pop
        }
}

- (void)eventForDatePicker:(id)sender
{
    if (sender && [sender isKindOfClass:[UIDatePicker class]]) {
        UIDatePicker *datePicker = (UIDatePicker *)sender;
        self.selectedDate = datePicker.date;
        self.countDownDuration = datePicker.countDownDuration;
    } else if (sender && [sender isKindOfClass:[UIPickerView class]]) {
        self.selectedDate = [self getSelectedHijriDate];
    }
}

- (void)customButtonPressed:(id)sender {
    UIBarButtonItem *button = (UIBarButtonItem*)sender;
    NSInteger index = button.tag;
    NSAssert((index >= 0 && index < self.customButtons.count), @"Bad custom button tag: %zd, custom button count: %zd", index, self.customButtons.count);
    NSDictionary *buttonDetails = (self.customButtons)[(NSUInteger) index];
    NSAssert(buttonDetails != NULL, @"Custom button dictionary is invalid");

    ActionType actionType = (ActionType) [buttonDetails[kActionType] integerValue];
    switch (actionType) {
        case ActionTypeValue: {
            NSAssert([self.pickerView respondsToSelector:@selector(setDate:animated:)], @"Bad pickerView for ActionSheetDatePicker, doesn't respond to setDate:animated:");
            NSDate *itemValue = buttonDetails[kButtonValue];
            UIDatePicker *picker = (UIDatePicker *)self.pickerView;
            if (self.datePickerMode != UIDatePickerModeCountDownTimer)
            {
                [picker setDate:itemValue animated:YES];
                [self eventForDatePicker:picker];
            }
            break;
        }

        case ActionTypeBlock:
        case ActionTypeSelector:
            [super customButtonPressed:sender];
            break;

        default:
            NSAssert(false, @"Unknown action type");
            break;
    }
}


#pragma mark - UIPickerViewDelegate / DataSource

- (NSArray*) getDateData:(int)daysInMonth {
    NSMutableArray* dayArray = [NSMutableArray array];
    for (int num = 1; num <= daysInMonth; num++) {
        [dayArray addObject: [FDUtility localizeInt:num ] ];
    }

    NSMutableArray* monthArray = [NSMutableArray array];
    for (int num = 1; num <= 12; num++) {
        self.hijriFormatter.dateFormat = @"MM";
        NSDate* date = [self.hijriFormatter dateFromString:[NSString stringWithFormat:@"%d", num]];
        self.hijriFormatter.dateFormat = @"MMM";
        NSString* monthName = [self.hijriFormatter stringFromDate:date];

        if (monthName == nil) {
            [monthArray addObject: [FDUtility localizeInt:num ] ];
        } else {
            [monthArray addObject: monthName];
        }
    }

    NSMutableArray* yearArray = [NSMutableArray array];
    for (int num = HijiriYearBegin; num <= HijiriYearEnd; num++) {
        [yearArray addObject: [FDUtility localizeInt:num ] ];
    }

    NSArray* dateArray = [NSArray arrayWithObjects:dayArray, monthArray, yearArray, nil];

    return dateArray;
}

- (void)setInitDate:(NSDate*)date {
    if (date == nil) {
        date = [NSDate date];
    }

    NSCalendar * hijriCalendar =[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamic];

    NSDateComponents* c = [hijriCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];

    self.initialSelection = [NSArray arrayWithObjects:
                             [NSNumber numberWithLong:[c day]-1],
                             [NSNumber numberWithLong:[c month]-1],
                              [NSNumber numberWithLong:([c year]-HijiriYearBegin)],
                             nil];
}

- (NSDate*)getSelectedHijriDate {
    NSArray* selectedIndexes = [self selectedIndexes];

    int day = [selectedIndexes[0] intValue] + 1;
    int month = [selectedIndexes[1] intValue] + 1;
    int year = [selectedIndexes[2] intValue] + HijiriYearBegin;

    self.hijriFormatter.dateFormat = @"dd MM yyyy";
    NSString* ds = [NSString stringWithFormat:@"%2d %2d %4d", day, month, year];
    return [self.hijriFormatter dateFromString:ds];
}

- (NSDate*)getSelectedHijriDay {
    NSArray* selectedIndexes = [self selectedIndexes];
//    int day = [selectedIndexes[0] intValue];
    int month = [selectedIndexes[1] intValue] + 1;
    int year = [selectedIndexes[2] intValue] + HijiriYearBegin;

    self.hijriFormatter.dateFormat = @"dd MM yyyy";
    NSString* ds = [NSString stringWithFormat:@"%2d %2d %4d", 1, month, year];
    return [self.hijriFormatter dateFromString:ds];
}

- (void)updateHijriDay:(UIPickerView *)pickerView {
    NSArray* selectedIndexes = [self selectedIndexes];
    int day = [selectedIndexes[0] intValue];

    NSDate* date = [self getSelectedHijriDate];

    if (date == nil) {
        date = [self getSelectedHijriDay];
        unsigned long daysInMonth = [self.hijriCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date].length;

        if (day >= daysInMonth) {
          NSInteger numberOfRows = [pickerView numberOfRowsInComponent:0];
            [pickerView selectRow:MIN(daysInMonth-1, numberOfRows-1) inComponent:0 animated:YES];
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self updateHijriDay:pickerView];
    [self eventForDatePicker:pickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return [self.data count];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return ((NSArray *)self.data[component]).count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    id obj = (self.data)[(NSUInteger) row];

    // return the object if it is already a NSString,
    // otherwise, return the description, just like the toString() method in Java
    // else, return nil to prevent exception

    if ([obj isKindOfClass:[NSString class]])
        return obj;

    if ([obj respondsToSelector:@selector(description)])
        return [obj performSelector:@selector(description)];

    return nil;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    id obj = (self.data)[component][(NSUInteger) row];

    // return the object if it is already a NSString,
    // otherwise, return the description, just like the toString() method in Java
    // else, return nil to prevent exception

    if ([obj isKindOfClass:[NSString class]])
        return [[NSAttributedString alloc] initWithString:obj attributes:self.pickerTextAttributes];

    if ([obj respondsToSelector:@selector(description)])
        return [[NSAttributedString alloc] initWithString:[obj performSelector:@selector(description)] attributes:self.pickerTextAttributes];

    return nil;
}

- (void)performInitialSelectionInPickerView:(UIPickerView *)pickerView {
    for (int i = 0; i < self.selectedIndexes.count; i++) {
        NSInteger row = [(NSNumber *)self.initialSelection[i] integerValue];
      NSInteger numberOfRows = [pickerView numberOfRowsInComponent:0];
      if (row >= 0 && row < numberOfRows) {
        [pickerView selectRow:row inComponent:i animated:NO];
      }
    }
}

- (NSArray *)selection {
    NSMutableArray * array = [NSMutableArray array];
    for (int i = 0; i < self.data.count; i++) {
        id object = self.data[i][[(UIPickerView *)self.pickerView selectedRowInComponent:(NSInteger)i]];
        [array addObject: object];
    }
    return [array copy];
}

- (NSArray *)selectedIndexes {
    NSMutableArray * indexes = [NSMutableArray array];
    for (int i = 0; i < self.data.count; i++) {
        NSNumber *index = [NSNumber numberWithInteger:[(UIPickerView *)self.pickerView selectedRowInComponent:(NSInteger)i]];
        [indexes addObject: index];
    }
    return [indexes copy];
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    return pickerView.frame.size.width - 30;
//}

@end
