//
//  CustomSearchBar.m
//

#import "CustomSearchBar.h"

@implementation CustomSearchBar

+ (void)setAppearance {
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSFontAttributeName: [UIFont fontWithName:@"Helvetica Neue" size:20]
                                                                                                 }];
}

- (void)setSearchIcon:(NSString*) imageName {
    // Really a UISearchBarTextField, but the header is private.
    UITextField *searchField = [self getTextfield:self];

    if (searchField) {
//        BOOL clipsToBounds = searchField.clipsToBounds;
        searchField.clipsToBounds = NO;
        UIImage *image = [UIImage imageNamed: imageName];
        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
        searchField.leftView = iView;
    }
}

-(UITextField*)getTextfield:(UIView *)rootview
{
    for (UIView *aview in rootview.subviews)
    {
        if([aview isKindOfClass:[UITextField class]]){
            return (UITextField*)aview;
        }
        else {
            UITextField *searchField = [self getTextfield:aview];
            if (searchField != nil) {
                return searchField;
            }
        }
    }

    return nil;
}


@end
