//
//  UIAlertView+Extension.swift
//  flynas-iOS-dotRez
//
//  Created by River Huang on 21/10/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//


import UIKit
private var UIAlertViewWrapperPropertyKey : UInt8 = 0
typealias AlertViewCompletionHandler = (_ alertView: UIAlertView, _ buttonIndex: Int) -> Void

extension UIAlertView
{
    // MARK: - Associated Properties
    fileprivate var wrapper : UIAlertViewWrapper?
        {
        get { return objc_getAssociatedObject(self, &UIAlertViewWrapperPropertyKey) as? UIAlertViewWrapper }
        set { objc_setAssociatedObject(self, &UIAlertViewWrapperPropertyKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    // MARK - Convenience Initializers
    convenience init(title: String?, message: String?, cancelButtonTitle: String?)
    {
        self.init(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle)
    }

    convenience init(title: String?, message: String?, cancelButtonTitle: String?, otherButtonTitles: String...)
    {
        self.init(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle)

        for buttonTitle in otherButtonTitles { self.addButton(withTitle: buttonTitle) }
    }

    // MARK: - Show with Completion Handler
    func showWithCompletion(_ completionHandler: AlertViewCompletionHandler? = nil)
    {
        self.wrapper = UIAlertViewWrapper(completionHandler: completionHandler)
        self.delegate = self.wrapper

        self.show()
    }

    // MARK: - Show Class Methods
    class func showWithTitle(_ title: String?, message: String?, cancelButtonTitle: String?, completionHandler: AlertViewCompletionHandler? = nil)
    {
        showWithTitle(title, message: message, cancelButtonTitle: cancelButtonTitle, otherButtonTitles: nil, completionHandler: completionHandler)
    }

    class func showWithTitle(_ title: String?, message: String?, cancelButtonTitle: String?, otherButtonTitle: String?, completionHandler: AlertViewCompletionHandler? = nil)
    {
        let otherButtonTitles : [String]? = otherButtonTitle != nil ? [otherButtonTitle!] : nil

        showWithTitle(title, message: message, cancelButtonTitle: cancelButtonTitle, otherButtonTitles: otherButtonTitles, completionHandler: completionHandler)
    }

    class func showWithTitle(_ title: String?, message: String?, cancelButtonTitle: String?, otherButtonTitles: [String]?, completionHandler: AlertViewCompletionHandler? = nil)
    {
        let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle)

        if let otherButtonTitles = otherButtonTitles
        {
            for buttonTitle in otherButtonTitles
            {
                alertView.addButton(withTitle: buttonTitle)
            }
        }

        alertView.showWithCompletion(completionHandler)
    }
}
// Private class that handles delegation and completion handler (do not instantiate)
private final class UIAlertViewWrapper : NSObject, UIAlertViewDelegate
{
    // MARK: - Completion Handlers

    var completionHandler: AlertViewCompletionHandler?

    // MARK: - Initializers

    init(completionHandler: AlertViewCompletionHandler?)
    {
        self.completionHandler = completionHandler
    }

    // MARK: - UIAlertView Delegate

    @objc fileprivate func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        completionHandler?(alertView, buttonIndex)
    }
}
