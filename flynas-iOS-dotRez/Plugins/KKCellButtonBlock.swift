//
//  KKCellButtonBlock.swift
//  flynas-iOS-dotRez
//
//  Created by kenankarakecili on 6/4/18.
//  Copyright © 2018 Matchbyte. All rights reserved.
//

import UIKit

class KKCellButtonBlock {
  
  private var actionDic: [String: (() -> Void)] = [:]
  
  func setAction(button: UIButton, action: @escaping () -> Void) {
    let uniqueId = "\(Unmanaged.passUnretained(button).toOpaque())"
    actionDic[uniqueId] = action
    button.addTarget(self, action: #selector(self.didTapButton), for: .touchUpInside)
  }
  
  @objc private func didTapButton(sender: UIButton) {
    let uniqueId = "\(Unmanaged.passUnretained(sender).toOpaque())"
    let action = actionDic[uniqueId]
    action?()
  }
  
}
