//
//  CustomSearchBar.h
//

#import <UIKit/UIKit.h>

@interface CustomSearchBar : UISearchBar

+ (void)setAppearance;

- (void)setSearchIcon:(NSString*) imageName;

@end
