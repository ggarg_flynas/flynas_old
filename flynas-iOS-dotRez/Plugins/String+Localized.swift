//
//  String+Localized.swift
//  flynas-iOS-mb
//
//  Created by River Huang on 28/09/2015.
//  Copyright © 2015 Matchbyte. All rights reserved.
//

import Foundation


func FDLocalized(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    func localizedWithComment(_ comment:String) -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
