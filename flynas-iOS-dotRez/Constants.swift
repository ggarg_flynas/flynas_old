//
//  Constants.swift
//  flynas-iOS-dotRez
//
//  Created by Kenan Karakecili on 18/8/17.
//  Copyright © 2017 Matchbyte. All rights reserved.
//

import UIKit

let clrTurquoise = UIColor(red: 35.0/255.0, green: 188.0/255.0, blue: 185.0/255.0, alpha: 1.0)

func fontRegular(_ size: CGFloat) -> UIFont {
  return UIFont(name: "Roboto-Regular", size: size)!
}

func fontBold(_ size: CGFloat) -> UIFont {
  return UIFont(name: "Roboto-Bold", size: size)!
}
