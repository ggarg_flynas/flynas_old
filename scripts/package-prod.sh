mkdir -p Build

rm -f -r ./Build
rm ./flynas-iOS-dotRez.ipa
rm ./flynas-iOS-dotRez.app.dSYM.zip

xcodebuild \
  BUILD_DIR=$PWD/Build \
  -sdk iphoneos \
  -workspace flynas-iOS-dotRez.xcworkspace \
  -configuration Release \
  -scheme "flynas-iOS-dotRez" \
  archive \
  -archivePath ./Build/flynas-iOS-dotRez.xcarchive
  
xcodebuild -exportArchive -exportFormat ipa \
    -archivePath "./Build/flynas-iOS-dotRez.xcarchive" \
    -exportPath "./Build/flynas-iOS-dotRez.ipa" \
    -exportProvisioningProfile "flynas-mb-adhoc"

ipa info ./Build/flynas-iOS-dotRez.ipa

aws s3 cp ./Build/flynas-iOS-dotRez.ipa s3://zww/flynas/ --acl public-read
# aws s3 cp ./flynas-iOS-dotRez.ipa s3://zww/flynas/ --acl public-read

xcodebuild -exportArchive -exportFormat ipa \
    -archivePath "./Build/flynas-iOS-dotRez.xcarchive" \
    -exportPath "./flynas-iOS-dotRez.ipa" \
    -exportProvisioningProfile "flynas-product-mb"

ipa info ./flynas-iOS-dotRez.ipa

ipa distribute:itunesconnect -a river@matchbyte.com -i 871947665 --upload  --verbose
# xcodebuild -exportArchive -exportFormat ipa -archivePath "flynas-iOS-dotRez 16-03-2016, 14.51.xcarchive" -exportPath "./flynas-iOS-dotRez.ipa" -exportProvisioningProfile "flynas-mb-adhoc"



